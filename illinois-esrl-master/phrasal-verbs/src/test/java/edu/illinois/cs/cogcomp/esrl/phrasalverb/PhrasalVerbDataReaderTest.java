package edu.illinois.cs.cogcomp.esrl.phrasalverb;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PhrasalVerbDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.PHRASAL_VERB_DATA_DIR);
    }

    @Test
    public void testReader() {
        PhrasalVerbDataReader reader = new PhrasalVerbDataReader(dataDir + "/pvcData-BIO-test.txt", "test");
        Constituent constituent = (Constituent) reader.next();
        assertEquals("given in", constituent.getSurfaceForm());
        assertEquals(DataReader.CANDIDATE, constituent.getLabel());
        constituent = (Constituent) reader.next();
        assertEquals("get out", constituent.getSurfaceForm());
        assertEquals("PV", constituent.getLabel());
    }
}