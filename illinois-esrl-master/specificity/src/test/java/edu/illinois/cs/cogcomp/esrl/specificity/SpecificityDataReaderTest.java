package edu.illinois.cs.cogcomp.esrl.specificity;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SpecificityDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.SPECIFICITY_DATA_DIR);
    }

    @Test
    public void testDataReader() throws Exception {
        SpecificityDataReader reader = new SpecificityDataReader(dataDir + "/nyt.nonlex.annotations.txt", "train");
        reader.next();
        Constituent constituent = (Constituent) reader.next();
        assertEquals("gen", constituent.getLabel());
        assertEquals("Maybe they just wanted a better view of the stars.", constituent.getSurfaceForm());
        assertEquals("Maybe they just wanted a better view of the stars .", constituent.getTokenizedSurfaceForm());
    }
}