package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.ner.NERClassifier;
import edu.illinois.cs.cogcomp.esrl.ner.NERDataReader;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesClassifier;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesDataReader;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalClassifier;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalDataReader;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Joint inference definition for NER and VerbSRL
 */
public class QuantitiesTemporalNERInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameNER = modelsDir + File.separator + NERClassifier.CLASS_NAME;
    private static String modelNameQuantities = modelsDir + File.separator + QuantitiesClassifier.CLASS_NAME;
    private static String modelNameTemporal = modelsDir + File.separator + TemporalClassifier.CLASS_NAME;
    private static NERClassifier ner = new NERClassifier(modelNameNER + ".lc", modelNameNER + ".lex");
    private static QuantitiesClassifier quantities = new QuantitiesClassifier(modelNameQuantities + ".lc", modelNameQuantities + ".lex");
    private static TemporalClassifier temporal = new TemporalClassifier(modelNameTemporal + ".lc", modelNameTemporal + ".lex");

    private static final NERDataReader nerReader = new NERDataReader(
            rm.getString(ESRLConfigurator.NER_ONTONOTES_DATA_DIR) + "/Train", "train", ViewNames.NER_ONTONOTES);
    private static final QuantitiesDataReader quantitiesReader = new QuantitiesDataReader(
            rm.getString(ESRLConfigurator.QUANTITIES_DATA_DIR) + "/trainingData.txt", "train");
    private static final TemporalDataReader temporalReader = new TemporalDataReader(
            rm.getString(ESRLConfigurator.TEMPORAL_DATA_DIR) + "/trainingData.txt", "train");

    private static final Classifier nerOracle = new NERClassifier.Label();
    private static final Classifier quantitiesOracle = new QuantitiesClassifier.Label();
    private static final Classifier temporalOracle = new TemporalClassifier.Label();

    public QuantitiesTemporalNERInference(Constituent head) {
        super(head);

        FirstOrderConstraint nerQuant, nerCardinal, nerMoney, quantNumberPred, quantRangePred;
        FirstOrderConstraint nerDate, tempDate;
        FirstOrderConstraint quantDate, quantTempDate;

        // QUANTITIES & NER
        nerQuant = getBILabel(head, "QUANTITY", ner);
        nerCardinal = getBILabel(head, "CARDINAL", ner);
        nerMoney = getBILabel(head, "MONEY", ner);
        FirstOrderConstraint nerDisjunction = new FirstOrderDisjunction(nerQuant, new FirstOrderDisjunction(nerCardinal, nerMoney));
        quantNumberPred = getBILabel(head, "NUMBER", quantities);
        quantRangePred = getBILabel(head, "RANGE", quantities);
        FirstOrderConstraint quantDisjunction = new FirstOrderDisjunction(quantNumberPred, quantRangePred);
        FirstOrderConstraint implicationQuant = new FirstOrderImplication(nerDisjunction, quantDisjunction);

        // TEMPORAL & NER
        nerDate = getBILabel(head, "DATE", ner);
        tempDate = getBILabel(head, "DATE", temporal);
        FirstOrderConstraint implicationTemp = new FirstOrderImplication(tempDate, nerDate);

        //QUANTITIES & TEMPORAL
        quantDate = getBILabel(head, "DATE", quantities);
        quantTempDate = new FirstOrderImplication(quantDate, tempDate);

        FirstOrderConstraint quantTempNERConj = new FirstOrderConjunction(implicationQuant, implicationTemp);
        FirstOrderConstraint finalConj = new FirstOrderConjunction(quantTempNERConj, quantTempDate);
        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), finalConj);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(ner.toString(), 1.0);
        alphas.put(quantities.toString(), 1.0);
        alphas.put(temporal.toString(), 1.0);
        JointTrainer jointTrainer = new JointTrainer(QuantitiesTemporalNERInference.class, alphas);

        jointTrainer.trainWeightsOnline(3, ner, quantities, temporal,
                nerReader, quantitiesReader, temporalReader, nerOracle, quantitiesOracle, temporalOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        nerReader.setDataset("test");
        jointTrainer.test(tester, ner, nerReader, nerOracle, false);
        tester = new TestDiscrete();
        tester.addNull("O");
        quantitiesReader.setDataset("test");
        jointTrainer.test(tester, quantities, quantitiesReader, quantitiesOracle, false);
        tester = new TestDiscrete();
        tester.addNull("O");
        temporalReader.setDataset("test");
        jointTrainer.test(tester, temporal, temporalReader, temporalOracle, false);
    }
}
