package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.core.features.Contains;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.lightverb.LightVerbClassifier;
import edu.illinois.cs.cogcomp.esrl.lightverb.LightVerbDataReader;
import edu.illinois.cs.cogcomp.esrl.phrasalverb.PhrasalVerbDataReader;
import edu.illinois.cs.cogcomp.esrl.phrasalverb.PhrasalVerbClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static edu.illinois.cs.cogcomp.esrl.core.features.Contains.containsPrepPartAdv;

/**
 * Joint inference definition for Light and Phrasal Verb Constructions
 */
public class LightPhrasalInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameLVC = modelsDir + File.separator + LightVerbClassifier.CLASS_NAME;
    private static String modelNamePVC = modelsDir + File.separator + PhrasalVerbClassifier.CLASS_NAME;
    private static LightVerbClassifier lvClassifier = new LightVerbClassifier(modelNameLVC + ".lc", modelNameLVC + ".lex");
    private static PhrasalVerbClassifier pvClassifier = new PhrasalVerbClassifier(modelNamePVC + ".lc", modelNamePVC + ".lex");

    private static final LightVerbClassifier.Label lvOracle = new LightVerbClassifier.Label();
    private static final PhrasalVerbClassifier.Label pvOracle = new PhrasalVerbClassifier.Label();

    private static final LightVerbDataReader lvReader = new LightVerbDataReader(
            rm.getString(ESRLConfigurator.LIGHT_VERB_DATA_DIR) + "/lvcData-BIO-train.txt", "test");
    private static final PhrasalVerbDataReader pvReader = new PhrasalVerbDataReader(
            rm.getString(ESRLConfigurator.PHRASAL_VERB_DATA_DIR) + "/pvcData-BIO-train.txt", "test");

    public LightPhrasalInference(Constituent head) {
        super(head);
        FirstOrderConstraint lv;
        lv = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(lvClassifier, head), "LV");
        FirstOrderConstraint notPV;
        notPV = new FirstOrderEqualityWithValue(false, new FirstOrderVariable(pvClassifier, head), "PV");
        FirstOrderConstraint constraint = new FirstOrderImplication(lv, notPV);

        // PV -> head contains a preposition/particle/adverb
        FirstOrderConstraint prep;
        prep = new FirstOrderConstant(containsPrepPartAdv.classify(head).toString().equals(Contains.YValue));
        FirstOrderConstraint pv;
        pv = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(pvClassifier, head), "PV");
        FirstOrderConstraint pvPrep = new FirstOrderImplication(pv, prep);

        FirstOrderConstraint conjunction = new FirstOrderConjunction(pvPrep, constraint);

        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), conjunction);
    }

    public static void main(String[] args) {
        System.out.println("=========================================================================================");
        System.out.println("Original models");
        System.out.println("=========================================================================================");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, lvClassifier, lvOracle, lvReader, true, 0);
        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, pvClassifier, pvOracle, pvReader, true, 0);

        Map<String, Double> alphas = new HashMap<>();
        alphas.put(lvClassifier.toString(), 0.5);
        alphas.put(pvClassifier.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(LightPhrasalInference.class, alphas);
        lvReader.setDataset("train");
        pvReader.setDataset("train");
        jointTrainer.trainWeightsOnline(3, pvClassifier, lvClassifier, pvReader, lvReader, pvOracle, lvOracle);
        System.out.println(alphas);
        System.out.println("=========================================================================================");
        System.out.println("Joint models");
        System.out.println("=========================================================================================");
        lvReader.setDataset("test");
        pvReader.setDataset("test");
        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        jointTrainer.test(tester, lvClassifier, lvReader, lvOracle, false);
        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        jointTrainer.test(tester, pvClassifier, pvReader, pvOracle, false);
    }
}
