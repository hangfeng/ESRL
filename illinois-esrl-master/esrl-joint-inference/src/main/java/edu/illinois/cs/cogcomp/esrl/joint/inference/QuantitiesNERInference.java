package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.ner.NERClassifier;
import edu.illinois.cs.cogcomp.esrl.ner.NERDataReader;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesClassifier;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesDataReader;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Joint inference definition for NER and VerbSRL
 */
public class QuantitiesNERInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameNER = modelsDir + File.separator + NERClassifier.CLASS_NAME;
    private static String modelNameQuantities = modelsDir + File.separator + QuantitiesClassifier.CLASS_NAME;
    private static QuantitiesClassifier quantities = new QuantitiesClassifier(modelNameQuantities + ".lc", modelNameQuantities + ".lex");
    private static NERClassifier ner = new NERClassifier(modelNameNER + ".lc", modelNameNER + ".lex");

    private static final NERDataReader nerReader = new NERDataReader(
            rm.getString(ESRLConfigurator.NER_ONTONOTES_DATA_DIR) + "/Train", "train", ViewNames.NER_ONTONOTES);
    private static final QuantitiesDataReader quantitiesReader = new QuantitiesDataReader(
            rm.getString(ESRLConfigurator.QUANTITIES_DATA_DIR) + "/trainingData.txt", "train");

    private static final Classifier nerOracle = new NERClassifier.Label();
    private static final Classifier quantitiesOracle = new QuantitiesClassifier.Label();

    public QuantitiesNERInference(Constituent head) {
        super(head);
        FirstOrderConstraint nerQuant, nerCardinal, nerMoney, quantNumberPred, quantRangePred;
        nerQuant = getBILabel(head, "QUANTITY", ner);
        nerCardinal = getBILabel(head, "CARDINAL", ner);
        nerMoney = getBILabel(head, "MONEY", ner);
        FirstOrderConstraint nerDisjunction = new FirstOrderDisjunction(nerQuant, new FirstOrderDisjunction(nerCardinal, nerMoney));
        quantNumberPred = getBILabel(head, "NUMBER", quantities);
        quantRangePred = getBILabel(head, "RANGE", quantities);
        FirstOrderConstraint quantDisjunction = new FirstOrderDisjunction(quantNumberPred, quantRangePred);
        FirstOrderConstraint implicationQuant = new FirstOrderImplication(nerDisjunction, quantDisjunction);
        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), implicationQuant);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(ner.toString(), 0.5);
        alphas.put(quantities.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(QuantitiesNERInference.class, alphas);

        jointTrainer.trainWeightsOnline(3, ner, quantities, nerReader, quantitiesReader, nerOracle, quantitiesOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        nerReader.setDataset("test");
        jointTrainer.test(tester, ner, nerReader, nerOracle, false);
        tester = new TestDiscrete();
        tester.addNull("O");
        quantitiesReader.setDataset("test");
        jointTrainer.test(tester, quantities, quantitiesReader, quantitiesOracle, false);
    }
}
