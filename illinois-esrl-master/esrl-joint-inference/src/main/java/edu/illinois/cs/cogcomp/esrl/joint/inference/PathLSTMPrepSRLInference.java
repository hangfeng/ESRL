package edu.illinois.cs.cogcomp.esrl.joint.inference;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent; import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager; import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.core.features.Contains;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.prepsrl.PrepSRLClassifier;
import edu.illinois.cs.cogcomp.esrl.prepsrl.PrepSRLDataReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.VerbSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;
import se.lth.cs.srl.pipeline.LBJavaArgumentClassifier;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static edu.illinois.cs.cogcomp.esrl.core.features.Contains.containsPrep;
import static edu.illinois.cs.cogcomp.esrl.core.features.Contains.isAt;

public class PathLSTMPrepSRLInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String dataDir = rm.getString(ESRLConfigurator.PREP_DATA_DIR);

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNamePrepSRL = modelsDir + File.separator + PrepSRLClassifier.CLASS_NAME;

    private static String[] pathLSTMArgs = {"uiuc",
           "/dev/null", "models/uiuc+vecs.model",
          "-pos", "V", "-nopi",
   "-reranker",  "/dev/null"};

    private static LBJavaArgumentClassifier verbSRL = new LBJavaArgumentClassifier(pathLSTMArgs);
    private static PrepSRLClassifier prepSRL = new PrepSRLClassifier(modelNamePrepSRL + ".lc", modelNamePrepSRL + ".lex");

    private static final VerbSRLArgumentReader verbSRLReader = new VerbSRLArgumentReader("train", false, true);
    private static final PrepSRLDataReader prepSRLReader = new PrepSRLDataReader(dataDir, "train");

    private static final Classifier verbSRLOracle = new VerbSRLArgumentClassifier.Label();
    private static final Classifier prepSRLOracle = new PrepSRLClassifier.Label();

    public PathLSTMPrepSRLInference(Constituent head) {
        super(head);

        // First, check that a preposition exists
        FirstOrderConstraint at = new FirstOrderConstant(isAt.classify(head).toString().equals(Contains.YValue));
        FirstOrderConstraint prep = new FirstOrderConstant(containsPrep.classify(head).toString().equals(Contains.YValue));
        FirstOrderConstraint verbTemporal, prepTemporal, prepNumeric;
        verbTemporal = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, head), "AM-TMP");
        prepTemporal = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Temporal");
        prepNumeric = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Numeric/Level");
        FirstOrderConstraint implicationTemp = new FirstOrderImplication(
                new FirstOrderConjunction(at, verbTemporal),
                new FirstOrderDisjunction(prepTemporal, prepNumeric));

        FirstOrderConstraint verbLocative, prepLocation, prepDestination;
        verbLocative = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, head), "AM-LOC");
        prepLocation = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Location");
        prepDestination = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(prepSRL, head), "Destination");
        FirstOrderConstraint implicationLoc = new FirstOrderImplication(
                new FirstOrderConjunction(prep, verbLocative),
                new FirstOrderDisjunction(prepLocation, prepDestination));

        this.constraint =  new FirstOrderConjunction(implicationLoc, implicationTemp);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(verbSRL.toString(), 0.5);
        alphas.put(prepSRL.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(PathLSTMPrepSRLInference.class, alphas);
        jointTrainer.trainWeightsOnline(2, prepSRL, verbSRL, prepSRLReader, verbSRLReader, prepSRLOracle, verbSRLOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        verbSRLReader.setDataset("test");
        jointTrainer.test(tester, verbSRL, verbSRLReader, verbSRLOracle, true );

        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        prepSRLReader.setDataset("test");
        jointTrainer.test(tester, prepSRL, prepSRLReader, prepSRLOracle, true);

    }
}
