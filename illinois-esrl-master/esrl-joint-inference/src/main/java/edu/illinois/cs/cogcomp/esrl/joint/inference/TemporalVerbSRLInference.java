package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalClassifier;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalDataReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.VerbSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Joint inference definition for NER and VerbSRL
 */
public class TemporalVerbSRLInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String dataDir = rm.getString(ESRLConfigurator.TEMPORAL_DATA_DIR);

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameTemporal = modelsDir + File.separator + TemporalClassifier.CLASS_NAME;
    private static String modelNameSRL = modelsDir + File.separator + VerbSRLArgumentClassifier.CLASS_NAME;
    // TODO: 4/19/16 This is the unconstrained version of the classifier
    private static VerbSRLArgumentClassifier verbSRL = new VerbSRLArgumentClassifier(modelNameSRL + ".lc", modelNameSRL + ".lex");
    private static TemporalClassifier temporal = new TemporalClassifier(modelNameTemporal + ".lc", modelNameTemporal + ".lex");

    private static final TemporalDataReader temporalReader = new TemporalDataReader(dataDir + "/trainingData.txt", "train");
    private static final VerbSRLArgumentReader verbSRLReader = new VerbSRLArgumentReader("train", true);

    private static final Classifier temporalOracle = new TemporalClassifier.Label();
    private static final Classifier verbSRLOracle = new VerbSRLArgumentClassifier.Label();

    public TemporalVerbSRLInference(Constituent head) {
        super(head);
        List<Constituent> srlConstituents = VerbSRLArgumentReader.getAllArgumentInstances(head);
        List<Constituent> temporalConstituents = head.getTextAnnotation()
                .getView(ViewNames.TOKENS).getConstituentsCovering(head);

        Object[] context = {head};

        FirstOrderConstraint srlExistsTemp, tempBDate, tempIDate, temporalPredictions;
        srlExistsTemp = new ExistentialQuantifier("srlTemp", srlConstituents,
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(verbSRL, null), "AM-TMP", new LeftEAR(context)));
        tempBDate = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(temporal, null), "B-DATE", new LeftEAR(context));
//        tempIDate = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(temporal, null), "I-DATE", new LeftEAR(context));
//        temporalPredictions = new ExistentialQuantifier("tempDate", temporalConstituents, new FirstOrderDisjunction(tempBDate, tempIDate));
        temporalPredictions = new ExistentialQuantifier("tempDate", temporalConstituents, tempBDate);
        FirstOrderConstraint implicationTemp = new FirstOrderImplication(srlExistsTemp, temporalPredictions);

        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), implicationTemp);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(temporal.toString(), 0.5);
        alphas.put(verbSRL.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(TemporalVerbSRLInference.class, alphas);
        jointTrainer.trainWeightsOnline(2, temporal, verbSRL, temporalReader, verbSRLReader, temporalOracle, verbSRLOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        temporalReader.setDataset("test");
        jointTrainer.test(tester, temporal, temporalReader, temporalOracle, false);

        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        tester.addNull("missed");
        verbSRLReader.setDataset("test");
        jointTrainer.test(tester, verbSRL, verbSRLReader, verbSRLOracle, false);
    }
}
