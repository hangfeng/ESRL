package edu.illinois.cs.cogcomp.esrl.joint.training;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Score;
import edu.illinois.cs.cogcomp.lbjava.classify.ScoreSet;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.FirstOrderVariable;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;

import java.util.Arrays;
import java.util.Map;

public class JointTrainer {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";

    private Class inferenceCl;
    private Map<String, Double> alphas;

    public JointTrainer(Class inferenceCl, Map<String, Double> alphas) {
        this.inferenceCl = inferenceCl;
        this.alphas = alphas;
    }

    public void trainWeightsOnline(int rounds, Learner learner1, Learner learner2, Parser parser1, Parser parser2,
                                   Classifier oracle1, Classifier oracle2) {
        trainWeightsOnline(rounds, learner1, parser1, oracle1);
        trainWeightsOnline(rounds, learner2, parser2, oracle2);
    }

    public void trainWeightsOnline(int rounds, Learner learner1, Learner learner2, Learner learner3,
                                   Parser parser1, Parser parser2, Parser parser3,
                                   Classifier oracle1, Classifier oracle2, Classifier oracle3) {
        trainWeightsOnline(rounds, learner1, parser1, oracle1);
        trainWeightsOnline(rounds, learner2, parser2, oracle2);
        trainWeightsOnline(rounds, learner3, parser3, oracle3);
    }

    public void trainWeightsBatch(int rounds, Learner learner1, Learner learner2, Parser parser1, Parser parser2,
                                  Classifier oracle1, Classifier oracle2) {
        trainWeightsBatch(rounds, learner1, parser1, oracle1);
        trainWeightsBatch(rounds, learner2, parser2, oracle2);
    }

    private void trainWeightsOnline(int rounds, Learner learner, Parser parser, Classifier oracle) {
        double rate = 0.1;

        int updateCounter = 1;
        for (int round = 0; round < rounds; round++) {
            // The prediction of the unconstrained learner should match
            for (Object example = parser.next(); example != null; example = parser.next()) {
                Constituent c = (Constituent) example;
                try {
                    JointInference inference = (JointInference) this.inferenceCl.getConstructors()[0].newInstance(c);
                    inference.setAlphas(alphas);
                    boolean violation = inference.run();
                    String prediction = inference.getVariable(new FirstOrderVariable(learner, c)).getValue();
                    //String indPrediction = learner.discreteValue(c);
                    boolean error = !oracle.discreteValue(c).equals(prediction);
                    //boolean error = !oracle.discreteValue(c).equals(prediction) && !prediction.equals(indPrediction);

                    double prevAlpha = alphas.get(learner.toString());
                    double update = rate / Math.sqrt(updateCounter);
                    if (update == 0) {
                        System.out.println("Update rate is 0. Returning...");
                        parser.reset();
                        return;
                    }

                    if (error && violation) {
                        updateCounter++;
                        alphas.put(learner.toString(), prevAlpha + update);
                    }
                } catch (Exception e) {
                    System.err.println("Fatal error while evaluating " + learner + ": " + e);
                    e.printStackTrace();
                    System.exit(1);
                }
            }
            parser.reset();
        }
    }

    private void trainWeightsBatch(int rounds, Learner learner, Parser parser, Classifier oracle) {
        // The weight for constrained learner cX should be:
        //      the number of disagreements with iX (independent learner X) / the number of examples in Parser X
        for (int round = 0; round < rounds; round++) {
            double errorCounter = 0, exampleCounter = 0;

            for (Object example = parser.next(); example != null; example = parser.next()) {
                exampleCounter++;
                Constituent c = (Constituent) example;
                try {
                    JointInference inference = (JointInference) this.inferenceCl.getConstructors()[0].newInstance(c);
                    inference.setAlphas(alphas);
                    boolean violation = inference.run();
                    String prediction = inference.getVariable(new FirstOrderVariable(learner, c)).getValue();
                    //String indPrediction = learner.discreteValue(c);
                    boolean error = !oracle.discreteValue(c).equals(prediction);
                    //boolean error = !oracle.discreteValue(c).equals(prediction) && !prediction.equals(indPrediction);

                    if (error && violation) errorCounter++;
                } catch (Exception e) {
                    System.err.println("Fatal error while evaluating " + learner + ": " + e);
                    e.printStackTrace();
                    System.exit(1);
                }
            }
            double prevAlpha = alphas.get(learner.toString());
            double update = prevAlpha + (1 - (errorCounter / exampleCounter)) * prevAlpha;
            alphas.put(learner.toString(), update);
            parser.reset();
        }
    }

    public void test(TestDiscrete tester, Learner learner, Parser parser, Classifier oracle, boolean showDiff) {
        for (Object example = parser.next(); example != null; example = parser.next()) {
            Constituent c = (Constituent) example;
            try {
                JointInference inference = (JointInference) inferenceCl.getConstructors()[0].newInstance(c);
                inference.setAlphas(alphas);
                boolean violation = inference.run();
                FirstOrderVariable inferenceVariable = inference.getVariable(new FirstOrderVariable(learner, c));
                String prediction = inferenceVariable.getValue();
                String indPrediction = learner.discreteValue(c);
                String gold = oracle.discreteValue(example);
                if (showDiff && !indPrediction.equals(prediction)) {
                    String colour;
                    if (prediction.equals(gold)) colour = ANSI_GREEN;
                    else colour = ANSI_RED;
                    System.out.println(c.getTextAnnotation().getText().trim());
                    System.out.print(c.getTokenizedSurfaceForm() + " (" + c.getStartSpan() + ")");
                    System.out.println(colour + "\tjoint: " + prediction + ", ind: " + indPrediction + ", gold: " + gold + ANSI_RESET);
                    System.out.println("\tjoint:" + reverseSortString(inferenceVariable.getScores()));
                    System.out.println("\tind:" + reverseSortString(learner.scores(c)));
                    System.out.println("\tviolation: " + violation);
                    System.out.println();
                }
                tester.reportPrediction(prediction, gold);
            } catch (Exception e) {
                System.err.println("Fatal error while evaluating " + learner + ": " + e);
                e.printStackTrace();
                System.exit(1);
            }
        }
        tester.printPerformance(System.out);
    }

    private String reverseSortString(ScoreSet scoreSet) {
        StringBuilder result = new StringBuilder("{");
        Score[] scores = scoreSet.toArray();
        Arrays.sort(scores, (o1, o2) -> {
            if (o1.score < o2.score) return 1;
            if (o1.score > o2.score) return -1;
            return 0;
        });
        result.append(" ").append(scores[0]);
        for (int i = 1; i < scores.length; ++i)
            result.append(", ").append(scores[i]);
        return result + " }";
    }
}
