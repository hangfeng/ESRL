package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesClassifier;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesDataReader;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalClassifier;
import edu.illinois.cs.cogcomp.esrl.temporal.TemporalDataReader;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Joint inference definition for NER and VerbSRL
 */
public class QuantitiesTemporalInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameQuantities = modelsDir + File.separator + QuantitiesClassifier.CLASS_NAME;
    private static String modelNameTemporal = modelsDir + File.separator + TemporalClassifier.CLASS_NAME;
    private static QuantitiesClassifier quantities = new QuantitiesClassifier(modelNameQuantities + ".lc", modelNameQuantities + ".lex");
    private static TemporalClassifier temporal = new TemporalClassifier(modelNameTemporal + ".lc", modelNameTemporal + ".lex");

    private static final QuantitiesDataReader quantitiesReader = new QuantitiesDataReader(
            rm.getString(ESRLConfigurator.QUANTITIES_DATA_DIR) + "/trainingData.txt", "train");
    private static final TemporalDataReader temporalReader = new TemporalDataReader(
            rm.getString(ESRLConfigurator.TEMPORAL_DATA_DIR) + "/trainingData.txt", "train");

    private static final Classifier quantitiesOracle = new QuantitiesClassifier.Label();
    private static final Classifier temporalOracle = new TemporalClassifier.Label();

    public QuantitiesTemporalInference(Constituent head) {
        super(head);
        FirstOrderConstraint quantTempDate, tempDate, quantDate, quantTime;

        //QUANTITIES & TEMPORAL
        tempDate = getBILabel(head, "DATE", temporal);
        quantDate = getBILabel(head, "DATE", quantities);
        quantTime = getBILabel(head, "TIME", quantities);
        quantTempDate = new FirstOrderImplication(tempDate, new FirstOrderDisjunction(quantDate, quantTime));

        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), quantTempDate);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(quantities.toString(), 0.5);
        alphas.put(temporal.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(QuantitiesTemporalInference.class, alphas);

        jointTrainer.trainWeightsOnline(3, temporal, quantities, temporalReader, quantitiesReader, temporalOracle, quantitiesOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        quantitiesReader.setDataset("test");
        jointTrainer.test(tester, quantities, quantitiesReader, quantitiesOracle, false);
        tester = new TestDiscrete();
        tester.addNull("O");
        temporalReader.setDataset("test");
        jointTrainer.test(tester, temporal, temporalReader, temporalOracle, false);
    }
}
