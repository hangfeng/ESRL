package edu.illinois.cs.cogcomp.esrl.joint.inference;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.joint.JointInference;
import edu.illinois.cs.cogcomp.esrl.joint.training.JointTrainer;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesClassifier;
import edu.illinois.cs.cogcomp.esrl.quantities.QuantitiesDataReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.VerbSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Joint inference definition for NER and VerbSRL
 */
public class QuantitiesVerbSRLInference extends JointInference {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String dataDir = rm.getString(ESRLConfigurator.TEMPORAL_DATA_DIR);

    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelNameQuantities = modelsDir + File.separator + QuantitiesClassifier.CLASS_NAME;
    private static String modelNameSRL = modelsDir + File.separator + VerbSRLArgumentClassifier.CLASS_NAME;
    // TODO: 4/19/16 This is the unconstrained version of the classifier
    private static VerbSRLArgumentClassifier verbSRL = new VerbSRLArgumentClassifier(modelNameSRL + ".lc", modelNameSRL + ".lex");
    private static QuantitiesClassifier quantities = new QuantitiesClassifier(modelNameQuantities + ".lc", modelNameQuantities + ".lex");

    private static final QuantitiesDataReader quantitiesTrainReader = new QuantitiesDataReader(dataDir + "/trainingData.txt", "train");
    private static final QuantitiesDataReader quantitiesTestReader = new QuantitiesDataReader(dataDir + "/testData.txt", "test");
    private static final VerbSRLArgumentReader verbSRLTrainReader = new VerbSRLArgumentReader("train", true);
    private static final VerbSRLArgumentReader verbSRLTestReader = new VerbSRLArgumentReader("test", true);

    private static final Classifier quantitiesOracle = new QuantitiesClassifier.Label();
    private static final Classifier verbSRLOracle = new VerbSRLArgumentClassifier.Label();

    public QuantitiesVerbSRLInference(Constituent head) {
        super(head);
        List<Constituent> srlConstituents = VerbSRLArgumentReader.getAllArgumentInstances(head);
        List<Constituent> quantConstituents = head.getTextAnnotation()
                .getView(ViewNames.TOKENS).getConstituentsCovering(head);

        Object[] context = {head};

        FirstOrderConstraint srlNotA0, srlNotA1, srlPredictions, quantityPredictions;
        srlNotA0 = new UniversalQuantifier("srlTemp", srlConstituents,
                 new FirstOrderEqualityWithValue(false, new FirstOrderVariable(verbSRL, null), "A0", new LeftEAR(context)));
        srlNotA1 = new UniversalQuantifier("srlTemp", srlConstituents,
                 new FirstOrderEqualityWithValue(false, new FirstOrderVariable(verbSRL, null), "A1", new LeftEAR(context)));
        srlPredictions = new FirstOrderConjunction(srlNotA0, srlNotA1);
        quantityPredictions = new ExistentialQuantifier("quantDate", quantConstituents,
                new FirstOrderEqualityWithValue(false, new FirstOrderVariable(quantities, null), "O", new LeftEAR(context)));
        FirstOrderConstraint implicationQuant = new FirstOrderImplication(quantityPredictions, srlPredictions);

        this.constraint = new FirstOrderConjunction(new FirstOrderConstant(true), implicationQuant);
    }

    public static void main(String[] args) {
        Map<String, Double> alphas = new HashMap<>();
        alphas.put(quantities.toString(), 0.5);
        alphas.put(verbSRL.toString(), 0.5);
        JointTrainer jointTrainer = new JointTrainer(QuantitiesVerbSRLInference.class, alphas);
        jointTrainer.trainWeightsOnline(2, quantities, verbSRL, quantitiesTrainReader, verbSRLTrainReader, quantitiesOracle, verbSRLOracle);
        System.out.println(alphas);

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        jointTrainer.test(tester, quantities, quantitiesTestReader, quantitiesOracle, false);

        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        tester.addNull("missed");
        jointTrainer.test(tester, verbSRL, verbSRLTestReader, verbSRLOracle, false);
    }
}
