package edu.illinois.cs.cogcomp.esrl.joint;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.infer.ilp.OJalgoHook;
import edu.illinois.cs.cogcomp.lbjava.classify.Score;
import edu.illinois.cs.cogcomp.lbjava.classify.ScoreSet;
import edu.illinois.cs.cogcomp.lbjava.infer.*;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.learn.Normalizer;
import edu.illinois.cs.cogcomp.lbjava.learn.Softmax;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class JointInference extends ILPInference {
    private Map<String, Double> alphas;

    public JointInference(Constituent head) {
        super(head, new OJalgoHook());
    }

    public String getHeadType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String[] getHeadFinderTypes() {
        return new String[]{"edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent"};
    }

    public Normalizer getNormalizer(Learner c) {
        return new WeightedNormalizer(c);
    }

    public void setAlphas(Map<String, Double> alphas) {
        this.alphas = alphas;
    }

    @SuppressWarnings("unchecked")
    public boolean run() throws Exception {
        if (tautology || solver.isSolved()) return false;

        solver.setMaximize(true);
        constraint.consolidateVariables(variables);
        indexMap = new HashMap();

        boolean constraintViolated = !constraint.evaluate();

        for (Object o : variables.values()) {
            FirstOrderVariable v = (FirstOrderVariable) o;
            ScoreSet ss = getNormalizer(v.getClassifier()).normalize(v.getScores());
            assert (ss != null);
            Score[] scores = ss.toArray();
            double[] scoreArray = new double[scores.length];
            for (int sI = 0; sI < scores.length; sI++) {
                scoreArray[sI] = scores[sI].score;
            }

            int[] indexes = solver.addDiscreteVariable(scoreArray);

            for (int j = 0; j < scores.length; ++j) {
                indexMap.put(new PropositionalVariable(v.getClassifier(), v.getExample(), scores[j].value), indexes[j]);
            }
        }

        PropositionalConstraint propositional = ((FirstOrderConstraint) constraint).propositionalize();

        if (propositional instanceof PropositionalConjunction)
            propositional = ((PropositionalConjunction) propositional).simplify(true);
        else propositional = propositional.simplify();

        topLevel = true;
        propositional.runVisit(this);

        if (!solver.solve()) throw new InferenceNotOptimalException(solver, head);
        int variableIndex = 0;

        for (Object o : variables.values()) {
            FirstOrderVariable v = (FirstOrderVariable) o;
            Score[] scores = v.getScores().toArray();
            for (int j = 0; j < scores.length; ++j, ++variableIndex)
                if (solver.getBooleanValue(variableIndex))
                    v.setValue(scores[j].value);
        }
        return constraintViolated;
    }

    private class WeightedNormalizer extends Normalizer {
        private final Learner learner;
        private Softmax softmax = new Softmax();

        WeightedNormalizer(Learner c) {
            this.learner = c;
        }

        @Override
        public ScoreSet normalize(ScoreSet scores) {
            for (Score score : scores.toArray()) score.score *= alphas.get(learner.toString());
            scores = softmax.normalize(scores);
            return scores;
        }
    }

    /**
     * Convenience object for constructing {@link EqualityArgumentReplacer}s to be used with
     * {@link FirstOrderEqualityWithValue} (only the left argument is replaced).
     */
    protected static class LeftEAR extends EqualityArgumentReplacer {
        public LeftEAR(Object[] c) {
            super(c, true);
        }

        public Object getLeftObject() {
            return quantificationVariables.get(0);
        }
    }

    protected FirstOrderConstraint getBILabel(Constituent head, String label, Learner classifier) {
        return new FirstOrderDisjunction(
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(classifier, head), "B-" + label),
                new FirstOrderEqualityWithValue(true, new FirstOrderVariable(classifier, head), "I-" + label));
    }
}

