package edu.illinois.cs.cogcomp.esrl.mwe;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.mwe.lbjava.MWEClassifier;
import edu.illinois.cs.cogcomp.esrl.mwe.lbjava.MWELabel;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import java.io.File;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.MWE_DATA_DIR);
    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String modelName = modelsDir + File.separator + MWEClassifier.CLASS_NAME;

    public void train() {
        Learner classifier = new MWEClassifier(modelName + ".lc", modelName + ".lex");
        Parser trainDataReader = new MWEDataReader(dataDir + "/brown1", "train");
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader, 1000);
        trainer.train(5);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        Learner classifier = new MWEClassifier(modelName + ".lc", modelName + ".lex");
        Parser testDataReader = new MWEDataReader(dataDir + "/brown2", "test");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, classifier, new MWELabel(), testDataReader, true, 100);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}
