package edu.illinois.cs.cogcomp.esrl.mwe;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Converts the SemCor files in {@code brown1} and {@code brown2} directories to valid XMLs
 */
public class DataConversionUtil {
    public static void main(String[] args) throws IOException {
        ResourceManager rm = ESRLConfigurator.defaults();
        String dataDir = rm.getString(ESRLConfigurator.MWE_DATA_DIR);
        FilenameFilter filter = new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return !name.endsWith(".converted");
            }
        };

        for (String fileName : IOUtils.lsFiles(dataDir + "/brown1", filter)) {
            List<String> outLines = new ArrayList<>();
            for (String line : LineIO.read(fileName)) {
                outLines.add(line.replaceAll("=(.+?)([\\s>])", "=\"$1\"$2").replaceAll("&", "&amp;"));
            }
            LineIO.write(fileName + ".converted", outLines);
        }


        for (String fileName : IOUtils.lsFiles(dataDir + "/brown2", filter)) {
            List<String> outLines = new ArrayList<>();
            for (String line : LineIO.read(fileName)) {
                outLines.add(line.replaceAll("=(.+?)([\\s>])", "=\"$1\"$2").replaceAll("&", "&amp;"));
            }
            LineIO.write(fileName + ".converted", outLines);
        }
    }
}
