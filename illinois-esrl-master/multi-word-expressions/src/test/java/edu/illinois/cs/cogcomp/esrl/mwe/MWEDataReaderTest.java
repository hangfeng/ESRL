package edu.illinois.cs.cogcomp.esrl.mwe;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MWEDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.MWE_DATA_DIR);
    }

    @Test
    public void testDataReader() throws Exception {
        MWEDataReader reader = new MWEDataReader(dataDir + "/brown1", "train");
        Constituent constituent = (Constituent) reader.next();
        assertEquals("group", constituent.getLabel());
        assertEquals("Fulton County Grand Jury", constituent.getTokenizedSurfaceForm());

        Constituent candidate = (Constituent) reader.next();
        assertEquals("candidate", candidate.getLabel());
        assertEquals("recent primary election", candidate.getTokenizedSurfaceForm());
    }
}