# Multi-word Expressions

## Dataset
Using the [SemCor 3.0](http://web.eecs.umich.edu/~mihalcea/downloads.html#semcor) Concordance dataset by Rada Mihalcea  

**NB:** The dataset had to be altered to make it conform to XML standards (attributes surrounded by quotes)