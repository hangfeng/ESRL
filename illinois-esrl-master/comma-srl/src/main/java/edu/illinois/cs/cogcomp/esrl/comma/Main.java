package edu.illinois.cs.cogcomp.esrl.comma;

//import edu.illinois.cs.cogcomp.comma.annotators.PreProcessor;
import edu.illinois.cs.cogcomp.comma.datastructures.CommaProperties;
import edu.illinois.cs.cogcomp.comma.lbj.CommaLabel;
import edu.illinois.cs.cogcomp.comma.lbj.LocalCommaClassifier;
import edu.illinois.cs.cogcomp.comma.readers.CommaParser;
//import edu.illinois.cs.cogcomp.comma.readers.SrikumarAnnotationReader;
//import edu.illinois.cs.cogcomp.curator.CuratorConfigurator;
import edu.illinois.cs.cogcomp.comma.readers.PrettyCorpusReader;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import java.io.IOException;
import java.io.*;
import java.util.Properties;
import java.io.IOException;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.COMMA_DATA_DIR);

    public void test() throws IOException{
        /*String configfile = "config/comma.properties";

        try {
            OutputStream out = new FileOutputStream(configfile);
            Properties properties = rm.getProperties();
            properties.store(out, "comma.properties");
            out.flush();
            out.close();
        }
        catch (IOException var1) {
            var1.printStackTrace();
            System.err.println("Unable to read the configuration file");
            System.exit(-1);
        }*/


        CommaProperties tmp = new CommaProperties("config/comma.properties");

        CommaProperties properties = CommaProperties.getInstance();
        String treebankHome = properties.getPTBHDir();
        String propbankHome = properties.getPropbankDir();
        String nombankHome = properties.getNombankDir();
        System.out.println(treebankHome);
        System.out.println(propbankHome);
        System.out.println(nombankHome);
        Learner classifier = new LocalCommaClassifier();
        PrettyCorpusReader reader = new PrettyCorpusReader(dataDir + "/comma-labeled-data.txt");
        CommaParser testDataReader = new CommaParser(reader.getSentences(), CommaParser.Ordering.RANDOM, true);
        TestDiscrete.testDiscrete(new TestDiscrete(), classifier, new CommaLabel(), testDataReader, true, 1000);
    }

    public static void main(String[] args) throws Exception {

        Main trainer = new Main();
        trainer.test();
    }
}
