package edu.illinois.cs.cogcomp.esrl.clauses;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotationUtilities;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClausesDataReader extends DataReader {

    /**
     * Data reader for the 2001 CoNLL clauser task. We will always be using the complete clause task
     * definition, which corresponds to the "part 3" data.
     *
     * @param file The base name of the input data (without a part number)
     */
    public ClausesDataReader(String file, String corpusName) {
        super(file + "3", corpusName, ESRLViewNames.CLAUSES);
    }

    @Override
    public List<TextAnnotation> readData() {
        List<String> lines;
        try {
            lines = LineIO.read(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Couldn't read " + file);
        }
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        // token POS label
        List<String> tokens = new ArrayList<>();
        List<String> pos = new ArrayList<>();
        List<String> chunks = new ArrayList<>();
        List<String> clauses = new ArrayList<>();
		int taID = 0;
        for (String line : lines) {
            if (line.isEmpty()) {
                List<String[]> tokenizedSentence = Collections.singletonList(tokens.toArray(new String[tokens.size()]));
                TextAnnotation ta = BasicTextAnnotationBuilder.
						createTextAnnotationFromTokens("clauses", String.valueOf(taID++), tokenizedSentence);
                if (rm.getBoolean(ESRLConfigurator.USE_GOLD_POS))
                    addGoldPOSView(ta, pos);
                addGoldChunkView(ta, chunks);
				addGoldClausesView(ta, clauses);
                textAnnotations.add(ta);
                tokens = new ArrayList<>();
                pos = new ArrayList<>();
                clauses = new ArrayList<>();
				chunks = new ArrayList<>();
            }
            else {
                String[] split = line.split("\\s+");
                tokens.add(split[0]);
                pos.add(split[1]);
                chunks.add(split[2]);
                clauses.add(split[3]);
            }
        }
        return textAnnotations;
    }

    private void addGoldChunkView(TextAnnotation ta, List<String> labels) {
        String viewName = ViewNames.SHALLOW_PARSE;
        SpanLabelView chunkView = new SpanLabelView(viewName, ta);
        int startSpan = -1;
        String prevLabel = null;
        for (int i = 0; i < labels.size(); i++) {
            String label = labels.get(i);
            if (label.startsWith("B") || label.equals("O")) {
				if (startSpan != -1) {
					chunkView.addSpanLabel(startSpan, i, prevLabel.substring(2), 1.0);
				}
				if (label.startsWith("B")) {
					startSpan = i;
					prevLabel = label;
				}
				else if (label.startsWith("O")) {
					startSpan = -1;
					prevLabel = null;
				}
            }
        }
        ta.addView(viewName, chunkView);
    }

    public void addGoldClausesView(TextAnnotation ta, List<String> labels) {
        SpanLabelView clausesView = new SpanLabelView(viewName, viewName+"-annotator", ta, 1.0, true);
		List<Integer> stack = new ArrayList<>();
		for (int i = 0; i < labels.size(); i++) {
			String label = labels.get(i);
			// Start new clause(s)
			if (label.contains("(")) {
				String[] split = label.split("(\\(|\\*)");
				// Count the number of ('s and add that many ints into the stack
				for (int i1 = 0; i1 < split.length-1; i1++) stack.add(i);
			}
			// End clause(s) from stack
			else if (label.contains(")")) {
				for (String clauseLabel : label.split("(\\)|\\*)")){
					if (clauseLabel.isEmpty()) continue;
					// pop this many ints from the stack and create clauses from them
					int start = stack.remove(stack.size() - 1);
					clausesView.addSpanLabel(start, i + 1, clauseLabel, 1.0);
				}
			}
		}
		ta.addView(viewName, clausesView);
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        SpanLabelView goldClausesView = (SpanLabelView) ta.getView(viewName);
        SpanLabelView tokensView = (SpanLabelView) ta.getView(ViewNames.TOKENS);
        SpanLabelView chunkView = (SpanLabelView) ta.getView(ViewNames.SHALLOW_PARSE);

        List<Constituent> candidates = new ArrayList<>();

        for (Constituent chunk : chunkView.getConstituents()) candidates.add(chunk);

        addInternalCandidates(ta, candidates);

        // Add the final candidate which is the entire sentence
        candidates.add(new Constituent(CANDIDATE, viewName, ta, tokensView.getStartSpan(), tokensView.getEndSpan()));

        return getFinalCandidates(goldClausesView, candidates);
    }

    private void addInternalCandidates(TextAnnotation ta, List<Constituent> candidates) {
        Collections.sort(candidates, TextAnnotationUtilities.constituentStartComparator);
        List<Constituent> toAdd = new ArrayList<>();
        for (int i = 0; i < candidates.size()-1; i++) {
            Constituent c = candidates.get(i);
            Constituent cNext = candidates.get(i+1);
            int start = c.getStartSpan();
            int end = cNext.getEndSpan();
            toAdd.add(new Constituent(CANDIDATE, viewName, ta, start, end));
        }
        if (toAdd.size() > 1)
            addInternalCandidates(ta, toAdd);
        candidates.addAll(toAdd);
    }
}
