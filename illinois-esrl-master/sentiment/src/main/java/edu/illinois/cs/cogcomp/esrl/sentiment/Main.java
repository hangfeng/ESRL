package edu.illinois.cs.cogcomp.esrl.sentiment;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.sentiment.lbjava.SentimentClassifier;
import edu.illinois.cs.cogcomp.esrl.sentiment.lbjava.SentimentLabel;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;

import java.io.File;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.SENTIMENT_ASPECT_DATA_DIR);
    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String modelName = modelsDir + File.separator + SentimentClassifier.CLASS_NAME;

    public void train(String trainFile, String corpusName) {
        Learner classifier = new SentimentClassifier(modelName + ".lc", modelName + ".lex");
        Parser trainDataReader = new SentimentDataReader(trainFile, corpusName, ESRLViewNames.SENTIMENT_ASPECT);
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader, 1000);
        trainer.train(5);
        classifier.save();
        trainDataReader.close();
    }

    public void test(String testFile, String corpusName) {
        Learner classifier = new SentimentClassifier(modelName + ".lc", modelName + ".lex");
        Parser testDataReader = new SentimentDataReader(testFile, corpusName, ESRLViewNames.SENTIMENT_ASPECT);
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, classifier, new SentimentLabel(), testDataReader, true, 100);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train(trainer.dataDir + "/Laptop_Train_v2.xml", "laptop-train");
        trainer.test(trainer.dataDir + "/laptops-trial.xml", "laptop-test");

        trainer.train(trainer.dataDir + "/Restaurants_Train_v2.xml", "restaurants-train");
        trainer.test(trainer.dataDir + "/restaurants-trial.xml", "restaurants-test");
    }
}
