package edu.illinois.cs.cogcomp.esrl.sentiment;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SentimentDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.SENTIMENT_ASPECT_DATA_DIR);
    }

    @Test
    public void testReader() {
        SentimentDataReader reader = new SentimentDataReader(dataDir + "/Laptop_Train_v2.xml", "laptop-train",
                ESRLViewNames.SENTIMENT_ASPECT);
        reader.next(); reader.next(); reader.next();
        Constituent constituent = (Constituent) reader.next();
        assertEquals("cord", constituent.getSurfaceForm());
        reader.next();reader.next();
        constituent = (Constituent) reader.next();
        assertEquals("battery life", constituent.getTokenizedSurfaceForm());
        assertEquals("positive", constituent.getLabel());
    }
}