package edu.illinois.cs.cogcomp.esrl.temporal;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.lbjava.nlp.Sentence;
import edu.illinois.cs.cogcomp.lbjava.nlp.SentenceSplitter;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Reader for the TimeBank dataset.
 */
public class TemporalDataReader extends DataReader {
    private static TokenizerTextAnnotationBuilder tokenizer = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());

    public TemporalDataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.TEMPORAL);
    }

    @Override
    public List<TextAnnotation> readData() {
        String corpusId = IOUtils.getFileName(file);
        List<String> lines;
        try {
            lines = LineIO.read(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Couldn't read " + file);
        }
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        String doc = "";
        int taId = 0;
        for (String line : lines) {
            if (line.isEmpty()) {
                if (doc.isEmpty()) continue;
                SentenceSplitter splitter = new SentenceSplitter(new String[]{doc.trim()});
                Sentence[] sentences = splitter.splitAll();
                for (int i = 0; i < sentences.length; i++) {
                    Sentence s = sentences[i];
                    String sentText = s.text;
                    // Sometimes the sentence splitter decides to split in the middle of a time expression
                    while (numberOfOccurrences(sentText, "TIMEX3") != (numberOfOccurrences(sentText, "/TIMEX3") * 2))
                        sentText += sentences[++i].text;
                    sentText = "<sent>" + sentText + "</sent>";
                    textAnnotations.add(processSentence(sentText, corpusId, String.valueOf(taId)));
                    taId++;

                }
                doc = "";
            }
            else doc += line + " ";
        }

        return textAnnotations;
    }

    private int numberOfOccurrences(String s, String string) {
        return (s.contains(string)) ? (s.length() - s.replace(string, "").length()) / string.length() : 0;
    }

    private TextAnnotation processSentence(String sent, String corpusId, String taId) {
        TextAnnotation ta = null;
        try {
            DocumentBuilder newDocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document parse = newDocumentBuilder.parse(new ByteArrayInputStream(sent.getBytes()));
            Node sentence = parse.getFirstChild();
            int size = sentence.getChildNodes().getLength();
            String sentenceText = sentence.getTextContent();
            ta = tokenizer.createTextAnnotation(corpusId, taId, sentenceText);
            TokenLabelView view = new TokenLabelView(viewName, ta);
            if (size > 1) {
                for (int i = 0; i < size; i++) {
                    Node child = sentence.getChildNodes().item(i);
                    if (child.getNodeName().startsWith("TIMEX")) {
                        String timeText = child.getTextContent();
                        String timeLabel = child.getAttributes().getNamedItem("type").getNodeValue();
                        int prevIndex = 0;
                        if (i > 0) {
                            String prevText = sentence.getChildNodes().item(i - 1).getTextContent();
                            prevIndex = sentenceText.indexOf(prevText) + prevText.length();
                        }
                        int timeIndexStart = sentenceText.indexOf(timeText, prevIndex);
                        int timeIndexEnd = timeIndexStart + timeText.length();
                        int start = ta.getTokenIdFromCharacterOffset(timeIndexStart);
                        int end = ta.getTokenIdFromCharacterOffset(timeIndexEnd);
                        view.addTokenLabel(start, "B-" + timeLabel, 1.0);
                        for (int tokenInd = start + 1; tokenInd < end; tokenInd++) {
                            view.addTokenLabel(tokenInd, "I-" + timeLabel, 1.0);
                        }
                    }
                }
            }
            for (Constituent c : ta.getView(ViewNames.TOKENS).getConstituents()) {
                if (view.getLabelsCovering(c).isEmpty())
                    view.addTokenLabel(c.getStartSpan(), "O", 1.0);
            }
            ta.addView(viewName, view);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(sent);
            System.exit(-1);
        }
        return ta;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        return getFinalCandidates(ta.getView(viewName), ta.getView(ViewNames.TOKENS).getConstituents());
    }
}
