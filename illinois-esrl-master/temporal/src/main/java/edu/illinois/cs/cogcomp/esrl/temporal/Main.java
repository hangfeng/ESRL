package edu.illinois.cs.cogcomp.esrl.temporal;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;

import java.io.File;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.TEMPORAL_DATA_DIR);
    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String modelName = modelsDir + File.separator + TemporalClassifier.CLASS_NAME;

    public void train() {
        TemporalClassifier classifier = new TemporalClassifier(modelName + ".lc", modelName + ".lex");
        TemporalDataReader trainDataReader = new TemporalDataReader(dataDir + "/trainingData.txt", "train");
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader);
        trainer.train(25);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        TemporalClassifier classifier = new TemporalClassifier(modelName + ".lc", modelName + ".lex");
        TemporalDataReader testDataReader = new TemporalDataReader(dataDir + "/testData.txt", "test");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        TestDiscrete.testDiscrete(tester, classifier, new TemporalClassifier.Label(), testDataReader, true, 1000);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}