package edu.illinois.cs.cogcomp.esrl.lightverb;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LightVerbDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.LIGHT_VERB_DATA_DIR);
    }

    @Test
    public void testReader() {
        LightVerbDataReader reader = new LightVerbDataReader(dataDir + "/lvcData-BIO-test.txt", "test");
        Constituent constituent = (Constituent) reader.next();
        assertEquals("makes good copy", constituent.getSurfaceForm());
        assertEquals(DataReader.CANDIDATE, constituent.getLabel());
        reader.next();
        constituent = (Constituent) reader.next();
        assertEquals("make a rational assessment", constituent.getSurfaceForm());
        assertEquals("LV", constituent.getLabel());
    }
}