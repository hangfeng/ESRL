package edu.illinois.cs.cogcomp.esrl.lightverb;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A {@link TextAnnotation}-based BIO data reader.
 */
public class LightVerbDataReader extends DataReader {

    public LightVerbDataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.LIGHT_VERB);
    }

    @SuppressWarnings("Duplicates")
    @Override
    public List<TextAnnotation> readData() {
        String corpusId = IOUtils.getFileName(file);
        List<String> lines;
        try {
            lines = LineIO.read(file);
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Couldn't read " + file);
        }
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        // token POS label
        List<String> tokens = new ArrayList<>();
        List<String> pos = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        int taId = 0;
        for (String line : lines) {
            if (line.isEmpty()) {
                List<String[]> tokenizedSentence = Collections.singletonList(tokens.toArray(new String[tokens.size()]));
                TextAnnotation ta = BasicTextAnnotationBuilder.createTextAnnotationFromTokens(
                        corpusId, String.valueOf(taId), tokenizedSentence);
                if (rm.getBoolean(ESRLConfigurator.USE_GOLD_POS))
                    addGoldPOSView(ta, pos);
                addGoldBIOView(ta, labels);
                textAnnotations.add(ta);
                tokens.clear();
                pos.clear();
                labels.clear();
                taId++;
            }
            else {
                String[] split = line.split("\\s+");
                tokens.add(split[0]);
                pos.add(split[1]);
                labels.add(split[2]);
            }
        }
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        List<String> allowedVerbs = Arrays.asList("have", "take", "give", "do", "get", "make");
        View depView = ta.getView(ViewNames.DEPENDENCY); // change DEPENDENCY_STANFORD to DEPENDENCY
        View parseView = ta.getView(ViewNames.SHALLOW_PARSE);   //change PARSE_STANFORD to SHALLOW_PARSE
        View lemmaView = ta.getView(ViewNames.LEMMA);
        List<Constituent> candidates = new ArrayList<>();

        for (Constituent c : depView.getConstituents()) {
            if (!allowedVerbs.contains(lemmaView.getConstituentsCovering(c).get(0).getLabel())) continue;
            for (Relation rel : c.getOutgoingRelations()) {
                if (!rel.getRelationName().equals("dobj")) continue;
                Constituent obj = rel.getTarget();
                int from = Math.min(c.getStartSpan(), obj.getStartSpan());
                int to = Math.max(c.getStartSpan(), obj.getStartSpan());
                List<Constituent> constituents = parseView.getConstituentsCoveringSpan(from, to);
                for (Constituent phrase : constituents)
                    if (phrase.getLabel().equals("VP") && phrase.size() < 7) candidates.add(phrase);

            }
        }
        return getFinalCandidates(ta.getView(viewName), candidates);
    }
}
