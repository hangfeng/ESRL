package edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features;

import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

public class NomArgumentClassifierFeatures extends Classifier{
    public  NomArgumentClassifierFeatures ()
    {
        containingPackage ="edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features";
        name = "nomArgumentClassifierFeatures";
    }
    //private static final SRLFeatures predAndHeadWord = SRLFeatures.predAndHeadWord;
   // private static final SRLFeatures predAndPosition = SRLFeatures.predAndPosition;
    private static final SRLFeatures predicateAndPtype = SRLFeatures.predicateAndPtype;
    private static final SRLFeatures predicateAndLastword = SRLFeatures.predicateAndLastword;
    private static final SRLFeatures nomtypeAndPosition = SRLFeatures.nomtypeAndPosition;

    public FeatureVector classify(Object example){
        FeatureVector result = new FeatureVector();

    //    result.addFeatures(predAndHeadWord.classify(example));
     //   result.addFeatures(predAndPosition.classify(example));
        result.addFeatures(predicateAndPtype.classify(example));
        result.addFeatures(predicateAndLastword.classify(example));
        result.addFeatures(nomtypeAndPosition.classify(example));
        return result;

    }
}
