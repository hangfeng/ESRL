package edu.illinois.cs.cogcomp.esrl.nomsrl;

import edu.illinois.cs.cogcomp.pipeline.server.ServerClientAnnotator;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;

public class TestStanfordParse {
    public static void main(String[] args) throws Exception {
        ServerClientAnnotator annotator = new ServerClientAnnotator();
        annotator.setUrl("http://austen.cs.illinois.edu", "5800"); // set the url and port name of your server here
        annotator.setViews(ViewNames.PARSE_STANFORD); // specify the views that you want
        TextAnnotation ta = annotator.annotate("Five officials of this investment banking firm were elected directors : E. Garrett Bewkes III , a 38-year-old managing director in the mergers and acquisitions department ; Michael R. Dabney , 44 , a managing director who directs the principal activities group which provides funding for leveraged acquisitions ; Richard Harriton , 53 , a general partner who heads the correspondent clearing services ; Michael Minikes , 46 , a general partner who is treasurer ; and William J. Montgoris , 42 , a general partner who is also senior vice president of finance and chief financial officer .");
        System.out.println(ta.getAvailableViews()); // here you should see that the required views are added
        System.out.println(ta.getView(ViewNames.PARSE_STANFORD));
    }
}
