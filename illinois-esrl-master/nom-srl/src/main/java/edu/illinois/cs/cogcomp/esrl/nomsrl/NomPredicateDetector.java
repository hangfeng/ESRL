package edu.illinois.cs.cogcomp.esrl.nomsrl;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TokenLabelView;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.edison.utilities.NomLexEntry;
import edu.illinois.cs.cogcomp.edison.utilities.NomLexReader;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLPredicateIdentifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.nlp.utilities.POSUtils;
import org.tartarus.snowball.SnowballStemmer;
import org.tartarus.snowball.ext.englishStemmer;

import java.io.File;
import java.util.*;

public class NomPredicateDetector {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelName = modelsDir + File.separator + NomSRLPredicateIdentifier.CLASS_NAME;

    private static final NomSRLPredicateIdentifier identifier =
            new NomSRLPredicateIdentifier(modelName + "_auto.lc", modelName + ".lex");

    private static final SnowballStemmer stemmer = new englishStemmer();
    private final static Map<String, String> nonStandard;
    private final static Set<String> pluralLemmas;

    private static final Set<NomLexEntry.NomLexClasses> classes;

    static {
        nonStandard = new HashMap<>();

        nonStandard.put("bondholder", "holder");
        nonStandard.put("earthquake", "quake");
        nonStandard.put("spokesperson", "person");
        nonStandard.put("allies", "ally");
        nonStandard.put("liabilities", "liability");
        nonStandard.put("reelection", "election");
        nonStandard.put("coauthor", "author");
        nonStandard.put("people", "person");
        nonStandard.put("supressor", "suppressor");
        nonStandard.put("buyout", "buy-out");
        nonStandard.put("hookup", "hook-up");
        nonStandard.put("ceasefire", "cease-fire");
        nonStandard.put("startup", "start-up");
        nonStandard.put("eurobond", "bond");

        pluralLemmas = new HashSet<>();
        pluralLemmas.addAll(Arrays.asList("filers", "hundreds", "thousands", "millions", "billions", "tens"));

        // All NomLex classes are accounted for here.
        classes = new HashSet<>();
        classes.addAll(NomLexEntry.VERBAL);
        classes.addAll(NomLexEntry.ADJECTIVAL);
        classes.addAll(NomLexEntry.NON_VERB_ADJ);

    }
    private final NomLexReader nomLex;

    public NomPredicateDetector() {
        try {
            nomLex = NomLexReader.getInstance();
        } catch (EdisonException e) {
            throw new RuntimeException("Could not load NomLex");
        }
    }

    public List<Constituent> getPredicates(TextAnnotation ta) {
        List<Constituent> output = new ArrayList<>();
        for (Constituent c : generateCandidates(ta)) {
            // If this is not an argument according to the identifier
            if (identifier.discreteValue(c).equals("false")) continue;
            output.add(c);
        }
        return output;
    }

    public List<Constituent> generateCandidates(TextAnnotation ta) {
        List<Constituent> list = new ArrayList<>();
        for (int tokenId = 0; tokenId < ta.size(); tokenId++) {
            String pos = POSUtils.getPOS(ta, tokenId);
            boolean isNoun = POSUtils.isPOSNoun(pos);
            if (!isNoun) continue;
            String opt;
            String token = ta.getToken(tokenId).toLowerCase();
            if (pluralLemmas.contains(token)) opt = testTokenVariations(token);
            else {
                TokenLabelView lemmaView = (TokenLabelView) ta.getView(ViewNames.LEMMA);
                String lemma = lemmaView.getConstituentAtToken(tokenId).getLabel();

                opt = testTokenVariations(lemma);

                if (opt == null && !lemma.matches("-*")) {
                    opt = testWithDelim(lemma, '-');
                }

                if (opt == null) {
                    opt = testWithDelim(lemma, ' ');
                }
            }
            Constituent c = new Constituent(DataReader.CANDIDATE, "HeuristicPredicateView", ta, tokenId, tokenId + 1);
            c.addAttribute(PredicateArgumentView.LemmaIdentifier, opt);
            list.add(c);
        }
        return list;
    }

    private String testWithDelim(String token, char delim) {
        String found = null;

        if (token.indexOf(delim) >= 0) {
            String[] split = token.split("" + delim);
            String lastElement = split[split.length - 1];

            found = testTokenVariations(lastElement);
        }
        return found;
    }

    private String testTokenVariations(String token) {
        String opt;
        if (nomLex.isPlural(token)) {
            token = nomLex.getSingular(token);
        }

        opt = testTokenInstance(token);

        if (opt == null) {
            if (token.endsWith("s")) {
                String prefix = token.substring(0, token.length() - 1);
                opt = testTokenInstance(prefix);
            }
        }

        if (opt == null) {
            // hard coded lemma for bookmaker, steelmaker, downpayments, etc
            if (token.endsWith("maker") || token.endsWith("makers"))
                opt = "maker";
            else if (token.endsWith("payment") || token.endsWith("payments"))
                opt = "payment";
        }

        if (opt == null) {
            if (token.startsWith("counter")) {
                String suffix = token.replace("counter", "");
                opt = testTokenInstance(suffix);
            }
        }

        if (opt == null) {
            token = getPorterLemma(token);
            opt = testTokenInstance(token);
        }
        return opt;
    }

    private String testTokenInstance(String token) {
        if (nonStandard.containsKey(token)) {
            token = nonStandard.get(token);
        }

        String found = null;
        if (nomLex.containsEntry(token)) {
            List<NomLexEntry> entry = nomLex.getNomLexEntry(token);
            for (NomLexEntry e : entry) {
                if (classes.contains(e.nomClass)) {
                    found = token;
                    break;
                }
            }
        }
        return found;
    }

    private String getPorterLemma(String token) {
        stemmer.setCurrent(token);
        stemmer.stem();
        return stemmer.getCurrent();
    }

    private void train() throws Exception {
        NomSRLPredicateReader trainReader = new NomSRLPredicateReader("train", false);
        BatchTrainer trainer = new BatchTrainer(identifier, trainReader, 10000);
        trainer.train(10);
        identifier.save();
    }

    private void test() throws Exception {
        NomSRLPredicateReader testDataReader = new NomSRLPredicateReader("test", false);
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, identifier, new NomSRLPredicateIdentifier.Label(), testDataReader, true, 10000);
    }

    public static void main(String[] args) throws Exception {
        NomPredicateDetector predicateDetector = new NomPredicateDetector();
        predicateDetector.train();
        predicateDetector.test();
    }
}
