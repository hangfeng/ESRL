package edu.illinois.cs.cogcomp.esrl.metonymy;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.metonymy.lbjava.MetonymyClassifier;
import edu.illinois.cs.cogcomp.esrl.metonymy.lbjava.MetonymyLabel;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;

import java.io.File;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.METONYMY_DATA_DIR);
    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String modelName = modelsDir + File.separator + MetonymyClassifier.CLASS_NAME;

    public void train() {
        Parser trainDataReader = new MetonymyDataReader(dataDir + "/companies.train", "companies-train");
        Learner classifier = new MetonymyClassifier(modelName + ".lc", modelName + ".lex");
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader, 1000);
        trainer.train(5);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        Parser testDataReader = new MetonymyDataReader(dataDir + "/companies.test", "companies-test");
        Learner classifier = new MetonymyClassifier(modelName + ".lc", modelName + ".lex");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, classifier, new MetonymyLabel(), testDataReader, true, 100);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}
