package edu.illinois.cs.cogcomp.esrl.metonymy;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Queries;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.SpanLabelView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetonymyDataReader extends DataReader {

    public MetonymyDataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.METONYMY);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<TextAnnotation> readData() {
        List<TextAnnotation> textAnnotations;
        Map<String, String> sentAnnotations = new HashMap<>();
        try {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(file + ".BNCtagged.annotation");
            List<Element> annotations = doc.getRootElement().getChildren();

            for (Element annotation : annotations) {
                Element type = (Element) annotation.getChildren().get(0);
                String label = type.getAttributeValue("reading");
                String metLabel = (label.equals("metonymic")) ? type.getAttributeValue("metotype") : label;
                List<Element> children = type.getChildren();

                // Assume the annotations are all in the same sentence
                String id = children.get(0).getAttributeValue("id");
                String sentenceId = id.substring(0, id.lastIndexOf("-"));

                // Assume the annotations are contiguous tokens
                String annotationStart = children.get(0).getAttributeValue("id");
                String annotationEnd = children.get(children.size()-1).getAttributeValue("id");

                sentAnnotations.put(sentenceId, annotationStart+"%"+annotationEnd+"%"+metLabel);
            }

            // Now read the texts
            textAnnotations = readTexts(sentAnnotations);
        } catch (Exception e) {
            throw new RuntimeException("Couldn't read " + file + ".BNCtagged.annotation");
        }
        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        SpanLabelView goldView = (SpanLabelView) ta.getView(viewName);
        SpanLabelView chunkView = (SpanLabelView) ta.getView(ViewNames.SHALLOW_PARSE);
        SpanLabelView posView = (SpanLabelView) ta.getView(ViewNames.POS);
        List<Constituent> candidates = new ArrayList<>();

        for (Constituent c : chunkView.where(Queries.hasLabel("NP"))) {
            if (posView.getLabelsCoveringSpan(c.getStartSpan(), c.getEndSpan()).contains("NNP"))
                candidates.add(c);
        }
        return getFinalCandidates(goldView, candidates);
    }

    @SuppressWarnings("unchecked")
    private List<TextAnnotation> readTexts(Map<String, String> sentAnnotations) {
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        try {
            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(file + ".BNCtagged.samples");
            //sample->par->[s]->[w]
            List<Element> samples = doc.getRootElement().getChildren();
            for (Element sample : samples) {
                String sampleId = sample.getAttributeValue("id");
                Element paragraph = sample.getChild("par");
                List<Element> sentences = paragraph.getChildren();
                IntPair labelSpan = null;
                String label = null;
                List<String[]> tokenizedSentences = new ArrayList<>();
                Map<String, Integer> wordIdsToTokens = new HashMap<>();
                int tokenCounter = 0;
                for (Element sentence : sentences) {
                    String id = sentence.getAttributeValue("id");
                    // Collect all the words
                    List<String> wordStrings = new ArrayList<>();
                    List<Element> words = sentence.getChildren();
                    for (Element word : words) {
                        String wordId  = word.getAttributeValue("id");
                        wordIdsToTokens.put(wordId, tokenCounter++);
                        String text = word.getTextNormalize();
                        text = text.replaceAll("\\[hellip\\]", "...");
                        wordStrings.add(text);
                    }
                    tokenizedSentences.add(wordStrings.toArray(new String[wordStrings.size()]));

                    if (sentAnnotations.containsKey(id)) {
                        String[] splits = sentAnnotations.get(id).split("%");
                        int spanStart = wordIdsToTokens.get(splits[0]);
                        int spanEnd = wordIdsToTokens.get(splits[1]) + 1;
                        label = splits[2];
                        // Create the span relative to the start of the TextAnnotation
                        labelSpan = new IntPair(spanStart, spanEnd);
                    }
                }
                // Create the TextAnnotation and Metonymy view
                TextAnnotation ta = BasicTextAnnotationBuilder
                        .createTextAnnotationFromTokens("metonymy", sampleId, tokenizedSentences);
                SpanLabelView view = new SpanLabelView(viewName, ta);
                assert (labelSpan != null) && (label != null);
                view.addSpanLabel(labelSpan.getFirst(), labelSpan.getSecond(), label, 1.0);
                ta.addView(viewName, view);

                textAnnotations.add(ta);
            }
        } catch (Exception e) {
			e.printStackTrace();
            throw new RuntimeException("Couldn't read " + file + ".BNCtagged.samples");
        }
        return textAnnotations;
    }
}
