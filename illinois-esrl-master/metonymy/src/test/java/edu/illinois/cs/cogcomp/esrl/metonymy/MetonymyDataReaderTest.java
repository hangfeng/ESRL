package edu.illinois.cs.cogcomp.esrl.metonymy;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MetonymyDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.METONYMY_DATA_DIR);
    }

    @Test
    public void testReader() throws Exception {
        MetonymyDataReader reader = new MetonymyDataReader(dataDir + "/companies.test", "companies-test");
        reader.next();
        Constituent constituent = (Constituent) reader.next();
        assertEquals(2, constituent.size());
        assertEquals("Microsoft Corp", constituent.getTokenizedSurfaceForm());
        assertEquals("literal", constituent.getLabel());
    }
}