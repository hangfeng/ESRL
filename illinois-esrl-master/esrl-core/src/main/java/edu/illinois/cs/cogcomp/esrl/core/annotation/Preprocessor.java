package edu.illinois.cs.cogcomp.esrl.core.annotation;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.annotation.AnnotatorService;
import edu.illinois.cs.cogcomp.annotation.BasicAnnotatorService;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.curator.CuratorClient;
import edu.illinois.cs.cogcomp.curator.CuratorFactory;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.pipeline.main.PipelineFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An annotation preprocessor used by all the modules. Can use either the {@link CuratorClient}
 * (via the {@link CuratorFactory}) or {@link BasicAnnotatorService} (via the {@link PipelineFactory}).
 * The configurations parameters are set in {@link PreprocessorConfigurator} and
 * should be merged with {@link ESRLConfigurator}.
 */
public class Preprocessor {
    private enum Type {pipeline, curator}

    private final ResourceManager rm;

    private static Logger logger = LoggerFactory.getLogger(Preprocessor.class);

    private AnnotatorService annotator;

    public Preprocessor(ResourceManager rm) {
        this.rm = rm;
        Type annotatorType = Type.valueOf(rm.getString(ESRLConfigurator.ANNOTATOR));
        try {
            if (annotatorType == Type.pipeline) {
                annotator = PipelineFactory.buildPipeline(rm);
            } else if (annotatorType == Type.curator) {
                annotator = CuratorFactory.buildCuratorClient(rm);
            }
        } catch (Exception e) {
            logger.error("Unable to create {} preprocessor. \n{}", annotatorType.name(), e.getMessage());
        }
    }

    /**
     * Add the required views to the {@link TextAnnotation}. The views are specified in
     * {@link PreprocessorConfigurator#VIEWS_TO_ADD}.
     *
     * @param ta The {@link TextAnnotation} to be annotated
     * @return Whether new views were added
     */
    public boolean annotate(TextAnnotation ta) throws AnnotatorException {
        boolean addedViews = false;
        for (String view : rm.getCommaSeparatedValues(PreprocessorConfigurator.VIEWS_TO_ADD)) {
            if (ta.hasView(view)) continue;
            annotator.addView(ta, view);
            addedViews = true;
        }
        return addedViews;
    }
}
