package edu.illinois.cs.cogcomp.esrl.core;

import edu.illinois.cs.cogcomp.core.utilities.configuration.Configurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.Property;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;

public class ESRLConfigurator extends Configurator {
    public static Property USE_GOLD_POS = new Property("UseGoldPOS", Configurator.FALSE);
    public static Property ANNOTATOR = new Property("Annotator", "pipeline");

    // Module data directories
    public static String ROOT_DIR = System.getProperty("user.dir");
    public static Property WSJ_RAW_DIR = new Property("WSJRawDir", ROOT_DIR + "/data/ptb2-wsj-raw"); //no data
    public static Property PROPBANK_DIR = new Property("PropBankDir",  ROOT_DIR + "/data/propbank"); //no data
    public static Property NOMBANK_DIR = new Property("NomBankDir",  ROOT_DIR + "/data/nombank"); // no data
    public static Property TREEBANK_DIR = new Property("TreeBankDir", ROOT_DIR + "/data/ptb3-wsj-parsed");// no data
    public static Property LIGHT_VERB_DATA_DIR = new Property("LightVerbDataDir", ROOT_DIR + "/data/light-verbs");
    public static Property PHRASAL_VERB_DATA_DIR = new Property("PhrasalVerbDataDir", ROOT_DIR + "/data/phrasal-verbs");
    public static Property PPA_DATA_DIR = new Property("PPAttachmentDataDir", ROOT_DIR + "/data/ppa");
    public static Property SENTIMENT_ASPECT_DATA_DIR = new Property("SentimentAspectDataDir", ROOT_DIR + "/data/sentiment/aspect");
    public static Property CLAUSES_DATA_DIR = new Property("ClausesDataDir", ROOT_DIR + "/data/clauses");
    public static Property VP_ELLIPSIS_DATA_DIR = new Property("VPEllipsisDataDir", ROOT_DIR + "/data/vp-ellipsis/wsj");
    public static Property METONYMY_DATA_DIR = new Property("MetonymyDataDir", ROOT_DIR + "/data/metonymy/BNCtagged-standoff");
    public static Property MWE_DATA_DIR = new Property("MWEDataDir", ROOT_DIR + "/data/mwes");
    public static Property SPECIFICITY_DATA_DIR = new Property("SpecificityDataDir", ROOT_DIR + "/data/specificity");
    public static Property FRAMENET_DATA_DIR = new Property("FramenetDataDir", ROOT_DIR + "/data/framenet");
    public static Property COMMA_DATA_DIR = new Property("CommaDataDir", ROOT_DIR + "/data/commas");//no data
    public static Property NER_CONLL_DATA_DIR = new Property("NERCoNLLDataDir", ROOT_DIR + "/data/ner/conll");
    public static Property NER_MUC_DATA_DIR = new Property("MUC7DataDir", ROOT_DIR + "/data/ner/muc");
    public static Property NER_ONTONOTES_DATA_DIR = new Property("NEROntonotesDataDir", "/data/ner/ontonotes");
    public static Property QUANTITIES_DATA_DIR = new Property("Quantities", ROOT_DIR + "/data/quantities");//no data
    public static Property TEMPORAL_DATA_DIR = new Property("Temporal", ROOT_DIR + "/data/temporal");
    public static Property PREP_DATA_DIR = new Property("Prepositions", ROOT_DIR + "/data/srl-prep");
    public static Property ONTONOTES_DIR = new Property("Ontonotes", ROOT_DIR + "/data/conll-2012/ontonotes-release-5.0/data/files");

    public static Property SRL_USE_GOLD = new Property("SRLUseGold", Configurator.FALSE);

    // Module model directory
    public static Property MODELS_DIR = new Property("ModelsDir", ROOT_DIR + "/models");
    public static Property ERRORS_DIR = new Property("ErrorsDir", ROOT_DIR + "/errors");
    public static Property RESULTS_DIR = new Property("ResultsDir", ROOT_DIR + "/results");

    @Override
    public ResourceManager getDefaultConfig() {
        Property[] props = {WSJ_RAW_DIR, USE_GOLD_POS, ANNOTATOR, LIGHT_VERB_DATA_DIR, PHRASAL_VERB_DATA_DIR,
				PPA_DATA_DIR, SENTIMENT_ASPECT_DATA_DIR, CLAUSES_DATA_DIR, VP_ELLIPSIS_DATA_DIR, METONYMY_DATA_DIR,
				MWE_DATA_DIR, SPECIFICITY_DATA_DIR, FRAMENET_DATA_DIR, COMMA_DATA_DIR, NER_CONLL_DATA_DIR,
                NER_MUC_DATA_DIR, NER_ONTONOTES_DATA_DIR, MODELS_DIR, PROPBANK_DIR, TEMPORAL_DATA_DIR,
                QUANTITIES_DATA_DIR, TREEBANK_DIR, SRL_USE_GOLD, NOMBANK_DIR, PREP_DATA_DIR, ERRORS_DIR, RESULTS_DIR, ONTONOTES_DIR};
        return new ResourceManager(generateProperties(props));
    }

	public static ResourceManager defaults() {
		return new ESRLConfigurator().getDefaultConfig();
	}
}
