package edu.illinois.cs.cogcomp.esrl.core;

import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.DiscretePrimitiveStringFeature;
import edu.illinois.cs.cogcomp.lbjava.classify.Feature;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.lbjava.learn.SparseAveragedPerceptron;
import edu.illinois.cs.cogcomp.lbjava.learn.SparseNetworkLearner;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Class provides access to the basic LBJava API with default Feature Extractor, Label and Parameters.
 * Implementations should override the {@link #initialize()} method as well as override the constructor for
 * {@link FeatureExtractor} and the {@link Label#discreteValue(Object)} method.
 * For example:
 * <pre>
 * {@code
 *   @Override
 *   protected void initialize() {
 *       containingPackage = PACKAGE_NAME;
 *       name = CLASS_NAME;
 *       setLabeler(new Label());
 *       setExtractor(new FeatureExtractor());
 *   }
 *
 *   public static class FeatureExtractor extends AbstractClassifier.FeatureExtractor {
 *       public FeatureExtractor() {
 *           containingPackage = PACKAGE_NAME;
 *           name = CLASS_NAME + "$FeatureExtractor";
 *           featureSet = new Classifier[]{new wordFeatures(), new posFeatures(),
 *           new shallowParseFeatures(), new identifierParseFeatures()};
 *       }
 *   }
 *
 *   public static class Label extends AbstractClassifier.Label {
 *       public Label() {
 *           containingPackage = PACKAGE_NAME;
 *           name = CLASS_NAME + "$Label";
 *        }
 *
 *       public String discreteValue(Object example) {
 *           return String.valueOf(!((Constituent) example).getLabel().equals("candidate"));
 *       }
 *   }
 * }
 * </pre>
 */
public abstract class AbstractClassifier extends SparseNetworkLearner {
    public static boolean isTraining;

    public AbstractClassifier(String name) {
        super(name);
    }

    public AbstractClassifier(String name, String modelPath, String lexiconPath) {
        super(name, new Parameters());
        try {
            lcFilePath = new URL("file:" + modelPath);
            lexFilePath = new URL("file:" + lexiconPath);
        } catch (MalformedURLException e) {
            System.err.println("Cannot create model/lexicon URLs (check path definition)");
            System.exit(-1);
        }
        if (new File(modelPath).exists()) {
            System.out.println("Reading model from " + modelPath);
            readModel(modelPath);
            System.out.println("Reading lexicon from " + lexiconPath);
            readLexiconOnDemand(lexiconPath);
        }
        else {
            System.out.println("Creating new model/lexicon");
            initialize();
        }
    }

    protected abstract void initialize();

    protected static class Parameters extends SparseNetworkLearner.Parameters {
        public Parameters() {
            SparseAveragedPerceptron.Parameters p = new SparseAveragedPerceptron.Parameters();
            p.learningRate = 0.1;
            p.thickness = 2;
            baseLTU = new SparseAveragedPerceptron(p);
        }
    }

    public abstract static class FeatureExtractor extends Classifier {
        protected Classifier[] featureSet;

        public FeatureVector classify(Object example) {
            FeatureVector result = new FeatureVector();
            for (Classifier featExtractor : featureSet)
                result.addFeatures(featExtractor.classify(example));
            return result;
        }
    }

    public abstract static class Label extends Classifier {
        public FeatureVector classify(Object example) {
            return new FeatureVector(featureValue(example));
        }

        public Feature featureValue(Object example) {
            String result = discreteValue(example);
            return new DiscretePrimitiveStringFeature(containingPackage, name, "", result,
                    valueIndexOf(result), (short) allowableValues().length);
        }
    }
}
