package edu.illinois.cs.cogcomp.esrl.core.annotation;

import edu.illinois.cs.cogcomp.annotation.AnnotatorServiceConfigurator;
import edu.illinois.cs.cogcomp.core.constants.CoreConfigNames;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.utilities.configuration.Configurator;
import edu.illinois.cs.cogcomp.core.utilities.configuration.Property;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.pipeline.common.PipelineConfigurator;
import edu.illinois.cs.cogcomp.pipeline.common.Stanford331Configurator;

import java.util.Arrays;

/**
 * The properties for the {@link Preprocessor}.
 */
public class PreprocessorConfigurator extends Configurator {
    public static String[] views = {ViewNames.LEMMA, ViewNames.POS, ViewNames.SHALLOW_PARSE,
            ViewNames.DEPENDENCY};
    public static Property VIEWS_TO_ADD = new Property(CoreConfigNames.VIEWS_TO_ADD, getViewsString());

    public static Property DISABLE_CACHE = new Property(AnnotatorServiceConfigurator.DISABLE_CACHE.key, Configurator.TRUE);

	// Curator properties
	public static Property CURATOR_HOST = new Property(CoreConfigNames.CURATOR_HOST, "trollope.cs.illinois.edu");
	public static Property CURATOR_PORT = new Property(CoreConfigNames.CURATOR_PORT, "9010");

	// Pipeline properties
    public static Property STFRD_TIME_PER_SENTENCE = new Property(Stanford331Configurator.STFRD_TIME_PER_SENTENCE.key, "2000");
    public static Property STFRD_MAX_SENTENCE_LENGTH = new Property(Stanford331Configurator.STFRD_MAX_SENTENCE_LENGTH.key, "60");
	// Pipeline properties (related to VIEWS_TO_ADD above)
	public static Property USE_POS = new Property(PipelineConfigurator.USE_POS.key, isView(ViewNames.POS));
	public static Property USE_LEMMA = new Property(PipelineConfigurator.USE_LEMMA.key, isView(ViewNames.LEMMA));
	public static Property USE_NER_CONLL = new Property(PipelineConfigurator.USE_NER_CONLL.key, isView(ViewNames.NER_CONLL));
	public static Property USE_NER_ONTONOTES = new Property(PipelineConfigurator.USE_NER_ONTONOTES.key, isView(ViewNames.NER_ONTONOTES));
	public static Property USE_SHALLOW_PARSE = new Property(PipelineConfigurator.USE_SHALLOW_PARSE.key, isView(ViewNames.SHALLOW_PARSE));
	public static Property USE_DEP_PARSE_STANFORD = new Property(PipelineConfigurator.USE_STANFORD_DEP.key, isView(ViewNames.DEPENDENCY_STANFORD));
	public static Property USE_DEP_PARSE = new Property(PipelineConfigurator.USE_DEP.key, isView(ViewNames.DEPENDENCY));
	public static Property USE_CONST_PARSE = new Property(PipelineConfigurator.USE_STANFORD_PARSE.key, isView(ViewNames.PARSE_STANFORD));
	public static Property USE_SRL_VERB = new Property(PipelineConfigurator.USE_SRL_VERB.key, isView(ViewNames.SRL_VERB));
	public static Property USE_SRL_NOM = new Property(PipelineConfigurator.USE_SRL_NOM.key, isView(ViewNames.SRL_NOM));

	@Override
	public ResourceManager getDefaultConfig() {
		Property[] props = {VIEWS_TO_ADD, DISABLE_CACHE, CURATOR_HOST, CURATOR_PORT,
                STFRD_MAX_SENTENCE_LENGTH, STFRD_TIME_PER_SENTENCE,
                USE_POS, USE_LEMMA, USE_NER_CONLL, USE_NER_ONTONOTES, USE_SHALLOW_PARSE, USE_DEP_PARSE,
				USE_DEP_PARSE_STANFORD, USE_CONST_PARSE, USE_SRL_VERB, USE_SRL_NOM};
		return new ResourceManager(generateProperties(props));
	}

	public static ResourceManager defaults() {
		return new PreprocessorConfigurator().getDefaultConfig();
	}

    private static String isView(String view) {
        if (Arrays.asList(views).contains(view)) return Configurator.TRUE;
        return Configurator.FALSE;
    }

    private static String getViewsString() {
        StringBuilder viewsString = new StringBuilder();
        for (String view : views) viewsString.append(",").append(view);
        return viewsString.substring(1);
    }
}
