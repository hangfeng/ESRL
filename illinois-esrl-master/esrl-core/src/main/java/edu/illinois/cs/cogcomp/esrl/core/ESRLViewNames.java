package edu.illinois.cs.cogcomp.esrl.core;

import edu.illinois.cs.cogcomp.core.datastructures.ViewTypes;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.View;

/**
 * A container for the names of the new {@link View}s introduced here.
 */
public class ESRLViewNames {
    public static final String LIGHT_VERB = "LIGHT_VERB";
    public static final String PHRASAL_VERB = "PHRASAL_VERB";
    public static final String PP_ATTACHMENT = "PP_ATTACHMENT";
    public static final String SENTIMENT_ASPECT = "SENTIMENT_ASPECT";
    public static final String SENTIMENT_AFFECT = "SENTIMENT_AFFECT";
    public static final String CLAUSES = "CLAUSES";
    public static final String VP_ELLIPSIS = "VP_ELLIPSIS";
    public static final String METONYMY = "METONYMY";
    public static final String MWEs = "MULTI_WORD_EXPRESSIONS";
    public static final String SPECIFICITY = "SPECIFICITY";
    public static final String FRAMENET = "FRAMENET";
    public static final String TEMPORAL = "TEMPORAL";

    public static ViewTypes getViewType(String viewName) {
        switch (viewName) {
            case LIGHT_VERB:
            case PHRASAL_VERB:
            case PP_ATTACHMENT:
            case CLAUSES:
            case METONYMY:
            case MWEs:
            case SPECIFICITY:
                return ViewTypes.SPAN_LABEL_VIEW;
            case VP_ELLIPSIS:
            case FRAMENET:
                return ViewTypes.PREDICATE_ARGUMENT_VIEW;
            case TEMPORAL:
                return ViewTypes.TOKEN_LABEL_VIEW;
        }
        return null;
    }
}
