package edu.illinois.cs.cogcomp.esrl.core.data;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.transformers.ITransformer;
import edu.illinois.cs.cogcomp.core.utilities.SerializationHelper;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.annotation.Preprocessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Creates and maintains a simple file-based data cache.
 */
public class DataCache {
    private static Logger logger = LoggerFactory.getLogger(DataCache.class);
    private Map<Integer, TextAnnotation> taMap;
    private List<Integer> failed;
    private final String failedFile;
    private Preprocessor preprocessor;
    private ResourceManager rm;
    private String directory;

    public static String[] lsFiles(String directory, FilenameFilter filter)
            throws IOException {
        File dir = new File(directory);
        ArrayList<String> files = new ArrayList<>();
        for (File filepath : dir.listFiles(filter)) {
            if (IOUtils.isFile(filepath.getAbsolutePath()))
                files.add(filepath.getAbsolutePath());
        }
        return files.toArray(new String[files.size()]);
    }

    public DataCache(String directory, ResourceManager rm) {
        this.directory = directory;
        this.rm = rm;
        taMap = new HashMap<>();
        failed = new ArrayList<>();
        failedFile = directory + File.separator + "failed.txt";
        if (IOUtils.exists(directory) && IOUtils.isDirectory(directory)) {
            String[] files = new String[0];
            try {
                if (IOUtils.exists(failedFile)) {
                    failed = LineIO.read(failedFile, new ITransformer<String, Integer>() {
                        @Override
                        public Integer transform(String s) {
                            return Integer.parseInt(s);
                        }
                    });
                }

                files = lsFiles(directory, new FilenameFilter() {
                    @Override
                    public boolean accept(File dir, String name) {
                        return IOUtils.getFileExtension(name).equals("json");
                    }
                });
            } catch (IOException e) {
                logger.error("Cannot list the contents of {}\n{}", directory, e.getMessage());
            }
            logger.info("Using directory {} to read/store TextAnnotations", directory);
            if (files.length > 0)
                logger.info("Reading {} pre-processed TextAnnotations (including {} that failed to process)",
                        (files.length + failed.size()), failed.size());
            for (String file : files) {
                String taHashStr = IOUtils.stripFileExtension(IOUtils.getFileName(file));
                // Remove the prefix ta.
                int taHash = Integer.parseInt(taHashStr.substring(3));
                TextAnnotation ta;
                try {
                    ta = SerializationHelper.deserializeFromJson(LineIO.slurp(file));
                } catch (Exception e) {
                    logger.error("Error while reading file {}\n{}", file, e.getMessage());
                    continue;
                }
                taMap.put(taHash, ta);
            }
        }
        else {
            logger.info("Creating directory {} to store the data cache", directory);
            IOUtils.mkdir(directory);
        }
    }

    public TextAnnotation getProcessed(TextAnnotation ta) {
        int key = ta.hashCode();
        //TODO Check that the returned TextAnnotation contains all the views needed
        if (!taMap.containsKey(key) && !failed.contains(key)) {
            try {
                getPreprocessor().annotate(ta);
            } catch (AnnotatorException | RuntimeException e) {
                logger.error("Unable to preprocess TextAnnotation {}. Skipping", ta.getId());
                failed.add(key);
                try {
                    LineIO.append(failedFile, Integer.toString(key));
                } catch (IOException e1) {
                    logger.error("Unable to add TextAnnotation {} to the failed file", ta.getId());
                }
                return null;
            }
            try {
                LineIO.append(directory + "/ta." + key + ".json", SerializationHelper.serializeToJson(ta));
            } catch (IOException e) {
                logger.error("Unable to store processed TextAnnotation {}", ta.getId());
            }
            taMap.put(key, ta);
        }
        return taMap.get(key);
    }

    private Preprocessor getPreprocessor() {
        if (preprocessor == null)
            preprocessor = new Preprocessor(rm);
        return preprocessor;
    }
}
