package edu.illinois.cs.cogcomp.esrl.nomsrl;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.nlp.corpusreaders.NombankReader;

import java.io.IOException;
import java.util.*;

/**
 * A wrapper for {@link NombankReader}.
 */
public class NomSRLArgumentReader extends DataReader {
    private static final ResourceManager rm = ESRLConfigurator.defaults();
    private static final String nombankDir = rm.getString(ESRLConfigurator.NOMBANK_DIR);
    private static final String treebankDir = rm.getString(ESRLConfigurator.TREEBANK_DIR);

    private static final List<String> coreArgs = Arrays.asList("A0", "A1", "A2", "A3", "A4", "A5", "AA");
    private static final List<String> nonCoreArgs = Arrays.asList("AM-ADV", "AM-CAU", "AM-DIR", "AM-DIS", "AM-EXT",
            "AM-LOC", "AM-MNR", "AM-MOD", "AM-NEG", "AM-PNC", "AM-PRD", "AM-REC", "AM-TMP");
    private static final List<String> allArgs = Arrays.asList("A0", "A1", "A2", "A3", "A4", "A5", "AA", "AM-ADV",
            "AM-CAU", "AM-DIR", "AM-DIS", "AM-EXT", "AM-LOC", "AM-MNR", "AM-MOD", "AM-NEG", "AM-PNC", "AM-PRD",
            "AM-REC", "AM-TMP");
    private static Map<String, Set<String>> legalArgs;

    private static final NomPredicateDetector predicateDetector = new NomPredicateDetector();
    private final static NomArgumentDetector argumentDetector = new NomArgumentDetector();

    private static boolean useGold = rm.getBoolean(ESRLConfigurator.SRL_USE_GOLD);
    private final boolean usePredictor;

    public NomSRLArgumentReader(String corpusName, boolean usePredictor) {
        super(corpusName, corpusName, ViewNames.SRL_NOM);
        this.usePredictor = usePredictor;
    }

    @Override
    public List<TextAnnotation> readData() {
        String[] sections;
        // Here, `file` is the corpus name
        switch (file) {
            case "train":
                sections = new String[]{"02", "03", "04", "05", "06", "07", "08", "09",
                        "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"};
                break;
            case "dev":
                sections = new String[]{"24"};
                break;
            case "test":
                sections = new String[]{"23"};
                break;
            default:
                throw new RuntimeException("Wrong dataset name. Choose one of train, dev, test");
        }
        NombankReader reader;
        try {
            reader = new NombankReader(treebankDir, nombankDir, sections, viewName, false);
        } catch (Exception e) {
            throw new RuntimeException("Reader exception: " + e);
        }
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        while (reader.hasNext()) textAnnotations.add(reader.next());

        return textAnnotations;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        List<Constituent> goldArgs = new ArrayList<>();
        PredicateArgumentView predArgView = (PredicateArgumentView) ta.getView(viewName);
        List<Constituent> predicates = predArgView.getPredicates();
        for (Constituent predicate : predicates) {
            goldArgs.addAll(getArguments(predicate, true));
        }
        if (useGold) return goldArgs;
        Set<Constituent> argCandidates = new HashSet<>();
        //if (usePredictor)
         //   predicates = predicateDetector.getPredicates(ta);
        //else predicates = predicateDetector.generateCandidates(ta);
        for (Constituent predCandidate : predicates) {
            List<Constituent> candidates;
            if (usePredictor)
                candidates = argumentDetector.getArguments(predCandidate);
            else candidates = argumentDetector.generateCandidates(predCandidate);
            Constituent matchingGoldPred = findConstituent(predArgView.getPredicates(), predCandidate);
            // If the predicate is not in the set of gold predicates all the arguments are going to be candidates
            if (matchingGoldPred == null) {
                argCandidates.addAll(candidates);
                continue;
            }
            goldArgs = getArguments(matchingGoldPred, true);
            // Keep the gold argument (and its labels) if it matches the candidate (by span)
            for (Constituent arg : candidates) {
                Constituent goldArg = findConstituent(goldArgs, arg);
                if (goldArg == null) argCandidates.add(arg);
                else argCandidates.add(goldArg);
            }
            // Now add any missing gold arguments
            for (Constituent arg : goldArgs) {
                if (findConstituent(candidates, arg) == null)
                    argCandidates.add(argumentDetector.getNewConstituent("missed", predCandidate,
                            arg.getStartSpan(), arg.getEndSpan()));
            }
        }
        return new ArrayList<>(argCandidates);
    }

    private Constituent findConstituent(List<Constituent> constituentList, Constituent constituent) {
        for (Constituent otherConstituent : constituentList)
            if (constituent.getSpan().equals(otherConstituent.getSpan()))
                return otherConstituent;
        return null;
    }

    private static List<Constituent> getArguments(Constituent predicate, boolean useGold) {
        if (!useGold)
            return new ArrayList<>(argumentDetector.getArguments(predicate));

        List<Constituent> args = new ArrayList<>();
        TextAnnotation ta = predicate.getTextAnnotation();
        List<Relation> arguments = ((PredicateArgumentView) ta.getView(ViewNames.SRL_NOM)).getArguments(predicate);
        for (Relation arg : arguments) {
            Constituent argument = arg.getTarget();
            if (argument.getIncomingRelations().size() > 1 || argument.getIncomingRelations().size() < 0)
                throw new RuntimeException("Argument has more than 1 incoming relations");
            args.add(argument);
        }
        return args;
    }

    public static List<Constituent> getArguments(Constituent predicate) {
        return getArguments(predicate, useGold);
    }

    /**
     * Generates all the predicate-argument relations, given a specific argument
     * for all possible predicates in the sentence (given by {@link NomPredicateDetector}).
     * The relations will be implicitly created as {@link Constituent#incomingRelations}.
     *
     * @param arg The argument mention
     * @return A list of all arguments participating in the predicate-argument relations
     */
    public static List<Constituent> getAllArgumentInstances(Constituent arg) {
        List<Constituent> args = new ArrayList<>();
        for (Constituent predCandidate : predicateDetector.getPredicates(arg.getTextAnnotation())) {
            for (Constituent candidateArg : argumentDetector.getArguments(predCandidate)) {
                if (contains(arg, candidateArg))
                    args.add(candidateArg);
            }
        }
        return args;
    }

    private static boolean contains(Constituent arg, Constituent container) {
        return container.getStartSpan() <= arg.getStartSpan() && container.getEndSpan() >= arg.getEndSpan();
    }

    public static List<String> getCoreArgRoles() {
        return coreArgs;
    }

    public static List<String> getAllArgRoles() {
        return allArgs;
    }

    public static Set<String> getLegalRoles(Constituent predicate) {
        if (legalArgs == null) legalArgs = readLegalArguments();
        return legalArgs.get(predicate.getAttribute(PredicateArgumentView.LemmaIdentifier));
    }

    private static Map<String, Set<String>> readLegalArguments() {
        Map<String, Set<String>> legalArgs = new HashMap<>();
        List<String> lines;
        try {
            lines = LineIO.readFromClasspath("Verb.legal.arguments");
        } catch (IOException e) {
            throw new RuntimeException("Cannot load lemma dictionary from classpath");
        }

        for (String line : lines) {
            if (line.length() == 0) continue;

            String[] parts = line.split("\t");

            String lemma = parts[0].trim();
            Set<String> set = new HashSet<>();

            if (parts.length == 2) {
                for (String argsForSense : parts[1].split("\\s+")) {
                    List<String> args = Arrays.asList(argsForSense.split("#")[1].split(","));
                    set.addAll(args);
                }
            }
            set.addAll(nonCoreArgs);
            legalArgs.put(lemma, set);
        }
        return legalArgs;
    }
}
