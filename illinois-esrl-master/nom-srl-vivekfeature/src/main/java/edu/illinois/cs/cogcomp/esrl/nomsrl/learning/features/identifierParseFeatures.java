package edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features;

import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

public class identifierParseFeatures extends Classifier {
    private static final SRLFeatures linearPosition = SRLFeatures.linearPosition;
    private static final SRLFeatures path = SRLFeatures.path;
    private static final SRLFeatures phraseType = SRLFeatures.phraseType;
    private static final SRLFeatures parseHeadWord = SRLFeatures.parseHeadWord;
    private static final SRLFeatures subcategorization = SRLFeatures.subcategorization;
    private static final SRLFeatures predSubcat = SRLFeatures.predSubcat;
    private static final SRLFeatures parseSiblings = SRLFeatures.parseSiblings;

    public identifierParseFeatures() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features";
        name = "identifierParseFeatures";
    }

    public FeatureVector classify(Object __example) {
        FeatureVector result = new FeatureVector();
        result.addFeatures(linearPosition.classify(__example));
        result.addFeatures(path.classify(__example));
        result.addFeatures(phraseType.classify(__example));
        result.addFeatures(parseHeadWord.classify(__example));
        result.addFeatures(subcategorization.classify(__example));
        result.addFeatures(predSubcat.classify(__example));
        return result;
    }
}

