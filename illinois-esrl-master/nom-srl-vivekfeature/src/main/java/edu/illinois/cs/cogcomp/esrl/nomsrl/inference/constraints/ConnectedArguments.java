package edu.illinois.cs.cogcomp.esrl.nomsrl.inference.constraints;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.nomsrl.NomSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.List;

public class ConnectedArguments extends ParameterizedConstraint {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelName = modelsDir + File.separator + NomSRLArgumentClassifier.CLASS_NAME;

    private static final NomSRLArgumentClassifier baseClassifier =
            new NomSRLArgumentClassifier(modelName + ".lc", modelName + ".lex");
    private final String type;

    public ConnectedArguments(String type) {
        super("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints.ConnectedArguments");
        this.type = type + "-";
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String discreteValue(Object example) {
        Constituent predicate = (Constituent) example;

        boolean LBJava$constraint$result$0 = true;

        for (String role : NomSRLArgumentReader.getAllArgRoles()) {
            boolean LBJava$constraint$result$1 = false;

            List<Constituent> arguments = NomSRLArgumentReader.getArguments(predicate);
            for (Constituent arg : arguments)
                LBJava$constraint$result$1 = baseClassifier.discreteValue(arg).equals(type + role);

            if (LBJava$constraint$result$1) {
                LBJava$constraint$result$0 = false;
                for (Constituent arg : arguments) {
                    LBJava$constraint$result$0 = baseClassifier.discreteValue(arg).equals(role);
                }
            } else LBJava$constraint$result$0 = true;
        }
        if (!LBJava$constraint$result$0) return "false";

        return "true";
    }

    public FirstOrderConstraint makeConstraint(Object example) {
        Constituent predicate = (Constituent) example;
        List<Constituent> arguments = NomSRLArgumentReader.getArguments(predicate);

        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = predicate;

        EqualityArgumentReplacer LBJ$EAR = new EqualityArgumentReplacer(LBJ$constraint$context) {
            public Object getLeftObject() {
                return quantificationVariables.get(1);
            }

            public String getRightValue() {
                String coreRole = (String) quantificationVariables.get(0);
                return type + coreRole;
            }
        };
        FirstOrderConstraint LBJava$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, LBJ$EAR);
        FirstOrderConstraint LBJava$constraint$result$2 = new ExistentialQuantifier("arg", arguments, LBJava$constraint$result$3);

        LBJ$EAR = new EqualityArgumentReplacer(LBJ$constraint$context) {
            public Object getLeftObject() {
                return quantificationVariables.get(1);
            }

            public String getRightValue() {
                return (String) quantificationVariables.get(0);
            }
        };

        FirstOrderConstraint LBJava$constraint$result$5 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), null, LBJ$EAR);
        FirstOrderConstraint LBJava$constraint$result$4 = new ExistentialQuantifier("arg", arguments, LBJava$constraint$result$5);
        FirstOrderConstraint LBJava$constraint$result$1 = new FirstOrderImplication(LBJava$constraint$result$2, LBJava$constraint$result$4);
        FirstOrderConstraint LBJava$constraint$result$0 = new UniversalQuantifier("role", NomSRLArgumentReader.getAllArgRoles(), LBJava$constraint$result$1);

        return new FirstOrderConjunction(new FirstOrderConstant(true), LBJava$constraint$result$0);
    }
}
