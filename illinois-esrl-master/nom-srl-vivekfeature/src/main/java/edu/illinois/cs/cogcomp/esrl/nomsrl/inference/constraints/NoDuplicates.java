package edu.illinois.cs.cogcomp.esrl.nomsrl.inference.constraints;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.nomsrl.NomSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.io.File;
import java.util.List;

public class NoDuplicates extends ParameterizedConstraint {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelName = modelsDir + File.separator + NomSRLArgumentClassifier.CLASS_NAME;

    private static final NomSRLArgumentClassifier baseClassifier =
            new NomSRLArgumentClassifier(modelName + ".lc", modelName + ".lex");

    public NoDuplicates() {
        super("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints.NoDuplicates");
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String discreteValue(Object example) {
        Constituent predicate = (Constituent) example;
        List<Constituent> arguments = NomSRLArgumentReader.getArguments(predicate);

        boolean LBJava$constraint$result$0;

        LBJava$constraint$result$0 = true;
        for (String coreRole : NomSRLArgumentReader.getCoreArgRoles()) {
            int LBJ$m$1 = 0;
            int LBJ$bound$1 = 1;
            for (Constituent arg : arguments) {
                boolean LBJava$constraint$result$1 = baseClassifier.discreteValue(arg).equals(coreRole);
                if (LBJava$constraint$result$1) ++LBJ$m$1;
            }
            LBJava$constraint$result$0 = LBJ$m$1 <= LBJ$bound$1;
        }

        if (!LBJava$constraint$result$0) return "false";

        return "true";
    }

    public int hashCode() {
        return "NoDuplicates".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof NoDuplicates;
    }

    public FirstOrderConstraint makeConstraint(Object example) {
        Constituent predicate = (Constituent) example;
        List<Constituent> arguments = NomSRLArgumentReader.getArguments(predicate);
        FirstOrderConstraint result = new FirstOrderConstant(true);

        for (String coreRole : NomSRLArgumentReader.getCoreArgRoles()) {
            Object[] LBJ$constraint$context = new Object[2];
            LBJ$constraint$context[0] = predicate;
            LBJ$constraint$context[1] = coreRole;

            EqualityArgumentReplacer LBJ$EAR = new EqualityArgumentReplacer(LBJ$constraint$context, true) {
                public Object getLeftObject() {
                    return quantificationVariables.get(0);
                }
            };

            FirstOrderConstraint LBJava$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), coreRole, LBJ$EAR);
            FirstOrderConstraint LBJava$constraint$result$0 = new AtMostQuantifier("arg", arguments, LBJava$constraint$result$2, 1);

            result = new FirstOrderConjunction(result, LBJava$constraint$result$0);
        }
        return result;
    }
}

