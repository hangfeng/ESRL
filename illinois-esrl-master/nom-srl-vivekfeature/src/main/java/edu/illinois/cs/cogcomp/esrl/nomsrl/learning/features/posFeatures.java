package edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features;

import edu.illinois.cs.cogcomp.esrl.core.features.POSBigrams;
import edu.illinois.cs.cogcomp.esrl.core.features.POSContextBigrams;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

public class posFeatures extends Classifier {
    private static final POSBigrams POSBigrams = new POSBigrams();
    private static final POSContextBigrams POSContextBigrams = new POSContextBigrams();

    public posFeatures() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.nomsrl.learning.features";
        name = "posFeatures";
    }

    public FeatureVector classify(Object example) {
        FeatureVector result = new FeatureVector();
        result.addFeatures(POSBigrams.classify(example));
        result.addFeatures(POSContextBigrams.classify(example));
        return result;
    }
}

