package edu.illinois.cs.cogcomp.esrl.nomsrl.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.nomsrl.inference.constraints.SRLInferenceConstraints;
import edu.illinois.cs.cogcomp.infer.ilp.OJalgoHook;
import edu.illinois.cs.cogcomp.lbjava.infer.ILPInference;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.learn.Normalizer;
import edu.illinois.cs.cogcomp.lbjava.learn.Softmax;


public class SRLInference extends ILPInference {
    public static Constituent findHead(Constituent c) {
        return c.getIncomingRelations().get(0).getSource();
    }

    public SRLInference(Constituent head) {
        super(head, new OJalgoHook());
        constraint = new SRLInferenceConstraints().makeConstraint(head);
    }

    public String getHeadType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String[] getHeadFinderTypes() {
        return new String[]{"edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent"};
    }

    public Normalizer getNormalizer(Learner c) {
        return new Softmax();
    }
}

