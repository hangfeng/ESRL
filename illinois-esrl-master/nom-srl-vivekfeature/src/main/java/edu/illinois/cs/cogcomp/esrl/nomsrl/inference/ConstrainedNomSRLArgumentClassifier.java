package edu.illinois.cs.cogcomp.esrl.nomsrl.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.nomsrl.learning.NomSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.DiscretePrimitiveStringFeature;
import edu.illinois.cs.cogcomp.lbjava.classify.Feature;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.lbjava.infer.InferenceManager;

import java.io.File;

public class ConstrainedNomSRLArgumentClassifier extends Classifier {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String modelName = modelsDir + File.separator + NomSRLArgumentClassifier.CLASS_NAME;

    private static final NomSRLArgumentClassifier baseClassifier =
            new NomSRLArgumentClassifier(modelName + ".lc", modelName + ".lex");

    public ConstrainedNomSRLArgumentClassifier() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.nomsrl.inference";
        name = "ConstrainedNomSRLArgumentClassifier";
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String getOutputType() {
        return "discrete";
    }


    public FeatureVector classify(Object example) {
        return new FeatureVector(featureValue(example));
    }

    public Feature featureValue(Object example) {
        String result = discreteValue(example);
        return new DiscretePrimitiveStringFeature(containingPackage, name, "", result, valueIndexOf(result),
                (short) allowableValues().length);
    }

    public String discreteValue(Object example) {
        Constituent head = SRLInference.findHead((Constituent) example);
        SRLInference inference = (SRLInference)
                InferenceManager.get("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.SRLInference", head);

        if (inference == null) {
            inference = new SRLInference(head);
            InferenceManager.put(inference);
        }

        String result = null;

        try {
            result = inference.valueOf(baseClassifier, example);
        } catch (Exception e) {
            System.err.println("LBJava ERROR while evaluating classifier ConstrainedNomSRLArgumentClassifier: " + e);
            e.printStackTrace();
            System.exit(1);
        }

        return result;
    }

    public FeatureVector[] classify(Object[] examples) {
        return super.classify(examples);
    }

    public int hashCode() {
        return "ConstrainedNomSRLArgumentClassifier".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof ConstrainedNomSRLArgumentClassifier;
    }
}

