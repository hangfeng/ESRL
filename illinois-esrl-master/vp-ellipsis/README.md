# VP-Ellipsis

## The Task
The proposed shared task consists of two subtasks: 

1. automatically detecting VPE in free text
2. selecting the textual antecedent of each found VPE. 

Task 1 is reasonably difficult (Nielsen 2004 reports an F-score of 71% on Wall Street Journal data).

Task 2 is challenging. With a "head match" evaluation Hardt 1997 reports a success rate of 62% for a 
baseline system based on recency only, and an accuracy of 84% for an improved system taking recency, 
clausal relations, parallelism, and quotation into account. We will make the task more realistic 
(but more difficult) by not using head match but rather precision and recall over each token of the antecedent.

We will provide texts where sentence boundaries are detected and each sentence is tokenised and 
printed on a new line. An occurrence of VPE is marked by a line number plus token positions of the 
auxiliary or modal verb. Textual antecedents are assumed to be on one line, and are marked by 
the line number plus begin/end token position.
   
## Dataset
The dataset is [VPE Version 0.9](http://semeval2.fbk.eu/get.php?id=0&time=2015-11-02%2019:40:46) (July 2009), 
created by Johan Bos and Jennifer Spenader

This is version 0.9 of the verb phrase ellipsis annotation for the
Wall Street Journal (WSJ) sections of the Penn Treebank (PTB version
3.0). The stand-off annotations are provided in 25 files named after
the section numbers of the WSJ. Each occurrence of VPE found in the
corpus is listed on a separate line. Information for a VPE is
organised in columns, which are indicated by spaces, organised by the
following schema:

<pre>
&lt;FILENAME&gt; &lt;EL&gt; &lt;EB&gt; &lt;EE&gt; &lt;AL&gt; &lt;AB&gt; &lt;AE&gt; &lt;TR&gt; &lt;STA&gt; &lt;STP&gt; ....
</pre>

where &lt;FILENAME&gt; is the raw filename as distributed with the PTB, &lt;EL&gt;
is the line number of the elliptical expression, and &lt;EB&gt; and &lt;EE&gt; are
character positions marking the begin and end of the ellipsis; &lt;AL&gt; is
the line number of the antecedent expression, and &lt;AB&gt; and &lt;AE&gt; the
character positions marking the begin and end of the antecedent;
finally &lt;TR&gt; is the trigger type, &lt;STA&gt; the syntactic type of the
antecedent, and &lt;STP&gt; source-target pattern of the ellipsis.

**NB:** You need to provide the code with the location of the WSJ in your system. 
See `edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator.WSJ_RAW_DIR`