package edu.illinois.cs.cogcomp.esrl.vpellipsis;

import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.vpellipsis.lbjava.AntecedentClassifier;
import edu.illinois.cs.cogcomp.esrl.vpellipsis.lbjava.AntecedentLabel;
import edu.illinois.cs.cogcomp.esrl.vpellipsis.lbjava.EllipsisLabel;
import edu.illinois.cs.cogcomp.esrl.vpellipsis.lbjava.PredicateClassifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;

import java.io.File;

public class Main {

    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.VP_ELLIPSIS_DATA_DIR);
    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String predicatemodelName = modelsDir + File.separator + PredicateClassifier.CLASS_NAME;
    private String antecedentmodelName = modelsDir + File.separator + AntecedentClassifier.CLASS_NAME;

    public void train() {
        Parser trainDataReader = new PredicateReader("02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22", "train");
        Learner classifier = new PredicateClassifier(predicatemodelName + ".lc", predicatemodelName + ".lex");
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader, 1000);
        trainer.train(5);
        classifier.save();
        trainDataReader.close();

        trainDataReader = new ArgumentReader("02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22", "train");
        classifier = new AntecedentClassifier(antecedentmodelName + ".lc", antecedentmodelName + ".lex");
        trainer = new BatchTrainer(classifier, trainDataReader, 1000);
        trainer.train(5);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        Parser testDataReader = new PredicateReader("00,01", "dev");
        Learner classifier = new PredicateClassifier(predicatemodelName + ".lc", predicatemodelName + ".lex");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, classifier, new EllipsisLabel(), testDataReader, true, 100);
        testDataReader.close();


        testDataReader = new ArgumentReader("00,01", "dev");
        classifier = new AntecedentClassifier(antecedentmodelName + ".lc", antecedentmodelName + ".lex");
        tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, classifier, new AntecedentLabel(), testDataReader, true, 100);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}
