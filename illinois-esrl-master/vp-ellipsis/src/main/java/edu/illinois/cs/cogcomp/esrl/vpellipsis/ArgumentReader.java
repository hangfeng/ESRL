package edu.illinois.cs.cogcomp.esrl.vpellipsis;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;

import java.util.ArrayList;
import java.util.List;

/**
 * A data reader for the SemEval-2010 task 4 (VP Ellipsis) corpus.
 * <br/>
 * <b>NB:</b> The dataset is <b>NOT</b> tokenized!
 */
public class ArgumentReader extends VPEllipsisDataReader {
    public ArgumentReader(String folders, String corpusName) {
        super(folders, corpusName);
    }


    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        //TODO This should be replaced with a non-gold candidate generator
        List<Constituent> candidates = new ArrayList<>();
        PredicateArgumentView view = (PredicateArgumentView) ta.getView(viewName);
        for (Relation r : view.getRelations())
            candidates.add(r.getTarget());

        return candidates;
    }
}
