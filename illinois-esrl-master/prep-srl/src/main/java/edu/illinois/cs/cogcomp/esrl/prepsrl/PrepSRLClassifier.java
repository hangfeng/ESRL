package edu.illinois.cs.cogcomp.esrl.prepsrl;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.core.AbstractClassifier;
import edu.illinois.cs.cogcomp.esrl.core.features.POSBigrams;
import edu.illinois.cs.cogcomp.esrl.core.features.POSContextBigrams;
import edu.illinois.cs.cogcomp.esrl.core.features.WordBigrams;
import edu.illinois.cs.cogcomp.esrl.core.features.WordContextBigrams;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;

import static edu.illinois.cs.cogcomp.esrl.core.features.PrepSRLFeatures.*;

public class PrepSRLClassifier extends AbstractClassifier {
    private static final String PACKAGE_NAME = "edu.illinois.cs.cogcomp.esrl.prepsrl";
    public static final String CLASS_NAME = "PrepSRLClassifier";

    public PrepSRLClassifier(String modelPath, String lexiconPath) {
        super(PACKAGE_NAME + "." + CLASS_NAME, modelPath, lexiconPath);
    }

    @Override
    protected void initialize() {
        containingPackage = PACKAGE_NAME;
        name = CLASS_NAME;
        setLabeler(new Label());
        setExtractor(new FeatureExtractor());
    }

    public static class FeatureExtractor extends AbstractClassifier.FeatureExtractor {
        public FeatureExtractor() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$FeatureExtractor";
            featureSet = new Classifier[]{new WordContextBigrams(), new POSContextBigrams(),
                    new WordBigrams(), new POSBigrams(),
                    govFeatures, objFeatures, prevWordFeatures, prevVerbFeatures};
        }
    }

    public static class Label extends AbstractClassifier.Label {
        public Label() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$Label";
        }

        public String discreteValue(Object example) {
            return ((Constituent) example).getLabel();
        }
    }
}
