package edu.illinois.cs.cogcomp.esrl.prepsrl;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.transformers.ITransformer;
import edu.illinois.cs.cogcomp.core.utilities.XMLUtils;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.util.*;

/**
 * Data reader for the Semeval dataset. Creates the {@link ViewNames#SRL_PREP} view using the preposition roles
 * instead of the original senses (uses the {@code sense2role.csv} map).
 *
 * @author Vivek Srikumar
 * @author Christos Christodoulopoulos
 */
public class PrepSRLDataReader extends DataReader {
	private static Logger log = LoggerFactory.getLogger(PrepSRLDataReader.class);

	private static final String semevalTrainDataDirectory = "train/xml";
	private static final String semevalTestDataDirectory = "test/xml";
	private static final String semevalKeyDirectory = "Answers";

	private int currentNodeId;
	private Hashtable<String, String> keys;

	private static Map<String, String> senseToRole;
	private static Map<String, Set<String>> prepositionToValidRoles;

	public PrepSRLDataReader(String dataDir, String corpusName) {
		super(dataDir, corpusName, ViewNames.SRL_PREP);
	}

	@Override
	public List<TextAnnotation> readData() {
		lazyReadMaps();

		List<TextAnnotation> textAnnotations = new ArrayList<>();
		String dataDir = file + File.separator;
		dataDir += (corpusName.equals("train") ? semevalTrainDataDirectory : semevalTestDataDirectory);
		for (String currentFile : getFiles(dataDir)) {
			NodeList instanceNodeList;
			try {
				// read the xml
				Document dom = XMLUtils.getXMLDOM(dataDir + File.separator + currentFile + ".xml");
				Element docElem = dom.getDocumentElement();
				instanceNodeList = docElem.getElementsByTagName("instance");
			} catch (Exception ex) {
				System.err.println("Unable to get the DOM" + ex);
				return null;
			}

			// read the key file
			if (corpusName.equals("test")) {
				String keyFileName;

				int start = currentFile.indexOf('-') + 1;
				int end = currentFile.indexOf('.');
				keyFileName = currentFile.substring(start, end);

				try {
					keys = new Hashtable<>();
					LineIO.read(file + File.separator + semevalKeyDirectory + File.separator + keyFileName + ".key",
							new ITransformer<String, Void>() {

								public Void transform(String input) {
									String[] parts = input.split(" ");
									keys.put(parts[1], parts[2]);
									return null;
								}
							});
				}
				catch (FileNotFoundException e) {
					System.err.println("File " + semevalKeyDirectory
							+ File.separator + keyFileName + ".key not found" + e);
					return null;
				}
			}
			while (currentNodeId < instanceNodeList.getLength()) {
				TextAnnotation ta = makeNewTextAnnotation((Element) instanceNodeList.item(currentNodeId));
				if (ta == null) {
					logger.error("{} returned null.", instanceNodeList.item(currentNodeId));
					currentNodeId++;
					continue;
				}
				textAnnotations.add(ta);
				currentNodeId++;
			}
		}
		return consolidate(textAnnotations);
	}

	/**
	 * Consolidate {@link TextAnnotation}s that have the same text but separate gold views. This is
	 * required because of the nature of the Semeval annotations (one annotation per example).
	 *
	 * @param tas The list of {@link TextAnnotation}s with the Semeval annotations
	 * @return The consolidated list of {@link TextAnnotation}s
	 */
	private List<TextAnnotation> consolidate(List<TextAnnotation> tas) {
		List<TextAnnotation> consolidatedTAs = new ArrayList<>();
		Map<Integer, List<TextAnnotation>> taMap = new HashMap<>();
		for (TextAnnotation ta : tas) {
			int key = ta.getText().hashCode();
			List<TextAnnotation> annotations = taMap.getOrDefault(key, new ArrayList<>());
			annotations.add(ta);
			taMap.put(key, annotations);
		}
		for (int key : taMap.keySet()) {
			List<TextAnnotation> annotations = taMap.get(key);
			TextAnnotation ta1 = annotations.get(0);
			TokenLabelView view1 = ((TokenLabelView) ta1.getView(viewName));
			for (int i = 1; i < annotations.size(); i++) {
				TextAnnotation taI = annotations.get(i);
				View viewI = taI.getView(viewName);
				for (Constituent c : viewI.getConstituents())
					try {
						view1.addTokenLabel(c.getStartSpan(), c.getLabel(), 1.0);
					}
					catch (IllegalArgumentException e) {
						log.info("Example {} has been annotated twice.", c);
					}
			}
			consolidatedTAs.add(ta1);
		}
		return consolidatedTAs;
	}

	private static void lazyReadMaps() {
		//Read the sense2role conversion
		senseToRole = new HashMap<>();
		prepositionToValidRoles = new HashMap<>();
		boolean firstLine = true;
		try {
			for (String line : LineIO.readFromClasspath("sense2role.csv")) {
				if (firstLine) {
					firstLine = false;
					continue;
				}

				line = line.trim();
				if (line.length() == 0)
					continue;

				if (line.matches("^,+$"))
					continue;
				String[] parts = line.split(",");

				String preposition = parts[0].trim();
				String sense = preposition + ":" + parts[1].trim();
				String role = parts[3].trim();

				if (preposition.length() == 0)
					continue;

				senseToRole.put(sense, role);

				if (!prepositionToValidRoles.containsKey(preposition))
					prepositionToValidRoles.put(preposition, new HashSet<>());
				prepositionToValidRoles.get(preposition).add(role);
			}
		}
		catch (Exception ex) {
			System.err.println("Error reading the sense2role file" + ex);
		}
	}

	@Override
	public List<Constituent> candidateGenerator(TextAnnotation ta) {
		List<Constituent> candidates = new ArrayList<>();
		// The Semeval data is annotated with only one preposition per example sentence.
		// Thus, every other preposition in the same sentence will not have a gold annotation,
		// and will be mistakenly considered as a CANDIDATE. To fix this, only add prepositions
		// from the gold annotations.
		for (Constituent c : ta.getView(viewName)) {
			Constituent depConstituent = ta.getView(ViewNames.DEPENDENCY).getConstituentsCovering(c).get(0);
			new Relation("Governor", depConstituent.getIncomingRelations().get(0).getSource(), c, 1.0);
			if (depConstituent.getOutgoingRelations().size() > 0)
				new Relation("Object", c, depConstituent.getOutgoingRelations().get(0).getTarget(), 1.0);
		}
		return getFinalCandidates(ta.getView(viewName), candidates);
	}

	public static Set<String> getLegalRoles(Constituent predicate) {
		if (prepositionToValidRoles == null) lazyReadMaps();
		Set<String> strings = new HashSet<>();
		strings.add(DataReader.CANDIDATE);
		if (prepositionToValidRoles.containsKey(predicate.getSurfaceForm().toLowerCase()))
			strings.addAll(prepositionToValidRoles.get(predicate.getSurfaceForm().toLowerCase()));
		return strings;
	}

	private TextAnnotation makeNewTextAnnotation(Element item) {
		String id = item.getAttribute("id");

		NodeList nl = item.getElementsByTagName("context");
		NodeList children = nl.item(0).getChildNodes();

		String rawSentenceString = nl.item(0).getTextContent().replaceAll("[\\t\\n]", "").trim();

		String preposition = "";
		int prepositionPosition = -1;

		for (int i = 0; i < children.getLength(); i++) {
			Node currentNode = children.item(i);

			if (currentNode.getNodeName().equals("head")) {
				preposition = currentNode.getTextContent().toLowerCase();
				int previousLength = 0;
				if (i > 0) previousLength = tokenize(children.item(i - 1).getTextContent()).size();
				prepositionPosition = previousLength;
			}
		}
		String label;
		if (corpusName.equals("test")) {
			if (keys.containsKey(id))
				label = keys.get(id);
			else
				return null;
		}
		else {
			label = ((Element) (item.getElementsByTagName("answer").item(0))).getAttribute("senseid");
		}

		// Take only the first label for the 500 or so instances which are given multiple labels.
		if (label.contains(" "))
			label = label.substring(0, label.indexOf(" ")).trim();

		if (label.length() == 0) {
			log.info("No label for id {}, ignoring sentence", id);
			return null;
		}

		rawSentenceString = rawSentenceString.replaceAll("`", "``");
		rawSentenceString = rawSentenceString.replaceAll("\"", "''");
		//XXX Assume text is pre-tokenized
		String[] tokens = rawSentenceString.split("\\s+");

		TextAnnotation ta = BasicTextAnnotationBuilder
				.createTextAnnotationFromTokens("Semeval2007Prepositions", id, Collections.singletonList(tokens));

		if (!ta.getTokens()[prepositionPosition].toLowerCase().equals(preposition)) {
			assert false;
		}

		TokenLabelView prepositionLabelView = new TokenLabelView(viewName, ta);
		String role = senseToRole.get(preposition + ":" + label);
		prepositionLabelView.addTokenLabel(prepositionPosition, role, 1.0);

		ta.addView(viewName, prepositionLabelView);

		return ta;
	}

	private static List<String> tokenize(String line) {
		StringTokenizer tokenizer = new StringTokenizer(line);

		List<String> tokens = new ArrayList<>();
		while (tokenizer.hasMoreTokens())
			tokens.add(tokenizer.nextToken());

		return tokens;
	}

	private List<String> getFiles(String dataDir) {
		List<String> files = new ArrayList<>();
		FilenameFilter xmlFilter = (dir, name) -> (name.startsWith("pp-") && name.endsWith(".xml") && IOUtils
                .isFile(dir.getAbsolutePath() + File.separator + name));

		String[] xmlFiles = (new File(dataDir)).list(xmlFilter);

		assert xmlFiles != null;
		for (String fileName : xmlFiles) {
			String rawFileName = IOUtils.stripFileExtension(fileName);
			files.add(rawFileName);
		}
		return files;
	}
}
