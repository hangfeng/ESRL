package edu.illinois.cs.cogcomp.esrl.prepsrl.inference;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.prepsrl.inference.constraints.PrepSRLInferenceConstraints;
import edu.illinois.cs.cogcomp.infer.ilp.OJalgoHook;
import edu.illinois.cs.cogcomp.lbjava.infer.ILPInference;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.learn.Normalizer;
import edu.illinois.cs.cogcomp.lbjava.learn.Softmax;

public class PrepSRLInference extends ILPInference {
    public PrepSRLInference() {}

    public PrepSRLInference(Constituent head) {
        super(head, new OJalgoHook());
        constraint = new PrepSRLInferenceConstraints().makeConstraint(head);
    }

    public static Constituent findHead(Constituent c) {
        return c;
    }

    public String getHeadType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String[] getHeadFinderTypes() {
        return new String[]{"edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent"};
    }

    public Normalizer getNormalizer(Learner c) {
        return new Softmax();
    }
}
