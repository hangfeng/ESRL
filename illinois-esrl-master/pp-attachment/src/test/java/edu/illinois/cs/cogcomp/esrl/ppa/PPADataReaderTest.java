package edu.illinois.cs.cogcomp.esrl.ppa;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import junit.framework.TestCase;

public class PPADataReaderTest extends TestCase {
    private String dataDir;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.PPA_DATA_DIR);
    }

    public void testReader() {
        PPADataReader reader = new PPADataReader(dataDir + "/test-PTB", "test");
        reader.next();
        Constituent constituent = (Constituent) reader.next();
        assertEquals("on its two classes of preferred stock", constituent.getSurfaceForm());
        assertEquals("N", constituent.getLabel());
    }
}