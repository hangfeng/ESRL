package edu.illinois.cs.cogcomp.esrl.ppa;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.ppa.lbjava.PPAClassifier;
import edu.illinois.cs.cogcomp.esrl.ppa.lbjava.PPALabel;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;

import java.io.File;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.PPA_DATA_DIR);
    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String modelName = modelsDir + File.separator + new PPAClassifier();

    public void train() {
        Learner classifier = new PPAClassifier(modelName + ".lc", modelName + ".lex");
        Parser trainDataReader = new PPADataReader(dataDir + "/train-PTB", "train");
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader, 100000);
        trainer.train(5);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        Learner classifier = new PPAClassifier(modelName + ".lc", modelName + ".lex");
        Parser testDataReader = new PPADataReader(dataDir + "/test-PTB", "test");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, classifier, new PPALabel(), testDataReader, true, 10000);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}
