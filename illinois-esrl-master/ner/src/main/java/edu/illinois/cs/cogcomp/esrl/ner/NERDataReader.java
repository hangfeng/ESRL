package edu.illinois.cs.cogcomp.esrl.ner;

import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NERDataReader extends DataReader {

    public NERDataReader(String folder, String corpusName, String viewName) {
        super(folder, corpusName, viewName);
    }

    @Override
    public List<TextAnnotation> readData() {
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        String dataFolder = file;
        String[] files;
        try {
            files = IOUtils.lsFiles(dataFolder);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Couldn't read the contents of " + dataFolder);
        }
        for (String dataFile : files) {
            List<String> lines;
            try {
                lines = LineIO.read(dataFile);
                lines = lines.subList(2, lines.size()-1);
            } catch (FileNotFoundException e) {
                throw new RuntimeException("Couldn't read " + dataFile);
            }
            String corpusId = IOUtils.getFileName(dataFile);
            List<String> labels = new ArrayList<>();
            List<String> pos = new ArrayList<>();
            List<String> tokens = new ArrayList<>();
            int taId = 0;
            for (String line : lines) {
                if (line.isEmpty()) {
                    List<String[]> tokenizedSentence = Collections.singletonList(tokens.toArray(new String[tokens.size()]));
                    TextAnnotation ta = BasicTextAnnotationBuilder.createTextAnnotationFromTokens(
                            corpusId, String.valueOf(taId), tokenizedSentence);

                    if (isAllPunct(tokens)) {
                        logger.info("Skipping empty sentence {} ("+corpusId+":sent-{}).", ta.getText().trim(), ta.getId());
                        continue;
                    }
                    if (rm.getBoolean(ESRLConfigurator.USE_GOLD_POS))
                        addGoldPOSView(ta, pos);
                    addView(ta, labels);
                    textAnnotations.add(ta);
                    labels.clear();
                    tokens.clear();
                    pos.clear();
                    taId++;
                }
                else {
                    labels.add(line.split("\\s+")[0]);
                    pos.add(line.split("\\s+")[4]);
                    tokens.add(line.split("\\s+")[5]);
                }
            }
        }

        return textAnnotations;
    }

    private boolean isAllPunct(List<String> tokens) {
        boolean allPunct = true;
        for (String token : tokens){
            allPunct &= token.matches("\\p{Punct}");
        }
        return allPunct;
    }

    private void addView(TextAnnotation ta, List<String> labels) {
        TokenLabelView labelView = new TokenLabelView(viewName, ta);
        List constituents = ta.getView(ViewNames.TOKENS).getConstituents();

        assert constituents.size() == labels.size();

        for (int i = 0; i < constituents.size(); ++i) {
            Constituent constituent = (Constituent) constituents.get(i);
            labelView.addTokenLabel(constituent.getStartSpan(), labels.get(i), 1.0D);
        }
        ta.addView(viewName, labelView);
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        return getFinalCandidates(ta.getView(viewName), ta.getView(ViewNames.TOKENS).getConstituents());
    }
}
