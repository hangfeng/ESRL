package edu.illinois.cs.cogcomp.esrl.ner;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NERDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.NER_CONLL_DATA_DIR);
    }

    @Test
    public void testReader() {
        NERDataReader reader = new NERDataReader(dataDir + "/Dev", "dev", ViewNames.NER_CONLL);
        Constituent constituent = (Constituent) reader.next();
        assertEquals("SOCCER", constituent.getSurfaceForm());
        assertEquals(DataReader.CANDIDATE, constituent.getLabel());
        reader.next();reader.next();
        reader.next();reader.next();
        reader.next();reader.next();
        constituent = (Constituent) reader.next();
        assertEquals("English", constituent.getSurfaceForm());
        assertEquals("MISC", constituent.getLabel());
    }
}