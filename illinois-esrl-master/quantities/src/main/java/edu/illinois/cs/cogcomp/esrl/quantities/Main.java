package edu.illinois.cs.cogcomp.esrl.quantities;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;

import java.io.File;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.QUANTITIES_DATA_DIR);

    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String modelName = modelsDir + File.separator + QuantitiesClassifier.CLASS_NAME;

    public void train() {
        QuantitiesClassifier classifier = new QuantitiesClassifier(modelName + ".lc", modelName + ".lex");
        QuantitiesDataReader trainDataReader = new QuantitiesDataReader(dataDir + "/trainingData.txt", "train");
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader);
        trainer.train(45);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        QuantitiesClassifier classifier = new QuantitiesClassifier(modelName + ".lc", modelName + ".lex");
        QuantitiesDataReader testDataReader = new QuantitiesDataReader(dataDir + "/testData.txt", "test");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull("O");
        TestDiscrete.testDiscrete(tester, classifier, new QuantitiesClassifier.Label(), testDataReader, true, 1000);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}
