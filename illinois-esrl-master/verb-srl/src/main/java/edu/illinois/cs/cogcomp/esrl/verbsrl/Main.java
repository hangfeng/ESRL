package edu.illinois.cs.cogcomp.esrl.verbsrl;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.AbstractClassifier;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.verbsrl.inference.ConstrainedVerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentIdentifier;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.learn.Lexicon;
import edu.illinois.cs.cogcomp.nlp.corpusreaders.ontonotes.OntonotesPropbankReader;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;

public class Main {
    private static ResourceManager rm = ESRLConfigurator.defaults();
    private static String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private static String identifierModelName = modelsDir + File.separator + VerbSRLArgumentIdentifier.CLASS_NAME;
    private static String classifierModelName = modelsDir + File.separator + VerbSRLArgumentClassifier.CLASS_NAME;

    private final VerbSRLArgumentIdentifier identifier = new VerbSRLArgumentIdentifier(identifierModelName + ".lc", identifierModelName + ".lex");
    private final VerbSRLArgumentClassifier classifier = new VerbSRLArgumentClassifier(classifierModelName + ".lc", classifierModelName + ".lex");
    private boolean useArgPredictor = !rm.getBoolean(ESRLConfigurator.SRL_USE_GOLD);

    private void train() throws Exception{
        VerbSRLArgumentReader trainReader = new VerbSRLArgumentReader("dev", false);
        BatchTrainer trainer = new BatchTrainer(identifier, trainReader, 1);
        trainer.train(5);
        identifier.save();
        trainReader.close();

        trainReader = new VerbSRLArgumentReader("train", useArgPredictor);
        trainer = new BatchTrainer(classifier, trainReader, 10000);
        trainer.train(15);
        classifier.save();
        trainReader.close();
    }

    private void train_ontonotes() throws Exception {

        //VerbSRLArgumentIdentifier identifier = new VerbSRLArgumentIdentifier(identifierModelName + "_ontonotes.lc", identifierModelName + "_ontonotes.lex");
        VerbOntonotesParser trainIdentifyReader = new VerbOntonotesParser("dev", false);
        Object example = trainIdentifyReader.next();
        BatchTrainer trainer = new BatchTrainer(identifier, trainIdentifyReader, 10000);
        trainer.train(5);
        identifier.save();
        trainIdentifyReader.close();
    }

    public void evaluateOntonotesIdentification() throws Exception{
        ConstrainedVerbSRLArgumentClassifier constClassifier = new ConstrainedVerbSRLArgumentClassifier();
        AbstractClassifier.Label oracle = new VerbSRLArgumentClassifier.Label();
        String resultsDir = rm.getString(ESRLConfigurator.RESULTS_DIR);
        String resultsPath = resultsDir + File.separator + "VerbSRL_results_identify_ontonotes.txt";
        File resultsFile = new File(resultsPath);
        PrintStream out = new PrintStream(resultsFile);

        TestDiscrete tester = new TestDiscrete();
        String ontonotesDir = rm.getString(ESRLConfigurator.ONTONOTES_DIR);
        String dir = ontonotesDir + File.separator + "Verb/test";
        VerbOntonotesParser testDataReader = new VerbOntonotesParser(false,"test" ,useArgPredictor);

        boolean preExtraction = false;
        Object example;
        Lexicon labelLexicon = null;
        String prediction;
        String gold;
        example = testDataReader.next();
        if (example instanceof Object[] && ((Object[]) ((Object[]) example))[0] instanceof int[]) {
            preExtraction = true;
            labelLexicon = ((Learner) ((Classifier) constClassifier)).getLabelLexicon();
            gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
        } else {
            gold = oracle.discreteValue(example);
        }
        if (gold.contains("missed")) {
            prediction = "missed";
            gold = "candidate";
        }
        else if(gold == "candidate"){
            gold = "missed";
            prediction = "candidate";
        }
        else {
            gold = "candidate";
            prediction = gold;
        }

        tester.reportPrediction(prediction, gold);


        while ((example = testDataReader.next()) != null) {
            if (preExtraction) {
                gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
            } else {
                gold = oracle.discreteValue(example);
            }

            if (gold.contains("missed")) {
                prediction = "missed";
                gold = "candidate";
            }
            else if(gold == "candidate"){
                gold = "missed";
                prediction = "candidate";
            }
            else {
                gold = "candidate";
                prediction = gold;
            }
            tester.reportPrediction(prediction, gold);
        }

        tester.printPerformance(out);

        out.close();
        testDataReader.close();

    }



    private void test() throws Exception{

        TestDiscrete tester = new TestDiscrete();
        tester.addNull("missed");
        tester.addNull("candidate");
        ConstrainedVerbSRLArgumentClassifier constClassifier = new ConstrainedVerbSRLArgumentClassifier();
        VerbSRLArgumentReader testDataReader = new VerbSRLArgumentReader(false,"test", useArgPredictor);


        String resultsDir = rm.getString(ESRLConfigurator.RESULTS_DIR);
        String resultsPath = resultsDir + File.separator + "VerbSRL_results.txt";
        File resultsFile = new File(resultsPath);
        resultsFile.createNewFile();
        PrintStream out = new PrintStream(resultsFile);

        AbstractClassifier.Label oracle = new VerbSRLArgumentClassifier.Label();

        Lexicon labelLexicon = null;
        boolean preExtraction = false;
        Object example;
        String prediction;
        String gold;
        example = testDataReader.next();
        if (example instanceof Object[] && ((Object[])((Object[])example))[0] instanceof int[]) {
            preExtraction = true;
            labelLexicon = ((Learner)((Classifier)constClassifier)).getLabelLexicon();
            gold = labelLexicon.lookupKey(((int[])((int[])((Object[])((Object[])example))[2]))[0]).getStringValue();
        } else {
            gold = oracle.discreteValue(example);
        }
        //missed => not identified
        if(gold.contains("missed"))
        {
            prediction = "missed";
            gold = gold.substring(6);
        }
        else {
            prediction = constClassifier.discreteValue(example);
        }
        tester.reportPrediction(prediction, gold);


        while((example = testDataReader.next()) != null)
        {

            if (preExtraction) {
                gold = labelLexicon.lookupKey(((int[])((int[])((Object[])((Object[])example))[2]))[0]).getStringValue();
            } else {
                gold = oracle.discreteValue(example);
            }

            if(gold.contains("missed"))
            {
                prediction = "missed";
                gold = gold.substring(6);
            }
            else {
                prediction = constClassifier.discreteValue(example);
                assert prediction != null : "Classifier returned null prediction for example " + example;
            }
            tester.reportPrediction(prediction, gold);

        }

        tester.printPerformance(out);

        out.close();
        testDataReader.close();
    }

    public void evaluateIdentification() throws Exception{
        ConstrainedVerbSRLArgumentClassifier constClassifier = new ConstrainedVerbSRLArgumentClassifier();
        AbstractClassifier.Label oracle = new VerbSRLArgumentClassifier.Label();
        String resultsDir = rm.getString(ESRLConfigurator.RESULTS_DIR);
        String resultsPath = resultsDir + File.separator + "VerbSRL_results_identify.txt";
        File resultsFile = new File(resultsPath);
        PrintStream out = new PrintStream(resultsFile);

        TestDiscrete tester = new TestDiscrete();
        VerbSRLArgumentReader testDataReader = new VerbSRLArgumentReader(false,"test", useArgPredictor);

        boolean preExtraction = false;
        Object example;
        Lexicon labelLexicon = null;
        String prediction;
        String gold;
        example = testDataReader.next();
        if (example instanceof Object[] && ((Object[]) ((Object[]) example))[0] instanceof int[]) {
            preExtraction = true;
            labelLexicon = ((Learner) ((Classifier) constClassifier)).getLabelLexicon();
            gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
        } else {
            gold = oracle.discreteValue(example);
        }
        if (gold.contains("missed")) {
            prediction = "missed";
            gold = "candidate";
        }
        else if(gold == "candidate"){
            gold = "missed";
            prediction = "candidate";
        }
        else {
            gold = "candidate";
            prediction = gold;
        }

        tester.reportPrediction(prediction, gold);


        while ((example = testDataReader.next()) != null) {
            if (preExtraction) {
                gold = labelLexicon.lookupKey(((int[]) ((int[]) ((Object[]) ((Object[]) example))[2]))[0]).getStringValue();
            } else {
                gold = oracle.discreteValue(example);
            }

            if (gold.contains("missed")) {
                prediction = "missed";
                gold = "candidate";
            }
            else if(gold == "candidate"){
                gold = "missed";
                prediction = "candidate";
            }
            else {
                gold = "candidate";
                prediction = gold;
            }
            tester.reportPrediction(prediction, gold);
        }

        tester.printPerformance(out);

        out.close();
        testDataReader.close();

    }


    public static void main(String[] args) throws Exception {
        Main main = new Main();
        //main.train();
        //main.test();
        //main.evaluateIdentification();
        main.train_ontonotes();
        main.evaluateOntonotesIdentification();
    }
}
