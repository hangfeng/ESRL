package edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features;

import edu.illinois.cs.cogcomp.esrl.core.features.ChunkContextBigrams;
import edu.illinois.cs.cogcomp.esrl.core.features.ChunkEmbeddings;
import edu.illinois.cs.cogcomp.esrl.core.features.SRLFeatures;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;

public class shallowParseFeatures extends Classifier {
    private static final ChunkEmbeddings ChunkEmbeddings = new ChunkEmbeddings();
    private static final ChunkContextBigrams ChunkContextBigrams = new ChunkContextBigrams();
    private static final SRLFeatures chunkPathPattern = SRLFeatures.chunkPathPattern;
    private static final SRLFeatures containsNEG = SRLFeatures.containsNEG;
    private static final SRLFeatures containsMOD = SRLFeatures.containsMOD;

    public shallowParseFeatures() {
        containingPackage = "edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features";
        name = "shallowParseFeatures";
    }

    public FeatureVector classify(Object example) {
        FeatureVector result = new FeatureVector();
        result.addFeatures(ChunkEmbeddings.classify(example));
        result.addFeatures(ChunkContextBigrams.classify(example));
        result.addFeatures(chunkPathPattern.classify(example));
        result.addFeatures(containsNEG.classify(example));
        result.addFeatures(containsMOD.classify(example));
        return result;
    }
}