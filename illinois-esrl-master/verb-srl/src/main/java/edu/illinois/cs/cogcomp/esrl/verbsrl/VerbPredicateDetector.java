package edu.illinois.cs.cogcomp.esrl.verbsrl;

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.nlp.utilities.POSUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VerbPredicateDetector {

    private static final Map<String, String> lemmaMap = readLemmaDict();

    public List<Constituent> getPredicates(TextAnnotation ta) {
        List<Constituent> list = new ArrayList<>();

        for (int i = 0; i < ta.size(); i++) {
            String lemma = getLemma(ta, i);

            if (lemma != null) {
                Constituent c = new Constituent("", "", ta, i, i + 1);
                c.addAttribute(PredicateArgumentView.LemmaIdentifier, lemma);
                list.add(c);
            }
        }
        return list;
    }

    public String getLemma(TextAnnotation ta, int tokenId) {
        String pos = POSUtils.getPOS(ta, tokenId);
        String token = ta.getToken(tokenId).toLowerCase();
        TokenLabelView lemmaView = (TokenLabelView) ta.getView(ViewNames.LEMMA);
        String lemma = lemmaView.getConstituentAtToken(tokenId).getLabel();
        boolean predicate = false;

        // any token that is a verb is a predicate
        if (POSUtils.isPOSVerb(pos) && !pos.equals("AUX")) {
            if (token.equals("'s") || token.equals("'re") || token.equals("'m"))
                lemma = "be";
            else if (token.equals("'d") || lemma.equals("wo") || lemma.equals("'ll"))
                lemma = "xmodal";

            predicate = true;

            // modals and some
            if (lemma.equals("xmodal") || pos.equals("MD") || token.equals("'ve"))
                predicate = false;

            // ignore all instances of has + "to be" if they are followed by a
            // verb or if the token is "be" followed by a verb

            boolean doVerb = lemma.equals("do");
            boolean be = lemma.equals("be");
            boolean have = lemma.equals("have");

            if (tokenId < ta.size() - 1) {

                if (be) {
                    SpanLabelView chunk = (SpanLabelView) ta.getView(ViewNames.SHALLOW_PARSE);
                    for (Constituent c : chunk.getConstituentsCoveringToken(tokenId)) {
                        // if the token under consideration is not the last
                        // token, then there is another verb here
                        if (c.getEndSpan() - 1 != tokenId) {
                            predicate = false;
                            break;
                        }
                    }
                }

                // ignore "have + be"
                if (have && lemmaView.getConstituentAtToken(tokenId + 1).getLabel().equals("be")) {
                    predicate = false;
                }

                // ignore "have/do + verb"
                if ((have || doVerb) && POSUtils.isPOSVerb(POSUtils.getPOS(ta, tokenId + 1)))
                    predicate = false;

                // for some reason "according" in 'according to' is tagged as a
                // verb. we want to avoid this.

                if (token.equals("according") && ta.getToken(tokenId + 1).toLowerCase().equals("to"))
                    predicate = false;
            }

            if (tokenId < ta.size() - 2) {
                // ignore don't + V or haven't + V
                if (doVerb || have) {
                    String nextToken = ta.getToken(tokenId + 1).toLowerCase();

                    if ((nextToken.equals("n't") || nextToken.equals("not"))
                            && POSUtils.isPOSVerb(POSUtils.getPOS(ta, tokenId + 2)))
                        predicate = false;

                }
            }
        }
        else if (token.startsWith("re-")) {
            String trim = token.replace("re-", "");
            predicate = lemmaMap.containsKey(trim);
        }

        if (predicate) return lemma;
        return null;
    }

    private static Map<String, String> readLemmaDict() {
        Map<String, String> map = new HashMap<>();

        List<String> lines;
        try {
            lines = LineIO.readFromClasspath("verb-lemDict.txt");
        } catch (IOException e) {
            throw new RuntimeException("Cannot load lemma dictionary from classpath");
        }

        for (String line : lines) {
            if (line.length() == 0)
                continue;
            String[] parts = line.split("\\s+");

            String lemma = parts[0];
            for (String p : parts) {
                map.put(p, lemma);
            }
        }
        return map;
    }
}
