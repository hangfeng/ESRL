package edu.illinois.cs.cogcomp.esrl.verbsrl;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.nlp.corpusreaders.PropbankReader;
import edu.illinois.cs.cogcomp.nlp.corpusreaders.ontonotes.OntonotesPropbankReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VerbOntonotesParser extends VerbSRLArgumentReader {


    public VerbOntonotesParser(String corpusName, boolean useArgPredictor) throws Exception {
        super(corpusName, useArgPredictor);
    }

    public VerbOntonotesParser(boolean isTrain, String corpusName, boolean useArgPredictor) throws Exception {
        super(isTrain, corpusName, useArgPredictor);
    }

    public VerbOntonotesParser(String corpusName, boolean useArgPredictor, boolean tokenCandidates) throws Exception {
        super(corpusName, useArgPredictor, tokenCandidates);
    }

    @Override
    public List<TextAnnotation> readData() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        String ontonotesDir = rm.getString(ESRLConfigurator.ONTONOTES_DIR);
        String dir = ontonotesDir + File.separator + "Verb" + File.separator + corpusName;
        OntonotesPropbankReader ontonotesPropbankReader = new OntonotesPropbankReader(dir, "english");
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        while (ontonotesPropbankReader.hasNext()) textAnnotations.add(ontonotesPropbankReader.next());
        return textAnnotations;
    }



}
