package edu.illinois.cs.cogcomp.esrl.verbsrl.learning;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.core.AbstractClassifier;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features.parseFeatures;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features.posFeatures;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features.shallowParseFeatures;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.features.wordFeatures;
import edu.illinois.cs.cogcomp.lbjava.classify.Classifier;

public class VerbSRLArgumentClassifier extends AbstractClassifier {
    private static final String PACKAGE_NAME = "edu.illinois.cs.cogcomp.esrl.verbsrl.learning";
    public static final String CLASS_NAME = "VerbSRLArgumentClassifier";

    private static VerbSRLArgumentClassifier instance;

    public VerbSRLArgumentClassifier(String modelPath, String lexiconPath) {
        super(PACKAGE_NAME + "." + CLASS_NAME, modelPath, lexiconPath);
        instance = this;
    }

    public static VerbSRLArgumentClassifier getInstance() {
        return instance;
    }

    @Override
    protected void initialize() {
        containingPackage = PACKAGE_NAME;
        name = CLASS_NAME;
        setLabeler(new Label());
        setExtractor(new FeatureExtractor());
    }

    public static class FeatureExtractor extends AbstractClassifier.FeatureExtractor {
        public FeatureExtractor() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$FeatureExtractor";
            featureSet = new Classifier[]{new wordFeatures(), new posFeatures(),
                    new shallowParseFeatures(), new parseFeatures()};
        }
    }

    public static class Label extends AbstractClassifier.Label {
        public Label() {
            containingPackage = PACKAGE_NAME;
            name = CLASS_NAME + "$Label";
        }

        public String discreteValue(Object example) {
            return ((Constituent) example).getLabel();
        }
    }
}

