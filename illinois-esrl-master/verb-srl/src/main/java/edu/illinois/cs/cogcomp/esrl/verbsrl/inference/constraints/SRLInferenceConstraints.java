package edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.lbjava.classify.FeatureVector;
import edu.illinois.cs.cogcomp.lbjava.infer.FirstOrderConjunction;
import edu.illinois.cs.cogcomp.lbjava.infer.FirstOrderConstant;
import edu.illinois.cs.cogcomp.lbjava.infer.FirstOrderConstraint;
import edu.illinois.cs.cogcomp.lbjava.infer.ParameterizedConstraint;


public class SRLInferenceConstraints extends ParameterizedConstraint {
    private static final NoDuplicates noDuplicates = new NoDuplicates();
    private static final ConnectedArguments rArgs = new ConnectedArguments("R");
    private static final ConnectedArguments cArgs = new ConnectedArguments("C");
    private static final LegalRoles legalRoles = new LegalRoles();

    public SRLInferenceConstraints() {
        super("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints.SRLInferenceConstraints");
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String discreteValue(Object example) {
        Constituent predicate = (Constituent) example;

        boolean conjunction = cArgs.discreteValue(predicate).equals("true") &&
                rArgs.discreteValue(predicate).equals("true") &&
                noDuplicates.discreteValue(predicate).equals("true") &&
                legalRoles.discreteValue(predicate).equals("true");

        if (!conjunction) return "false";

        return "true";
    }

    public FeatureVector[] classify(Object[] examples) {
        return super.classify(examples);
    }

    public int hashCode() {
        return "SRLInferenceConstraints".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof SRLInferenceConstraints;
    }

    public FirstOrderConstraint makeConstraint(Object example) {
        Constituent predicate = (Constituent) example;

        FirstOrderConstraint conjunctionRC = new FirstOrderConjunction(rArgs.makeConstraint(predicate),
                cArgs.makeConstraint(predicate));

        FirstOrderConstraint conjunctionRCLegalRoles = new FirstOrderConjunction(conjunctionRC,
                legalRoles.makeConstraint(predicate));

        FirstOrderConstraint finalConjunction = new FirstOrderConjunction(conjunctionRCLegalRoles,
                noDuplicates.makeConstraint(predicate));

        return new FirstOrderConjunction(new FirstOrderConstant(true), finalConjunction);
    }
}

