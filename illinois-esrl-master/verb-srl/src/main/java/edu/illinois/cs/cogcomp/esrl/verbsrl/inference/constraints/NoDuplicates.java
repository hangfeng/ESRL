package edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.esrl.verbsrl.VerbSRLArgumentReader;
import edu.illinois.cs.cogcomp.esrl.verbsrl.learning.VerbSRLArgumentClassifier;
import edu.illinois.cs.cogcomp.lbjava.infer.*;

import java.util.List;

public class NoDuplicates extends ParameterizedConstraint {
    private static final VerbSRLArgumentClassifier baseClassifier = VerbSRLArgumentClassifier.getInstance();

    public NoDuplicates() {
        super("edu.illinois.cs.cogcomp.esrl.verbsrl.inference.constraints.NoDuplicates");
    }

    public String getInputType() {
        return "edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent";
    }

    public String discreteValue(Object example) {
        Constituent predicate = (Constituent) example;
        List<Constituent> arguments = VerbSRLArgumentReader.getArguments(predicate);

        boolean LBJava$constraint$result$0;

        LBJava$constraint$result$0 = true;
        for (String coreRole : VerbSRLArgumentReader.getCoreArgRoles()) {
            int LBJ$m$1 = 0;
            int LBJ$bound$1 = 1;
            for (Constituent arg : arguments) {
                boolean LBJava$constraint$result$1 = baseClassifier.discreteValue(arg).equals(coreRole);
                if (LBJava$constraint$result$1) ++LBJ$m$1;
            }
            LBJava$constraint$result$0 = LBJ$m$1 <= LBJ$bound$1;
        }

        if (!LBJava$constraint$result$0) return "false";

        return "true";
    }

    public int hashCode() {
        return "NoDuplicates".hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof NoDuplicates;
    }

    public FirstOrderConstraint makeConstraint(Object example) {
        Constituent predicate = (Constituent) example;
        List<Constituent> arguments = VerbSRLArgumentReader.getArguments(predicate);
        FirstOrderConstraint result = new FirstOrderConstant(true);

        for (String coreRole : VerbSRLArgumentReader.getCoreArgRoles()) {
            Object[] LBJ$constraint$context = new Object[2];
            LBJ$constraint$context[0] = predicate;
            LBJ$constraint$context[1] = coreRole;

            EqualityArgumentReplacer LBJ$EAR = new EqualityArgumentReplacer(LBJ$constraint$context, true) {
                public Object getLeftObject() {
                    return quantificationVariables.get(0);
                }
            };

            FirstOrderConstraint LBJava$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(baseClassifier, null), coreRole, LBJ$EAR);
            FirstOrderConstraint LBJava$constraint$result$0 = new AtMostQuantifier("arg", arguments, LBJava$constraint$result$2, 1);

            result = new FirstOrderConjunction(result, LBJava$constraint$result$0);
        }
        return result;
    }
}

