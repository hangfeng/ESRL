package edu.illinois.cs.cogcomp.esrl.framenet;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FramenetDataReaderTest {
    private String dataDir;

    @Before
    public void setUp() throws Exception {
        ResourceManager rm = ESRLConfigurator.defaults();
        dataDir = rm.getString(ESRLConfigurator.FRAMENET_DATA_DIR);
    }

    @Test
    public void testReader() {
        FramenetDataReader reader = new FramenetDataReader(dataDir, "train");
        Constituent predicate = (Constituent) reader.next();
        assertEquals("thousand", predicate.getSurfaceForm());
        assertEquals("Cardinal_numbers", predicate.getAttribute(FramenetDataReader.FRAME_NAME));
    }
}