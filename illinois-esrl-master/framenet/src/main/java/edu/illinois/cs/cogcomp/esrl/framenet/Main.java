package edu.illinois.cs.cogcomp.esrl.framenet;

import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;
import edu.illinois.cs.cogcomp.esrl.core.ESRLConfigurator;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.esrl.framenet.lbjava.FramenetClassifier;
import edu.illinois.cs.cogcomp.esrl.framenet.lbjava.FramenetLabel;
import edu.illinois.cs.cogcomp.lbjava.classify.TestDiscrete;
import edu.illinois.cs.cogcomp.lbjava.learn.BatchTrainer;
import edu.illinois.cs.cogcomp.lbjava.learn.Learner;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;

import java.io.File;

public class Main {
    private ResourceManager rm = ESRLConfigurator.defaults();
    private String dataDir = rm.getString(ESRLConfigurator.FRAMENET_DATA_DIR);
    private String modelsDir = rm.getString(ESRLConfigurator.MODELS_DIR);
    private String modelName = modelsDir + File.separator + FramenetClassifier.CLASS_NAME;


    public void train() {
        Learner classifier = new FramenetClassifier(modelName + ".lc", modelName + ".lex");
        Parser trainDataReader = new FramenetDataReader(dataDir, "train");
        BatchTrainer trainer = new BatchTrainer(classifier, trainDataReader, 10000);
        trainer.train(5);
        classifier.save();
        trainDataReader.close();
    }

    public void test() {
        Learner classifier = new FramenetClassifier(modelName + ".lc", modelName + ".lex");
        Parser testDataReader = new FramenetDataReader(dataDir, "test");
        TestDiscrete tester = new TestDiscrete();
        tester.addNull(DataReader.CANDIDATE);
        TestDiscrete.testDiscrete(tester, classifier, new FramenetLabel(), testDataReader, true, 1000);
        testDataReader.close();
    }

    public static void main(String[] args) {
        Main trainer = new Main();
        trainer.train();
        trainer.test();
    }
}
