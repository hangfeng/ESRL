package edu.illinois.cs.cogcomp.esrl.framenet;

import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.PredicateArgumentView;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Relation;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.TextAnnotation;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.esrl.core.ESRLViewNames;
import edu.illinois.cs.cogcomp.esrl.core.data.DataReader;
import edu.illinois.cs.cogcomp.nlp.tokenizer.IllinoisTokenizer;
import edu.illinois.cs.cogcomp.nlp.utility.TokenizerTextAnnotationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FramenetDataReader extends DataReader {
    protected static Logger logger = LoggerFactory.getLogger(FramenetDataReader.class);
    public static final String FRAME_NAME = "FrameName";

    /** Since the data is not tokenized, we need this */
    private static TokenizerTextAnnotationBuilder annotationBuilder;

    /**
     * The reader for Framenet data (lexical unit files).
     *
     * @param file The directory containing {@code luIndex.xml} and the {@code lu} directory
     */
    public FramenetDataReader(String file, String corpusName) {
        super(file, corpusName, ESRLViewNames.FRAMENET);
    }

    @Override
    public List<TextAnnotation> readData() {
        logger.info("Started reading framenet data");
        annotationBuilder = new TokenizerTextAnnotationBuilder(new IllinoisTokenizer());
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        try {
            String debugluPath = file + "/lu/" + "lu" + "2236" + ".xml";
            System.out.println(debugluPath);
            textAnnotations.addAll(processXML("Dimension", debugluPath));
            for (String line : LineIO.read(file + "/" + "luIndex.xml")) {
                line = line.trim();
                if (!line.startsWith("<lu ")) continue;
                String frameName = getXMLAttributeValue(line, "frameName");
                String luName = getXMLAttributeValue(line, "name");
                String luId = getXMLAttributeValue(line, "ID");
                boolean hasAnnotation = Boolean.parseBoolean(getXMLAttributeValue(line, "hasAnnotation"));
                if (hasAnnotation && !luName.contains(" ")) {
                    String luPath = file + "/lu/" + "lu" + luId + ".xml";
                    System.out.println(luPath);
                    textAnnotations.addAll(processXML(frameName, luPath));
                }
            }
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return textAnnotations;
    }

    private String getXMLAttributeValue(String tag, String attribute) {
        String key = " " + attribute + "=";
        String value = null;

        if (tag.contains(key)) {
            int i1 = tag.indexOf("\"", tag.indexOf(key)) + 1;
            int i2 = tag.indexOf("\"", i1);
            value = tag.substring(i1, i2);
        }
        return value;
    }

    private List<TextAnnotation> processXML(String frameName, String luFile)
            throws IOException, SAXException, ParserConfigurationException {
        List<TextAnnotation> examples = new ArrayList<>();
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(luFile);
        doc.getDocumentElement().normalize();
        NodeList nList = doc.getElementsByTagName("sentence");
        for (int sentInd = 0; sentInd < nList.getLength(); sentInd++) {
            Node nNode = nList.item(sentInd);
            Element eElement = (Element) nNode;
            String text = eElement.getElementsByTagName("text").item(0).getTextContent();
            String textId = frameName + "_" + IOUtils.getFileName(luFile) + ":" + sentInd;
            TextAnnotation ta = annotationBuilder.createTextAnnotation("framenet", textId, text);
            Element annotations = (Element) eElement.getElementsByTagName("annotationSet").item(1);
            NodeList annNodes = annotations.getElementsByTagName("layer");
            PredicateArgumentView pav = new PredicateArgumentView(viewName, ta);
            // find the predicate
            Constituent predicate = null;
            for (int i = 0; i < annNodes.getLength(); i++) {
                Element annNode = (Element) annNodes.item(i);
                if (annNode.getAttribute("name").equals("Target")) {
                    NodeList labels = annNode.getElementsByTagName("label");
                    for (int j = 0; j < labels.getLength(); j++) {
                        Element label = (Element) labels.item(j);
                        if (label.getAttribute("start").equals("") || label.getAttribute("end").equals(""))
                            continue;

                        int start = Integer.parseInt(label.getAttribute("start"));
                        int end = Integer.parseInt(label.getAttribute("end"));
                        int spanStart = ta.getTokenIdFromCharacterOffset(start);
                        int spanEnd = ta.getTokenIdFromCharacterOffset(end) + 1;
                        predicate = new Constituent("predicate", viewName, ta, spanStart, spanEnd);
                        predicate.addAttribute(FRAME_NAME, frameName);
                        if (!predicate.getSurfaceForm().equals(ta.getText().substring(start, end + 1))) {
                            predicate = null;
                            break;
                        }
                    }
                }
            }
            if (predicate == null) continue;

            List<Constituent> args = new ArrayList<>();
            List<String> relations = new ArrayList<>();
            for (int i = 0; i < annNodes.getLength(); i++) {
                Element annNode = (Element) annNodes.item(i);
                if (annNode.getAttribute("name").equals("FE")) {
                    NodeList labels = annNode.getElementsByTagName("label");
                    for (int j = 0; j < labels.getLength(); j++) {
                        Element label = (Element) labels.item(j);
                        if (label.getAttribute("start").equals("") || label.getAttribute("end").equals(""))
                            continue;

                        int start = Integer.parseInt(label.getAttribute("start"));
                        int end = Integer.parseInt(label.getAttribute("end"));
                        int spanStart = ta.getTokenIdFromCharacterOffset(start);
                        int index = 1;
                        while(spanStart == -1)
                        {
                            spanStart = ta.getTokenIdFromCharacterOffset(start + index);
                            index += 1;
                        }
                        int spanEnd = ta.getTokenIdFromCharacterOffset(end) + 1;
                        index = 1;
                        while(spanEnd == 0)
                        {
                            spanEnd = ta.getTokenIdFromCharacterOffset(end - index) + 1;
                            index += 1;
                        }
                        Constituent cons = new Constituent("argument", viewName, ta, spanStart, spanEnd);
                        if (!cons.getSurfaceForm().equals(ta.getText().substring(start, end + 1))) {
                            continue;
                        }
                        String relation = label.getAttribute("name");
                        args.add(cons);
                        relations.add(relation);
                    }
                }
            }
            double[] scores = new double[args.size()];
            Arrays.fill(scores, 1.0);
            pav.addPredicateArguments(predicate, args, relations.toArray(new String[relations.size()]), scores);
            ta.addView(viewName, pav);
            examples.add(ta);
        }
        return examples;
    }

    @Override
    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        //TODO This should be replaced with a non-gold candidate generator
        List<Constituent> candidates = new ArrayList<>();
        PredicateArgumentView view = (PredicateArgumentView) ta.getView(viewName);
        for (Relation r : view.getRelations())
            candidates.add(r.getSource());

        return candidates;
    }
}
