// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000D6EC1BA02C0401400DF59B2C2265E708D815B112C9094A4B95F813E9C5673CDDE9802EFBB9880A54AD973303E4839642FCA6CA4FD4D5D18F2880678D28AF6041DD57192F9F607AA254B87869CC2A4AE58D420BE4DD5A97A9DC8B7B27CB3260A1251F3A5D9E3651A3A056CE27F3ACC8B59D7E128774A891DD90BA7EC35633A53EB7AFFC6D6E5F600D0F0C32DD000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomSRLInferenceBeamSearch$subjectto extends ParameterizedConstraint
{
  private static final NomNoOverlaps __NomNoOverlaps = new NomNoOverlaps();
  private static final NomNoDuplicates __NomNoDuplicates = new NomNoDuplicates();
  private static final NomReferences __NomReferences = new NomReferences();
  private static final NomLegalArguments __NomLegalArguments = new NomLegalArguments();
  private static final NomContinuences __NomContinuences = new NomContinuences();

  public NomSRLInferenceBeamSearch$subjectto() { super("edu.illinois.cs.cogcomp.srl.learners.NomSRLInferenceBeamSearch$subjectto"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomSRLInferenceBeamSearch$subjectto(TextAnnotation)' defined on line 285 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    {
      boolean LBJ2$constraint$result$0;
      {
        boolean LBJ2$constraint$result$1;
        {
          boolean LBJ2$constraint$result$2;
          {
            boolean LBJ2$constraint$result$3;
            {
              boolean LBJ2$constraint$result$4;
              LBJ2$constraint$result$4 = __NomNoOverlaps.discreteValue(sentence).equals("true");
              if (LBJ2$constraint$result$4)
                LBJ2$constraint$result$3 = __NomLegalArguments.discreteValue(sentence).equals("true");
              else LBJ2$constraint$result$3 = false;
            }
            if (LBJ2$constraint$result$3)
              LBJ2$constraint$result$2 = __NomNoDuplicates.discreteValue(sentence).equals("true");
            else LBJ2$constraint$result$2 = false;
          }
          if (LBJ2$constraint$result$2)
            LBJ2$constraint$result$1 = __NomContinuences.discreteValue(sentence).equals("true");
          else LBJ2$constraint$result$1 = false;
        }
        if (LBJ2$constraint$result$1)
          LBJ2$constraint$result$0 = __NomReferences.discreteValue(sentence).equals("true");
        else LBJ2$constraint$result$0 = false;
      }
      if (!LBJ2$constraint$result$0) return "false";
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomSRLInferenceBeamSearch$subjectto(TextAnnotation)' defined on line 285 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomSRLInferenceBeamSearch$subjectto".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomSRLInferenceBeamSearch$subjectto; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomSRLInferenceBeamSearch$subjectto(TextAnnotation)' defined on line 285 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    {
      FirstOrderConstraint LBJ2$constraint$result$0 = null;
      {
        FirstOrderConstraint LBJ2$constraint$result$1 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$2 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$3 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$4 = null;
              LBJ2$constraint$result$4 = __NomNoOverlaps.makeConstraint(sentence);
              FirstOrderConstraint LBJ2$constraint$result$5 = null;
              LBJ2$constraint$result$5 = __NomLegalArguments.makeConstraint(sentence);
              LBJ2$constraint$result$3 = new FirstOrderConjunction(LBJ2$constraint$result$4, LBJ2$constraint$result$5);
            }
            FirstOrderConstraint LBJ2$constraint$result$6 = null;
            LBJ2$constraint$result$6 = __NomNoDuplicates.makeConstraint(sentence);
            LBJ2$constraint$result$2 = new FirstOrderConjunction(LBJ2$constraint$result$3, LBJ2$constraint$result$6);
          }
          FirstOrderConstraint LBJ2$constraint$result$7 = null;
          LBJ2$constraint$result$7 = __NomContinuences.makeConstraint(sentence);
          LBJ2$constraint$result$1 = new FirstOrderConjunction(LBJ2$constraint$result$2, LBJ2$constraint$result$7);
        }
        FirstOrderConstraint LBJ2$constraint$result$8 = null;
        LBJ2$constraint$result$8 = __NomReferences.makeConstraint(sentence);
        LBJ2$constraint$result$0 = new FirstOrderConjunction(LBJ2$constraint$result$1, LBJ2$constraint$result$8);
      }
      __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
    }

    return __result;
  }
}

