package edu.illinois.cs.cogcomp.srl.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LBJ2.learn.Learner;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.core.datastructures.trees.TreeTraversal;
import edu.illinois.cs.cogcomp.edison.features.helpers.ParseHelper;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.utilities.ParseTreeProperties;
import edu.illinois.cs.cogcomp.edison.utilities.ParseUtils;
import edu.illinois.cs.cogcomp.srl.main.NomSRLSystem;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.testers.ArgumentIdentifier;

public class NomArgumentCandidateHeuristic {

	private static final Logger log = LoggerFactory
			.getLogger(NomArgumentCandidateHeuristic.class);

	private static String NomArgumentCandidateHeuristicView = "NomArgumentCandidateHeuristicView";

	public static List<Constituent> generateCandidatesForPredicate(
			Constituent predicate) {

		TextAnnotation ta = predicate.getTextAnnotation();

		int predicateSentenceId = ta.getSentenceId(predicate);

		int predicateSentenceStart = ta.getSentence(predicateSentenceId)
				.getStartSpan();

		// get the parse tree
		Tree<String> tree = ParseHelper.getParseTree(SRLConfig.getInstance()
				.getDefaultParser(), ta, predicateSentenceId);
		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		// List<Relation> args = predicate.sourceOfRelations();

		List<Relation> args = predicate.getOutgoingRelations();

		// identify the spans of all arguments. This step is only for labeling
		// candidates as arguments or not.
		Map<Pair<Integer, Integer>, String> argSpans = new HashMap<Pair<Integer, Integer>, String>();
		for (Relation r : args) {
			Constituent arg = r.getTarget();
			argSpans.put(
					new Pair<Integer, Integer>(arg.getStartSpan(), arg
							.getEndSpan()), r.getRelationName());
		}

		Constituent predicateClone = predicate
				.cloneForNewView(NomArgumentCandidateHeuristicView);

		int predicatePosition = predicate.getStartSpan()
				- predicateSentenceStart;

		Set<Constituent> out = new HashSet<Constituent>();

		// add all non terminals in the tree
		for (Tree<Pair<String, IntPair>> c : TreeTraversal
				.depthFirstTraversal(spanLabeledTree)) {
			if (!c.isRoot() && !c.isLeaf() && !c.getChild(0).isLeaf()) {
				int start = c.getLabel().getSecond().getFirst()
						+ predicateSentenceStart;
				int end = c.getLabel().getSecond().getSecond()
						+ predicateSentenceStart;

				Constituent newConstituent = getNewConstituent(ta, argSpans,
						predicateClone, start, end);

				out.add(newConstituent);
			}
		}

		// add all siblings of the predicate

		Tree<Pair<String, IntPair>> predicateNode = spanLabeledTree.getYield()
				.get(predicatePosition);
		Tree<Pair<String, IntPair>> predicatePhrase = predicateNode.getParent()
				.getParent();

		for (int i = predicatePhrase.getLabel().getSecond().getFirst()
				+ predicateSentenceStart; i < predicatePosition
				+ predicateSentenceStart; i++) {
			int start = i;
			int end = predicatePosition + predicateSentenceStart;
			Constituent newConstituent = getNewConstituent(ta, argSpans,
					predicateClone, start, end);

			out.add(newConstituent);

			Constituent ithWord = getNewConstituent(ta, argSpans,
					predicateClone, i, i + 1);
			out.add(ithWord);
		}

		for (int i = predicatePosition + 1 + predicateSentenceStart; i < predicatePhrase
				.getLabel().getSecond().getSecond()
				+ predicateSentenceStart; i++) {
			int start = i;
			int end = predicatePhrase.getLabel().getSecond().getSecond()
					+ predicateSentenceStart;

			Constituent newConstituent = getNewConstituent(ta, argSpans,
					predicateClone, start, end);

			out.add(newConstituent);

			Constituent ithWord = getNewConstituent(ta, argSpans,
					predicateClone, i, i + 1);
			out.add(ithWord);

		}

		// the predicate itself
		Constituent newConstituent = getNewConstituent(ta, argSpans,
				predicateClone, predicate.getStartSpan(),
				predicate.getEndSpan());
		out.add(newConstituent);

		// verb nodes that dominate the predicate
		Tree<Pair<String, IntPair>> node = predicateNode.getParent();

		while (!node.isRoot()
				&& !ParseTreeProperties.isNonTerminalVerb(node.getLabel()
						.getFirst()))
			node = node.getParent();

		for (Tree<Pair<String, IntPair>> verbCandidate : node.getYield()) {
			if (ParseTreeProperties.isPOSVerb(verbCandidate.getParent()
					.getLabel().getFirst())) {
				int start = verbCandidate.getLabel().getSecond().getFirst()
						+ predicateSentenceStart;
				int end = start + 1;

				Constituent verbConstituent = getNewConstituent(ta, argSpans,
						predicateClone, start, end);
				out.add(verbConstituent);
			}
		}

		// pronouns in NPs within the same clause that dominate this predicate
		node = predicateNode.getParent();
		while (!node.isRoot()) {
			String label = node.getLabel().getFirst();
			if (label.startsWith("S"))
				break;

			if (ParseTreeProperties.isNonTerminalNoun(label)) {
				for (Tree<Pair<String, IntPair>> nominalCandidate : node
						.getYield()) {
					if (ParseTreeProperties
							.isPOSPossessivePronoun(nominalCandidate
									.getParent().getLabel().getFirst())) {
						int start = nominalCandidate.getLabel().getSecond()
								.getFirst()
								+ predicateSentenceStart;
						int end = start + 1;

						Constituent verbConstituent = getNewConstituent(ta,
								argSpans, predicateClone, start, end);
						out.add(verbConstituent);
					}
				}

			}
			node = node.getParent();

		}

		// if predicate is dominated by a PP, then the head of that PP
		node = predicateNode.getParent();
		boolean ppParentFound = false;
		while (!node.isRoot()) {
			String label = node.getLabel().getFirst();
			if (ParseTreeProperties.isNonTerminalPP(label)) {
				ppParentFound = true;
				break;
			} else if (label.startsWith("S"))
				break;

			node = node.getParent();
		}

		if (ppParentFound) {
			int start = node.getLabel().getSecond().getFirst()
					+ predicateSentenceStart;
			int end = start + 1;
			Constituent verbConstituent = getNewConstituent(ta, argSpans,
					predicateClone, start, end);
			out.add(verbConstituent);

		}

		log.debug("Number of candidates for {} from heuristic: {}",
				predicate.toString(), out.toString());

		return new ArrayList<Constituent>(out);
	}

	public static List<Constituent> generateFilteredCandidatesForPredicate(
			Constituent predicate, Learner nomIdentifier) {

		TextAnnotation ta = predicate.getTextAnnotation();

		if (!ta.hasView(NomArgumentCandidateHeuristicView))
			ta.addView(NomArgumentCandidateHeuristicView,
					new PredicateArgumentView(
							NomArgumentCandidateHeuristicView,
							"HeuristicIdentifier", ta, 1.0));

		PredicateArgumentView pav = (PredicateArgumentView) ta
				.getView(NomArgumentCandidateHeuristicView);

		List<Constituent> predicatesSoFar = pav.getPredicates();

		for (Constituent p : predicatesSoFar) {
			if (p.getSpan().equals(predicate.getSpan())) {
				List<Constituent> candidates = new ArrayList<Constituent>();
				for (Relation r : pav.getArguments(p))
					candidates.add(r.getTarget());

				return candidates;
			}
		}

		List<Constituent> out = generateCandidatesForPredicate(predicate);

		out = identifierFilter(out, nomIdentifier);

		if (out.size() > 0) {

			// clone the predicate.
			Constituent predicateClone = predicate
					.cloneForNewView(NomArgumentCandidateHeuristicView);

			String[] labels = new String[out.size()];
			for (int i = 0; i < out.size(); i++) {
				labels[i] = "ChildOf";
			}
			pav.addPredicateArguments(predicateClone, out, labels,
					new double[out.size()]);
		}

		return out;
	}

	private static List<Constituent> identifierFilter(List<Constituent> out,
			Learner nomIdentifier) {
		ArgumentIdentifier identifier = new ArgumentIdentifier(
				NomSRLSystem.nomIdThreshold, NomSRLSystem.nomIdBeta,
				nomIdentifier);

		List<Constituent> cc = new ArrayList<Constituent>();

		for (Constituent c : out) {

			if (identifier.classify(c).getFirst()) {
				cc.add(c.cloneForNewView(NomArgumentCandidateHeuristicView));
			}
		}

		return cc;
	}

	private static Constituent getNewConstituent(TextAnnotation ta,
			Map<Pair<Integer, Integer>, String> argSpans,
			Constituent predicateClone, int start, int end) {

		Pair<Integer, Integer> span = new Pair<Integer, Integer>(start, end);
		boolean isArgument = argSpans.containsKey(span);

		String label = "";
		if (isArgument) {
			label = argSpans.get(span);
		} else {
			label = "null";
		}

		Constituent newConstituent = new Constituent(label, 1.0,
				NomArgumentCandidateHeuristicView, ta, start, end);

		newConstituent.addAttribute("ParentTreeId", "0");

		new Relation("ChildOf", predicateClone, newConstituent, 1.0);
		return newConstituent;
	}

}
