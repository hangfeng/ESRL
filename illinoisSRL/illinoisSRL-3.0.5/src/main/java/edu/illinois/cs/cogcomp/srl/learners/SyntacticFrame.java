// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D2945580EACCB294C4E29CC467B2A4CCD45D07ECFCB2E29CC292D4DCB215846D450B1D558A6500AAB2D2AC3505A2EACC3565250D6580B4D2A427B4D440A06AA74A6E414A619E5A7A690A9939CA96DA05B000C879CA5856000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.main.Constants;
import java.util.*;


public class SyntacticFrame extends Classifier
{
  public SyntacticFrame()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "SyntacticFrame";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete"; }


  public FeatureVector classify(Object __example)
  {
    return new FeatureVector(featureValue(__example));
  }

  public Feature featureValue(Object __example)
  {
    String result = discreteValue(__example);
    return new DiscretePrimitiveStringFeature(containingPackage, name, "", result, valueIndexOf(result), (short) allowableValues().length);
  }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'SyntacticFrame(Constituent)' defined on line 15 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    Constituent c = (Constituent) __example;

    return "" + ("syn#" + VerbFeatureHelper.getSyntacticFrame(c));
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'SyntacticFrame(Constituent)' defined on line 15 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "SyntacticFrame".hashCode(); }
  public boolean equals(Object o) { return o instanceof SyntacticFrame; }
}

