/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.thrift.TException;

import edu.illinois.cs.cogcomp.core.algorithms.Sorters;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorClient;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.data.CoNLLColumnFormatReaderSRL;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.main.VerbSRLSystem;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;

/**
 * Compares the illinois-srl v2.0 (the SNoW system) in the curator with the java
 * system
 * 
 * @author Vivek Srikumar
 * 
 */
public class SRLSystemComparator {
	public static void main(String[] args) throws FileNotFoundException,
			ServiceUnavailableException, AnnotationFailedException, TException {

		if (args.length != 1 && args.length != 2) {
			System.err
					.println("Usage: SRLSystemComparator input-column-file [beam|ilp (default ilp)]");
			System.exit(-1);
		}

		String inputFile = args[0];

		boolean beam = false;
		if (args.length == 2) {
			if (args[1].equals("beam"))
				beam = true;
		}

		CuratorClient curator = new CuratorClient(SRLConfig.getInstance()
				.getCuratorHost(), SRLConfig.getInstance().getCuratorPort(),
				true);

		String goldSRLName = ViewNames.SRL + "Gold";
		CoNLLColumnFormatReaderSRL reader = new CoNLLColumnFormatReaderSRL("",
				"", inputFile, goldSRLName);

		long start = System.currentTimeMillis();

		Counter<String> counter = new Counter<String>();
		Set<String> allArguments = new HashSet<String>();
		VerbSRLSystem srlSystem = VerbSRLSystem.getInstance();

		int count = 0;
		for (TextAnnotation ta : reader) {

			PredicateArgumentView prediction = srlSystem.getSRL(ta, beam);
			curator.addSRLView(ta, false);

			PredicateArgumentView curatorPrediction = (PredicateArgumentView) ta
					.getView(ViewNames.SRL);

			updateCounts(counter, allArguments, prediction, curatorPrediction);

			count++;

			if (count % 100 == 0) {
				long end = System.currentTimeMillis();
				System.out.println(count + " examples completed. Took "
						+ (end - start) + "ms");
			}
		}

		long end = System.currentTimeMillis();
		System.out.println("Done. Took " + (end - start) + "ms");

		printResults(counter, allArguments);
	}

	private static void printResults(Counter<String> counter,
			Set<String> allArguments) {

		int predicatesFound = (int) counter.getCount("Predicate found");
		int predicateOnlyIllinoisSRL = (int) counter
				.getCount("Missing predicate, found in illinoisSRL");
		int predicateOnlyCurator = (int) counter
				.getCount("Missing predicate, found on curator");

		System.out
				.println("||Found by both  | Curator  only | illinoisSRL only|");
		System.out.println("|---");
		System.out.println("| Predicate | " + predicatesFound + "|"
				+ predicateOnlyCurator + "|" + predicateOnlyIllinoisSRL);

		System.out.println("|---");
		System.out.println("|Arguments | " + counter.getCount("Correct") + "|"
				+ counter.getCount("Missing argument in illinoisSRL") + "| "
				+ counter.getCount("Missing argument in curator"));

		System.out.println("|----");

		for (String arg : Sorters.sortSet(allArguments)) {
			System.out.println("|" + arg + "|"
					+ counter.getCount("Correct: " + arg) + "|"
					+ counter.getCount("Error: Curator: " + arg) + "|"
					+ counter.getCount("Error: illinoisSRL: " + arg));
		}
	}

	private static void updateCounts(Counter<String> counter,
			Set<String> allArguments, PredicateArgumentView prediction,
			PredicateArgumentView curatorPrediction) {

		Map<IntPair, Constituent> myPredicates = getPredicates(prediction);
		Map<IntPair, Constituent> curatorPredicates = getPredicates(curatorPrediction);

		for (IntPair myPredicateSpan : myPredicates.keySet()) {

			Constituent myPredicate = myPredicates.get(myPredicateSpan);
			if (!curatorPredicates.containsKey(myPredicateSpan)) {
				counter.incrementCount("Missing predicate, found in illinoisSRL");
				counter.incrementCount("Missing predicate, found in illinoisSRL: "
						+ myPredicate
								.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier));
				continue;
			}

			counter.incrementCount("Predicate found");

			Map<IntPair, String> myArgs = getArguments(prediction, myPredicate);
			Map<IntPair, String> curatorArgs = getArguments(curatorPrediction,
					curatorPredicates.get(myPredicateSpan));

			for (IntPair span : myArgs.keySet()) {
				if (!curatorArgs.containsKey(span)) {
					counter.incrementCount("Missing argument in curator");
					counter.incrementCount("Missing argument in curator: "
							+ myArgs.get(span));
					continue;
				}

				String myLabel = myArgs.get(span).replaceAll("C-", "");
				String curatorLabel = curatorArgs.get(span)
						.replaceAll("C-", "");

				allArguments.add(myLabel);
				allArguments.add(curatorLabel);

				if (myLabel.equals(curatorLabel)) {
					counter.incrementCount("Correct");
					counter.incrementCount("Correct: " + myLabel);
				} else {
					counter.incrementCount("Error");
					counter.incrementCount("Error: Curator: " + curatorLabel);
					counter.incrementCount("Error: illinoisSRL: " + myLabel);
				}

			}

			for (IntPair span : curatorArgs.keySet()) {
				if (!myArgs.containsKey(span)) {
					counter.incrementCount("Missing argument in illinoisSRL");
					counter.incrementCount("Missing argument in illinoisSRL: "
							+ curatorArgs.get(span));
				}
			}

		}

		for (IntPair curatorPredicateSpan : curatorPredicates.keySet()) {
			Constituent curatorPredicate = curatorPredicates
					.get(curatorPredicateSpan);

			if (!myPredicates.containsKey(curatorPredicateSpan)) {
				counter.incrementCount("Missing predicate, found on curator");
				counter.incrementCount("Missing predicate, found on curator: "
						+ curatorPredicate
								.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier));
			}

		}

	}

	private static Map<IntPair, String> getArguments(PredicateArgumentView pav,
			Constituent predicate) {
		Map<IntPair, String> map = new HashMap<IntPair, String>();

		for (Relation r : pav.getArguments(predicate)) {
			map.put(r.getTarget().getSpan(), r.getRelationName());
		}

		return map;
	}

	private static Map<IntPair, Constituent> getPredicates(
			PredicateArgumentView pav) {
		Map<IntPair, Constituent> map = new HashMap<IntPair, Constituent>();

		for (Constituent predicate : pav.getPredicates())
			map.put(predicate.getSpan(), predicate);
		return map;
	}
}
