package edu.illinois.cs.cogcomp.srl.main;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LBJ2.classify.Classifier;
import LBJ2.classify.Feature;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.utilities.ArrayUtilities;
import edu.illinois.cs.cogcomp.edison.features.helpers.WordHelpers;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.ParseTreeProperties;
import edu.illinois.cs.cogcomp.srl.data.CoNLLColumnFormatReaderSRL;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;

public class SemanticRoleLabeler implements Callable<PredicateArgumentView> {

	private final static Logger log = LoggerFactory
			.getLogger(SemanticRoleLabeler.class);

	private final boolean beamSearch;
	private final SRLSystem srlSystem;
	private final TextAnnotation ta;
	private final List<Constituent> predicates;

	public SemanticRoleLabeler(SRLSystem srlSystem, TextAnnotation ta,
			List<Constituent> predicates, boolean beamSearch) {
		this.srlSystem = srlSystem;
		this.ta = ta;
		this.predicates = predicates;
		this.beamSearch = beamSearch;
	}

	@Override
	public PredicateArgumentView call() throws Exception {
		log.debug("Getting {} SRL for '{}'", srlSystem.getPredicateType(),
				ta.getText());

		if (!ta.hasView(ViewNames.PSEUDO_PARSE)) {
			View psuedoParseView = SRLUtils.createPsuedoParseView(ta);
			ta.addView(ViewNames.PSEUDO_PARSE, psuedoParseView);
		}

		Classifier srlPredictor;
		if (beamSearch)
			srlPredictor = srlSystem.getSRLBeamSearch();
		else
			srlPredictor = srlSystem.getSRLXpressMP();

		log.debug("Got predictor of type {}", beamSearch ? "Beamsearch" : "ILP");

		log.debug("Number of predicates = {} ", predicates.size());

		double score = 0;

		Map<Constituent, List<Constituent>> preds = new HashMap<Constituent, List<Constituent>>();
		Map<Constituent, Pair<String[], double[]>> predArgs = new HashMap<Constituent, Pair<String[], double[]>>();

		for (Constituent goldPredicate : predicates) {
			Constituent predicate = goldPredicate.cloneForNewView(srlSystem
					.getViewName());

			if (!predicate
					.hasAttribute(CoNLLColumnFormatReaderSRL.SenseIdentifer)) {
				predicate.addAttribute(
						CoNLLColumnFormatReaderSRL.SenseIdentifer, "01");
			}

			List<Constituent> arguments = new ArrayList<Constituent>();
			List<String> relations = new ArrayList<String>();
			List<Double> scores = new ArrayList<Double>();

			List<Constituent> filteredArgumentCandidates = srlSystem
					.getFilteredCandidatesForPredicate(goldPredicate);

			log.debug("Number of arguments candidates after filtering = {}",
					filteredArgumentCandidates.size());

			BitSet labeledTokens = new BitSet();
			boolean duplicateCore = false;
			Set<String> coreArgumentsSeen = new HashSet<String>();

			for (Constituent candidate : filteredArgumentCandidates) {

				Feature predictionFeature = srlPredictor
						.featureValue(candidate);

				String prediction = predictionFeature.getStringValue();
				double pScore = predictionFeature.getStrength();

				score += pScore;

				if (!prediction.equals("null")) {

					boolean overlap = false;
					for (int c = candidate.getStartSpan(); c < candidate
							.getEndSpan(); c++) {
						if (labeledTokens.get(c)) {
							// This means that there is an overlap. As a
							// heuristic, we can fix this by just picking the
							// first one and ignoring all subsequent
							// overlapping labels.
							overlap = true;
							break;
						}
					}

					if (!overlap) {
						duplicateCore |= labelArgument(arguments, relations,
								scores, labeledTokens, coreArgumentsSeen,
								candidate, prediction, pScore);

					} else {
						score -= pScore;
						score += srlSystem.getClassifierNullScore(candidate);
					}

				} // end if prediction is not null

				if (duplicateCore)
					removeDuplicateArguments(arguments, relations, scores);
			}

			if (SRLConfig.getInstance().getTrimLeadingPrepositions())
				arguments = trimLeadingPrepositions(arguments);

			preds.put(predicate, arguments);
			Pair<String[], double[]> pair = new Pair<String[], double[]>(
					relations.toArray(new String[relations.size()]),
					ArrayUtilities.asDoubleArray(scores));
			predArgs.put(predicate, pair);

		} // foreach predicate

		return createPredicateArgumentView(score, preds, predArgs);
	}

	private List<Constituent> trimLeadingPrepositions(
			List<Constituent> arguments) {

		List<Constituent> trimmedArguments = new ArrayList<Constituent>();

		for (Constituent c : arguments) {
			if (c.length() == 1)
				trimmedArguments.add(c);
			else if (!ParseTreeProperties.isPOSPreposition(WordHelpers.getPOS(
					c.getTextAnnotation(), c.getStartSpan())))
				trimmedArguments.add(c);
			else {
				// the first token is a preposition and the constituent is
				// longer than one token.

				Constituent c1 = new Constituent(c.getLabel(), c.getViewName(),
						c.getTextAnnotation(), c.getStartSpan() + 1,
						c.getEndSpan());

				for (String key : c1.getAttributeKeys()) {
					c1.addAttribute(key, c1.getAttribute(key));
				}
				trimmedArguments.add(c1);
			}
		}

		return trimmedArguments;
	}

	/**
	 * Go over arguments and remove all but the highest scoring core.
	 */
	private void removeDuplicateArguments(List<Constituent> arguments,
			List<String> relations, List<Double> scores) {
		arguments.clear();
		scores.clear();
		relations.clear();

		Map<String, Pair<Constituent, Double>> coreArgs = new HashMap<String, Pair<Constituent, Double>>();

		for (int i = 0; i < arguments.size(); i++) {
			String label = relations.get(i);
			Constituent constituent = arguments.get(i);
			Double myScore = scores.get(i);
			if (srlSystem.isCoreArgument(label)) {
				if (coreArgs.containsKey(label)) {
					Double otherScore = coreArgs.get(label).getSecond();
					if (myScore > otherScore) {
						coreArgs.put(label, new Pair<Constituent, Double>(
								constituent, myScore));
					}

				} else {
					coreArgs.put(label, new Pair<Constituent, Double>(
							constituent, myScore));
				}

			} else {
				relations.add(label);
				arguments.add(constituent);
				scores.add(myScore);
			}
		}

		for (String label : coreArgs.keySet()) {
			Pair<Constituent, Double> pair = coreArgs.get(label);
			relations.add(label);
			scores.add(pair.getSecond());
			arguments.add(pair.getFirst());
		}
	}

	private boolean labelArgument(List<Constituent> arguments,
			List<String> relations, List<Double> scores, BitSet labeledTokens,
			Set<String> coreArgumentsSeen, Constituent candidate,
			String prediction, double pScore) {

		boolean duplicateCore = false;
		Constituent constituent = new Constituent(prediction, ViewNames.SRL,
				ta, candidate.getStartSpan(), candidate.getEndSpan());

		if (coreArgumentsSeen.contains(prediction)) {
			duplicateCore = true;
		} else {

			arguments.add(constituent);
			relations.add(prediction);
			scores.add(pScore);

			if (srlSystem.isCoreArgument(prediction))
				coreArgumentsSeen.add(prediction);

			for (int c = candidate.getStartSpan(); c < candidate.getEndSpan(); c++) {
				labeledTokens.set(c);
			}
		}
		return duplicateCore;
	}

	private PredicateArgumentView createPredicateArgumentView(double score,
			Map<Constituent, List<Constituent>> preds,
			Map<Constituent, Pair<String[], double[]>> predArgs) {
		PredicateArgumentView predictedView = new PredicateArgumentView(
				ViewNames.SRL, srlSystem.getSRLSystemIdentifier(), ta, score);

		for (Entry<Constituent, List<Constituent>> entry : preds.entrySet()) {
			Constituent predicate = entry.getKey();
			List<Constituent> args = entry.getValue();

			if (srlSystem.ignorePredicatesWithoutArguments()
					&& args.size() == 0)
				continue;

			Pair<String[], double[]> pair = predArgs.get(predicate);

			predictedView.addPredicateArguments(predicate, args,
					pair.getFirst(), pair.getSecond());
		}

		return predictedView;
	}

}
