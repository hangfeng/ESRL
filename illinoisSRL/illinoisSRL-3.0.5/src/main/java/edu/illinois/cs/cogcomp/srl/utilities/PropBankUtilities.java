package edu.illinois.cs.cogcomp.srl.utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.srl.features.VerbFeatureHelper;

public class PropBankUtilities {

	private final static Logger log = LoggerFactory
			.getLogger(PropBankUtilities.class);

	public static final String[] possibleArguments = { "A0", "A1", "A2", "A3",
			"A4", "A5", "AA", "AM", "AM-ADV", "AM-CAU", "AM-DIR", "AM-DIS",
			"AM-EXT", "AM-LOC", "AM-MNR", "AM-MOD", "AM-NEG", "AM-PNC",
			"AM-PRD", "AM-REC", "AM-TMP", "C-A0", "C-A1", "C-A2", "C-A3",
			"C-AM-ADV", "C-AM-CAU", "C-AM-DIS", "C-AM-EXT", "C-AM-LOC",
			"C-AM-MNR", "R-A0", "R-A1", "R-A2", "R-A3", "R-AA", "R-AM-ADV",
			"R-AM-LOC", "R-AM-MNR", "R-AM-PNC", "R-AM-TMP", "V", "C-V", "null",
			"C-A4", "C-A5", "C-AM-DIR", "C-AM-NEG", "C-AM-PNC", "C-AM-TMP",
			"R-A4", "R-AA", "R-AM-CAU", "R-AM-DIR", "R-AM-EXT" };

	public static final String[] coreArguments = { "A0", "A1", "A2", "A3",
			"A4", "A5", "AA" };

	protected static HashMap<String, LinkedList<String>> legalArguments;

	protected static PropFramesReader frameData;

	public final static Set<String> coreArgumentSet = Collections
			.unmodifiableSet(new HashSet<String>(Arrays.asList(coreArguments)));

	protected static void loadLegalArgs() {
		BufferedReader in = open();
		legalArguments = new HashMap<String, LinkedList<String>>();

		int lineNumber = 0;

		String verb = null;
		boolean first = true;

		for (String line = readLine(in); line != null; line = readLine(in)) {
			line = line.trim();
			if (line.equals(""))
				continue;

			String[] fields = line.split("\\s+");
			assert fields.length == 2 : "Incorrect number of fields on line "
					+ lineNumber + "!";

			String[] args = fields[1].split(",");

			LinkedList<String> legal = new LinkedList<String>();
			legal.add("null");
			for (String arg : args)
				legal.add(arg);

			legalArguments.put(fields[0], legal);
			if (first) {
				first = false;
				verb = fields[0];
			}
		}

		if (verb != null) {
			log.info(
					"Successfully loaded legal arguments. Legal arguments for {} are {}",
					verb, legalArguments.get(verb));
		} else {
			log.error("Error loading legal arguments. Check classpath.");
			System.exit(-1);
		}
	}

	public static LinkedList<String> getLegalArguments(String verb) {

		synchronized (PropBankUtilities.class) {
			if (legalArguments == null) {
				loadLegalArgs();
			}
		}

		LinkedList<String> result = legalArguments.get(verb);

		if (result == null) {
			result = new LinkedList<String>(Arrays.asList(possibleArguments));
			legalArguments.put(verb, result);
		}

		return result;
	}

	public static LinkedList<String> getValidCoreArgsForSense(String lemma,
			String sense) {

		assert sense.startsWith(lemma) : sense + " does not start with "
				+ lemma;

		synchronized (PropBankUtilities.class) {
			if (frameData == null) {
				try {
					frameData = new PropFramesReader("data/pb-frames");
				} catch (Exception e) {
					log.error(
							"Unable to read propbank frames from data/pb-frames",
							e);
					System.exit(-1);
				}
			}
		}

		if (!frameData.getPredicates().contains(lemma)) {
			return new LinkedList<String>(Arrays.asList(coreArguments));
		} else {
			Set<String> argsForSense = frameData.getFrame(lemma)
					.getArgsForSense(sense);

			if (argsForSense.size() == 0) {
				argsForSense = new HashSet<String>(getLegalArguments(lemma));
				argsForSense.retainAll(coreArgumentSet);

				for (String arg : argsForSense)
					frameData.getFrame(lemma).addArgument(sense, arg);
			}

			return new LinkedList<String>(argsForSense);
		}

	}

	protected static BufferedReader open() {

		InputStream stream = VerbFeatureHelper.class
				.getResourceAsStream("verb.legal.arguments");

		BufferedReader in = null;

		try {
			in = new BufferedReader(new InputStreamReader(stream));
		} catch (Exception e) {
			log.error("Unable to read verb legal arguments", e);
			System.exit(-1);
		}

		return in;
	}

	protected static String readLine(BufferedReader in) {
		if (in == null)
			return null;

		String line = null;
		try {
			line = in.readLine();
		} catch (Exception e) {
			log.error("Unable to read verb legal arguments", e);
			System.exit(-1);
		}

		if (line == null) {
			try {
				in.close();
			} catch (Exception e) {
				log.error("Unable to read verb legal arguments", e);
				System.exit(-1);
			}

			in = null;
		}

		return line;
	}

}
