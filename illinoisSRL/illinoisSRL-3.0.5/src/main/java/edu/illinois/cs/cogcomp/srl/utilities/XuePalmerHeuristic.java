package edu.illinois.cs.cogcomp.srl.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LBJ2.learn.Learner;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.edison.features.helpers.ParseHelper;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.utilities.ParseUtils;
import edu.illinois.cs.cogcomp.srl.main.Constants;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.main.VerbSRLSystem;
import edu.illinois.cs.cogcomp.srl.testers.ArgumentIdentifier;

public class XuePalmerHeuristic {

	private static final Logger log = LoggerFactory
			.getLogger(XuePalmerHeuristic.class);

	private static String XuePalmerHeuristicView = "XuePalmerHeuristicView";

	public static List<Constituent> generateCandidatesForPredicate(
			Constituent predicate) {

		Constituent predicateClone = predicate
				.cloneForNewView(XuePalmerHeuristicView);

		TextAnnotation ta = predicate.getTextAnnotation();
		int sentenceId = ta.getSentenceId(predicate);
		Tree<String> tree = ParseHelper.getParseTree(SRLConfig.getInstance()
				.getDefaultParser(), ta, sentenceId);

		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		List<Relation> args = predicate.getOutgoingRelations();

		// identify the spans of all arguments. This step is only for labeling
		// candidates as arguments or not.
		Map<IntPair, String> argSpans = new HashMap<IntPair, String>();
		for (Relation r : args) {
			Constituent arg = r.getTarget();

			argSpans.put(new IntPair(arg.getStartSpan(), arg.getEndSpan()),
					r.getRelationName());
		}

		int sentenceStart = ta.getSentence(sentenceId).getStartSpan();
		int predicatePosition = predicate.getStartSpan() - sentenceStart;

		Set<Constituent> out = new HashSet<Constituent>();

		Tree<Pair<String, IntPair>> predicateTree = spanLabeledTree.getYield()
				.get(predicatePosition);

		Tree<Pair<String, IntPair>> currentNode = predicateTree.getParent();

		boolean done = false;
		boolean first = true;
		while (!done) {
			if (currentNode.isRoot())
				done = true;
			else {

				List<Constituent> candidates = new ArrayList<Constituent>();
				boolean conjunctionFound = false;

				for (Tree<Pair<String, IntPair>> sibling : currentNode
						.getParent().getChildren()) {
					// if this is the lowest level, then ignore conjunctions
					Pair<String, IntPair> siblingNode = sibling.getLabel();
					// do not take the predicate as the argument
					if (siblingNode.getSecond().equals(predicate.getSpan()))
						continue;
					// do not take any constituent including the predicate as an
					// argument
					if ((predicatePosition >= siblingNode.getSecond()
							.getFirst())
							&& (predicate.getEndSpan() <= siblingNode
									.getSecond().getSecond()))
						continue;

					String siblingLabel = siblingNode.getFirst();

					if (first && siblingLabel.equals("CC")) {
						// conjunctionFound = true;
						// break;
					}
					candidates
							.add(getNewConstituent(ta, argSpans, siblingNode
									.getSecond().getFirst() + sentenceStart,
									siblingNode.getSecond().getSecond()
											+ sentenceStart, predicateClone));

					if (siblingLabel.startsWith("PP")) {
						for (Tree<Pair<String, IntPair>> child : sibling
								.getChildren()) {
							candidates.add(getNewConstituent(ta, argSpans,
									child.getLabel().getSecond().getFirst()
											+ sentenceStart, child.getLabel()
											.getSecond().getSecond()
											+ sentenceStart, predicateClone));
						}
					}
				}

				if (!conjunctionFound) {
					out.addAll(candidates);
				}

				first = false;
				currentNode = currentNode.getParent();
			}
		}

		log.debug("Candidates for {} from heuristic: {}", predicate.toString(),
				out.toString());

		return new ArrayList<Constituent>(out);

	}

	public static List<Constituent> generateFilteredCandidatesForPredicate(
			Constituent predicate, Learner srlIdentifier) {

		TextAnnotation ta = predicate.getTextAnnotation();

		if (!ta.hasView(XuePalmerHeuristicView))
			ta.addView(XuePalmerHeuristicView, new PredicateArgumentView(
					XuePalmerHeuristicView, "HeuristicIdentifier", ta, 1.0));

		PredicateArgumentView pav = (PredicateArgumentView) ta
				.getView(XuePalmerHeuristicView);

		List<Constituent> predicatesSoFar = pav.getPredicates();

		for (Constituent p : predicatesSoFar) {
			if (p.getSpan().equals(predicate.getSpan())) {
				List<Constituent> candidates = new ArrayList<Constituent>();
				for (Relation r : pav.getArguments(p))
					candidates.add(r.getTarget());

				return candidates;
			}
		}

		List<Constituent> out = generateCandidatesForPredicate(predicate);

		out = identifierFilter(out, srlIdentifier);

		if (out.size() > 0) {

			// clone the predicate.
			Constituent predicateClone = predicate
					.cloneForNewView(XuePalmerHeuristicView);

			String[] labels = new String[out.size()];
			for (int i = 0; i < out.size(); i++) {
				labels[i] = "ChildOf";
			}
			pav.addPredicateArguments(predicateClone, out, labels,
					new double[out.size()]);
		}

		return out;

	}

	private static List<Constituent> identifierFilter(List<Constituent> out,
			Learner srlIdentifier) {
		ArgumentIdentifier identifier = new ArgumentIdentifier(
				VerbSRLSystem.srlIdThreshold, VerbSRLSystem.srlIdBeta,
				srlIdentifier);

		List<Constituent> cc = new ArrayList<Constituent>();

		for (Constituent c : out) {

			if (identifier.classify(c).getFirst()) {
				cc.add(c.cloneForNewView(XuePalmerHeuristicView));
			}
		}

		return cc;
	}

	private static Constituent getNewConstituent(TextAnnotation ta,
			Map<IntPair, String> argSpans, int start, int end,
			Constituent predicateClone) {

		// System.out.println(start + " " + end);

		IntPair span = new IntPair(start, end);
		boolean isArgument = argSpans.containsKey(span);

		String label = "";
		if (isArgument) {
			label = argSpans.get(span);
		} else {
			label = "null";
		}

		Constituent newConstituent = new Constituent(label, 1.0,
				XuePalmerHeuristicView, ta, start, end);

		newConstituent.addAttribute("ParentTreeId", "0");

		new Relation("ChildOf", predicateClone, newConstituent, 1.0);
		return newConstituent;
	}

}
