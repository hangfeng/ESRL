package edu.illinois.cs.cogcomp.srl.learn;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import LBJ2.learn.BatchTrainer;
import LBJ2.learn.Learner;
import LBJ2.learn.Learner.Parameters;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.srl.data.NomClassifierParser;
import edu.illinois.cs.cogcomp.srl.data.NomIdentifierFilterParser;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentClassifier;
import edu.illinois.cs.cogcomp.srl.learners.NomArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.testers.NomIdentifierThresholdTuner;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

public class NomSRLTrainer extends SRLTrainer {

	private final Set<NomLexClasses> nomLexType;

	public NomSRLTrainer(String conllColumnFormatFile, String devColumnFile,
			String modelsDirectory, Set<NomLexClasses> nomLexType) {
		super(conllColumnFormatFile, devColumnFile, modelsDirectory);
		this.nomLexType = nomLexType;

	}

	@Override
	protected void cvClassifier(Pair<Double, Double> idParams,
			int numClTrainingRounds, String cvFile) throws Exception {

		double threshold = idParams.getFirst();
		double beta = idParams.getSecond();

		NomArgumentIdentifier identfier = new NomArgumentIdentifier(
				getIdModelFile(), this.getIdLexiconFile());

		log.info("Starting cross validation");

		NomClassifierParser cvParser = new NomClassifierParser(cvFile,
				threshold, beta, identfier, this.nomLexType);
		NomArgumentClassifier cvLearner = new NomArgumentClassifier(
				getClModelFile() + ".cv", getClLexiconFile() + ".cv");

		BatchTrainer cvLbj = new BatchTrainer(cvLearner, cvParser,
				progressOutput);

		Parameters params = cvParams(cvLearner, cvParser, modelsDirectory
				+ File.separator + "nom.cl.preextract.cv", cvLbj);

		IOUtils.rm(getClModelFile() + ".cv");
		IOUtils.rm(getClLexiconFile() + ".cv");

		log.info("Pre-extracting classifier");

		NomClassifierParser parser = new NomClassifierParser(
				conllColumnFormatFile, threshold, beta, identfier,
				this.nomLexType);
		NomArgumentClassifier learner = new NomArgumentClassifier(
				getClModelFile(), getClLexiconFile());

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String clTrainPreExtractFile = modelsDirectory + File.separator
				+ "nom.cl.preextract";
		Learner preExtract = preExtract(lbj, clTrainPreExtractFile, 4);

		// learn
		log.info("Starting classifier training");

		learner.setLabelLexicon(preExtract.getLabelLexicon());
		learner.setParameters(params);
		lbj.train(numClTrainingRounds);

		log.info("Finished training");
		log.info("Cleaning up");

		learner.save();
		IOUtils.rm(clTrainPreExtractFile);

		log.info("Finished training classifier");
	}

	@Override
	protected void trainClassifier(Pair<Double, Double> idParams,
			int numClTrainingRounds, double learningRate, double thickness)
			throws IOException {

		NomArgumentIdentifier identfier = new NomArgumentIdentifier(
				getIdModelFile(), this.getIdLexiconFile());

		log.info("Pre-extracting classifier");

		double threshold = idParams.getFirst();
		double beta = idParams.getSecond();
		NomClassifierParser parser = new NomClassifierParser(
				conllColumnFormatFile, threshold, beta, identfier,
				this.nomLexType);
		NomArgumentClassifier learner = new NomArgumentClassifier(
				getClModelFile(), getClLexiconFile());

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String clTrainPreExtractFile = modelsDirectory + File.separator
				+ "nom.cl.preextract";
		Learner preExtract = preExtract(lbj, clTrainPreExtractFile, 4);

		// learn
		log.info("Starting classifier training");

		learner.setLabelLexicon(preExtract.getLabelLexicon());
		setLearnerParameters(learner, learningRate, thickness);
		lbj.train(numClTrainingRounds);

		log.info("Finished training");
		log.info("Cleaning up");

		learner.save();
		IOUtils.rm(clTrainPreExtractFile);

		log.info("Finished training classifier");
	}

	@Override
	protected Pair<Double, Double> tuneIdentifier() {
		NomArgumentIdentifier identfier = new NomArgumentIdentifier(
				getIdModelFile(), this.getIdLexiconFile());

		NomIdentifierThresholdTuner tuner = new NomIdentifierThresholdTuner(
				identfier);

		return tuner.tuneThreshold(devColumnFile, 0.01, 0.01, 0.5, 0.01, 0.01,
				0.5);
	}

	@Override
	protected void trainIdentifier(int idNumTrainingRounds,
			double learningRate, double thickness) throws Exception {

		// first preExtract

		log.info("Preextracting identifier");
		Learner learner = new NomArgumentIdentifier(getIdModelFile(),
				getIdLexiconFile());
		NomIdentifierFilterParser parser = new NomIdentifierFilterParser("",
				conllColumnFormatFile, this.nomLexType);

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String idTrainPreExtractFile = modelsDirectory + File.separator
				+ "nom.id.preextract";
		Learner preExtract = preExtract(lbj, idTrainPreExtractFile, 3);

		// learn

		log.info("Starting identifier training");
		learner.setLabelLexicon(preExtract.getLabelLexicon());
		setLearnerParameters(learner, learningRate, thickness);
		lbj.train(idNumTrainingRounds);

		log.info("Finished training");
		// cleanup

		log.info("Cleaning up.");

		learner.save();
		IOUtils.rm(idTrainPreExtractFile);

		log.info("Finished training");

	}

	@Override
	protected void cvIdentifier(int idNumTrainingRounds, String cvFile)
			throws Exception {

		Learner cvLearner = new NomArgumentIdentifier(getIdModelFile() + ".cv",
				getIdLexiconFile() + ".cv");
		NomIdentifierFilterParser cvParser = new NomIdentifierFilterParser("",
				cvFile, this.nomLexType);
		BatchTrainer cvLbj = new BatchTrainer(cvLearner, cvParser,
				progressOutput);

		Parameters params = cvParams(cvLearner, cvParser, modelsDirectory
				+ File.separator + "nom.id.preextract.cv", cvLbj);

		IOUtils.rm(getIdModelFile() + ".cv");
		IOUtils.rm(getIdLexiconFile() + ".cv");

		// first preExtract

		log.info("Preextracting identifier");
		Learner learner = new NomArgumentIdentifier(getIdModelFile(),
				getIdLexiconFile());
		NomIdentifierFilterParser parser = new NomIdentifierFilterParser("",
				conllColumnFormatFile, this.nomLexType);

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String idTrainPreExtractFile = modelsDirectory + File.separator
				+ "nom.id.preextract";
		Learner preExtract = preExtract(lbj, idTrainPreExtractFile, 3);

		// learn

		log.info("Starting identifier training");
		learner.setLabelLexicon(preExtract.getLabelLexicon());
		learner.setParameters(params);

		lbj.train(idNumTrainingRounds);

		log.info("Finished training");
		// cleanup

		log.info("Cleaning up.");

		learner.save();
		IOUtils.rm(idTrainPreExtractFile);

		log.info("Finished training");

	}

	@Override
	protected String getClassifierName() {
		return NomArgumentClassifier.class.getSimpleName();
	}

	@Override
	protected String getIdentifierName() {
		return NomArgumentIdentifier.class.getSimpleName();
	}
}
