/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.srl.data.XuePalmerIdentifierFilterParser;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentIdentifierLabel;
import edu.illinois.cs.cogcomp.srl.main.VerbSRLSystem;

/**
 * @author Vivek Srikumar
 * 
 */
public class IdentifierThresholdTuner {

	private final VerbArgumentIdentifier identifier;
	private final double n_F;

	/**
	 * @param identifier
	 */
	public IdentifierThresholdTuner(VerbArgumentIdentifier identifier,
			double N_F) {
		this.identifier = identifier;
		n_F = N_F;
	}

	public Pair<Double, Double> tuneThreshold(String testFile, double thStart,
			double thStep, double thEnd, double beStart, double beStep,
			double beEnd) {

		XuePalmerIdentifierFilterParser parser = new XuePalmerIdentifierFilterParser(
				"", testFile);
		VerbArgumentIdentifierLabel goldLabeler = new VerbArgumentIdentifierLabel();

		Map<Pair<Double, Double>, ArgumentIdentifier> identifiers = new HashMap<Pair<Double, Double>, ArgumentIdentifier>();
		for (double threshold = thStart; threshold < thEnd; threshold += thStep) {
			for (double beta = beStart; beta < beEnd; beta += beStep) {

				identifiers.put(new Pair<Double, Double>(threshold, beta),
						new ArgumentIdentifier(threshold, beta, null));
			}
		}

		System.out.println("Searching over " + identifiers.size()
				+ " parameters");

		int numExamples = 0;
		Counter<String> counter = new Counter<String>();

		for (Constituent c : parser) {

			if (c == null)
				break;

			boolean goldLabel = Boolean.parseBoolean(goldLabeler
					.discreteValue(c));

			counter.incrementCount("TOTAL");
			counter.incrementCount("TOTAL" + goldLabel);

			double rawScore = identifier.scores(c).get("true");

			// System.out.println(rawScore + "\t" + goldLabel);

			for (Pair<Double, Double> key : identifiers.keySet()) {
				Pair<Boolean, Double> prediction = identifiers.get(key)
						.thresholdScore(rawScore);

				counter.incrementCount(key + "TOTAL" + prediction.getFirst());
				if (prediction.getFirst() == goldLabel) {
					counter.incrementCount(key + "CORRECT");
					counter.incrementCount(key + "CORRECT" + goldLabel);
				}
			}

			numExamples++;
			if (numExamples % 1000 == 0) {
				System.out.println(numExamples + " examples complete");
			}
		}

		double totalGold = counter.getCount("TOTALtrue");

		double maxF = Double.NEGATIVE_INFINITY;
		Pair<Double, Double> maxer = null;

		System.out
				.println("(Threshold, beta)\ttotalGold\ttotalPredicted\tcorrect\tP\tR\tF"
						+ n_F);

		for (Pair<Double, Double> key : identifiers.keySet()) {
			double totalPredicted = counter.getCount(key + "TOTALtrue");
			double correct = counter.getCount(key + "CORRECTtrue");

			double precision = 0, recall = 0, f = 0;

			if (totalPredicted > 0)
				precision = correct / totalPredicted;

			if (totalGold > 0)
				recall = correct / totalGold;

			f = fN(precision, recall, n_F);

			String output = key.toString();
			output += "\t" + (int) (totalGold);
			output += "\t" + (int) (totalPredicted);
			output += "\t" + (int) (correct);

			output += "\t"
					+ StringUtils.getFormattedTwoDecimal(precision * 100);
			output += "\t" + StringUtils.getFormattedTwoDecimal(recall * 100);
			output += "\t" + StringUtils.getFormattedTwoDecimal(f * 100);
			System.out.println(output);

			if (f > maxF) {
				maxF = f;
				maxer = key;
			}
		}

		System.out.println();
		System.out.println("Based on F" + n_F
				+ " measure, recommended (threshold, beta) = " + maxer);

		return maxer;
	}

	private final static double fN(double precision, double recall, double n) {
		double denom = n * n * precision + recall;
		double num = (n * n + 1) * precision * recall;

		if (denom == 0)
			return 0;

		return num / denom;

	}

	public static void main(String[] args) throws FileNotFoundException {
		try {
			if (args.length != 3)
				throw new Exception();
		} catch (Exception ex) {
			System.err
					.println("Usage: TestIdentifierThresholds file thresholdStart:thresholdStep:thresholdEnd betaStart:betaStep:betaEnd");

			System.exit(-1);

		}

		String file = args[0];
		String th = args[1];
		String be = args[2];

		String[] parts = th.split(":");
		assert parts.length == 3;

		double thStart = Double.parseDouble(parts[0]);
		double thStep = Double.parseDouble(parts[1]);
		double thEnd = Double.parseDouble(parts[2]);

		parts = be.split(":");
		assert parts.length == 3;

		double beStart = Double.parseDouble(parts[0]);
		double beStep = Double.parseDouble(parts[1]);
		double beEnd = Double.parseDouble(parts[2]);

		VerbSRLSystem srlSystem = VerbSRLSystem.getInstance();

		VerbArgumentIdentifier identifier = (VerbArgumentIdentifier) srlSystem
				.getIdentifier();

		IdentifierThresholdTuner tuner = new IdentifierThresholdTuner(
				identifier, 2.0);

		tuner.tuneThreshold(file, thStart, thStep, thEnd, beStart, beStep,
				beEnd);

	}
}
