// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B880000000000000005B7DFDF42C0301700FF75E2C3D6158082CB8889C489A4236E23E74C7D56B34B90476ADE20A6CFFDDBA2E89197CBEBC2B4BDBF6F3DBEE5697925B159909602E27B92EA151ACC15B7B0CDB904AC2D4664492143A43672C78F08848630FAA0B019766053C806E964B43263AB3FC8682629A7ECBFDAB37B1F11EE34964DB576516A7B144EF0DE060AE9BAD2DAA9614F01B618A04C1F574021777A799745F6DD975A8CC6AED47A505557780BB711B140FE4C4F5734FD1D2ED1D3FDE9B6C6B712C454BA1469B5A41FA134E73B88EC97228904FDDB9CE06D1762B015C1A74D8334D3EF08592A2019350A4454327726368EA0A8369ED59AAED2C326D973AD34B12EE9204FF1B764D610FEF32CFF368D46A5FF4F133F1EAEA0A596B38E6B06473C819418CC2CE17B0B7CD2CB0E61E507B0F29B5879CD2CE37B0BFCD2C107B070CD2C08B58107B076DE062B2E65EF482BB47C1C29F5A6349D5A3996ACF25B1AE04A37712D93FB43C7A50FB4D682BB43A7C13FB4D682BB4761B38FE4D682FB4F172E04A41ACE2D83CB7E79AD05679621B38FE4D682FB435707BF6349D5A968E0A7A6349D5AB891F34923D18331710D484AFFFFE1FCEC680F90F9F504AE82A1629F00000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomReferences extends ParameterizedConstraint
{
  private static final NomArgumentClassifier __NomArgumentClassifier = new NomArgumentClassifier();

  public NomReferences() { super("edu.illinois.cs.cogcomp.srl.learners.NomReferences"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomReferences(TextAnnotation)' defined on line 73 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-A0"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A0"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-A1"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A1"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-A2"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A2"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-A3"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A3"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-A4"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A4"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-A5"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A5"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AA"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AA"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-ADV"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-ADV"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-CAU"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-CAU"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-DIR"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIR"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-DIS"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIS"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-EXT"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-EXT"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-LOC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-LOC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-MNR"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-MNR"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-MOD"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-MOD"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-NEG"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-NEG"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-PNC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-PNC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-PRD"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-PRD"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-REC"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-REC"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-TM"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-TM"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          boolean LBJ2$constraint$result$1;
          {
            LBJ2$constraint$result$1 = false;
            for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$1; )
            {
              Constituent a = (Constituent) __I0.next();
              LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("R-AM-TMP"));
            }
          }
          if (LBJ2$constraint$result$1)
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I0.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("AM-TMP"));
              }
            }
          else LBJ2$constraint$result$0 = true;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomReferences(TextAnnotation)' defined on line 73 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomReferences".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomReferences; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomReferences(TextAnnotation)' defined on line 73 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-A0"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A0"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-A1"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A1"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-A2"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A2"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-A3"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A3"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-A4"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A4"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-A5"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A5"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AA"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AA"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-ADV"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-ADV"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-CAU"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-CAU"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-DIR"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-DIR"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-DIS"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-DIS"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-EXT"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-EXT"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-LOC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-LOC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-MNR"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-MNR"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-MOD"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-MOD"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-NEG"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-NEG"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-PNC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-PNC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-PRD"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-PRD"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-REC"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-REC"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-TM"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-TM"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("R-AM-TMP"), LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          FirstOrderConstraint LBJ2$constraint$result$3 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$4 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context, true)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }
                };
              LBJ2$constraint$result$4 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("AM-TMP"), LBJ$EAR);
            }
            LBJ2$constraint$result$3 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$4);
          }
          LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$3);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      currentPredicateId++;
    }

    return __result;
  }
}

