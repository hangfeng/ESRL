package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Lexicon;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.features.FeatureExtractor;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.sl.core.IInstance;
import edu.illinois.cs.cogcomp.sl.util.FeatureVector;

public class ClauseInstance implements IInstance {

	public final TextAnnotation ta;
	private final Set<IntPair> candidateSpans;

	private final Map<IntPair, FeatureVector> candidateFeatures;

	public ClauseInstance(ClauseManager manager, TextAnnotation ta)
			throws Exception {
		this.ta = ta;

		Lexicon lexicon = manager.getLexicon();
		FeatureExtractor fex = manager.getFeatureExtractor();

		candidateSpans = ClauseCandidateHeuristic.getClauseCandidates(ta);
		candidateFeatures = new HashMap<IntPair, FeatureVector>();

		for (IntPair span : candidateSpans) {
			Constituent c = new Constituent("", "", ta, span.getFirst(),
					span.getSecond());

			Set<Feature> features = fex.getFeatures(c);

			Map<String, Float> featureMap = new HashMap<String, Float>();
			for (Feature f : features) {
				featureMap.put(f.getName(), f.getValue());
				if (manager.trainingMode && !lexicon.contains(f.getName()))
					lexicon.previewFeature(f.getName());
			}

			Pair<int[], float[]> feats = lexicon.getFeatureVector(featureMap);

			candidateFeatures.put(span, new FeatureVector(feats.getFirst(),
					feats.getSecond()));
		}

	}

	@Override
	public double size() {
		return 1;
	}

	public boolean isValidSpan(int s, int t) {
		return candidateSpans.contains(new IntPair(s, t));
	}

	public FeatureVector getFeatures(int s, int t) {
		return candidateFeatures.get(new IntPair(s, t));
	}

}
