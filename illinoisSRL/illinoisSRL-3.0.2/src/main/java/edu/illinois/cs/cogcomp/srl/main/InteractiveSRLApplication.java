package edu.illinois.cs.cogcomp.srl.main;

import java.io.Console;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.edison.data.curator.CuratorClient;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;

public class InteractiveSRLApplication implements Runnable {

	private static final Logger log = LoggerFactory
			.getLogger(InteractiveSRLApplication.class);

	private final SRLConfig config;
	private SRLSystem srlSystem;

	public InteractiveSRLApplication(SRLSystem srlSystem, SRLConfig config) {
		this.config = config;

		this.srlSystem = srlSystem;
	}

	@Override
	public void run() {

		boolean done = false;

		log.info("Creating curator client");

		CuratorClient curator = new CuratorClient(config.getCuratorHost(),
				config.getCuratorPort());

		boolean charniak = config.getDefaultParser().equals(
				ViewNames.PARSE_CHARNIAK);

		log.info("Default parser = {}", charniak ? "Charniak" : "Stanford");

		boolean forceUpdate = false;
		Console console = System.console();

		do {
			System.out.print("Enter text (_ to quit): ");
			String line = console.readLine();
			line = line.trim();

			if (line.length() == 0)
				continue;

			if (line.equals("_"))
				done = true;
			else {

				try {
					TextAnnotation ta = getTextAnnotationFromCurator(curator,
							charniak, forceUpdate, line);

					PredicateArgumentView predicateArgumentView = srlSystem
							.getSRL(ta, config.doBeamSearch());

					System.out.println(predicateArgumentView);

					System.out.println();

				} catch (Exception ex) {
					System.out.println(ex.getMessage());
					ex.printStackTrace();
				}
			}

		} while (!done);
	}

	private TextAnnotation getTextAnnotationFromCurator(CuratorClient curator,
			boolean charniak, boolean forceUpdate, String line)
			throws ServiceUnavailableException, AnnotationFailedException,
			TException {
		TextAnnotation ta = curator
				.getTextAnnotation("", "", line, forceUpdate);

		if (charniak)
			curator.addCharniakParse(ta, forceUpdate);
		else
			curator.addStanfordParse(ta, forceUpdate);

		curator.addPOSView(ta, forceUpdate);
		curator.addChunkView(ta, forceUpdate);
		curator.addNamedEntityView(ta, forceUpdate);

		ta.addView(ViewNames.PSEUDO_PARSE, SRLUtils.createPsuedoParseView(ta));

		return ta;
	}

}
