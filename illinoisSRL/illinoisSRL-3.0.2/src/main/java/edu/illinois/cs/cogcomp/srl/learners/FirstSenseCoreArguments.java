// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B880000000000000005725B4B43C0401EFB2387AD0592A7D6DE1A60A858025A5D2E57D46CA38B9DD23B31F98FFDD9D4F59A6DB4803BFDB6E1560F158D2971890174950AF889706C13FAA9A1DB443778FE236FE38851A0E12A611D79891C714141506DC8515965032C80613F2EE58C5CEFA05698CF83BDF3A9D317809CEAC6895B2B70C4B25AF5C0121B676E3DA4F34F448C0478FD1187C7BF707645DFDE99C1289316075D90BDF84F9862B4D64E968042D82A1E55565D1C47A695795AD7D905657D676167B1827BEB2AA6B3397860766D5D8C738D0B2A8A4551F8CAFC312728A2370CF420FE57D4A037ED96D5D6612C4E750E0BEADAA67244AC4361DAF363AC9C3CD6154E1C535B75DAADACC1D658CDF22136AD59A28CFB065D6EE75BE8AADEAD35483CD28A7CC83CAFADAF794B8521ADC0169FB81D28D4B1BEC7B9EA7076DFB8BC3357A7A0C6D9BD5679F853AE90089FFD5330C77DC51F86536B52DFD1176747B09B3B13E6E61C6603814B673C317E6DBED01EB1EBF70F5DD781120300000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class FirstSenseCoreArguments extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public FirstSenseCoreArguments() { super("edu.illinois.cs.cogcomp.srl.learners.FirstSenseCoreArguments"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'FirstSenseCoreArguments(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      String lemma = verb.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);
      LinkedList validCoreArgsForFirstSense = PropBankUtilities.getValidCoreArgsForSense(lemma, lemma + ".01");
      {
        boolean LBJ2$constraint$result$0;
        {
          LBJ2$constraint$result$0 = true;
          for (java.util.Iterator __I0 = (validCoreArgsForFirstSense).iterator(); __I0.hasNext() && LBJ2$constraint$result$0; )
          {
            String type = (String) __I0.next();
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I1 = (argumentCandidates).iterator(); __I1.hasNext() && !LBJ2$constraint$result$0; )
              {
                Constituent a = (Constituent) __I1.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + (type));
              }
            }
          }
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'FirstSenseCoreArguments(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "FirstSenseCoreArguments".hashCode(); }
  public boolean equals(Object o) { return o instanceof FirstSenseCoreArguments; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'FirstSenseCoreArguments(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      String lemma = verb.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);
      LinkedList validCoreArgsForFirstSense = PropBankUtilities.getValidCoreArgsForSense(lemma, lemma + ".01");
      {
        Object[] LBJ$constraint$context = new Object[2];
        LBJ$constraint$context[0] = validCoreArgsForFirstSense;
        LBJ$constraint$context[1] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(1);
                    return a;
                  }

                  public String getRightValue()
                  {
                    String type = (String) quantificationVariables.get(0);
                    return "" + (type);
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), null, LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("a", argumentCandidates, LBJ2$constraint$result$2);
          }
          LBJ2$constraint$result$0 = new UniversalQuantifier("type", validCoreArgsForFirstSense, LBJ2$constraint$result$1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      currentPredicateId++;
    }

    return __result;
  }
}

