// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000D5EC1BA02C0401481E759D2C2265E308D42A6324880985ADCA7C81F429DD077B72288FEEA10149B6FFF6A817A21D20B713AEADA7F27140883C60C3670E0EEAB889EC738333D2E88785522A6C6E558224C6EDE29E94563A78B32C0C35C2E7D7572A2B64FC3451AF43E727E8D8EE2D43877C68C96BA26E52DC127A61FD79F70B6A7DB1DFC231C1BC000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class SRLInferenceBeamSearch$subjectto extends ParameterizedConstraint
{
  private static final LegalArguments __LegalArguments = new LegalArguments();
  private static final References __References = new References();
  private static final Continuences __Continuences = new Continuences();
  private static final NoDuplicates __NoDuplicates = new NoDuplicates();
  private static final NoOverlaps __NoOverlaps = new NoOverlaps();

  public SRLInferenceBeamSearch$subjectto() { super("edu.illinois.cs.cogcomp.srl.learners.SRLInferenceBeamSearch$subjectto"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'SRLInferenceBeamSearch$subjectto(TextAnnotation)' defined on line 288 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    {
      boolean LBJ2$constraint$result$0;
      {
        boolean LBJ2$constraint$result$1;
        {
          boolean LBJ2$constraint$result$2;
          {
            boolean LBJ2$constraint$result$3;
            {
              boolean LBJ2$constraint$result$4;
              LBJ2$constraint$result$4 = __NoOverlaps.discreteValue(sentence).equals("true");
              if (LBJ2$constraint$result$4)
                LBJ2$constraint$result$3 = __LegalArguments.discreteValue(sentence).equals("true");
              else LBJ2$constraint$result$3 = false;
            }
            if (LBJ2$constraint$result$3)
              LBJ2$constraint$result$2 = __NoDuplicates.discreteValue(sentence).equals("true");
            else LBJ2$constraint$result$2 = false;
          }
          if (LBJ2$constraint$result$2)
            LBJ2$constraint$result$1 = __Continuences.discreteValue(sentence).equals("true");
          else LBJ2$constraint$result$1 = false;
        }
        if (LBJ2$constraint$result$1)
          LBJ2$constraint$result$0 = __References.discreteValue(sentence).equals("true");
        else LBJ2$constraint$result$0 = false;
      }
      if (!LBJ2$constraint$result$0) return "false";
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'SRLInferenceBeamSearch$subjectto(TextAnnotation)' defined on line 288 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "SRLInferenceBeamSearch$subjectto".hashCode(); }
  public boolean equals(Object o) { return o instanceof SRLInferenceBeamSearch$subjectto; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'SRLInferenceBeamSearch$subjectto(TextAnnotation)' defined on line 288 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    {
      FirstOrderConstraint LBJ2$constraint$result$0 = null;
      {
        FirstOrderConstraint LBJ2$constraint$result$1 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$2 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$3 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$4 = null;
              LBJ2$constraint$result$4 = __NoOverlaps.makeConstraint(sentence);
              FirstOrderConstraint LBJ2$constraint$result$5 = null;
              LBJ2$constraint$result$5 = __LegalArguments.makeConstraint(sentence);
              LBJ2$constraint$result$3 = new FirstOrderConjunction(LBJ2$constraint$result$4, LBJ2$constraint$result$5);
            }
            FirstOrderConstraint LBJ2$constraint$result$6 = null;
            LBJ2$constraint$result$6 = __NoDuplicates.makeConstraint(sentence);
            LBJ2$constraint$result$2 = new FirstOrderConjunction(LBJ2$constraint$result$3, LBJ2$constraint$result$6);
          }
          FirstOrderConstraint LBJ2$constraint$result$7 = null;
          LBJ2$constraint$result$7 = __Continuences.makeConstraint(sentence);
          LBJ2$constraint$result$1 = new FirstOrderConjunction(LBJ2$constraint$result$2, LBJ2$constraint$result$7);
        }
        FirstOrderConstraint LBJ2$constraint$result$8 = null;
        LBJ2$constraint$result$8 = __References.makeConstraint(sentence);
        LBJ2$constraint$result$0 = new FirstOrderConjunction(LBJ2$constraint$result$1, LBJ2$constraint$result$8);
      }
      __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
    }

    return __result;
  }
}

