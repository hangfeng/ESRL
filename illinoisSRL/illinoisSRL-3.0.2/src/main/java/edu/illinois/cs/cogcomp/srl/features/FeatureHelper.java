/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.features;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import edu.illinois.cs.cogcomp.core.algorithms.Mappers;
import edu.illinois.cs.cogcomp.core.datastructures.IQueryable;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.QueryableList;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.core.transformers.ITransformer;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.features.helpers.ParseHelper;
import edu.illinois.cs.cogcomp.edison.features.helpers.SpanLabelsHelper;
import edu.illinois.cs.cogcomp.edison.features.helpers.WordHelpers;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Queries;
import edu.illinois.cs.cogcomp.edison.sentences.SpanLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.TreeView;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.CollinsHeadFinder;
import edu.illinois.cs.cogcomp.edison.utilities.ParseUtils;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;

/**
 * @author Vivek Srikumar
 */
@SuppressWarnings("serial")
public class FeatureHelper {

	private static ITransformer<Tree<Pair<String, IntPair>>, String> transformer;
	private static CollinsHeadFinder headFinder = new CollinsHeadFinder();

	static {
		try {

			transformer = new ITransformer<Tree<Pair<String, IntPair>>, String>() {

				@Override
				public String transform(Tree<Pair<String, IntPair>> input) {
					return input.getLabel().getFirst();
				}
			};
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		}

	}

	public static Tree<Pair<String, IntPair>> getArgumentTree(
			Constituent argument) {

		Tree<Pair<String, IntPair>> tree = ParseHelper
				.getTokenIndexedCleanedParseTreeNodeCovering(argument,
						SRLConfig.getInstance().getDefaultParser());

		return tree;

		// return ParseHelper.getTokenIndexedParseTreeNodeCovering(SRLConfig
		// .getInstance().getDefaultParser(), argument);
	}

	// /**
	// * @param argument
	// * @return
	// */
	// private static Tree<String> getHead(Tree<Pair<String, IntPair>>
	// argumentTree) {
	// // final Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);
	// return getHead(argumentTree);
	// }

	private static Tree<String> getHead(Tree<Pair<String, IntPair>> tree) {
		final Tree<String> t1 = Mappers.mapTree(tree, transformer);

		final Tree<String> head = headFinder.getHeadWord(t1);
		return head;
	}

	// here, we assume that the argument candidate is a target of a relation
	// from the current predicate. All of this is created by the
	// NomIdentifierFilterParser and does not reflect upon the actual
	// TextAnnotation
	protected static Constituent getPredicateConstituent(
			Constituent argumentCandidate) {
		return argumentCandidate.getIncomingRelations().get(0).getSource();
	}

	protected static int getPredicatePosition(Constituent argumentCandidate) {
		return getPredicateConstituent(argumentCandidate).getStartSpan();
	}

	public static String getPredicateLemma(Constituent argument) {
		Constituent predicate = getPredicateConstituent(argument);

		return predicate.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier)
				.toLowerCase();
	}

	public static String getPredicatePOS(Constituent argument) {
		int predicateId = getPredicatePosition(argument);

		return WordHelpers.getPOS(argument.getTextAnnotation(), predicateId);
	}

	protected static int getConstituentSentenceStart(Constituent candidate) {
		return candidate.getTextAnnotation()
				.getSentence(candidate.getSentenceId()).getStartSpan();
	}

	public static String getPhraseType(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while ((argumentTree.getParent() != null)
				&& (argumentTree.getParent().getYield().size() == argument
						.getNumberOfTokens()))
			argumentTree = argumentTree.getParent();

		if ((argumentTree.getNumberOfChildren() == 1)
				&& (argumentTree.getChild(0).isLeaf()))
			argumentTree = argumentTree.getParent();

		return argumentTree.getLabel().getFirst();
	}

	public static String getParentPhraseType(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		if (argumentTree.isRoot())
			return "ROOT";

		argumentTree = argumentTree.getParent();
		return argumentTree.getLabel().getFirst();
	}

	public static String getParentHeadWord(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		if (argumentTree.isRoot())
			return "ROOT";

		argumentTree = argumentTree.getParent();
		return getHead(argumentTree).getLabel().toLowerCase();
	}

	public static String getNamedEntity(Constituent argument) {
		SpanLabelView sv = (SpanLabelView) argument.getTextAnnotation()
				.getView(ViewNames.NER);
		List<String> cs = sv.getLabelsCovering(argument);
		String s = StringUtils.join(cs.toArray(), "");
		return s;
	}

	public static String getLeftSisterPhraseType(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		int position = argumentTree.getPositionAmongParentsChildren();

		if (argumentTree.isRoot())
			return "ROOT";

		argumentTree = argumentTree.getParent();
		if (argumentTree.getNumberOfChildren() == 1)
			return "ONLY_CHILD";
		else if (position == 0)
			return "FIRST_CHILD";
		else
			return argumentTree.getChild(position - 1).getLabel().getFirst();
	}

	public static String getLeftSisterHeadWord(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		if (argumentTree.isRoot())
			return "ROOT";

		int position = argumentTree.getPositionAmongParentsChildren();

		argumentTree = argumentTree.getParent();
		if (argumentTree.getNumberOfChildren() == 1)
			return "ONLY_CHILD";
		else if (position == 0)
			return "LAST_CHILD";
		else
			return getHead(argumentTree.getChild(position - 1)).getLabel()
					.toLowerCase();
	}

	public static String getLeftSisterHeadPOS(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		if (argumentTree.isRoot())
			return "ROOT";

		int position = argumentTree.getPositionAmongParentsChildren();

		argumentTree = argumentTree.getParent();
		if (argumentTree.getNumberOfChildren() == 1)
			return "ONLY_CHILD";
		else if (position == 0)
			return "LAST_CHILD";
		else
			return getHead(argumentTree.getChild(position - 1)).getParent()
					.getLabel();
	}

	public static String getRightSisterPhraseType(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		int position = argumentTree.getPositionAmongParentsChildren();

		if (argumentTree.isRoot())
			return "ROOT";

		argumentTree = argumentTree.getParent();
		if (argumentTree.getNumberOfChildren() == 1)
			return "ONLY_CHILD";
		else if (position == argumentTree.getNumberOfChildren() - 1)
			return "FIRST_CHILD";
		else
			return argumentTree.getChild(position + 1).getLabel().getFirst();
	}

	public static String getRightSisterHeadWord(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		if (argumentTree.isRoot())
			return "ROOT";

		int position = argumentTree.getPositionAmongParentsChildren();

		argumentTree = argumentTree.getParent();
		if (argumentTree.getNumberOfChildren() == 1)
			return "ONLY_CHILD";
		else if (position == argumentTree.getNumberOfChildren() - 1)
			return "LAST_CHILD";
		else
			return getHead(argumentTree.getChild(position + 1)).getLabel()
					.toLowerCase();
	}

	public static String getRightSisterHeadPOS(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> tree = getArgumentTree(argument);

		while (argumentTree.getNumberOfChildren() == 1)
			argumentTree = argumentTree.getChild(0);

		if (argumentTree.isRoot())
			return "ROOT";

		int position = argumentTree.getPositionAmongParentsChildren();

		argumentTree = argumentTree.getParent();
		if (argumentTree.getNumberOfChildren() == 1)
			return "ONLY_CHILD";
		else if (position == argumentTree.getNumberOfChildren() - 1)
			return "LAST_CHILD";
		else
			return getHead(argumentTree.getChild(position + 1)).getParent()
					.getLabel();
	}

	public static Pair<String, String> getHeadWordPOS(
			Tree<Pair<String, IntPair>> tree) {
		Tree<String> head = getHead(tree);

		return new Pair<String, String>(head.getLabel(), head.getParent()
				.getLabel());
	}

	public static String getLinearPosition(Constituent argument) {
		int predicateId = getPredicatePosition(argument);

		int start = argument.getStartSpan();

		if (start < predicateId)
			return "before";
		else if (start > predicateId)
			return "after";
		else
			return "contains";
	}

	public static String getPathToPredicate(Constituent argument) {

		int sentenceId = argument.getSentenceId();
		Tree<String> tree = getParseTree(argument.getTextAnnotation(),
				sentenceId, argument);

		tree = ParseUtils.stripFunctionTags(tree);

		int sentenceStart = getConstituentSentenceStart(argument);
		int predicateId = getPredicatePosition(argument) - sentenceStart;

		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		Tree<Pair<String, IntPair>> predicateTree = spanLabeledTree.getYield()
				.get(predicateId).getParent().getParent();

		try {
			Tree<Pair<String, IntPair>> argBeginTree = spanLabeledTree
					.getYield().get(argument.getStartSpan() - sentenceStart);
			Tree<Pair<String, IntPair>> argEndTree = spanLabeledTree.getYield()
					.get(argument.getEndSpan() - 1 - sentenceStart);

			Tree<Pair<String, IntPair>> argTree;

			if (argument.getNumberOfTokens() == 1)
				argTree = argBeginTree.getParent().getParent();

			else
				argTree = ParseHelper.getCommonAncestor(argBeginTree,
						argEndTree, spanLabeledTree);

			while ((!argTree.getParent().isRoot())
					&& (argTree.getParent().getYield().size() == argument
							.getNumberOfTokens()))
				argTree = argTree.getParent();

			Pair<List<Tree<Pair<String, IntPair>>>, List<Tree<Pair<String, IntPair>>>> path = ParseHelper
					.getPath(argTree, predicateTree, spanLabeledTree, 400);

			StringBuffer buffer = new StringBuffer();
			List<Tree<Pair<String, IntPair>>> upPath = path.getFirst();
			for (Tree<Pair<String, IntPair>> up : upPath) {
				buffer.append(up.getLabel().getFirst()
						+ ParseHelper.PATH_UP_STRING);
			}

			List<Tree<Pair<String, IntPair>>> downPath = path.getSecond();

			int count = 0;

			for (Tree<Pair<String, IntPair>> down : downPath) {
				if (count != 0)
					buffer.append(down.getLabel().getFirst()
							+ ParseHelper.PATH_DOWN_STRING);
				count++;
			}

			return buffer.toString();
			// return ParseHelper.getSpanLabeledPathString(argTree,
			// predicateTree,
			// spanLabeledTree, 400);
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static int getPathLength(Constituent argument) {

		int argumentPosition = argument.getStartSpan()
				- getConstituentSentenceStart(argument);

		Tree<String> tree = getParseTree(argument.getTextAnnotation(),
				argument.getSentenceId(), argument);

		int predicateId = getPredicatePosition(argument)
				- getConstituentSentenceStart(argument);

		Tree<String> predicateTree = tree.getYield().get(predicateId)
				.getParent().getParent();

		Tree<String> argTree = tree.getYield().get(argumentPosition)
				.getParent().getParent();

		try {
			Pair<List<Tree<String>>, List<Tree<String>>> path = ParseHelper
					.getPath(argTree, predicateTree, tree, 400);

			return path.getFirst().size() + path.getSecond().size();
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	protected static Tree<String> getParseTree(TextAnnotation ta,
			int sentenceId, Constituent candidate) {
		List<View> parses = ta.getTopKViews(SRLConfig.getInstance()
				.getDefaultParser());

		int viewId = 0;
		if (candidate.hasAttribute("ParentTreeId"))
			viewId = Integer.parseInt(candidate.getAttribute("ParentTreeId"));

		return ((TreeView) parses.get(viewId)).getTree(sentenceId);
	}

	public static String getSubcatFrame(Constituent argument,
			Tree<Pair<String, IntPair>> argumentTree) {
		// Tree<Pair<String, IntPair>> argNode = getArgumentTree(argument);
		Tree<Pair<String, IntPair>> argParentNode;

		StringBuffer subcat = new StringBuffer();
		if (argumentTree.getParent() == null) {
			return "ROOT";
		} else
			argParentNode = argumentTree.getParent();

		subcat.append(argParentNode.getLabel().getFirst() + "--->");

		for (Tree<Pair<String, IntPair>> t : argParentNode.getChildren())
			if (t == argumentTree)
				subcat.append("(" + t.getLabel().getFirst() + ")" + "-");
			else
				subcat.append(t.getLabel().getFirst() + "-");

		return subcat.toString();
	}

	private static String getWord(TextAnnotation ta, int position) {
		if (position >= 0 && position < ta.size())
			return ta.getToken(position);
		else
			return "";
	}

	private static String getPOS(TextAnnotation ta, int position) {
		if (position >= 0 && position < ta.size())
			return WordHelpers.getPOS(ta, position);
		else
			return "";
	}

	public static String[] getArgumentWordsAndTags(Constituent argument) {
		TextAnnotation ta = argument.getTextAnnotation();

		String[] out = new String[4];
		int position = argument.getStartSpan();

		out[0] = "0:" + getWord(ta, position);
		out[1] = "0T:" + getPOS(ta, position);

		position = argument.getEndSpan() - 1;

		out[2] = "l:" + getWord(ta, position);
		out[3] = "lt:" + getPOS(ta, position);

		return out;
	}

	public static String[] getContextWordsAndTags(Constituent argument) {
		TextAnnotation ta = argument.getTextAnnotation();

		String[] out = new String[8];
		int position = argument.getStartSpan();

		position--;

		out[0] = "-1:" + getWord(ta, position);
		out[1] = "-1T:" + getPOS(ta, position);
		position--;

		out[2] = "-2:" + getWord(ta, position);
		out[3] = "-2T:" + getPOS(ta, position);

		position = argument.getEndSpan() - 1;

		position++;

		out[4] = "l1:" + getWord(ta, position);
		out[5] = "l1t:" + getPOS(ta, position);
		position++;

		out[6] = "l2:" + getWord(ta, position);
		out[7] = "l2t:" + getPOS(ta, position);

		return out;
	}

	public static String[] getContextTags(Constituent argument) {
		TextAnnotation ta = argument.getTextAnnotation();

		String[] out = new String[6];
		int position = argument.getStartSpan();

		position--;

		out[0] = "LLT:" + getPOS(ta, position);
		position--;

		out[1] = "LT:" + getPOS(ta, position);

		position = argument.getEndSpan() - 1;

		position++;

		out[2] = "RT:" + getPOS(ta, position);
		position++;

		out[3] = "RRT:" + getPOS(ta, position);

		out[4] = "L:" + out[0] + ":" + out[1];
		out[5] = "R:" + out[2] + ":" + out[3];

		return out;
	}

	public static String getFirstWord(Constituent argument) {
		return getWord(argument.getTextAnnotation(), argument.getStartSpan());
	}

	public static String getLastWord(Constituent argument) {
		return getWord(argument.getTextAnnotation(), argument.getEndSpan() - 1);
	}

	public static int getNumberOfWords(Constituent argument) {
		return argument.size();
	}

	public static int getNumberOfChunks(Constituent argument) {
		IQueryable<Constituent> shallowParseContained = argument
				.getTextAnnotation().select(ViewNames.SHALLOW_PARSE)
				.where(Queries.containedInConstituent(argument));

		return ((QueryableList<Constituent>) shallowParseContained).size();
	}

	public static String[] getChunkEmbedding(Constituent argument) {
		List<String> chunkEmbedding = new ArrayList<String>();

		IQueryable<Constituent> shallowParse = argument.getTextAnnotation()
				.select(ViewNames.SHALLOW_PARSE);

		IQueryable<Constituent> containedInConstituentExclusive = shallowParse
				.where(Queries.containedInConstituentExclusive(argument));

		for (Constituent chunk : containedInConstituentExclusive) {
			chunkEmbedding.add("embed-" + chunk.getLabel());
		}

		IQueryable<Constituent> containsConstituentExclusive = shallowParse
				.where(Queries.containsConstituentExclusive(argument));
		for (Constituent chunk : containsConstituentExclusive) {
			chunkEmbedding.add("embedded-in-" + chunk.getLabel());
		}

		IQueryable<Constituent> sameSpan = shallowParse.where(Queries
				.sameSpanAsConstituent(argument));
		for (Constituent chunk : sameSpan) {
			chunkEmbedding.add("is-" + chunk.getLabel());
		}

		IQueryable<Constituent> exclusivelyOverlaps = shallowParse
				.where(Queries.exclusivelyOverlaps(argument));
		for (Constituent chunk : exclusivelyOverlaps) {
			chunkEmbedding.add("exclusively-overlaps-" + chunk.getLabel());
		}

		return chunkEmbedding.toArray(new String[chunkEmbedding.size()]);
	}

	public static List<Constituent> getChunksBetweenPredicateAndArg(
			Constituent argument, int predicatePosition) {
		TextAnnotation ta = argument.getTextAnnotation();

		SpanLabelView shallowParse = (SpanLabelView) ta
				.getView(ViewNames.SHALLOW_PARSE);

		List<Constituent> constituents;
		if (argument.getEndSpan() < predicatePosition)
			constituents = SpanLabelsHelper.getConstituentsInBetween(
					shallowParse, argument.getEndSpan(), predicatePosition);
		else
			constituents = SpanLabelsHelper.getConstituentsInBetween(
					shallowParse, predicatePosition + 1,
					argument.getStartSpan());

		return constituents;
	}

	public static String getChunkPattern(Constituent argument,
			int predicatePosition) {
		List<Constituent> constituents = getChunksBetweenPredicateAndArg(
				argument, predicatePosition);

		Collections.sort(constituents, new Comparator<Constituent>() {
			public int compare(Constituent o1, Constituent o2) {
				if (o1.getStartSpan() < o2.getStartSpan())
					return -1;
				else if (o1.getStartSpan() > o2.getStartSpan())
					return 1;
				else
					return 0;

			}
		});

		StringBuffer sb = new StringBuffer();
		for (Constituent constituent : constituents) {
			sb.append(constituent.getLabel() + "-");
		}

		return sb.toString();
	}

	/**
	 * @param predicate
	 * @return
	 */
	public static Constituent getPredicateClause(Constituent predicate) {
		Comparator<Constituent> lengthComparator = new Comparator<Constituent>() {
			public int compare(Constituent arg0, Constituent arg1) {
				if (arg0.size() < arg1.size())
					return -1;
				else if (arg0.size() > arg1.size())
					return 1;
				else
					return 0;
			}
		};

		List<Constituent> clauses = (QueryableList<Constituent>) (predicate
				.getTextAnnotation().select(ViewNames.CLAUSES)
				.where(Queries.containsConstituent(predicate))
				.orderBy(lengthComparator));

		if (clauses.size() == 0)
			return null;
		else
			return clauses.get(0);
	}

}
