package edu.illinois.cs.cogcomp.srl.features;

import java.util.HashSet;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.edison.features.FeatureExtractor;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

public class NomFeatureGenerator implements FeatureExtractor {

	public NomFeatureGenerator() {
	}

	@Override
	public Set<String> getFeatures(Constituent candidate) {

		Tree<Pair<String, IntPair>> candidateTree = FeatureHelper
				.getArgumentTree(candidate);

		Pair<String, String> headInfo = FeatureHelper
				.getHeadWordPOS(candidateTree);

		int predicatePosition = FeatureHelper.getPredicatePosition(candidate);

		String ptype = FeatureHelper.getPhraseType(candidate, candidateTree);
		Constituent predicate = FeatureHelper
				.getPredicateConstituent(candidate);
		String pred = FeatureHelper.getPredicateLemma(candidate);

		String hw = headInfo.getFirst();
		String path = FeatureHelper.getPathToPredicate(candidate);
		String position = FeatureHelper.getLinearPosition(candidate);

		Set<NomLexClasses> nomType = NomFeatureHelper.getNomLexClass(predicate
				.getSurfaceString().toLowerCase().trim(), pred);

		Set<String> features = new HashSet<String>();

		features.add("pred.pos:" + FeatureHelper.getPredicatePOS(candidate));

		features.add("pred:" + pred);

		features.add("ptype:" + ptype);

		features.add("hw.pos:" + headInfo.getSecond());

		features.add("hw:" + hw);

		features.add("position:" + position);

		features.add("path:" + path);

		features.add("subcat:"
				+ FeatureHelper.getSubcatFrame(candidate, candidateTree));

		String[] nounClassDeverbalNoun = NomFeatureHelper
				.getNounClassDeverbalNoun(candidate);
		for (String c : nounClassDeverbalNoun)
			features.add("nounclass:" + c);

		features.add("length:" + FeatureHelper.getNumberOfWords(candidate));

		features.add("chunklength:"
				+ FeatureHelper.getNumberOfChunks(candidate));

		String[] chunkEmbedding = FeatureHelper.getChunkEmbedding(candidate);
		for (String embedding : chunkEmbedding)
			features.add("chunkembedding:" + embedding);

		features.add("chunkpattern:"
				+ FeatureHelper.getChunkPattern(candidate, predicatePosition));

		for (String wordTag : FeatureHelper.getArgumentWordsAndTags(candidate))
			features.add("word:" + wordTag);

		features.add("lsis.ptype:"
				+ FeatureHelper.getLeftSisterPhraseType(candidate,
						candidateTree));
		features.add("lsis.hw:"
				+ FeatureHelper.getLeftSisterHeadWord(candidate, candidateTree));
		features.add("lsis.pos:"
				+ FeatureHelper.getLeftSisterHeadPOS(candidate, candidateTree));

		features.add("rsis.ptype:"
				+ FeatureHelper.getRightSisterPhraseType(candidate,
						candidateTree));

		features.add("rsis.hw:"
				+ FeatureHelper
						.getRightSisterHeadWord(candidate, candidateTree));
		features.add("rsis.hw.pos:"
				+ FeatureHelper.getRightSisterHeadPOS(candidate, candidateTree));

		features.add("parent.ptype:"
				+ FeatureHelper.getParentPhraseType(candidate, candidateTree));
		features.add("parent.hw:"
				+ FeatureHelper.getParentHeadWord(candidate, candidateTree));
		features.add("partialpath" + path);

		features.add("ptype+length:" + ptype
				+ FeatureHelper.getPathLength(candidate));
		features.add("pred+hw:" + pred + hw);
		features.add("pred+path:" + pred + path);
		features.add("pred+position:" + pred + position);

		features.add("predicate+ptype:" + pred + ptype);
		features.add("predicate+lastword:" + pred
				+ FeatureHelper.getLastWord(candidate));
		features.add("predicate+headword:" + pred + hw);
		features.add("predicate+position:" + pred + position);

		for (NomLexClasses c : nomType) {
			features.add("nomtype+position:" + c + position);
		}

		// features.addAll(getConjunctions(features));

		features.add("bias");
		return features;
	}

	@Override
	public String getName() {
		return "nom";
	}

}