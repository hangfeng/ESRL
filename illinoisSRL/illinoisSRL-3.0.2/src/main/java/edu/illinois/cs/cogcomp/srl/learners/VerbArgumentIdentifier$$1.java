// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D294555580B4D2A427C2A4F2DCD4DCB21FC410219996999A54A2A268A1EC9F975C5299525A04135844D450B1D50BAD0E02F17B4D4C292D2A4D2634D1898876A005176596E52794620539E82000E514AEC9F5000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import java.util.*;


public class VerbArgumentIdentifier$$1 extends Classifier
{
  private static final VerbSRLFeatures1 __VerbSRLFeatures1 = new VerbSRLFeatures1();
  private static final VerbSRLIdConjunctions __VerbSRLIdConjunctions = new VerbSRLIdConjunctions();

  public VerbArgumentIdentifier$$1()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "VerbArgumentIdentifier$$1";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'VerbArgumentIdentifier$$1(Constituent)' defined on line 26 of VerbSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    FeatureVector __result;
    __result = new FeatureVector();
    __result.addFeatures(__VerbSRLFeatures1.classify(__example));
    __result.addFeatures(__VerbSRLIdConjunctions.classify(__example));
    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'VerbArgumentIdentifier$$1(Constituent)' defined on line 26 of VerbSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "VerbArgumentIdentifier$$1".hashCode(); }
  public boolean equals(Object o) { return o instanceof VerbArgumentIdentifier$$1; }

  public java.util.LinkedList getCompositeChildren()
  {
    java.util.LinkedList result = new java.util.LinkedList();
    result.add(__VerbSRLFeatures1);
    result.add(__VerbSRLIdConjunctions);
    return result;
  }
}

