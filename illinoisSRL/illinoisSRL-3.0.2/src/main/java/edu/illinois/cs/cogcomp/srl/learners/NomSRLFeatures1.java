// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000053CC13A02C0401500DBACF648DD2210BD86728631C22931C2B9F4C04C9589995258877F828E10EDB961BCA47E107F2FC178EFAC4E55967C079226EB875A832744E49F1C907E60F2815C896D51148BD71EF4DD824D4E5434C8DEC4FF761E3EB38B65E90FED1780F1C4327000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomSRLFeatures1 extends Classifier
{
  private static ThreadLocal __cache = new ThreadLocal(){ };
  private static ThreadLocal __exampleCache = new ThreadLocal(){ };
  public static void clearCache() { __exampleCache = new ThreadLocal(){ }; }

  public NomSRLFeatures1()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "NomSRLFeatures1";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  private FeatureVector cachedFeatureValue(Object __example)
  {
    if (__example == __exampleCache.get()) return (FeatureVector) __cache.get();
    __exampleCache.set(__example);
    Constituent c = (Constituent) __example;

    FeatureVector __result;
    __result = new FeatureVector();
    String __id;
    String __value;

    Object __values = (new NomFeatureGenerator()).getFeatures(c);

    if (__values instanceof java.util.Collection)
    {
      for (java.util.Iterator __I = ((java.util.Collection) __values).iterator(); __I.hasNext(); )
      {
        __id = __I.next().toString();
        __result.addFeature(new DiscretePrimitiveStringFeature(this.containingPackage, this.name, __id, "true", valueIndexOf("true"), (short) 0));
      }
    }
    else
    {
      for (java.util.Iterator __I = ((java.util.Map) __values).entrySet().iterator(); __I.hasNext(); )
      {
        java.util.Map.Entry __e = (java.util.Map.Entry) __I.next();
        __id = __e.getKey().toString();
        __value = __e.getValue().toString();
        __result.addFeature(new DiscretePrimitiveStringFeature(this.containingPackage, this.name, __id, __value, valueIndexOf(__value), (short) 0));
      }
    }

    __cache.set(__result);
    return __result;
  }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'NomSRLFeatures1(Constituent)' defined on line 16 of NomSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return cachedFeatureValue(__example);
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomSRLFeatures1(Constituent)' defined on line 16 of NomSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomSRLFeatures1".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomSRLFeatures1; }
}

