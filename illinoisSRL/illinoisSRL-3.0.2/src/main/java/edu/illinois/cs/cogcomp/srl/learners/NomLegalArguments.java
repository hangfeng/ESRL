// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000D615DCA62C0401E75993E6054A763DE1CA82512492AD6FEBD46C4707D98CEE405B5C77FE421D81B6E212CCE7FB33961C14CB526187D26B906E6DD4C7E5E619528977C38C489B01B2450340D1227A811CF650EB0E6199E786D48E18EAFBF40C8BFE7899826848280CEC36649A51C0A0F5D239F012716493A82DEDAD73371FD16311F03361F7C1D7EC69FC39445AAACE969EDB2AA52D22355C78816FFD4E01C4FC3F367CD7418E70D4455FA965BF029251D0C5C6555C476415794A93B748F2DDCE9B6F4D27649D9B3676D1DECF50B4FA40A455091DBE46E4E40551FA4D971EB5B03A1B68D9D47D68CB1CCA6B575D50B17AF2BCB9A63A424D4ADF77D55D15EEAB0F6D9BB9ED6575A06EEB04408705F800B21F4C938C1778534B63D8E6AD93B12437C6B11C87C5363EE933D06013C90E4F70CE118A5478200000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomLegalArguments extends ParameterizedConstraint
{
  private static final NomArgumentClassifier __NomArgumentClassifier = new NomArgumentClassifier();

  public NomLegalArguments() { super("edu.illinois.cs.cogcomp.srl.learners.NomLegalArguments"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomLegalArguments(TextAnnotation)' defined on line 244 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      LinkedList legal = NombankUtilities.getLegalArguments(nom);
      {
        boolean LBJ2$constraint$result$0;
        {
          LBJ2$constraint$result$0 = true;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ2$constraint$result$0; )
          {
            Constituent a = (Constituent) __I0.next();
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I1 = (legal).iterator(); __I1.hasNext() && !LBJ2$constraint$result$0; )
              {
                String type = (String) __I1.next();
                LBJ2$constraint$result$0 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + (type));
              }
            }
          }
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomLegalArguments(TextAnnotation)' defined on line 244 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomLegalArguments".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomLegalArguments; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomLegalArguments(TextAnnotation)' defined on line 244 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      LinkedList legal = NombankUtilities.getLegalArguments(nom);
      {
        Object[] LBJ$constraint$context = new Object[2];
        LBJ$constraint$context[0] = legal;
        LBJ$constraint$context[1] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }

                  public String getRightValue()
                  {
                    String type = (String) quantificationVariables.get(1);
                    return "" + (type);
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), null, LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("type", legal, LBJ2$constraint$result$2);
          }
          LBJ2$constraint$result$0 = new UniversalQuantifier("a", argumentCandidates, LBJ2$constraint$result$1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      currentPredicateId++;
    }

    return __result;
  }
}

