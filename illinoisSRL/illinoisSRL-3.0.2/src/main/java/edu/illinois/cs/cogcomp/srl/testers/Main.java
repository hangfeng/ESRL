package edu.illinois.cs.cogcomp.srl.testers;

import java.io.Console;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.algorithms.Sorters;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandIgnore;
import edu.illinois.cs.cogcomp.core.utilities.commands.InteractiveShell;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorClient;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorDataStructureInterface;
import edu.illinois.cs.cogcomp.edison.features.helpers.PredicateArgumentHelpers;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.Relation;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.main.SRLSystem;
import edu.illinois.cs.cogcomp.srl.main.VerbSRLSystem;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;
import edu.illinois.cs.cogcomp.thrift.base.Forest;

public class Main {

	@CommandIgnore
	public static void main(String[] arguments) {

		InteractiveShell<Main> tester = new InteractiveShell<Main>(Main.class);

		if (arguments.length == 0)
			tester.showDocumentation();
		else {
			long start_time = System.currentTimeMillis();
			try {

				tester.runCommand(arguments);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("This experiment took "
					+ (System.currentTimeMillis() - start_time) / 1000.0
					+ " secs");
		}
	}

	public static void printNomStatistics(String columnFile)
			throws FileNotFoundException {
		CoNLLColumnFormatReader reader = new CoNLLColumnFormatReader("", "",
				columnFile, ViewNames.SRL);

		Set<String> prepositions = new HashSet<String>(Arrays.asList("on",
				"in", "to", "with", "for", "at", "of"));
		Map<String, Counter<String>> counters = new HashMap<String, Counter<String>>();
		for (String s : prepositions)
			counters.put(s, new Counter<String>());

		Set<String> args = new HashSet<String>();

		for (TextAnnotation ta : reader) {
			if (!ta.hasView(ViewNames.SRL))
				continue;

			PredicateArgumentView pav = (PredicateArgumentView) ta
					.getView(ViewNames.SRL);

			for (Constituent predicate : pav.getPredicates()) {
				for (Relation r : pav.getArguments(predicate)) {
					Constituent arg = r.getTarget();
					args.add(r.getRelationName());

					String firstToken = ta.getToken(arg.getStartSpan())
							.toLowerCase().trim();

					if (prepositions.contains(firstToken)) {
						counters.get(firstToken).incrementCount(
								r.getRelationName());
					}
				}
			}
		}

		System.out.print("| |");
		for (String s : prepositions) {
			System.out.print(s + "|");
		}
		System.out.println();
		System.out.println("|--");

		for (String arg : Sorters.sortSet(args)) {
			System.out.print("| " + arg + " |");
			for (String prep : prepositions) {
				System.out
						.print((int) (counters.get(prep).getCount(arg)) + "|");
			}

			System.out.println();
		}
	}

	public static void testCuratorInterface() {
		SRLSystem srlSystem = VerbSRLSystem.getInstance();

		SRLConfig config = SRLConfig.getInstance();

		CuratorClient curator = new CuratorClient(config.getCuratorHost(),
				config.getCuratorPort());

		boolean charniak = config.getDefaultParser().equals(
				ViewNames.PARSE_CHARNIAK);
		boolean forceUpdate = false;
		Console console = System.console();

		boolean done = false;
		do {

			System.out.print("Enter text (_ to quit): ");
			String line = console.readLine();
			line = line.trim();

			if (line.length() == 0)
				continue;

			if (line.equals("_"))
				done = true;

			try {
				TextAnnotation ta = curator.getTextAnnotation("", "", line,
						forceUpdate);

				if (charniak)
					curator.addCharniakParse(ta, forceUpdate);
				else
					curator.addStanfordParse(ta, forceUpdate);

				curator.addPOSView(ta, forceUpdate);
				curator.addChunkView(ta, forceUpdate);
				curator.addNamedEntityView(ta, forceUpdate);

				ta.addView(ViewNames.PSEUDO_PARSE,
						SRLUtils.createPsuedoParseView(ta));

				PredicateArgumentView srl = srlSystem.getSRL(ta,
						config.doBeamSearch());
				System.out.println(srl);

				Forest srlForest = CuratorDataStructureInterface
						.convertPredicateArgumentViewToForest(srl);

				System.out.println();
				System.out.println(srlForest);

				PredicateArgumentView srl1 = CuratorDataStructureInterface
						.alignForestToPredicateArgumentView(ViewNames.SRL, ta,
								srlForest);

				System.out.println(srl1);

				System.out.println();

			} catch (Exception ex) {
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}

		} while (!done);
	}

	public static void writeThreshold(String file, String threshold, String beta)
			throws FileNotFoundException, IOException {
		SRLSystem.writeThresholds(file, Double.parseDouble(threshold),
				Double.parseDouble(beta));
	}
}
