// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D294555580B4D2A4A0E02F17DA8292A447B4D4C292D2A4D26D07ECFCB2E29CC292D4DCB215846D450B1D5580EACCB294C4E29CC467B2A4CCD45D158082A4D49CC4E4C2945F94DCDCD44D1CA6C8A8A811E29036C52162A3A0006958344BC9000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.main.Constants;
import java.util.*;


public class VerbSRLExtraFeatures extends Classifier
{
  private static final SyntacticFrame __SyntacticFrame = new SyntacticFrame();
  private static final PredicateLemma __PredicateLemma = new PredicateLemma();
  private static final VerbSRLExtraFeatures$$2 __VerbSRLExtraFeatures$$2 = new VerbSRLExtraFeatures$$2();
  private static final VerbSRLExtraFeatures$$3 __VerbSRLExtraFeatures$$3 = new VerbSRLExtraFeatures$$3();
  private static final VerbSRLExtraFeatures$$4 __VerbSRLExtraFeatures$$4 = new VerbSRLExtraFeatures$$4();

  public VerbSRLExtraFeatures()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "VerbSRLExtraFeatures";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'VerbSRLExtraFeatures(Constituent)' defined on line 22 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    FeatureVector __result;
    __result = new FeatureVector();
    __result.addFeature(__SyntacticFrame.featureValue(__example));
    __result.addFeature(__PredicateLemma.featureValue(__example));
    __result.addFeature(__VerbSRLExtraFeatures$$2.featureValue(__example));
    __result.addFeatures(__VerbSRLExtraFeatures$$3.classify(__example));
    __result.addFeatures(__VerbSRLExtraFeatures$$4.classify(__example));
    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'VerbSRLExtraFeatures(Constituent)' defined on line 22 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "VerbSRLExtraFeatures".hashCode(); }
  public boolean equals(Object o) { return o instanceof VerbSRLExtraFeatures; }

  public java.util.LinkedList getCompositeChildren()
  {
    java.util.LinkedList result = new java.util.LinkedList();
    result.add(__SyntacticFrame);
    result.add(__PredicateLemma);
    result.add(__VerbSRLExtraFeatures$$2);
    result.add(__VerbSRLExtraFeatures$$3);
    result.add(__VerbSRLExtraFeatures$$4);
    return result;
  }
}

