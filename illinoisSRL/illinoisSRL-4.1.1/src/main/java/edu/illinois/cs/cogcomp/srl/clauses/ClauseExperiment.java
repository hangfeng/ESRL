package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.experiments.ClassificationTester;
import edu.illinois.cs.cogcomp.sl.core.StructuredProblem;
import edu.illinois.cs.cogcomp.sl.inference.AbstractInferenceSolver;
import edu.illinois.cs.cogcomp.sl.util.WeightVector;
import edu.illinois.cs.cogcomp.srl.data.Dataset;
import edu.illinois.cs.cogcomp.srl.learn.CrossValidationHelper.Tester;
import edu.illinois.cs.cogcomp.srl.learn.JLISCVHelper;
import edu.illinois.cs.cogcomp.srl.learn.JLISLearner;
import edu.illinois.cs.cogcomp.srl.learn.LearnerParameters;
import edu.illinois.cs.cogcomp.srl.learn.PerformanceMeasure;
import edu.illinois.cs.cogcomp.srl.utilities.WeightVectorUtils;

public class ClauseExperiment {

	private final static Logger log = LoggerFactory
			.getLogger(ClauseExperiment.class);

	public static void train(Dataset trainingSet, double c, int numThreads)
			throws Exception {
		log.info("Training with c = {} on {}", c, trainingSet);

		ClauseManager manager = new ClauseManager(false);

		AbstractInferenceSolver[] inference = new AbstractInferenceSolver[numThreads];

		for (int i = 0; i < inference.length; i++) {
			inference[i] = new ClausePredictor(manager);
		}

		StructuredProblem problem = manager.getStructuredProblem(trainingSet,
				500);

		WeightVector w = JLISLearner.trainStructSVM(inference, problem, c);

		WeightVectorUtils.save("models/clauses.model.lc", w);
		log.info("Saved model to models/clauses.model.lc");

		manager.getLexicon().save("models/clauses.lex");
		log.info("Saved lexicon to models/clauses.lex");
	}

	public static double cv(Dataset trainingSet) throws Exception {
		log.info("CV on {}", trainingSet);

		ClauseManager manager = new ClauseManager(false);

		AbstractInferenceSolver[] inference = new AbstractInferenceSolver[20];

		for (int i = 0; i < inference.length; i++) {
			inference[i] = new ClausePredictor(manager);
		}

		StructuredProblem problem = manager.getStructuredProblem(trainingSet);
		Tester<StructuredProblem> evaluator = new Tester<StructuredProblem>() {

			@Override
			public PerformanceMeasure evaluate(StructuredProblem testSet,
					WeightVector weight,
					AbstractInferenceSolver inference)
					throws Exception {
				ClassificationTester results = ClauseExperiment.evaluate(
						testSet, weight, (ClausePredictor) inference);

				return new JLISCVHelper.RealMeasure(results
						.getEvaluationRecord("true").getF1());
			}
		};
		LearnerParameters params = JLISLearner.cvStructSVM(problem, inference,
				5, evaluator);

		return params.getcStruct();
	}

	public static void test(Dataset testSet) throws Exception {
		ClauseManager manager = new ClauseManager(false);

		ClausePredictor inference = new ClausePredictor(manager);

		WeightVector weights = manager.getWeightVector();

		ClassificationTester results = evaluate(
				manager.getStructuredProblem(testSet), weights, inference);

		System.out.println(results.getPerformanceTable().toOrgTable());
	}

	@SuppressWarnings("unused")
	protected static ClassificationTester evaluate(StructuredProblem problem,
			WeightVector w, ClausePredictor inference) throws Exception {

		ClassificationTester tester = new ClassificationTester();
		for (int i = 0; i < problem.size(); i++) {
			ClauseInstance x = (ClauseInstance) problem.input_list.get(i);
			ClauseStructure goldY = (ClauseStructure) problem.output_list
					.get(i);

			ClauseStructure prediction = (ClauseStructure) inference
					.getBestStructure(w, x);

			Set<IntPair> splits = new HashSet<IntPair>(prediction.constituents);

			for (IntPair goldSpan : goldY.constituents) {

				String goldLabel = "true";

				String predLabel = "false";
				if (splits.contains(goldSpan)) {
					predLabel = "true";
					splits.remove(goldSpan);
				}

				tester.record(goldLabel, predLabel);
			}

			for (IntPair predSpan : splits) {
				tester.record("false", "true");
			}
		}

		return tester;
	}
}
