package edu.illinois.cs.cogcomp.srl.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.srl.data.AbstractSRLAnnotationReader.Fields;

/**
 * 
 * 
 * @author Vivek Srikumar
 * 
 */
public class NombankReader extends AbstractSRLAnnotationReader {

	public NombankReader(String treebankHome, String nombankHome,
			String srlViewName, boolean mergeContiguousCArgs) throws Exception {
		super(treebankHome, nombankHome, null, srlViewName,
				mergeContiguousCArgs);
	}

	public NombankReader(String treebankHome, String nombankHome,
			String[] sections, String srlViewName, boolean mergeContiguousCArgs)
			throws Exception {

		super(treebankHome, nombankHome, sections, srlViewName,
				mergeContiguousCArgs);
	}

	public NombankReader(Iterable<TextAnnotation> list, String treebankHome, String nombankHome,
			String[] sections, String srlViewName, boolean mergeContiguousCArgs)
			throws Exception {
		super(list, treebankHome, nombankHome, sections, srlViewName, mergeContiguousCArgs);
	}

	@Override
	protected Fields readFields(String line) {
		return new NombankFields(line);
	}

	@Override
	protected String getDataFile(String dataHome) {
		return dataHome + "/nombank.1.0";
	}

}

class NombankFields extends Fields {

	final List<GoldLabel> nomLabels;

	public NombankFields(String line) {
		super(line);
		String[] fields = line.split("\\s+");

		wsjFileName = fields[0];
		sentence = Integer.parseInt(fields[1]);
		predicateTerminal = Integer.parseInt(fields[2]);

		lemma = fields[3];
		sense = fields[4];
		nomLabels = new ArrayList<GoldLabel>();

		Set<String> seen = new HashSet<String>();

		for (int i = 5; i < fields.length; i++) {
			GoldLabel goldLabel = new GoldLabel(fields[i]);

			// An ugly hack to deal with the handful of cases where Nombank
			// repeats some fields.
			if (!seen.contains(goldLabel.toString())) {

				nomLabels.add(goldLabel);
				seen.add(goldLabel.toString());
			}
		}

		section = wsjFileName.split("/")[1];

		identifier = wsjFileName + ":" + sentence;

	}

	@Override
	public Constituent createPredicate(TextAnnotation ta, String viewName,
			List<Tree<Pair<String, IntPair>>> yield) {
		Tree<Pair<String, IntPair>> l = yield.get(predicateTerminal);
		int start = l.getLabel().getSecond().getFirst();
		Constituent predicate = new Constituent("Predicate", viewName, ta,
				start, start + 1);

		predicate.addAttribute(PropbankReader.LemmaIdentifier, lemma);
		predicate.addAttribute(PropbankReader.SenseIdentifier, sense);

		return predicate;
	}

	@Override
	public List<? extends GoldLabel> getGoldLabels() {
		return nomLabels;
	}
}