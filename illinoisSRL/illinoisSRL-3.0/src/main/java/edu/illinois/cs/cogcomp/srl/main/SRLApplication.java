/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.main;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the main SRL application, which can be used for command line
 * processing of text.
 * 
 * @author Vivek Srikumar
 */
public class SRLApplication {

	private static final Logger log = LoggerFactory
			.getLogger(SRLApplication.class);

	private static String configFile;
	private static boolean interactiveMode;

	private static boolean serverMode;
	private static int serverPort;
	private static int serverNumThreads;

	private static boolean batchMode;
	private static String inputFile;
	private static String outputFile;

	private static boolean whitespace;

	private static boolean nom, verb;

	public static void main(String[] args) {
		readOptions(args);

		SRLConfig config = SRLConfig.getInstance(configFile);
		log.info("Finished reading configuration.");

		Runnable runnable = null;

		SRLSystem srlSystem = null;

		if (verb)
			srlSystem = VerbSRLSystem.getInstance();
		else if (nom)
			srlSystem = NomSRLSystem.getInstance();

		assert srlSystem != null : "One of verb or nom SRL should be chosen";

		if (serverMode) {
			runnable = new SRLApplicationServer(srlSystem, config, serverPort,
					serverNumThreads);
		} else if (batchMode) {
			runnable = new BatchSRLApplication(srlSystem, config, inputFile,
					outputFile, whitespace);
		} else if (interactiveMode) {
			runnable = new InteractiveSRLApplication(srlSystem, config);
		} else {
			assert false : "The application can be in one of server, "
					+ "batch or interactive modes";
		}

		runnable.run();
	}

	@SuppressWarnings("static-access")
	private static void readOptions(String[] args) {

		Options options = new Options();

		options.addOption(OptionBuilder
				.withLongOpt("config")
				.withArgName("config file")
				.hasArg()
				.withDescription(
						"The configuration file. Default = '"
								+ SRLConfig.SRL_CONFIG_PROPERTIES_FILE + "'.")
				.create('c'));

		options.addOption(OptionBuilder
				.withLongOpt("server")
				.withArgName("port")
				.hasArg()
				.withDescription(
						"Start as a curator server at the specified port. See also -t")
				.create('s'));

		options.addOption(OptionBuilder
				.withLongOpt("batch")
				.withArgName("input file")
				.hasArg()
				.withDescription(
						"Run in batch mode using the input file. "
								+ "The file should have one sentence per line.  Requires --batch-output-file.")
				.create('b'));

		options.addOption(OptionBuilder
				.withLongOpt("batch-output-file")
				.withArgName("output file")
				.hasArg()
				.withDescription(
						"The SRL output is written in the CoNLL column format to this file. "
								+ "Works only with batch mode.").create());

		options.addOption(OptionBuilder
				.withLongOpt("batch-whitespace")
				.withDescription(
						"Indicates that the sentences in the file "
								+ "are whitespace tokenized. If this is not "
								+ "present, the sentences are tokenized using the"
								+ " curator's tokenizer. "
								+ "Works only with batch mode").create());

		options.addOption("i", "interactive", false,
				"Start in interactive mode.");

		options.addOption(OptionBuilder
				.withLongOpt("server-threads")
				.withDescription(
						"Number of threads for the SRL curator server (Default = 1). "
								+ "This option is ignored if --server is not specified.. ")
				.create());

		options.addOption("h", "help", false, "Prints this message.");

		options.addOption("n", "nom", false, "Perform nominal SRL");
		options.addOption("v", "verb", false, "Perform verb SRL");

		CommandLineParser parser = new GnuParser();
		CommandLine cli = null;
		try {
			cli = parser.parse(options, args);
		} catch (ParseException ex) {
			System.err.println("Error: " + ex.getMessage());
			printHelp(options);
			System.exit(-1);
		}

		if (cli.hasOption('h') || cli.hasOption("help")) {
			printHelp(options);
			System.exit(0);
		}

		if (cli.hasOption('n') || cli.hasOption("nom"))
			nom = true;
		else if (cli.hasOption('v') || cli.hasOption("verb"))
			verb = true;
		else {
			System.err
					.println("One of verb or nom SRL should be chosen (-v or -n).");
			printHelp(options);
			System.exit(-1);
		}

		configFile = cli.getOptionValue("config",
				SRLConfig.SRL_CONFIG_PROPERTIES_FILE);

		serverMode = false;
		interactiveMode = false;
		batchMode = false;

		if (cli.hasOption("server")) {

			log.info("Starting SRL in server mode...");
			serverMode = true;
			serverPort = Integer.parseInt(cli.getOptionValue("server"));

			if (cli.hasOption("server-threads"))
				serverNumThreads = Integer.parseInt(cli
						.getOptionValue("server-threads"));
			else
				serverNumThreads = 1;

		} else if (cli.hasOption('i') || cli.hasOption("interactive")) {

			log.info("Starting SRL in interactive mode...");
			interactiveMode = true;
		} else {
			if (!cli.hasOption("batch")) {
				System.err.println("One of [interactive,batch,server] modes "
						+ "must be chosen");
				printHelp(options);
				System.exit(-1);
			}

			if (!cli.hasOption("batch-output-file")) {
				System.err.println("Error: Batch mode requires"
						+ "the output file to be specified");
				printHelp(options);
				System.exit(-1);
			}

			batchMode = true;
			inputFile = cli.getOptionValue("batch");
			outputFile = cli.getOptionValue("batch-output-file");
			whitespace = (cli.hasOption('w') || cli.hasOption("whitespace"));
		}

	}

	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();

		String usage = "[-v | -n] [-i | -s <port> | -b <file>] <options>";
		String header = "Run the Illinois Semantic Role Labeler. "
				+ "Either -v or -n should be specified for nominal "
				+ "or verb SRL.";
		String footer = "For more information, goto http://cogcomp.cs.illinois.edu";
		formatter.printHelp(usage, header, options, footer);
	}
}
