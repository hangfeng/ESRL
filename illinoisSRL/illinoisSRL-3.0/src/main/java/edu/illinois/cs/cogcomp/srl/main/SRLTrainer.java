/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.main;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LBJ2.learn.Lexicon;
import LBJ2.parse.ArrayFileParser;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.srl.data.SRLClassifierParser;
import edu.illinois.cs.cogcomp.srl.data.XuePalmerIdentifierFilterParser;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentClassifier;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.testers.IdentifierThresholdTuner;

/**
 * @author Vivek Srikumar
 * 
 */
public class SRLTrainer implements Runnable {

	private final static Logger log = LoggerFactory.getLogger(SRLTrainer.class);

	private final String trainFile;
	private final String devFile;

	private final String modelsDirectory;

	private final String lexiconFile;

	private final String idLexiconFile;

	private final String identifierModel;
	private final String classifierModel;
	private final String idPreExtractFile;
	private final String clPreExtractFile;

	private double threshold = -1;
	private double beta = -1;

	private final boolean cv;

	/**
	 * @param config
	 * @param trainFile
	 */
	public SRLTrainer(SRLConfig config, String trainFile, String devFile,
			boolean cv) {
		this.trainFile = trainFile;
		this.devFile = devFile;

		String pathName = "edu.illinois.cs.cogcomp.srl.learners".replaceAll(
				"\\.", File.separator);

		this.modelsDirectory = config.getModelsDirectory() + File.separatorChar
				+ pathName;

		this.lexiconFile = this.modelsDirectory + File.separatorChar
				+ config.getLexiconFile();

		this.idLexiconFile = this.modelsDirectory + File.separator
				+ "VerbArgumentIdentifier.lex";

		this.identifierModel = this.modelsDirectory + File.separatorChar
				+ config.getIdentifierModelFile();
		this.classifierModel = this.modelsDirectory + File.separatorChar
				+ config.getClassifierModelFile();

		this.idPreExtractFile = this.modelsDirectory + File.separatorChar
				+ "id.features";

		this.clPreExtractFile = this.modelsDirectory + File.separatorChar
				+ "cl.features";

		this.threshold = Constants.srlIdThreshold;
		this.beta = Constants.srlIdBeta;

		this.cv = cv;

	}

	@Override
	public void run() {

		log.info("Setting up directories");
		setupDirectories();

		log.info("Pre-extracting identifier features");
		preExtractIdentifierFeatures(this.idLexiconFile, 3);

		log.info("Training identifier");
		trainIdentifier(this.idLexiconFile);

		log.info("Tuning identifier for high recall");
		tuneIdentifier(this.idLexiconFile);

		log.info("Cleaning up identifier features");
		try {
			IOUtils.rm(this.idPreExtractFile);
		} catch (IOException e1) {
			log.error("Error deleting id preextract file! Exiting. Check {}",
					this.idPreExtractFile);

			System.exit(-1);
		}

		log.info("Pre-extracting classifier features");
		preExtractClassifierFeatures();

		log.info("Training argument classifier");
		trainClassifier();

		log.info("Cleaning up classifier features");
		try {
			IOUtils.rm(this.clPreExtractFile);
		} catch (IOException e) {
			log.error("Error deleting classifier preextract file! "
					+ "Exiting. Check {}", this.clPreExtractFile);

			System.exit(-1);

		}

		log.info("Finished training SRL. Retraining identifier using the same lexicon as the classifier");

		log.info("Cleaning up existing identifier");
		try {
			IOUtils.rm(this.identifierModel);
			IOUtils.rm(this.idLexiconFile);
		} catch (IOException e) {
			log.error("Error deleting identifier for cleanup. Check {}",
					this.identifierModel);
			System.exit(-1);
		}

		log.info("Pre-extracting identifier features");
		preExtractIdentifierFeatures(this.lexiconFile, 6);

		log.info("Training identifier");
		trainIdentifier(this.lexiconFile);

		this.threshold = -1;
		this.beta = -1;
		log.info("Tuning identifier for high recall");
		tuneIdentifier(this.lexiconFile);

		log.info("Cleaning up identifier features");
		try {
			IOUtils.rm(this.idPreExtractFile);
		} catch (IOException e1) {
			log.error("Error deleting id preextract file! Exiting. Check {}",
					this.idPreExtractFile);

			System.exit(-1);
		}

		log.info("Done!");
	}

	/**
     * 
     */
	private void preExtractClassifierFeatures() {
		if (IOUtils.exists(this.classifierModel)) {
			log.info("Classifier model found at {}. "
					+ "Exiting classifier pre-extract.", this.classifierModel);
			return;
		}
		if (IOUtils.exists(this.clPreExtractFile)) {
			log.info("Classifier pre-extracted features found at {}. "
					+ "Exiting classifier pre-extract.", this.clPreExtractFile);
			return;
		}

		VerbArgumentIdentifier identifier = new VerbArgumentIdentifier(
				this.identifierModel, this.idLexiconFile);

		SRLClassifierParser parser = new SRLClassifierParser(this.trainFile,
				threshold, beta, identifier);

		VerbArgumentClassifier learner = new VerbArgumentClassifier(
				this.classifierModel, this.lexiconFile);

		Lexicon lexicon = identifier.getLexicon();
		learner.setLexicon(lexicon);

		LBJTrainerUtility.preExtractTrain(parser, learner, clPreExtractFile, 4,
				10000);

		learner.save();
	}

	/**
     * 
     */
	private void setupDirectories() {
		if (!IOUtils.exists(modelsDirectory)) {
			log.info("Creating models directory: {}", this.modelsDirectory);
			IOUtils.mkdir(modelsDirectory);
		}
	}

	/**
     * 
     */
	private void trainClassifier() {

		assert threshold >= 0;
		assert beta >= 0;

		VerbArgumentClassifier classifier = new VerbArgumentClassifier(
				this.classifierModel, this.lexiconFile);

		if (this.cv) {

			LBJTrainerUtility.cv(60, this.clPreExtractFile, classifier);

		} else {

			ArrayFileParser parser = new ArrayFileParser(this.clPreExtractFile,
					true);

			LBJTrainerUtility.train(60, classifier, 0.05, 5, parser);

			classifier.save();

			System.gc();
		}
	}

	/**
     * 
     */
	private void tuneIdentifier(String lexiconFile) {

		if (threshold >= 0 && beta >= 0) {
			log.info("Identifier parameters have been already set! ");
			return;
		}

		VerbArgumentIdentifier identifier = new VerbArgumentIdentifier(
				this.identifierModel, lexiconFile);

		IdentifierThresholdTuner tuner = new IdentifierThresholdTuner(
				identifier);

		Pair<Double, Double> pair = tuner.tuneThreshold(devFile, 0.05, 0.05,
				1.0, 0.05, 0.05, 1.0);

		System.out.println("\n************");
		System.out
				.println("Modify the config file to use this threshold and beta!");
		System.out.println("\n************");

		this.threshold = pair.getFirst();
		this.beta = pair.getSecond();

	}

	/**
     * 
     */
	private void preExtractIdentifierFeatures(String lexiconFile, int pruneCount) {

		XuePalmerIdentifierFilterParser parser = new XuePalmerIdentifierFilterParser(
				"", this.trainFile);

		VerbArgumentIdentifier learner = new VerbArgumentIdentifier(
				this.identifierModel, lexiconFile);

		LBJTrainerUtility.preExtractTrain(parser, learner, idPreExtractFile,
				pruneCount, 10000);

		learner.save();
	}

	/**
     * 
     */
	private void trainIdentifier(String lexiconFile) {

		VerbArgumentIdentifier identifier = new VerbArgumentIdentifier(
				this.identifierModel, lexiconFile);

		if (this.cv) {
			LBJTrainerUtility.cv(20, this.idPreExtractFile, identifier);
		} else {
			ArrayFileParser parser = new ArrayFileParser(this.idPreExtractFile,
					true);

			// These parameters were fixed using cross validation
			LBJTrainerUtility.train(5, identifier, 0.1, 3.5, parser);

			identifier.save();

			System.gc();
		}
	}

}
