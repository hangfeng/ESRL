/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import org.apache.thrift.TException;

import edu.illinois.cs.cogcomp.edison.data.ColumnFormatWriter;
import edu.illinois.cs.cogcomp.edison.data.curator.CuratorClient;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.data.CoNLLColumnFormatReaderSRL;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.thrift.base.AnnotationFailedException;
import edu.illinois.cs.cogcomp.thrift.base.ServiceUnavailableException;

/**
 * @author Vivek Srikumar
 * 
 */
public class CuratorSRLTester {
	public static void main(String[] args) throws FileNotFoundException,
			ServiceUnavailableException, AnnotationFailedException, TException {

		if (args.length < 2) {
			System.err
					.println("Usage: CuratorSRLTester input-column-file output-column-file ");
			System.exit(-1);
		}

		String inputFile = args[0];
		String outputFile = args[1];

		String srlGoldViewName = ViewNames.SRL;
		CoNLLColumnFormatReaderSRL reader = new CoNLLColumnFormatReaderSRL("",
				"", inputFile, srlGoldViewName);

		PrintWriter goldWriter = new PrintWriter(outputFile + ".gold");
		PrintWriter predictedWriter = new PrintWriter(outputFile + ".predicted");

		ColumnFormatWriter writer = new ColumnFormatWriter();

		CuratorClient curator = new CuratorClient(SRLConfig.getInstance()
				.getCuratorHost(), SRLConfig.getInstance().getCuratorPort(),
				true);
		long start = System.currentTimeMillis();

		int count = 0;
		for (TextAnnotation ta : reader) {

			count++;
			if (!ta.hasView(srlGoldViewName)) {

				for (int i = 0; i < ta.size(); i++) {
					goldWriter.println("-");
					predictedWriter.println("-");
				}
				goldWriter.println();
				predictedWriter.println();

			} else {

				PredicateArgumentView gold = (PredicateArgumentView) ta
						.getView(srlGoldViewName);

				curator.addSRLView(ta, false);
				PredicateArgumentView predictedView = (PredicateArgumentView) ta
						.getView(ViewNames.SRL);

				writer.printPredicateArgumentView(gold, goldWriter);

				if (predictedView.getConstituents().size() == 0) {

					for (int i = 0; i < ta.size(); i++) {
						predictedWriter.println("-");
					}
					predictedWriter.println();
				} else {
					writer.printPredicateArgumentView(predictedView,
							predictedWriter);
				}

			}

			if (count % 100 == 0) {
				long end = System.currentTimeMillis();
				System.out.println(count + " examples completed. Took "
						+ (end - start) + "ms");
			}
		}

		goldWriter.flush();
		predictedWriter.flush();

		goldWriter.close();
		predictedWriter.close();

		long end = System.currentTimeMillis();
		System.out.println("Done. Took " + (end - start) + "ms");
	}
}
