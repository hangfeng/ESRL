/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.utilities;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses.*;

/**
 * @author Vivek Srikumar
 */
public class NomLexEntry {

	public final static Set<NomLexClasses> VERBAL = new HashSet<NomLexClasses>(
			Arrays.asList(NOM, NOMLIKE, NOMING, ABLE_NOM));

	public final static Set<NomLexClasses> ADJECTIVAL = new HashSet<NomLexEntry.NomLexClasses>(
			Arrays.asList(NOMADJ, NOMADJLIKE));

	public final static Set<NomLexClasses> NON_VERB_ADJ = new HashSet<NomLexEntry.NomLexClasses>(
			Arrays.asList(ABILITY, ATTRIBUTE, CRISSCROSS, ENVIRONMENT, EVENT,
					FIELD, GROUP, HALLMARK, ISSUE, JOB, PARTITIVE, RELATIONAL,
					SHARE, TYPE, VERSION, WORK_OF_ART));

	/**
	 * These are the possible NOMBANK classes. (See Section 5.3 in
	 * "Those Other Nombank Dictionaries"
	 */
	public static enum NomLexClasses {
		ABILITY, ABLE_NOM, ATTRIBUTE, CRISSCROSS,

		ENVIRONMENT, EVENT, FIELD, GROUP, HALLMARK,

		ISSUE, JOB, NOM, NOMADJ, NOMADJLIKE, NOMING,

		NOMLIKE, PARTITIVE, RELATIONAL, SHARE, TYPE,

		VERSION, WORK_OF_ART, UNKNOWN_CLASS;
	}

	// this is the root of the nomlex record
	public NomLexClasses nomClass;
	public String orth;
	public String plural;

	public String verb;
	public String adj;

	// this is the entry referenced by :NOMTYPE. This could be a structured
	// entry. So for now, I'll ignore it.
	// public String nom_type;

}