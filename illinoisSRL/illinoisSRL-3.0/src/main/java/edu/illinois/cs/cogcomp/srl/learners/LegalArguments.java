// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B880000000000000005725FDF42C0301EF75EE1BD0688FCC4F10790194613404D8FA56B36E58EA32DED4053CFFEE570C81A8F2D4377F5FBF177DCB6718DB127C091696CE8D79D458E838A76CD1F8D9BA9D035DE028411D5E8A1EB1E51DF2F40C961270DA80D30D9FA7F0E07BFF0E49E4023A0C0B1F8505E681380E713FCE589C681698C3BEA1AA39D402A7CCB1FE5A2D106A58C3DBD406BFE46114D596FD5F466818EB059E8112D89B98B1143C788D41A15DBA9EEFB21747589F492CC1306A6C5145C13DCB5383336B24FF88D871415E2C2E0DB4B724691586EC8F94DEB3E551DCCD4F649DAC8B5361DA89D8B321EF99FA73F06CDA3E8C889E062F7D203235C2F899D3D2B11EE4BE7AC2B4B6BD45E443B23C374350AF16685556ECB02D2A9BAADB1B6F26C4646300AEF6C5D08B31F610612A3EA40EFCD06B0D6DCAEB8F509A5312C1E7181D03A15B0E4EACA57038406F0BFF1027FC96B4F9200000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class LegalArguments extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public LegalArguments() { super("edu.illinois.cs.cogcomp.srl.learners.LegalArguments"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'LegalArguments(TextAnnotation)' defined on line 244 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      LinkedList legal = PropBankUtilities.getLegalArguments(verb.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier));
      {
        boolean LBJ2$constraint$result$0;
        {
          LBJ2$constraint$result$0 = true;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ2$constraint$result$0; )
          {
            Constituent a = (Constituent) __I0.next();
            {
              LBJ2$constraint$result$0 = false;
              for (java.util.Iterator __I1 = (legal).iterator(); __I1.hasNext() && !LBJ2$constraint$result$0; )
              {
                String type = (String) __I1.next();
                LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + (type));
              }
            }
          }
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'LegalArguments(TextAnnotation)' defined on line 244 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "LegalArguments".hashCode(); }
  public boolean equals(Object o) { return o instanceof LegalArguments; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'LegalArguments(TextAnnotation)' defined on line 244 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      LinkedList legal = PropBankUtilities.getLegalArguments(verb.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier));
      {
        Object[] LBJ$constraint$context = new Object[2];
        LBJ$constraint$context[0] = legal;
        LBJ$constraint$context[1] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              EqualityArgumentReplacer LBJ$EAR =
                new EqualityArgumentReplacer(LBJ$constraint$context)
                {
                  public Object getLeftObject()
                  {
                    Constituent a = (Constituent) quantificationVariables.get(0);
                    return a;
                  }

                  public String getRightValue()
                  {
                    String type = (String) quantificationVariables.get(1);
                    return "" + (type);
                  }
                };
              LBJ2$constraint$result$2 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), null, LBJ$EAR);
            }
            LBJ2$constraint$result$1 = new ExistentialQuantifier("type", legal, LBJ2$constraint$result$2);
          }
          LBJ2$constraint$result$0 = new UniversalQuantifier("a", argumentCandidates, LBJ2$constraint$result$1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      currentPredicateId++;
    }

    return __result;
  }
}

