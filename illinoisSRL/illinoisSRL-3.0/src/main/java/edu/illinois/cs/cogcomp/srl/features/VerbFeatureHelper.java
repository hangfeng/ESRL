/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.features;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.features.helpers.ParseHelper;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.SpanLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.TokenLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TreeView;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.ParseUtils;
import edu.illinois.cs.cogcomp.edison.utilities.VerbClassDictionary;

/**
 * @author Vivek Srikumar
 * 
 */
public class VerbFeatureHelper extends FeatureHelper {

	private static final Logger log = LoggerFactory
			.getLogger(VerbFeatureGenerator.class);

	static VerbClassDictionary verbClassDictionary;

	private static Set<String> toBeVerbs;

	static {
		InputStream resource = VerbFeatureHelper.class
				.getResourceAsStream("verbClass.txt");
		try {
			verbClassDictionary = new VerbClassDictionary(resource);
		} catch (IOException e) {
			log.error("Unable to read the verb class dictionary", e);
			System.exit(-1);
		}

		String[] strings = verbClassDictionary.getClass("give");
		log.info(
				"Loaded verb class dictionary. Test: classes for 'give' are {}",
				Arrays.asList(strings));

		toBeVerbs = new HashSet<String>();
		toBeVerbs.addAll(Arrays.asList("am", "are", "be", "been", "being",
				"is", "was", "were"));
	}

	public static String[] getVerbClass(Constituent argument) {
		Constituent predicate = argument.getIncomingRelations().get(0)
				.getSource();

		return verbClassDictionary.getClass(predicate
				.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier));
	}

	// A predicate is either active or passive. We use a simple rule to
	// determine
	// this: if the verb follows a "to-be" verb in the same verb phrase and its
	// POS tag is "VBN", then the predicate is passive. The verb may also be
	// nested in other verb phrases that are themselves nested in the verb
	// phrase
	// containing the "to-be" verb, so long as the only phrases found in between
	// the "to-be" verb and the target verb contain the target verb.
	//
	// This feature was not found to be useful for argument identification.
	// Also,
	// it is not useful in argument classification unless conjuncted with
	// LinearPosition. (Xue and Palmer, 2004)
	public static String getPredicateVoice(Constituent argument) {
		// a set of to-be verbs is contained in the static variable toBeVerbs.
		// should return "active" or "passive"

		int predicatePosition = getPredicatePosition(argument)
				- getConstituentSentenceStart(argument);

		Tree<String> tree = getParseTree(argument.getTextAnnotation(),
				argument.getSentenceId(), argument);
		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		Tree<Pair<String, IntPair>> currentNode = spanLabeledTree.getYield()
				.get(predicatePosition).getParent();

		if (currentNode.getLabel().getFirst().equals("VBN")) {
			Tree<Pair<String, IntPair>> parent = currentNode.getParent();
			Tree<Pair<String, IntPair>> sentinel = currentNode;

			while ((parent != null)
					&& !(parent.getLabel().getFirst().equals("VP"))) {
				sentinel = parent;
				parent = parent.getParent();
			}

			if (parent == null) {
				// System.out.println("Voice: VP tag not found "+argument.getTextAnnotation().getTokenizedText());
				return "unknown";
			}

			while (parent != null && parent.getLabel().getFirst().equals("VP")) {
				Vector<Tree<Pair<String, IntPair>>> children = new Vector<Tree<Pair<String, IntPair>>>();
				for (int i = 0; i < sentinel.getPositionAmongParentsChildren(); i++)
					children.addElement(parent.getChild(i));

				for (int i = children.size() - 1; i >= 0; --i) {
					if (children.elementAt(i).getChild(0).isLeaf()) {
						if (toBeVerbs.contains(children.elementAt(i)
								.getChild(0).getLabel().getFirst()))
							return "passive";
					}
				}
				sentinel = parent;
				parent = parent.getParent();
			}
		}
		/*
		 * if (currentNode.getLabel().getFirst().equals("VBN")) { currentNode =
		 * currentNode.getParent(); Tree<Pair<String, Pair<Integer, Integer>>>
		 * previous =currentNode.getParent().getChild(currentNode.
		 * getPositionAmongParentsChildren() - 1);
		 * if(previous.getLabel().getFirst().equals("NP")) return "passive"; }
		 */

		return "active";
	}

	public static String getModalVerb(Constituent argument) {
		Constituent predicate = getPredicateConstituent(argument);
		TextAnnotation ta = argument.getTextAnnotation();

		if (!ta.hasView(ViewNames.SHALLOW_PARSE))
			return "N";

		SpanLabelView shallowParse = (SpanLabelView) ta
				.getView(ViewNames.SHALLOW_PARSE);
		List<Constituent> lsc = shallowParse.getConstituentsCovering(predicate);

		if (lsc.size() == 0)
			return "N";

		if (lsc.size() != 1) {
			System.out.println("Error");
			System.out.println(ta.getTokenizedText());
			System.exit(0);
		}
		TokenLabelView tv = (TokenLabelView) ta.getView(ViewNames.POS);
		List<String> labels = tv.getLabelsCovering(lsc.get(0));

		if (labels.contains("MD"))
			return "Y";

		return "N";
	}

	public static String getNegatedVerb(Constituent argument) {
		Constituent predicate = argument.getIncomingRelations().get(0)
				.getSource();
		TextAnnotation ta = argument.getTextAnnotation();

		if (!ta.hasView(ViewNames.SHALLOW_PARSE))
			return "P";

		SpanLabelView shallowParse = (SpanLabelView) ta
				.getView(ViewNames.SHALLOW_PARSE);
		List<Constituent> lsc = shallowParse.getConstituentsCovering(predicate);

		if (lsc.size() == 0)
			return "P";

		if (lsc.size() != 1) {
			System.out.println("Error");
			System.out.println(ta.getTokenizedText());
			System.exit(0);
		}
		for (int i = lsc.get(0).getStartSpan(); i < lsc.get(0).getEndSpan(); i++)
			if (ta.getTokens()[i].equals("not")
					|| (ta.getTokens()[i].equals("n't")))
				return "N";

		return "P";

	}

	public static int getChunkPatternLength(Constituent candidate) {
		int predicatePosition = getPredicatePosition(candidate);
		return getChunksBetweenPredicateAndArg(candidate, predicatePosition)
				.size();
	}

	public static double getClauseCoverage(Constituent candidate) {
		Tree<String> tree = getPseudoParseTree(candidate);

		// tree = ParseUtils.stripFunctionTags(tree);

		int predicateId = getPredicatePosition(candidate);

		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		Tree<Pair<String, IntPair>> predicateTree;

		try {
			predicateTree = spanLabeledTree.getYield().get(predicateId);
		} catch (Exception e) {
			return 0.0;
		}

		while (!(predicateTree.getLabel().getFirst().startsWith("S"))
				&& predicateTree.getParent() != null)
			predicateTree = predicateTree.getParent();

		Pair<Integer, Integer> p = new Pair<Integer, Integer>(predicateTree
				.getLabel().getSecond().getFirst(), predicateTree.getLabel()
				.getSecond().getSecond());
		Pair<Integer, Integer> c = new Pair<Integer, Integer>(candidate
				.getSpan().getFirst(), candidate.getSpan().getSecond());

		int index = 0;

		if (c.getFirst() >= p.getSecond())
			;
		else if (c.getSecond() <= p.getFirst())
			;
		else
			index = (int) (p.getSecond() - p.getFirst()) * 100
					/ (c.getSecond() - c.getFirst());

		return index;
	}

	// Position of the constituent relative to the given predicate in terms of
	// parse tree ancestry. This classifier takes the place of the "clause
	// relative position" which computed something similar using the "semi-parse
	// tree" of clauses.
	public static String getClauseRelativePosition(Constituent candidate) {
		// System.out.println(candidate);
		Tree<String> tree = getPseudoParseTree(candidate);

		tree = ParseUtils.stripFunctionTags(tree);

		int predicateId = getPredicatePosition(candidate);

		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		Tree<Pair<String, IntPair>> predicateTree = null;
		Tree<Pair<String, IntPair>> argTree = null;

		try {
			predicateTree = spanLabeledTree.getYield().get(predicateId);
			Tree<Pair<String, IntPair>> argBeginTree = spanLabeledTree
					.getYield().get(candidate.getStartSpan());
			Tree<Pair<String, IntPair>> argEndTree = spanLabeledTree.getYield()
					.get(candidate.getEndSpan() - 1);
			argTree = ParseHelper.getCommonAncestor(argBeginTree, argEndTree,
					spanLabeledTree);
		} catch (Exception e) {
			// e.printStackTrace();
			return "unknown";
		}

		if (argTree.isLeaf())
			argTree = argTree.getParent();

		if (predicateTree.isLeaf())
			predicateTree = predicateTree.getParent();

		Tree<Pair<String, IntPair>> CA = null;

		try {
			CA = ParseHelper.getCommonAncestor(argTree, predicateTree,
					spanLabeledTree);
		} catch (Exception e) {

		}
		if ((argTree.getParent() == CA) && (predicateTree.getParent() == CA))
			return "S";

		if ((argTree.getParent() == CA) || (argTree == CA))
			return "A";

		if ((predicateTree.getParent() == CA) || (predicateTree == CA))
			return "B";

		return "O";
		/*
		 * String s = "";
		 * 
		 * try { s = ParseHelper.getSpanLabeledPathString(argTree,
		 * predicateTree, spanLabeledTree, 400); } catch(Exception e) {
		 * e.printStackTrace(); } return s;
		 */
	}

	/**
	 * @param candidate
	 * @return
	 */
	private static Tree<String> getPseudoParseTree(Constituent candidate) {
		Tree<String> tree = ((TreeView) (candidate.getTextAnnotation()
				.getView(ViewNames.PSEUDO_PARSE))).getTree(0);
		return tree;
	}

	public static String getSubcatFrame(Constituent argument) {
		Constituent predicate = argument.getIncomingRelations().get(0)
				.getSource();

		int predicatePosition = predicate.getStartSpan()
				- getConstituentSentenceStart(predicate);

		Tree<String> tree = getParseTree(predicate.getTextAnnotation(),
				argument.getSentenceId(), argument);
		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		Tree<Pair<String, IntPair>> currentNode = spanLabeledTree.getYield()
				.get(predicatePosition).getParent();

		StringBuffer subcat = new StringBuffer();
		Tree<Pair<String, IntPair>> argParentNode = currentNode.getParent();

		subcat.append(argParentNode.getLabel().getFirst() + "--->");

		for (Tree<Pair<String, IntPair>> t : argParentNode.getChildren())
			if (t == currentNode)
				subcat.append("(" + t.getLabel().getFirst() + ")" + "-");
			else
				subcat.append(t.getLabel().getFirst() + "-");

		return subcat.toString();
	}

	/*
	 * public static String getSyntacticFrame(Constituent candidate) {
	 * Constituent predicate = candidate.getIncomingRelations().get(0)
	 * .getSource(); String result = "v_";
	 * 
	 * int predicatePosition = predicate.getStartSpan();
	 * 
	 * Tree<String> tree = getParseTree(predicate.getTextAnnotation(), 0,
	 * candidate); Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
	 * .getSpanLabeledTree(tree);
	 * 
	 * Tree<Pair<String, IntPair>> currentNode = spanLabeledTree
	 * .getYield().get(predicatePosition).getParent();
	 * 
	 * Tree<Pair<String, IntPair>> parent = currentNode .getParent();
	 * 
	 * while (parent != null) { boolean before = true; String prefix = "";
	 * 
	 * for (Tree<Pair<String, IntPair>> sibling : parent .getChildren()) {
	 * Pair<String, Pair<Integer, Integer>> siblingNode = sibling .getLabel();
	 * 
	 * if ((siblingNode.getSecond().getFirst() <= predicate .getStartSpan()) &&
	 * (siblingNode.getSecond().getSecond() > predicate .getStartSpan())) {
	 * before = false; result = prefix + result; continue; }
	 * 
	 * if (!sibling.isLeaf()) { String phrase = siblingNode.getFirst();
	 * 
	 * if (phrase.equals("NP")) { if (before) { if
	 * (siblingNode.getSecond().equals( candidate.getSpan())) prefix += "|_";
	 * else prefix += "np_"; } else if (siblingNode.getSecond().equals(
	 * candidate.getSpan())) result += "_|"; else result += "np_"; }
	 * 
	 * if (phrase.equals("PP")) { for (Tree<Pair<String, IntPair>> siblingofPP :
	 * sibling .getChildren()) { Pair<String, Pair<Integer, Integer>>
	 * siblingNodeofPP = siblingofPP .getLabel();
	 * 
	 * if (!siblingofPP.isLeaf() && (siblingNodeofPP.getFirst().equals("NP"))) {
	 * if (before) { if (siblingNode.getSecond().equals( candidate.getSpan()))
	 * prefix += "|_"; else prefix += "np_"; } else if
	 * (siblingNode.getSecond().equals( candidate.getSpan())) result += "_|";
	 * else result += "np_"; } } } } } parent = parent.getParent(); } return
	 * result; }
	 */

	// /this was the past syntactic frame feature for ArgumentClassifier06044f
	// or whatever
	// also remove the contextwordforpredicate
	// remove these two features from verbfeaturegenerator
	// queries.java is modified to be correct for exclusive overlaps, has to
	// change it back for using ArgumentClasifier06044f or whatever
	public static String getSyntacticFrame(Constituent candidate) {
		Constituent predicate = candidate.getIncomingRelations().get(0)
				.getSource();
		StringBuilder sb = new StringBuilder();
		sb.append("v_");

		int predicatePosition = predicate.getStartSpan()
				- getConstituentSentenceStart(predicate);

		Tree<String> tree = getParseTree(predicate.getTextAnnotation(),
				candidate.getSentenceId(), candidate);
		Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
				.getSpanLabeledTree(tree);

		Tree<Pair<String, IntPair>> currentNode = spanLabeledTree.getYield()
				.get(predicatePosition).getParent();

		Tree<Pair<String, IntPair>> parent = currentNode.getParent();

		while (!parent.isRoot()) {
			boolean before = true;
			StringBuilder prefix = new StringBuilder();

			for (int i = 0; i < parent.getNumberOfChildren(); i++) {
				Tree<Pair<String, IntPair>> sibling = parent.getChildren().get(
						i);

				Pair<String, IntPair> siblingNode = sibling.getLabel();

				if (i == currentNode.getPositionAmongParentsChildren()) {
					before = false;

					sb.append(prefix.toString() + "V_");
					continue;
				}

				if (!sibling.isLeaf()) {
					String phrase = siblingNode.getFirst();

					if (phrase.equals("NP") || phrase.equals("PP")) {
						if (before)
							prefix.append(phrase + " ");
						else
							sb.append(phrase + "np_");
					}

				}
			}
			parent = parent.getParent();
		}
		return sb.toString();
	}

	public static String getPredicateContextWord(Constituent candidate) {
		int predicatePosition = getPredicatePosition(candidate)
				- getConstituentSentenceStart(candidate);
		TextAnnotation ta = candidate.getTextAnnotation();
		if (predicatePosition < ta.getSentence(candidate.getSentenceId())
				.size() - 1) {
			Tree<String> tree = getParseTree(candidate.getTextAnnotation(),
					candidate.getSentenceId(), candidate);
			return tree.getYield().get(predicatePosition + 1).getLabel()
					+ "^"
					+ tree.getYield().get(predicatePosition + 1).getParent()
							.getLabel();
		}
		return "";
	}

}