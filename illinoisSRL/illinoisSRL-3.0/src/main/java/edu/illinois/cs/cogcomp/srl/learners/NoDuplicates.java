// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B880000000000000005B09DCB62C04015CFF591C3D60B886F3E26AE124A8450922DF2A7DD623AD18DCEACEEC6D2D2EFFED9459811DE13790167EDCFEDCB7583B18DB62BC0F0EEE2EAD051A9138A76CF2ECCA57CA99C95808691D61890CF0CC9203CAD369B392C40E9E17EF2C462C0658CB8660AA95A41A6E711DB79796403B25657892CB2AF7FCCFA26523C9592F1A521A70A3CFE40C2E6EF1D921AF6E38C028A33607BDA347018EB15525748CBE4D4C154D0F92851705DA7B4ADB5299EC095C5FFA70DBF3827D6B4A27FD7CB54C586351AFB7C8E54545805C2A791F49C03A06E0AF9A3FD0755D7CC54B2AB8DC9A77FBF9286EAC98DF80CD2F82D869AA14D9E559C15D79B1D12CEAE3D90C87C0DBC68DBE00A3AE20A79D504FAAB08E57710DB9E20A99047B0BDF50121864B9E0400000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NoDuplicates extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public NoDuplicates() { super("edu.illinois.cs.cogcomp.srl.learners.NoDuplicates"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NoDuplicates(TextAnnotation)' defined on line 47 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      currentPredicateId++;
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A0"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A1"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A2"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A3"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A4"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A5"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AA"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NoDuplicates(TextAnnotation)' defined on line 47 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NoDuplicates".hashCode(); }
  public boolean equals(Object o) { return o instanceof NoDuplicates; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NoDuplicates(TextAnnotation)' defined on line 47 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      currentPredicateId++;
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A0"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A1"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A2"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A3"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A4"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A5"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AA"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
    }

    return __result;
  }
}

