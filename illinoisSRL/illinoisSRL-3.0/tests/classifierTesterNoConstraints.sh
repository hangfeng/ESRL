#!/bin/bash

JAVA=java

BASEDIR=.
LIBDIR=$BASEDIR/target/dependency

CP=$BASEDIR/models/verb:target/classes
for file in `ls $LIBDIR`; do
    CP=$CP:"$LIBDIR/$file"
done

MEMORY="-Xmx14g"

OPTIONS="-ea -cp $CP $MEMORY -Xverify:all"

$JAVA $OPTIONS edu.illinois.cs.cogcomp.srl.testers.SRLClassifierTesterNoConstraints $*