// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// discrete VerbSRLExtraFeatures$$2(Constituent c) <- SyntacticFrame && PredicateLemma

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.main.Constants;
import java.util.*;


public class VerbSRLExtraFeatures$$2 extends Classifier
{
  private static final SyntacticFrame left = new SyntacticFrame();
  private static final PredicateLemma right = new PredicateLemma();

  public VerbSRLExtraFeatures$$2()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "VerbSRLExtraFeatures$$2";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete"; }

  public Feature featureValue(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'VerbSRLExtraFeatures$$2(Constituent)' defined on line 22 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    Feature __result;
    __result = left.featureValue(__example).conjunction(right.featureValue(__example), this);
    return __result;
  }

  public FeatureVector classify(Object __example)
  {
    return new FeatureVector(featureValue(__example));
  }

  public String discreteValue(Object __example)
  {
    return featureValue(__example).getStringValue();
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'VerbSRLExtraFeatures$$2(Constituent)' defined on line 22 of VerbSRLClassifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "VerbSRLExtraFeatures$$2".hashCode(); }
  public boolean equals(Object o) { return o instanceof VerbSRLExtraFeatures$$2; }
}

