// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000055E81BA02C040150F75E5977D8F30652636022403616BBA9D89343BB277B12159CFBB13011C2760606284DC195E2C82F0976B0C9E11935AD718689A2C1970BD888A1950514261B9B8736BA29C28573910D4C19DAB820A5D59DEFB27E7D8112A1B5AB7871757E7248B749A5B5B4383F04F1CA99D56971FB117F51BCDCE45F6ECB7C8F10415C15195B000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;
import java.util.*;


public class SRLInferenceXpressMP extends ILPInference
{
  public static TextAnnotation findHead(Constituent a)
  {
    return a.getTextAnnotation();
  }


  public SRLInferenceXpressMP() { }
  public SRLInferenceXpressMP(TextAnnotation head)
  {
    super(head, new XpressMPHook());
    constraint = new SRLInferenceXpressMP$subjectto().makeConstraint(head);
  }

  public String getHeadType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }
  public String[] getHeadFinderTypes()
  {
    return new String[]{ "edu.illinois.cs.cogcomp.edison.sentences.Constituent" };
  }

  public Normalizer getNormalizer(Learner c)
  {
    return new Softmax();
  }
}

