// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// discrete% NomSRLIdConjunctions(Constituent c) cached <- NomSRLFeatures1 && NomSRLFeatures1

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomSRLIdConjunctions extends Classifier
{
  private static final NomSRLFeatures1 left = new NomSRLFeatures1();
  private static ThreadLocal __cache = new ThreadLocal(){ };
  private static ThreadLocal __exampleCache = new ThreadLocal(){ };
  public static void clearCache() { __exampleCache = new ThreadLocal() { }; }

  public NomSRLIdConjunctions()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "NomSRLIdConjunctions";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  private FeatureVector cachedFeatureValue(Object __example)
  {
    if (__example == __exampleCache.get()) return (FeatureVector) __cache.get();
    __exampleCache.set(__example);
    FeatureVector __result;
    __result = new FeatureVector();
    FeatureVector leftVector = left.classify(__example);
    int N = leftVector.featuresSize();
    for (int j = 1; j < N; ++j)
    {
      Feature rf = leftVector.getFeature(j);
      for (int i = 0; i < j; ++i)
        __result.addFeature(leftVector.getFeature(i).conjunction(rf, this));
    }
    __result.sort();
    __cache.set(__result);
    return __result;
  }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'NomSRLIdConjunctions(Constituent)' defined on line 19 of NomSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return cachedFeatureValue(__example);
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomSRLIdConjunctions(Constituent)' defined on line 19 of NomSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomSRLIdConjunctions".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomSRLIdConjunctions; }
}

