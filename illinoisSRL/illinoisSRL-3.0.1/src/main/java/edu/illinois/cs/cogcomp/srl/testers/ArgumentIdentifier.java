/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.testers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LBJ2.classify.*;
import LBJ2.learn.Learner;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.srl.main.Constants;

/**
 * @author Vivek Srikumar
 */
public class ArgumentIdentifier {

	private static Logger log = LoggerFactory
			.getLogger(ArgumentIdentifier.class);

	private double threshold;
	private final Learner identifier;
	private final double beta;

	public ArgumentIdentifier(double threshold, double beta, Learner identifier) {
		this.threshold = threshold;
		this.beta = beta;
		this.identifier = identifier;
	}

	public void setThreshold(double t) {
		this.threshold = t;
	}

	public Pair<Boolean, Double> classify(String c) {

		ScoreSet scores = identifier.scores(c);

		double trueScore = scores.get("true");

		return thresholdScore(trueScore);
	}

	public Pair<Boolean, Double> thresholdScore(double trueScore) {
		double trueProb = 1 / (1 + Math.exp(-beta * trueScore));

		boolean prediction;
		double score;
		if (trueProb > threshold) {
			prediction = true;
			score = trueProb;
		} else {
			prediction = false;
			score = 1 - trueProb;
		}

		if (Constants.VERBOSE)
			System.out.println(trueScore + " " + beta + " "
					+ (-beta * trueScore) + " " + Math.exp(-beta * trueScore)
					+ " " + trueProb + " " + prediction + " " + score);
		return new Pair<Boolean, Double>(prediction, score);
	}

	public Pair<Boolean, Double> classify(Constituent c) {

		log.debug("Classifying {}", c.toString());

		ScoreSet scores = identifier.scores(c);

		double trueScore = scores.get("true");

		Pair<Boolean, Double> output = thresholdScore(trueScore);

		log.debug("Prediction: {} ", output.toString());

		return output;
	}

	public Learner getIdentiferClassifer() {
		return this.identifier;
	}
}
