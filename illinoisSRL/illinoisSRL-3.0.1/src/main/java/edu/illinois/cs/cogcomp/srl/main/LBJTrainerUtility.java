/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LBJ2.learn.Accuracy;
import LBJ2.learn.BatchTrainer;
import LBJ2.learn.Learner;
import LBJ2.learn.Lexicon;
import LBJ2.learn.SparseAveragedPerceptron;
import LBJ2.learn.SparseNetworkLearner;
import LBJ2.learn.Learner.Parameters;
import LBJ2.parse.ArrayFileParser;
import LBJ2.parse.FoldParser;
import LBJ2.parse.Parser;
import LBJ2.util.ExceptionlessOutputStream;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;

/**
 * @author Vivek Srikumar
 * 
 * 
 */
public class LBJTrainerUtility {

	private static Logger log = LoggerFactory
			.getLogger(LBJTrainerUtility.class);

	public static Pair<Lexicon, Lexicon> preExtractTrain(Parser parser,
			Learner learner, String preExtractFile, int pruneCount,
			int progressOutput) {
		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		Learner preExtractLearner = lbj.preExtract(preExtractFile,
				Lexicon.CountPolicy.global);

		lbj.pruneDataset(preExtractFile, new Lexicon.PruningPolicy(pruneCount),
				preExtractLearner);

		Lexicon l = preExtractLearner.getLexicon();

		preExtractLearner.saveLexicon();
		System.gc();
		return new Pair<Lexicon, Lexicon>(l,
				preExtractLearner.getLabelLexicon());
	}

	public static void writeLabelLexicon(String trainPrefix, Lexicon ll) {

		String file = trainPrefix;
		if (!file.endsWith(".label.lex"))
			file = file + ".label.lex";

		ExceptionlessOutputStream outputStream = ExceptionlessOutputStream
				.openCompressedStream(file);
		ll.write(outputStream);
		outputStream.close();
	}

	public static void writeLexicon(String trainPrefix, Lexicon ll) {

		String file = trainPrefix;
		if (!file.endsWith(".lex"))
			file = file + ".lex";

		assert !file.endsWith(".label.lex");

		ExceptionlessOutputStream outputStream = ExceptionlessOutputStream
				.openCompressedStream(file);
		ll.write(outputStream);

		outputStream.close();
	}

	public static Lexicon loadLabelLexicon(String trainPrefix) {

		String file = trainPrefix;
		if (!trainPrefix.endsWith(".label.lex"))
			file = trainPrefix + ".label.lex";
		return Lexicon.readLexicon(file);
	}

	public static Lexicon loadLexicon(String trainPrefix) {

		String file = trainPrefix;
		if (!trainPrefix.endsWith(".lex"))
			file = trainPrefix + ".lex";
		return Lexicon.readLexicon(file);
	}

	public static void cv(int numTrainingRounds, String preExtractFile,
			Learner learner) {

		Parser parser = new ArrayFileParser(preExtractFile, true);

		BatchTrainer lbj = new BatchTrainer(learner, parser, 10000);

		double[] thicknesses = new double[] { 2, 2.5, 3, 3.5, 4, 5, 6, 10, 12 };
		double[] learningRates = new double[] { 0.01, 0.05, 0.1, 0.2 };

		Parameters[] parameterCombinations = new Parameters[thicknesses.length
				* learningRates.length];

		int paramId = 0;
		for (double t : thicknesses) {
			for (double l : learningRates) {
				if (learner instanceof SparseNetworkLearner) {

					SparseNetworkLearner.Parameters p = new SparseNetworkLearner.Parameters();

					SparseAveragedPerceptron.Parameters p1 = new SparseAveragedPerceptron.Parameters();
					p1.learningRate = l;
					p1.thickness = t;

					p.baseLTU = new SparseAveragedPerceptron(p1);
					parameterCombinations[paramId] = p;

				} else if (learner instanceof SparseAveragedPerceptron) {
					SparseAveragedPerceptron.Parameters p1 = new SparseAveragedPerceptron.Parameters();
					p1.learningRate = l;
					p1.thickness = t;

					parameterCombinations[paramId] = p1;
				} else {
					log.error("Invalid learner type. "
							+ "Neither SparseAveragedPerceptron "
							+ "nor SparseNetworkLearner");
				}

				paramId++;
			}
		}

		int k = 5;
		int[] rounds = { 10 };

		Parameters best = lbj.tune(parameterCombinations, rounds, k,
				FoldParser.SplitPolicy.random, 0.05, new Accuracy());

		System.out.println("best params: " + best.nonDefaultString());

		lbj.train(numTrainingRounds);

		learner.save();

		System.gc();
	}

	public static void train(int numTrainingRounds, String lcPrefix,
			String trainCachePrefix, Learner learner, double learningRate,
			double thickness) {

		Parser parser = new ArrayFileParser(trainCachePrefix + ".features",
				true);

		Lexicon ll = Lexicon.readLexicon(trainCachePrefix + ".label.lex");

		learner.setLabelLexicon(ll);

		train(numTrainingRounds, learner, learningRate, thickness, parser);

		learner.save();
	}

	public static void train(int numTrainingRounds, Learner learner,
			double learningRate, double thickness, Parser parser) {

		if (learner instanceof SparseNetworkLearner) {

			SparseNetworkLearner.Parameters p = new SparseNetworkLearner.Parameters();

			SparseAveragedPerceptron.Parameters p1 = new SparseAveragedPerceptron.Parameters();
			p1.learningRate = learningRate;
			p1.thickness = thickness;

			p.baseLTU = new SparseAveragedPerceptron(p1);

			learner.setParameters(p);
		} else if (learner instanceof SparseAveragedPerceptron) {
			SparseAveragedPerceptron.Parameters p1 = new SparseAveragedPerceptron.Parameters();
			p1.learningRate = learningRate;
			p1.thickness = thickness;
			learner.setParameters(p1);
		} else {
			log.error("Invalid learner type. "
					+ "Neither SparseAveragedPerceptron "
					+ "nor SparseNetworkLearner");
		}

		BatchTrainer lbj = new BatchTrainer(learner, parser, 10000);

		lbj.train(numTrainingRounds);

	}

	public static boolean checkClassifierExists(String name,
			boolean forceRetrain) {
		boolean value = false;
		if (IOUtils.exists(name)) {
			if (!forceRetrain) {
				log.debug("Found classifier with name: {}. Exiting train.",
						name);

				value = true;
			} else {
				try {
					IOUtils.rm(name);
					log.info("Forcing to retrain.");
				} catch (Exception ex) {
					log.error("Unable to remove the existing classifier", ex);
					return false;
				}
			}
		} else {
			log.info("{} not found. Training...", name);
		}
		return value;
	}

}
