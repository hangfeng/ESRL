// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// discrete ArgumentTypeBeamSearch(Constituent a) <- SRLInferenceBeamSearch(VerbArgumentClassifier)

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;

public class ArgumentTypeBeamSearch extends Classifier {
	private static final String lexFile = VerbArgumentClassifier.class
			.getResource("VerbArgumentClassifier.lex").getPath();
	private static final String lcFile = VerbArgumentClassifier.class
			.getResource("VerbArgumentClassifier.lc").getPath();

	private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier(
			lcFile, lexFile);

	public VerbArgumentClassifier getInnerClassifier() {
		return __VerbArgumentClassifier;
	}

	public ArgumentTypeBeamSearch() {
		containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
		name = "ArgumentTypeBeamSearch";
	}

	public String getInputType() {
		return "edu.illinois.cs.cogcomp.edison.sentences.Constituent";
	}

	public String getOutputType() {
		return "discrete";
	}

	public FeatureVector classify(Object __example) {
		return new FeatureVector(featureValue(__example));
	}

	public Feature featureValue(Object __example) {
		String result = discreteValue(__example);
		return new DiscretePrimitiveStringFeature(containingPackage, name, "",
				result, valueIndexOf(result), (short) allowableValues().length);
	}

	public String discreteValue(Object __example) {
		if (!(__example instanceof Constituent)) {
			String type = __example == null ? "null" : __example.getClass()
					.getName();
			System.err
					.println("Classifier 'ArgumentTypeBeamSearch(Constituent)' defined on line 288 of VerbSRLConstraints.lbj received '"
							+ type + "' as input.");
			new Exception().printStackTrace();
			System.exit(1);
		}

		TextAnnotation head = SRLInferenceBeamSearch
				.findHead((edu.illinois.cs.cogcomp.edison.sentences.Constituent) __example);
		SRLInferenceBeamSearch inference = (SRLInferenceBeamSearch) InferenceManager
				.get("edu.illinois.cs.cogcomp.srl.learners.SRLInferenceBeamSearch",
						head);

		if (inference == null) {
			inference = new SRLInferenceBeamSearch(head);
			InferenceManager.put(inference);
		}

		String result = null;

		try {
			result = inference.valueOf(__VerbArgumentClassifier, __example);
		} catch (Exception e) {
			System.err
					.println("LBJ ERROR: Fatal error while evaluating classifier ArgumentTypeBeamSearch: "
							+ e);
			e.printStackTrace();
			System.exit(1);
		}

		return result;
	}

	public FeatureVector[] classify(Object[] examples) {
		if (!(examples instanceof edu.illinois.cs.cogcomp.edison.sentences.Constituent[])) {
			String type = examples == null ? "null" : examples.getClass()
					.getName();
			System.err
					.println("Classifier 'ArgumentTypeBeamSearch(edu.illinois.cs.cogcomp.edison.sentences.Constituent)' defined on line 288 of VerbSRLConstraints.lbj received '"
							+ type + "' as input.");
			new Exception().printStackTrace();
			System.exit(1);
		}

		return super.classify(examples);
	}

	public int hashCode() {
		return "ArgumentTypeBeamSearch".hashCode();
	}

	public boolean equals(Object o) {
		return o instanceof ArgumentTypeBeamSearch;
	}
}
