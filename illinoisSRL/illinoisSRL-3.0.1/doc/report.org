
* Nominalizations

** Nombank dictionary statistics
   - The following data comes from NOMLEX-plus-clean-1.0.

| Nominalization Class | Count |
|----------------------+-------|
| NOM                  |  3934 |
| NOMLIKE              |  1244 |
| NOMING               |   359 |
| ABLE-NOM             |    18 |
|----------------------+-------|
| NOMADJ               |   503 |
| NOMADJLIKE           |   142 |
|----------------------+-------|
| PARTITIVE            |   509 |
| ATTRIBUTE            |   417 |
| RELATIONAL           |   331 |
| WORK-OF-ART          |   188 |
| ABILITY              |   112 |
| ENVIRONMENT          |    91 |
| GROUP                |    84 |
| HALLMARK             |    38 |
| JOB                  |    28 |
| VERSION              |    21 |
| TYPE                 |    17 |
| EVENT                |    12 |
| SHARE                |    12 |
| ISSUE                |    11 |
| CRISSCROSS           |     7 |
| FIELD                |     6 |
|----------------------+-------|
| Total                |  8084 |

   - Grouping these per type, we get

| Nominalization type | Count |
|---------------------+-------|
| Verbal              |  5555 |
| Adjectival          |   645 |
| Other               |  1884 |
|---------------------+-------|
| Total               |  8084 |



** Nombank training data statistics

   - Statistics for distribution of NOMLEX classes over the training data
| Type          | Number of occurrences |
|---------------+-----------------------|
| NOM           |                 50612 |
| NOMLIKE       |                 22583 |
| PARTITIVE     |                 11841 |
| ATTRIBUTE     |                 10456 |
| RELATIONAL    |                  7527 |
| ABILITY       |                  5904 |
| WORK_OF_ART   |                  3852 |
| GROUP         |                  3633 |
| NOMADJ        |                  3280 |
| NOMING        |                  3252 |
| SHARE         |                  3192 |
| ENVIRONMENT   |                  3108 |
| NOMADJLIKE    |                  2700 |
| JOB           |                  1502 |
| ISSUE         |                  1031 |
| VERSION       |                   725 |
| CRISSCROSS    |                   346 |
| FIELD         |                   330 |
| HALLMARK      |                   299 |
| EVENT         |                   172 |
| ABLE_NOM      |                    60 |
| UNKNOWN_CLASS |                     1 |


   - Statistics for distribution of NOMLEX classes for predicates that
     have at least one of the four Deverbal classes
| Type        | Number of occurrences |
|-------------+-----------------------|
| NOM         |                 50612 |
| NOMLIKE     |                 22583 |
| ATTRIBUTE   |                  4805 |
| PARTITIVE   |                  3998 |
| NOMING      |                  3252 |
| SHARE       |                  2646 |
| ABILITY     |                  2583 |
| WORK_OF_ART |                  2134 |
| RELATIONAL  |                  2090 |
| NOMADJLIKE  |                  1489 |
| ENVIRONMENT |                  1094 |
| GROUP       |                  1078 |
| JOB         |                   933 |
| NOMADJ      |                   650 |
| ISSUE       |                   592 |
| VERSION     |                   543 |
| TYPE        |                   328 |
| FIELD       |                   251 |
| CRISSCROSS  |                   212 |
| HALLMARK    |                   164 |
| ABLE_NOM    |                    60 |



