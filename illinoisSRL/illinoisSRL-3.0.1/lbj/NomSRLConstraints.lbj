package edu.illinois.cs.cogcomp.srl.learners;

import java.util.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;


constraint NomNoOverlaps(TextAnnotation sentence)
{
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);

    int currentPredicateId = 0;							

    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
						
    while(currentPredicateId < predicates.size()) {
	Constituent nom = (Constituent)predicates.get(currentPredicateId);


	List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);

	for (int j = 0; j < sentence.getTokens().length; ++j) {
	    if (nom.getStartSpan() == j) continue;

	    LinkedList containsWord = new LinkedList();
	    for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
		{
		    Constituent candidate = (Constituent) I.next();
    		 
		    if (candidate.getStartSpan()<=j && candidate.getEndSpan() > j)
			containsWord.add(candidate);
		}
	    if(containsWord.size()>1)
		{
		    atmost 1 of (Constituent a in containsWord)  NomArgumentClassifier(a) !: "null";
		}
	}
	currentPredicateId++;	     
    }
}

constraint NomNoDuplicates(TextAnnotation sentence)
{

    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);

    int currentPredicateId = 0;		
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();					
						
    while(currentPredicateId < predicates.size())
	{
	    Constituent nom = (Constituent)predicates.get(currentPredicateId);

	    List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);

	    currentPredicateId ++;
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A0";
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A1";
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A2";
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A3";
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A4";
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A5";
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A8";
	    atmost 1 of (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A9";
	}
}

constraint NomReferences(TextAnnotation sentence)
{
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);

    NomArgumentIdentifier identifier = new NomArgumentIdentifier();					
    int currentPredicateId = 0;							
							
    while(currentPredicateId < predicates.size())

	{
	    Constituent nom = (Constituent)predicates.get(currentPredicateId);

	    List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);


	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-A0")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A0");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-A1")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A1");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-A2")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A2");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-A3")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A3");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-A4")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A4");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-A5")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "A5");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AA")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AA");
	
	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-ADV")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-ADV");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-CAU")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-CAU");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-DIR")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-DIR");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-DIS")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-DIS");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-EXT")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-EXT");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-LOC")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-LOC");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-MNR")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-MNR");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-MOD")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-MOD");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-NEG")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-NEG");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-PNC")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-PNC");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-PRD")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-PRD");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-REC")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-REC");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-TM")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-TM");

	    (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "R-AM-TMP")
		=> (exists (Constituent a in argumentCandidates) NomArgumentClassifier(a) :: "AM-TMP");

	    currentPredicateId++;
	}

}

constraint NomContinuences(TextAnnotation sentence)
{

    NomArgumentIdentifier identifier = new NomArgumentIdentifier();					
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);

    int currentPredicateId = 0;							
							
    while(currentPredicateId < predicates.size()) {
	Constituent nom = (Constituent)predicates.get(currentPredicateId);

	List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);

	for (int j = 0; j < argumentCandidates.size(); ++j) {
	    Constituent cb = (Constituent)argumentCandidates.get(j);
	    LinkedList before = new LinkedList();

	    for (int k = 0; k < argumentCandidates.size(); ++k) {
		Constituent c = (Constituent)argumentCandidates.get(k);
		if (c.getEndSpan() <= cb.getStartSpan())
		    before.add(c);
	    }

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-A0"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "A0");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-A1"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "A1");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-A2"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "A2");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-A3"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "A3");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-A4"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "A4");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-A5"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "A5");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-ADV"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-ADV");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-CAU"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-CAU");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-DIR"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-DIR");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-DIS"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-DIS");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-EXT"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-EXT");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-LOC"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-LOC");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-MNR"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-MNR");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-MOD"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-MOD");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-NEG"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-NEG");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-PNC"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-PNC");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-PRD"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-PRD");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-REC"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-REC");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-TM"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-TM");

	    NomArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-TMP"
		=> (exists (Constituent a in before) NomArgumentClassifier(a) :: "AM-TMP");
	}
	currentPredicateId++;
    }

}

constraint NomLegalArguments(TextAnnotation sentence)
{
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();					
    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);

    int currentPredicateId = 0;							
						
    while(currentPredicateId < predicates.size()) {
	Constituent nom = (Constituent)predicates.get(currentPredicateId);

	List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
	
	LinkedList legal = NombankUtilities.getLegalArguments(nom);

	forall (Constituent a in argumentCandidates)
	    exists (String type in legal)
	    NomArgumentClassifier(a) :: type;

	currentPredicateId++;
    }
}

inference NomSRLInferenceXpressMP head  TextAnnotation sentence
{
    Constituent a      { return a.getTextAnnotation(); }
  
    normalizedby new Softmax()
    subjectto {
	@NomNoOverlaps(sentence) /\ @NomLegalArguments(sentence) /\ @NomNoDuplicates(sentence) /\@NomContinuences(sentence) /\ @NomReferences(sentence); 
    }

    with new ILPInference(new XpressMPHook())
}


inference NomSRLInferenceBeamSearch head  TextAnnotation sentence
{
    Constituent a      { return a.getTextAnnotation(); }
  
    normalizedby new Softmax()
    subjectto {
	@NomNoOverlaps(sentence) /\ @NomLegalArguments(sentence) /\ @NomNoDuplicates(sentence) /\@NomContinuences(sentence) /\ @NomReferences(sentence); 
    }

    with new ILPInference(new BeamSearch(SRLConfig.getInstance().getBeamSize()))
}


discrete NomArgumentTypeXpressMP(Constituent a) <- NomSRLInferenceXpressMP(NomArgumentClassifier)

discrete NomArgumentTypeBeamSearch(Constituent a) <- NomSRLInferenceBeamSearch(NomArgumentClassifier)
