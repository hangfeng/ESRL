#!/bin/bash

dir=`dirname $0`

LIBDIR=$dir/bin
MODELSDIR=$dir/models

CP=config

for file in `ls $MODELSDIR`; do
    CP=$CP:$MODELSDIR/$file
done

for file in `ls $LIBDIR`; do
    CP=$CP:$LIBDIR/$file
done
    
OPTIONS="-ea -XX:+UseParallelGC -cp $CP $MEMORY"

time nice java $MEMORY $OPTIONS  edu.illinois.cs.cogcomp.srl.main.SRLApplication $*
