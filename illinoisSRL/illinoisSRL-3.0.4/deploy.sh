#!/bin/bash

VERSION=`mvn org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | grep -v 'INFO'`

echo "Deploying Illinois SRL version $VERSION"

# First copy all the models
BIN=target/classes
LBJBIN=target/classes
SRC=src/main/java/
GSP=$SRC

MODELSDIR=models


# also package the models

echo "Packaging verb models into jar"
tmpdir=tmp-Srl-$RANDOM
mkdir -p $tmpdir/edu/illinois/cs/cogcomp/srl/{learners,features}
cp models/edu/illinois/cs/cogcomp/srl/learners/Verb* $tmpdir/edu/illinois/cs/cogcomp/srl/learners/
cp models/edu/illinois/cs/cogcomp/srl/features/verbClass.txt $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cp models/edu/illinois/cs/cogcomp/srl/features/verb.legal.arguments $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cd $tmpdir
jar cf illinoisSRL-verb-models-$VERSION.jar edu
mv illinoisSRL-verb-models-$VERSION.jar ..
cd ..
rm -rdf $tmpdir

echo "Done."

echo "Packaging nom models into jar"
tmpdir=tmp-Srl-$RANDOM
mkdir -p $tmpdir/edu/illinois/cs/cogcomp/srl/{learners,features}
cp models/edu/illinois/cs/cogcomp/srl/learners/Nom* $tmpdir/edu/illinois/cs/cogcomp/srl/learners/
cp models/edu/illinois/cs/cogcomp/srl/features/NOMLEX-plus-clean.1.0 $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cp models/edu/illinois/cs/cogcomp/srl/features/nombank.labels.allowed $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cd $tmpdir
jar cf illinoisSRL-nom-models-$VERSION.jar edu
mv illinoisSRL-nom-models-$VERSION.jar ..
cd ..
rm -rdf $tmpdir

echo "Done."


# now deploy
echo "Deploying code jar"
mvn clean package deploy


m2repository=scp://bilbo.cs.uiuc.edu:/mounts/bilbo/disks/0/www/cogcomp/html/m2repo
grp=edu.illinois.cs.cogcomp

file=illinoisSRL-verb-models-$VERSION.jar
artifact=illinoisSRL-verb-models


echo "Deploying verb models"

mvn deploy:deploy-file \
    -Durl=$m2repository \
    -DrepositoryId=CogcompSoftware \
    -Dfile=$file \
    -DgroupId=$grp \
    -Dversion=$VERSION \
    -DartifactId=$artifact \
    -Dpackaging=jar



file=illinoisSRL-nom-models-$VERSION.jar
artifact=illinoisSRL-nom-models


echo "Deploying nom models"
mvn deploy:deploy-file \
    -Durl=$m2repository \
    -DrepositoryId=CogcompSoftware \
    -Dfile=$file \
    -DgroupId=$grp \
    -Dversion=$VERSION \
    -DartifactId=$artifact \
    -Dpackaging=jar

