// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// discrete% VerbSRLIdConjunctions(Constituent c) <- VerbSRLFeatures1 && VerbSRLFeatures1

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import java.util.*;


public class VerbSRLIdConjunctions extends Classifier
{
  private static final VerbSRLFeatures1 left = new VerbSRLFeatures1();
  public VerbSRLIdConjunctions()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "VerbSRLIdConjunctions";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'VerbSRLIdConjunctions(Constituent)' defined on line 17 of VerbSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    FeatureVector __result;
    __result = new FeatureVector();
    FeatureVector leftVector = left.classify(__example);
    int N = leftVector.featuresSize();
    for (int j = 1; j < N; ++j)
    {
      Feature rf = leftVector.getFeature(j);
      for (int i = 0; i < j; ++i)
        __result.addFeature(leftVector.getFeature(i).conjunction(rf, this));
    }
    __result.sort();
    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'VerbSRLIdConjunctions(Constituent)' defined on line 17 of VerbSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "VerbSRLIdConjunctions".hashCode(); }
  public boolean equals(Object o) { return o instanceof VerbSRLIdConjunctions; }
}

