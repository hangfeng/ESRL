// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000DA79D5F6AD034168FFAC1175E88515F3F6A92392A0473420A111A55F6DD4E0D994AE45E895BAD4BFFEB378038C60863DC61212836BF93FCB6BF229CA2D5B532596B0175ADA2DB24D916D26E8FE632DAB2B2DAAA434D8AD2F0400FD1E10DC3546E975F245B15E4F5A61A0D08ADDFCE386C7B323F44012C855D61E5D06EA23961B6A9F9EC6C7F655957FE91D2FAB46B3826BDA3406BCC656C0556B316493D2F3D01EDEB8A2114C181EB965BAE5DAEB1A808364CC9C595ACB61EB25B42C8865D286FA22BA304EF5944E6246C257EA2FD42A7C51622BC7143F917568669AC882A1D0D0FDAA2D2216673FF6B23B5EA0699F0DADE4A63B8AC080E4FBC62CE2924DE77DD48B01ABDD5EF9903BDFC77000C9379BE85AB0CC7D1EE909A93E6E8477571D6BA2A1BA2EF6655CE95DFBA451C7AFB0A3E5EF734D97AFA25B800E6AF41C8B49A596C6354EE1D847F46E9B8C865EF0AF4BF636C5ACABE6E63E1DD4800EAFA1A31F9447AD18EF740C77A0D5FF62F29EE32886A550749FD078821EFFB2C99715833715837F2A07EE2A071E541E2C541E2DB82C5AB82C597158B271589C9443870F2A1D09C9452E8EED39A039C945603A9972516293AA4AED452573591E3ECD39A039C9456C771B725162939AC46AEBEEA039CD45EE60EB45884E4A23D1E72F4A2C42725946AEBEEA039CD4566EBE08894E4A23B1AFAD5162939ACC72E9C4804E8229873394A3CF4BABFFAA1DDEEAF926F72021DBB9D5EAD00000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class Continuences extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public Continuences() { super("edu.illinois.cs.cogcomp.srl.learners.Continuences"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'Continuences(TextAnnotation)' defined on line 156 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      for (int j = 0; j < argumentCandidates.size(); ++j)
      {
        Constituent cb = (Constituent) argumentCandidates.get(j);
        LinkedList before = new LinkedList();
        for (int k = 0; k < argumentCandidates.size(); ++k)
        {
          Constituent c = (Constituent) argumentCandidates.get(k);
          if (c.getEndSpan() <= cb.getStartSpan())
          {
            before.add(c);
          }
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A0"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A0"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A1"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A1"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A2"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A2"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A3"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A3"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A4"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A4"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-A5"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("A5"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-ADV"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-ADV"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-CAU"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-CAU"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-DIR"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIR"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-DIS"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-DIS"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-EXT"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-EXT"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-LOC"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-LOC"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-MNR"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-MNR"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-MOD"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-MOD"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-NEG"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-NEG"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-PNC"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-PNC"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-PRD"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-PRD"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-REC"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-REC"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-TM"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-TM"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
        {
          boolean LBJ2$constraint$result$0;
          {
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__VerbArgumentClassifier.discreteValue(argumentCandidates.get(j)))).equals("" + ("C-AM-TMP"));
            if (LBJ2$constraint$result$1)
              {
                LBJ2$constraint$result$0 = false;
                for (java.util.Iterator __I0 = (before).iterator(); __I0.hasNext() && !LBJ2$constraint$result$0; )
                {
                  Constituent a = (Constituent) __I0.next();
                  LBJ2$constraint$result$0 = ("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("AM-TMP"));
                }
              }
            else LBJ2$constraint$result$0 = true;
          }
          if (!LBJ2$constraint$result$0) return "false";
        }
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'Continuences(TextAnnotation)' defined on line 156 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "Continuences".hashCode(); }
  public boolean equals(Object o) { return o instanceof Continuences; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'Continuences(TextAnnotation)' defined on line 156 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      for (int j = 0; j < argumentCandidates.size(); ++j)
      {
        Constituent cb = (Constituent) argumentCandidates.get(j);
        LinkedList before = new LinkedList();
        for (int k = 0; k < argumentCandidates.size(); ++k)
        {
          Constituent c = (Constituent) argumentCandidates.get(k);
          if (c.getEndSpan() <= cb.getStartSpan())
          {
            before.add(c);
          }
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A0"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A0"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A1"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A1"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A2"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A2"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A3"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A3"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A4"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A4"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-A5"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("A5"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-ADV"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-ADV"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-CAU"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-CAU"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-DIR"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-DIR"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-DIS"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-DIS"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-EXT"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-EXT"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-LOC"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-LOC"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-MNR"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-MNR"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-MOD"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-MOD"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-NEG"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-NEG"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-PNC"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-PNC"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-PRD"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-PRD"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-REC"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-REC"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-TM"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-TM"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
        {
          Object[] LBJ$constraint$context = new Object[3];
          LBJ$constraint$context[0] = before;
          LBJ$constraint$context[1] = argumentCandidates;
          LBJ$constraint$context[2] = new Integer(j);
          FirstOrderConstraint LBJ2$constraint$result$0 = null;
          {
            FirstOrderConstraint LBJ2$constraint$result$1 = null;
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, argumentCandidates.get(j)), "" + ("C-AM-TMP"));
            FirstOrderConstraint LBJ2$constraint$result$2 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$3 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$3 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("AM-TMP"), LBJ$EAR);
              }
              LBJ2$constraint$result$2 = new ExistentialQuantifier("a", before, LBJ2$constraint$result$3);
            }
            LBJ2$constraint$result$0 = new FirstOrderImplication(LBJ2$constraint$result$1, LBJ2$constraint$result$2);
          }
          __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
        }
      }
      currentPredicateId++;
    }

    return __result;
  }
}

