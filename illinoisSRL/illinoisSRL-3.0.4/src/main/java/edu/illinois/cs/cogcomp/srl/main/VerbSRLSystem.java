package edu.illinois.cs.cogcomp.srl.main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import LBJ2.classify.Classifier;
import LBJ2.learn.Learner;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.TreeParserFactory;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.SpanLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.TokenLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TreeView;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.learners.ArgumentTypeBeamSearch;
import edu.illinois.cs.cogcomp.srl.learners.ArgumentTypeXpressMP;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentClassifier;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.utilities.PropBankUtilities;
import edu.illinois.cs.cogcomp.srl.utilities.SRLUtils;
import edu.illinois.cs.cogcomp.srl.utilities.XuePalmerHeuristic;

public class VerbSRLSystem extends SRLSystem {

	private static VerbArgumentIdentifier identifier;
	private static VerbArgumentClassifier classifier;
	private static ArgumentTypeXpressMP argumentTypeXpressMP;
	private static ArgumentTypeBeamSearch argumentTypeBeamSearch;

	final static Set<String> coreArgs = new HashSet<String>(
			Arrays.asList(PropBankUtilities.coreArguments));

	private static VerbSRLSystem INSTANCE;

	public static double srlIdThreshold, srlIdBeta;

	static {
		InputStream stream = VerbArgumentIdentifier.class
				.getResourceAsStream("VerbArgumentIdentifier.thresholds");

		try {
			Pair<Double, Double> pair = readThresholds(stream);
			srlIdThreshold = pair.getFirst();
			srlIdBeta = pair.getSecond();

		} catch (IOException e) {
			log.error(
					"Error loading identifier thresholds from VerbArgumentIdentifier.thresholds",
					e);
			System.exit(-1);
		}
	}

	public static VerbSRLSystem getInstance() {

		synchronized (VerbSRLSystem.class) {
			if (INSTANCE == null)
				INSTANCE = new VerbSRLSystem();
		}

		return INSTANCE;
	}

	protected void initializeClassifiers() {
		identifier = new VerbArgumentIdentifier();
		classifier = new VerbArgumentClassifier();
		argumentTypeXpressMP = new ArgumentTypeXpressMP();
		argumentTypeBeamSearch = new ArgumentTypeBeamSearch();
	}

	public Classifier getSRLXpressMP() {
		return argumentTypeXpressMP;
	}

	public Classifier getSRLBeamSearch() {
		return argumentTypeBeamSearch;
	}

	@Override
	List<Constituent> getFilteredCandidatesForPredicate(
			Constituent goldPredicate) {
		return XuePalmerHeuristic.generateFilteredCandidatesForPredicate(
				goldPredicate, identifier);
	}

	@Override
	protected List<Constituent> getPredicates(TextAnnotation ta) {
		return SRLUtils.getVerbPredicates(ta);
	}

	@Override
	String getPredicateType() {
		return "verb";
	}

	@Override
	public String getViewName() {
		return ViewNames.SRL;
	}

	@Override
	protected TextAnnotation initializeDummySentence() {
		TextAnnotation ta = new TextAnnotation("", "", Arrays.asList("I do ."));

		TokenLabelView tlv = new TokenLabelView(ViewNames.POS, "Test", ta, 1.0);
		tlv.addTokenLabel(0, "PRP", 1d);
		tlv.addTokenLabel(1, "VBP", 1d);
		ta.addView(ViewNames.POS, tlv);

		ta.addView(ViewNames.NER, new SpanLabelView(ViewNames.NER, "test", ta,
				1d));

		SpanLabelView chunks = new SpanLabelView(ViewNames.SHALLOW_PARSE,
				"test", ta, 1d);
		chunks.addSpanLabel(0, 1, "NP", 1d);
		chunks.addSpanLabel(1, 2, "VP", 1d);
		ta.addView(ViewNames.SHALLOW_PARSE, chunks);

		TreeView parse = new TreeView(SRLConfig.getInstance()
				.getDefaultParser(), "Charniak", ta, 1.0);
		parse.setParseTree(
				0,
				TreeParserFactory
						.getStringTreeParser()
						.parse("(S1 (S (NP (PRP I))       (VP (VPB do))        (. .)))"));
		ta.addView(SRLConfig.getInstance().getDefaultParser(), parse);

		return ta;
	}

	@Override
	public boolean isCoreArgument(String label) {
		return coreArgs.contains(label);
	}

	@Override
	public Learner getIdentifier() {
		return identifier;
	}

	@Override
	public Learner getArgumentClassifier() {
		return classifier;
	}

	@Override
	boolean ignorePredicatesWithoutArguments() {
		return false;
	}

	@Override
	public String getSRLSystemName() {
		return Constants.verbSRLSystemName;
	}
	   
	@Override
	public String getSRLCuratorName() {
	    return Constants.verbSRLSystemCuratorName;
	}
	    

	@Override
	public String getSRLSystemIdentifier() {
		return Constants.verbSRLSystemIdentifier;
	}

	@Override
	public String getSRLSystemVersion() {
		return Constants.verbSRLSystemVersion;
	}

}