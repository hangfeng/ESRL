package edu.illinois.cs.cogcomp.srl.data;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.features.NomFeatureHelper;
import edu.illinois.cs.cogcomp.srl.utilities.NomArgumentCandidateHeuristic;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

/**
 * This class implements a NOM specific version of Xue and Palmer's argument
 * identifier parser.
 * 
 * @author vsrikum2
 * 
 *         Dec 13, 2009
 */
public class NomIdentifierFilterParser extends IdentifierFilterParser {

	private final Set<NomLexClasses> nomlexTypes;

	/**
	 * @param section
	 * @throws FileNotFoundException
	 * 
	 */
	public NomIdentifierFilterParser(String section, String conllFile,
			Set<NomLexEntry.NomLexClasses> nomlexTypes) {
		super(section, conllFile, ViewNames.NOM);
		this.nomlexTypes = nomlexTypes;
	}

	public List<Constituent> generateCandidatesForPredicate(TextAnnotation ta,
			Constituent predicate) {
		return NomArgumentCandidateHeuristic
				.generateCandidatesForPredicate(predicate);
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}

	@Override
	protected List<Constituent> filterPredicates(List<Constituent> predicates) {

		List<Constituent> output = new ArrayList<Constituent>();
		for (Constituent c : predicates) {
			Set<NomLexClasses> nomType = NomFeatureHelper.getNomLexClass(c
					.getSurfaceString().toLowerCase().trim(),
					c.getAttribute(CoNLLColumnFormatReaderSRL.LemmaIdentifier));

			try {

				Set<NomLexClasses> s = new HashSet<NomLexEntry.NomLexClasses>(
						this.nomlexTypes);
				s.retainAll(nomType);

				if (s.size() > 0)
					output.add(c);
			} catch (Exception ex) {

			}
		}

		return output;
	}

}