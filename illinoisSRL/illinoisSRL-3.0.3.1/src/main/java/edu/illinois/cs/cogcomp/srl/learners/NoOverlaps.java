// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B8800000000000000057251DE43E0301CF5958704E8A7A80E59254245D1AB84800D59B387573ED6BE2EEAB2B7370201FFECA394394927F2193ED99D9D91F69E92270D6981E6DFDDF3C0E4F63AA70C75EB222FCA9DA728884C8452660FE0736323C630A1B5A66C803189DFEB9F3C6D5CC798CF713CCFEBB2AAE883684A0565128CD47082C80DF47C0985751695D64A85819F8D585C006BF3E4080F5EF38352DDF5656D128AF640E2603C6E1DEB1AAC29D8962FD69B214388BE9B828A1CD5634698FAF6A3B8A6D958E670A9A6236D4B99C35587FADD603CF2CA280AC692D5803849FADA364963D3EFAD78EAFAA43CCF8185719958F00A25E7BE62EA5B89AD52B968B70FFC8415569B34A52FA6C03A1DA3974BB08ABD52CCC8570E9D6539AC062318ABE5A71D0AA07C0F12658E91D4D682D5BC244C74F1C4B1C7F555BB97A0E4E64E45806E0338CD6B5F44822F59E87B2B359E7EB6AF5EE8370F4054E4D272939E07F5DEC588D1839318D30CF423D69F2B73BB3759B636AFE7952FFF1D86C014DC2B80FFC21F53F6CB483760E71B73EB69DCED7266B7B7A357A36C66F45760747E07C459377CD86E1E65D864575E3108436D3BF79300000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NoOverlaps extends ParameterizedConstraint
{
  private static final VerbArgumentClassifier __VerbArgumentClassifier = new VerbArgumentClassifier();

  public NoOverlaps() { super("edu.illinois.cs.cogcomp.srl.learners.NoOverlaps"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NoOverlaps(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      for (int j = 0; j < sentence.getTokens().length; ++j)
      {
        if (verb.getStartSpan() == j)
        {
          continue;
        }
        LinkedList containsWord = new LinkedList();
        for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
        {
          Constituent candidate = (Constituent) I.next();
          if (candidate.getStartSpan() <= j && candidate.getEndSpan() > j)
          {
            containsWord.add(candidate);
          }
        }
        if (containsWord.size() > 1)
        {
          {
            boolean LBJ2$constraint$result$0;
            {
              int LBJ$m$0 = 0;
              int LBJ$bound$0 = 1;
              for (java.util.Iterator __I0 = (containsWord).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
              {
                Constituent a = (Constituent) __I0.next();
                boolean LBJ2$constraint$result$1;
                LBJ2$constraint$result$1 = !("" + (__VerbArgumentClassifier.discreteValue(a))).equals("" + ("null"));
                if (LBJ2$constraint$result$1) ++LBJ$m$0;
              }
              LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
            }
            if (!LBJ2$constraint$result$0) return "false";
          }
        }
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NoOverlaps(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NoOverlaps".hashCode(); }
  public boolean equals(Object o) { return o instanceof NoOverlaps; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NoOverlaps(TextAnnotation)' defined on line 13 of VerbSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getVerbPredicates(sentence);
    int currentPredicateId = 0;
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent verb = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
      for (int j = 0; j < sentence.getTokens().length; ++j)
      {
        if (verb.getStartSpan() == j)
        {
          continue;
        }
        LinkedList containsWord = new LinkedList();
        for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
        {
          Constituent candidate = (Constituent) I.next();
          if (candidate.getStartSpan() <= j && candidate.getEndSpan() > j)
          {
            containsWord.add(candidate);
          }
        }
        if (containsWord.size() > 1)
        {
          {
            Object[] LBJ$constraint$context = new Object[1];
            LBJ$constraint$context[0] = containsWord;
            FirstOrderConstraint LBJ2$constraint$result$0 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$1 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(false, new FirstOrderVariable(__VerbArgumentClassifier, null), "" + ("null"), LBJ$EAR);
              }
              LBJ2$constraint$result$0 = new AtMostQuantifier("a", containsWord, LBJ2$constraint$result$1, 1);
            }
            __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
          }
        }
      }
      currentPredicateId++;
    }

    return __result;
  }
}

