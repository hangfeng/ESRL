/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.didion.jwnl.data.POS;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.core.datastructures.trees.TreeParserFactory;
import edu.illinois.cs.cogcomp.core.datastructures.trees.TreeTraversal;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.features.helpers.ParseHelper;
import edu.illinois.cs.cogcomp.edison.features.helpers.WordHelpers;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.PredicateArgumentView;
import edu.illinois.cs.cogcomp.edison.sentences.SpanLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.TokenLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TreeView;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.edison.utilities.ParseTreeProperties;
import edu.illinois.cs.cogcomp.edison.utilities.ParseUtils;
import edu.illinois.cs.cogcomp.edison.utilities.WordNetManager;
import edu.illinois.cs.cogcomp.srl.features.NomFeatureHelper;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

/**
 * @author Vivek Srikumar
 * 
 */
public class SRLUtils {

	private static Logger log = LoggerFactory.getLogger(SRLUtils.class);

	public static final String VerbPredicateView = "VerbPredicateView";

	public static final String NomPredicateView = "NomPredicateView";

	public static List<Constituent> getNomPredicates(TextAnnotation ta,
			Set<NomLexClasses> classes) {

		// This is only for printing a debug message if needed
		Exception ex = new Exception();
		String callerClass = ex.getStackTrace()[1].getClassName();
		String callerMethod = ex.getStackTrace()[1].getMethodName();
		log.debug("Getting predicates: Call by {}.{}", callerClass,
				callerMethod);

		if (ta.hasView(ViewNames.NOM)) {
			log.debug("Getting predicates from existing SRL view");
			List<Constituent> preds = ((PredicateArgumentView) (ta
					.getView(ViewNames.NOM))).getPredicates();

			List<Constituent> filteredPredicates = new ArrayList<Constituent>();
			for (Constituent p : preds) {
				String predicateWord = p.getSurfaceString().toLowerCase()
						.trim();
				String predicateLemma = p
						.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);
				Set<NomLexClasses> nomLexClass = new HashSet<NomLexEntry.NomLexClasses>(
						NomFeatureHelper.getNomLexClass(predicateWord,
								predicateLemma));

				nomLexClass.retainAll(NomLexEntry.VERBAL);

				if (nomLexClass.size() > 0)
					filteredPredicates.add(p);
			}

			return filteredPredicates;

		} else if (ta.hasView(NomPredicateView)) {
			log.debug("Getting predicates from existing predicate view");
			return ta.getView(NomPredicateView).getConstituents();
		} else {
			log.debug("Creating new predicate view");
			View predicateView = createNomPredicateView(ta, classes);
			ta.addView(NomPredicateView, predicateView);

			log.debug("Found {} predicates", predicateView.getConstituents()
					.size());
			return predicateView.getConstituents();
		}
	}

	public static List<Constituent> getVerbPredicates(TextAnnotation ta) {

		// This is only for printing a debug message if needed
		Exception ex = new Exception();
		String callerClass = ex.getStackTrace()[1].getClassName();
		String callerMethod = ex.getStackTrace()[1].getMethodName();
		log.debug("Getting predicates: Call by {}.{}", callerClass,
				callerMethod);

		if (ta.hasView(ViewNames.SRL)) {
			log.debug("Getting predicates from existing SRL view");
			return ((PredicateArgumentView) (ta.getView(ViewNames.SRL)))
					.getPredicates();
		} else if (ta.hasView(VerbPredicateView)) {
			log.debug("Getting predicates from existing predicate view");
			return ta.getView(VerbPredicateView).getConstituents();
		} else {
			log.debug("Creating new predicate view");
			View predicateView = createVerbPredicateView(ta);
			ta.addView(predicateView.getViewName(), predicateView);

			log.debug("Found {} predicates", predicateView.getConstituents()
					.size());
			return predicateView.getConstituents();
		}
	}

	protected static HashSet<String> toBeVerbs = new HashSet<String>(
			Arrays.asList("am", "are", "be", "been", "being", "is", "was",
					"were", "'s", "'re", "'m"));

	protected static boolean isVerbPredicate(TextAnnotation ta, int tokenIndex) {

		int sentenceId = ta.getSentenceId(tokenIndex);

		Tree<String> tree = ParseHelper.getParseTree(SRLConfig.getInstance()
				.getDefaultParser(), ta, sentenceId);

		tokenIndex -= ta.getSentence(sentenceId).getStartSpan();

		List<Tree<String>> yield = tree.getYield();

		// Deal with cases when the parse tree is weird.
		if (tokenIndex >= yield.size())
			return false;

		Tree<String> predicateTree = yield.get(tokenIndex).getParent();

		String[] words = ta.getTokens();

		final String pos = predicateTree.getLabel();
		// Using the POS tag from the parse tree to decide on predicates. The
		// LBJ POS tagger doesn't seem to be too good at recognizing MD/AUX

		// final String pos = WordHelpers.getPOS(ta, tokenIndex);

		if (pos.startsWith("V")) {
			return true;
		}

		if (pos.startsWith("AUX")) {

			Tree<String> parent = predicateTree.getParent();

			if (!parent.getLabel().equals("VP")) {
				return false;
			}

			if (words[tokenIndex].toUpperCase().equals("HAVE")
					|| words[tokenIndex].toUpperCase().equals("HAD")
					|| (words[tokenIndex].toUpperCase().equals("HAS"))) {
				if ((tokenIndex < words.length - 1)
						&& (words[tokenIndex + 1].toUpperCase().compareTo("TO") == 0)) {

					return false;
				}
			}
			if (predicateTree.isRoot() || predicateTree.getParent() == null) {
				return true;
			}

			for (int i = predicateTree.getPositionAmongParentsChildren() + 1; i < parent
					.getNumberOfChildren(); i++) {
				if (parent.getChild(i).getLabel().equals("VP")) {
					return false;
				}
			}

			if (toBeVerbs.contains(words[tokenIndex].toLowerCase())) {
				return true;
			}

			return false;
		}

		return false;
	}

	protected static View createVerbPredicateView(TextAnnotation ta) {

		SRLConfig config = SRLConfig.getInstance();

		String[] tokens = ta.getTokens();

		TokenLabelView predicateView = new TokenLabelView(
				SRLUtils.VerbPredicateView, "Heuristic", ta, 1.0);

		SpanLabelView spv = (SpanLabelView) ta.getView(ViewNames.POS);

		log.debug("Creating predicate view");

		for (int i = 0; i < ta.size(); i++) {

			if (isVerbPredicate(ta, i)) {
				if (!spv.getLabel(i).startsWith("V")) {

				} else {

					predicateView.addTokenLabel(i, "Predicate", 1.0);
					try {

						WordNetManager wordNetManager = WordNetManager
								.getInstance(config.getWordNetFile());

						predicateView.addTokenAttribute(i,
								CoNLLColumnFormatReader.SenseIdentifer, "01");
						predicateView.addTokenAttribute(i,
								CoNLLColumnFormatReader.LemmaIdentifier,
								wordNetManager.getLemma(tokens[i], POS.VERB));

					} catch (Exception e1) {
						System.err
								.println("Error adding attributes to predicate! "
										+ e1.getMessage());
					}
				}

			}// End if verb predicate
		}

		return predicateView;
	}

	protected static View createNomPredicateView(TextAnnotation ta,
			Set<NomLexClasses> classes) {

		TokenLabelView predicateView = new TokenLabelView(
				SRLUtils.NomPredicateView, "Heuristic", ta, 1.0);

		log.debug("Creating predicate view");

		for (int i = 0; i < ta.size(); i++) {

			String token = ta.getToken(i);
			String pos = WordHelpers.getPOS(ta, i);

			if (!isNominalization(token, pos, classes))
				continue;

			predicateView.addTokenLabel(i, "Predicate", 1.0);

			try {
				predicateView.addTokenAttribute(i,
						CoNLLColumnFormatReader.SenseIdentifer, "01");

				if (!NomFeatureHelper.nomLex.isPlural(token))
					predicateView.addTokenAttribute(i,
							CoNLLColumnFormatReader.LemmaIdentifier, token);
				else
					predicateView.addTokenAttribute(i,
							CoNLLColumnFormatReader.LemmaIdentifier,
							NomFeatureHelper.nomLex.getSingular(token));

			} catch (EdisonException e) {
				log.error(
						"Error adding attributes to predicate candidate {} for {}",
						token, ta.getText());
			}

		}

		return predicateView;
	}

	public static boolean isNominalization(String token, String pos,
			Set<NomLexClasses> classes) {
		boolean found = false;

		if (NomFeatureHelper.nomLex.isPlural(token))
			token = NomFeatureHelper.nomLex.getSingular(token);

		boolean nom = ParseTreeProperties.isPOSNoun(pos)
				&& NomFeatureHelper.nomLex.containsEntry(token);

		if (nom) {
			List<NomLexEntry> entry = NomFeatureHelper.nomLex
					.getNomLexEntry(token);

			for (NomLexEntry e : entry) {
				if (classes.contains(e.nomClass)) {
					found = true;
					break;
				}
			}
		}
		return found;
	}

	public static View getClauseView(TextAnnotation input) {
		SpanLabelView view = new SpanLabelView(ViewNames.CLAUSES,
				"GoldStandard", input, 1.0, true);

		for (int sentenceId = 0; sentenceId < input.getNumberOfSentences(); sentenceId++) {
			Tree<String> parseTree = ParseHelper.getParseTree(SRLConfig
					.getInstance().getDefaultParser(), input, sentenceId);

			Tree<Pair<String, IntPair>> spanLabeledTree = ParseUtils
					.getSpanLabeledTree(parseTree);

			int sentenceStartPosition = input.getSentence(sentenceId)
					.getStartSpan();

			for (Tree<Pair<String, IntPair>> node : TreeTraversal
					.breadthFirstTraversal(spanLabeledTree)) {
				if (node.isLeaf() || node.getChild(0).isLeaf())
					continue;

				Pair<String, IntPair> label = node.getLabel();
				String nonTerminalLabel = label.getFirst();
				if (nonTerminalLabel.startsWith("S")) {
					IntPair span = label.getSecond();
					view.addSpanLabel(span.getFirst() + sentenceStartPosition,
							span.getSecond() + sentenceStartPosition,
							nonTerminalLabel, 1.0);
				}
			}
		}
		return view;
	}

	public static View createPsuedoParseView(TextAnnotation ta) {
		if (!ta.hasView(ViewNames.CLAUSES))
			ta.addView(ViewNames.CLAUSES, getClauseView(ta));

		SpanLabelView cv = (SpanLabelView) ta.getView(ViewNames.CLAUSES);
		List<Constituent> clauses = cv.getConstituents();

		TokenLabelView tv = (TokenLabelView) ta.getView(ViewNames.POS);
		List<Constituent> poss = tv.getConstituents();

		List<Constituent> shallowParse;

		if (ta.hasView(ViewNames.SHALLOW_PARSE)) {
			SpanLabelView spv = (SpanLabelView) ta
					.getView(ViewNames.SHALLOW_PARSE);
			shallowParse = spv.getConstituents();
		} else {
			shallowParse = new ArrayList<Constituent>();
		}

		List<Pair<Double, String>> output = new ArrayList<Pair<Double, String>>();
		List<Integer> endOfChunks = new ArrayList<Integer>();
		String curToken;
		String[] tokens = ta.getTokens();

		for (int i = 0; i < ta.size(); i++) {
			if (tokens[i].equals("("))
				curToken = "-LRB-";
			else if (tokens[i].equals(")"))
				curToken = "-RRB-";
			else
				curToken = tokens[i];

			output.add(new Pair<Double, String>((double) i, curToken));
		}

		for (Constituent c : poss) {
			output.add(new Pair<Double, String>(c.getStartSpan() - 0.1, "("
					+ c.getLabel()));
			output.add(new Pair<Double, String>(c.getEndSpan() - 1 + 0.4, ")"));
		}

		for (Constituent c : shallowParse) {
			output.add(new Pair<Double, String>(c.getStartSpan() - 0.2, "("
					+ c.getLabel()));
			output.add(new Pair<Double, String>(c.getEndSpan() - 1 + 0.5, ")"));
			endOfChunks.add(c.getEndSpan());
		}

		for (Constituent c : clauses) {
			if (endOfChunks.contains(c.getStartSpan())) {
				int index = endOfChunks.indexOf(c.getStartSpan());
				output.add(new Pair<Double, String>(c.getStartSpan() - 0.3, "("
						+ c.getLabel() + "-"
						+ shallowParse.get(index).getLabel()));
			} else
				output.add(new Pair<Double, String>(c.getStartSpan() - 0.3, "("
						+ c.getLabel()));
			output.add(new Pair<Double, String>(c.getEndSpan() - 1 + 0.6, ")"));
		}

		Collections.sort(output, new Comparator<Pair<Double, String>>() {

			public int compare(Pair<Double, String> arg0,
					Pair<Double, String> arg1) {
				return arg0.getFirst().compareTo(arg1.getFirst());
			}
		});

		StringBuffer sb = new StringBuffer();

		for (Pair<Double, String> o : output) {
			sb.append(o.getSecond() + " ");
		}
		String parse = sb.toString();

		TreeView parseView = new TreeView(ViewNames.PSEUDO_PARSE,
				"GoldStandard", ta, 1.0);

		Tree<String> parseTree = TreeParserFactory.getStringTreeParser().parse(
				parse.toString());

		List<Tree<String>> leaves = parseTree.getYield();
		for (int i = 0; i < leaves.size(); i++)
			if (leaves.get(i).getLabel() == "-LRB-") {
				Tree<String> parent = leaves.get(i).getParent();
				parent.removeChildAt(leaves.get(i)
						.getPositionAmongParentsChildren());
				parent.addLeaf("(");
			} else if (leaves.get(i).getLabel() == "-RRB-") {
				Tree<String> parent = leaves.get(i).getParent();
				parent.removeChildAt(leaves.get(i)
						.getPositionAmongParentsChildren());
				parent.addLeaf(")");
			}

		parseView.setParseTree(0, parseTree);

		return parseView;

	}

}
