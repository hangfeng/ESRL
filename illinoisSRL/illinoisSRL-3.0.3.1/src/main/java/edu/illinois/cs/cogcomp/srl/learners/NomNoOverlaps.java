// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000D625D5F4B11301CFB2B0F08C650D9ACB2125925414D392A4111ADE3B57ED42E0795746FE51865CF7FEA37F91297939FCB333BB33ED2C354E06C113C2CFE61EF7CF1C05A9D745FC8F6C3522FC68D9728884C8450A68F703771916F10DAB2C0364890C2F96EF39D5913B532B8CC367535D2FEA39CFC1FD664C1ED3BF53B7AF63D9BE134AEB5451280AA325E6541FBC83116A16D5DE4A69B59F8B593C00EAF3E4080F5FC3C4986FBE6C5980AEC8CFDD06AFC2ABFB8A472F57F92B07C590A18CFE44F5D0EA4F0942E3FC80B43D34636A997EED09576B9C86037677FFD1BA02407588021609B9707523A826F4D70F1AB61A46CABE14402D075E308A413E6BE4D6B2EEAD0D3D89FCEF5092A2D95984B6EDC81643AD623BE65094D21469C620F27F6849689C40E05E2CB4B0AA07C0F12E98E50DE1C95AB695698FBD70BD4EFD7555B3E4EC9CB8C9271CC968199B6AE980976B131712BC69E7E3FB241D27E4ED22FC8A1E4232D1EE3BD9B31B30757507408919D6ACF5B73BDABACC85BDBE9E4EFF3AEE13405DB432CFB94C73CB3F21ECD08F5D1D8F646FBF898AF86D0A43136DBEA643C5CD2C52555697957BC3DDEA1D8E059FF0AEA60FEC2B300000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomNoOverlaps extends ParameterizedConstraint
{
  private static final NomArgumentClassifier __NomArgumentClassifier = new NomArgumentClassifier();

  public NomNoOverlaps() { super("edu.illinois.cs.cogcomp.srl.learners.NomNoOverlaps"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomNoOverlaps(TextAnnotation)' defined on line 13 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      for (int j = 0; j < sentence.getTokens().length; ++j)
      {
        if (nom.getStartSpan() == j)
        {
          continue;
        }
        LinkedList containsWord = new LinkedList();
        for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
        {
          Constituent candidate = (Constituent) I.next();
          if (candidate.getStartSpan() <= j && candidate.getEndSpan() > j)
          {
            containsWord.add(candidate);
          }
        }
        if (containsWord.size() > 1)
        {
          {
            boolean LBJ2$constraint$result$0;
            {
              int LBJ$m$0 = 0;
              int LBJ$bound$0 = 1;
              for (java.util.Iterator __I0 = (containsWord).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
              {
                Constituent a = (Constituent) __I0.next();
                boolean LBJ2$constraint$result$1;
                LBJ2$constraint$result$1 = !("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("null"));
                if (LBJ2$constraint$result$1) ++LBJ$m$0;
              }
              LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
            }
            if (!LBJ2$constraint$result$0) return "false";
          }
        }
      }
      currentPredicateId++;
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomNoOverlaps(TextAnnotation)' defined on line 13 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomNoOverlaps".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomNoOverlaps; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomNoOverlaps(TextAnnotation)' defined on line 13 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      for (int j = 0; j < sentence.getTokens().length; ++j)
      {
        if (nom.getStartSpan() == j)
        {
          continue;
        }
        LinkedList containsWord = new LinkedList();
        for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
        {
          Constituent candidate = (Constituent) I.next();
          if (candidate.getStartSpan() <= j && candidate.getEndSpan() > j)
          {
            containsWord.add(candidate);
          }
        }
        if (containsWord.size() > 1)
        {
          {
            Object[] LBJ$constraint$context = new Object[1];
            LBJ$constraint$context[0] = containsWord;
            FirstOrderConstraint LBJ2$constraint$result$0 = null;
            {
              FirstOrderConstraint LBJ2$constraint$result$1 = null;
              {
                EqualityArgumentReplacer LBJ$EAR =
                  new EqualityArgumentReplacer(LBJ$constraint$context, true)
                  {
                    public Object getLeftObject()
                    {
                      Constituent a = (Constituent) quantificationVariables.get(0);
                      return a;
                    }
                  };
                LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(false, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("null"), LBJ$EAR);
              }
              LBJ2$constraint$result$0 = new AtMostQuantifier("a", containsWord, LBJ2$constraint$result$1, 1);
            }
            __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
          }
        }
      }
      currentPredicateId++;
    }

    return __result;
  }
}

