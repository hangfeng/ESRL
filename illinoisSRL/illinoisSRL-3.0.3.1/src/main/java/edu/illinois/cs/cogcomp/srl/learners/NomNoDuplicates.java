// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B880000000000000005B09F4B62C04015CFBAC0E9638214BF70A5DA784DA2512841D6B7F5291DE042BB2BB314BD2E77FE4AAD811D3637901667EDFEDB979853E9D9623C033B9FCCE351BEC821DC8E5DB2E693236C2B662B60C3A164390600FD01397685B3C47FBA03485CC3E736ACC78B24610DB45F6AEF47D92D026CDE8D0BBFC0FD7C3F7C82E06005A37258372B55986A9A01BB3825019B5519BCBD4359F0D290D10D1F77806073797D490B73F149128AB08F78A5AF0D3D71AA0ACBB6456B14C58C6381B9B0F55D6414D54277E500B89EF673AF09764AD4A49E1A3AA5ECA6EFC85831105220438E46231AC81588749E4CABAC2494CAE4DA2013C3F41DE6F00437E65284F0CE2F4EC2D2D838A3FC71C94CBC4B7FBFA15700DFE34B2AE6BEFF99DB608975D003FAB106ED430CCBD608977D003FE589B38DDF0C1971190C7400000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import java.util.*;


public class NomNoDuplicates extends ParameterizedConstraint
{
  private static final NomArgumentClassifier __NomArgumentClassifier = new NomArgumentClassifier();

  public NomNoDuplicates() { super("edu.illinois.cs.cogcomp.srl.learners.NomNoDuplicates"); }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation"; }

  public String discreteValue(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomNoDuplicates(TextAnnotation)' defined on line 47 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;

    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      currentPredicateId++;
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A0"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A1"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A2"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A3"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A4"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A5"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A8"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
      {
        boolean LBJ2$constraint$result$0;
        {
          int LBJ$m$0 = 0;
          int LBJ$bound$0 = 1;
          for (java.util.Iterator __I0 = (argumentCandidates).iterator(); __I0.hasNext() && LBJ$m$0 <= LBJ$bound$0; )
          {
            Constituent a = (Constituent) __I0.next();
            boolean LBJ2$constraint$result$1;
            LBJ2$constraint$result$1 = ("" + (__NomArgumentClassifier.discreteValue(a))).equals("" + ("A9"));
            if (LBJ2$constraint$result$1) ++LBJ$m$0;
          }
          LBJ2$constraint$result$0 = LBJ$m$0 <= LBJ$bound$0;
        }
        if (!LBJ2$constraint$result$0) return "false";
      }
    }

    return "true";
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof TextAnnotation[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'NomNoDuplicates(TextAnnotation)' defined on line 47 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "NomNoDuplicates".hashCode(); }
  public boolean equals(Object o) { return o instanceof NomNoDuplicates; }

  public FirstOrderConstraint makeConstraint(Object __example)
  {
    if (!(__example instanceof TextAnnotation))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Constraint 'NomNoDuplicates(TextAnnotation)' defined on line 47 of NomSRLConstraints.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    TextAnnotation sentence = (TextAnnotation) __example;
    FirstOrderConstraint __result = new FirstOrderConstant(true);

    List predicates = SRLUtils.getNomPredicates(sentence, NomLexEntry.VERBAL);
    int currentPredicateId = 0;
    NomArgumentIdentifier identifier = new NomArgumentIdentifier();
    while (currentPredicateId < predicates.size())
    {
      Constituent nom = (Constituent) predicates.get(currentPredicateId);
      List argumentCandidates = NomArgumentCandidateHeuristic.generateFilteredCandidatesForPredicate(nom, identifier);
      currentPredicateId++;
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A0"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A1"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A2"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A3"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A4"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A5"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A8"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
      {
        Object[] LBJ$constraint$context = new Object[1];
        LBJ$constraint$context[0] = argumentCandidates;
        FirstOrderConstraint LBJ2$constraint$result$0 = null;
        {
          FirstOrderConstraint LBJ2$constraint$result$1 = null;
          {
            EqualityArgumentReplacer LBJ$EAR =
              new EqualityArgumentReplacer(LBJ$constraint$context, true)
              {
                public Object getLeftObject()
                {
                  Constituent a = (Constituent) quantificationVariables.get(0);
                  return a;
                }
              };
            LBJ2$constraint$result$1 = new FirstOrderEqualityWithValue(true, new FirstOrderVariable(__NomArgumentClassifier, null), "" + ("A9"), LBJ$EAR);
          }
          LBJ2$constraint$result$0 = new AtMostQuantifier("a", argumentCandidates, LBJ2$constraint$result$1, 1);
        }
        __result = new FirstOrderConjunction(__result, LBJ2$constraint$result$0);
      }
    }

    return __result;
  }
}

