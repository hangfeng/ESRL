package edu.illinois.cs.cogcomp.srl.curator;

import org.apache.thrift.TProcessorFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.server.THsHaServer;
import org.apache.thrift.server.TNonblockingServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.srl.main.SRLSystem;
import edu.illinois.cs.cogcomp.thrift.parser.Parser;

public class IllinoisSRLServer {

	private static Logger log = LoggerFactory
			.getLogger(IllinoisSRLHandler.class);
	private final boolean beamSearch;
	private final SRLSystem srlSystem;

	public IllinoisSRLServer(SRLSystem srlSystem, boolean beamSearch) {
		this.srlSystem = srlSystem;
		this.beamSearch = beamSearch;

	}

	public void runServer(int port, int numThreads) {
		Parser.Iface handler = new IllinoisSRLHandler(this.srlSystem, this.beamSearch);
		Parser.Processor processor = new Parser.Processor(handler);

		TNonblockingServerTransport serverTransport;
		TServer server;

		try {
			serverTransport = new TNonblockingServerSocket(port);

			if (numThreads == 1) {
				server = new TNonblockingServer(processor, serverTransport);
			} else {
				THsHaServer.Options serverOptions = new THsHaServer.Options();
				serverOptions.workerThreads = numThreads;
				server = new THsHaServer(new TProcessorFactory(processor),
						serverTransport, new TFramedTransport.Factory(),
						new TBinaryProtocol.Factory(), serverOptions);

			}

			Runtime.getRuntime().addShutdownHook(
					new Thread(new ShutdownListener(server, serverTransport),
							"Server shutdown listener"));

			log.info("Starting the server on port {} with {} threads", port,
					numThreads);

			server.serve();

		} catch (TTransportException e) {
			log.error("Thrift transport error", e);
			System.exit(-1);
		}
	}

	private static class ShutdownListener implements Runnable {

		private final TServer server;
		private final TNonblockingServerTransport serverTransport;

		public ShutdownListener(TServer server,
				TNonblockingServerTransport serverTransport) {
			this.server = server;
			this.serverTransport = serverTransport;
		}

		@Override
		public void run() {
			if (server != null) {
				server.stop();
			}

			if (serverTransport != null) {
				serverTransport.interrupt();
				serverTransport.close();
			}
		}

	}
}
