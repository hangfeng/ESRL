package edu.illinois.cs.cogcomp.srl.features;

import java.util.Set;
import java.util.TreeSet;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.datastructures.trees.Tree;
import edu.illinois.cs.cogcomp.edison.features.FeatureExtractor;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;

public class VerbFeatureGenerator implements FeatureExtractor {

	private static ThreadLocal<Constituent> currentConstituent = new ThreadLocal<Constituent>();
	private static ThreadLocal<Set<String>> currentFeatures = new ThreadLocal<Set<String>>();

	@Override
	public Set<String> getFeatures(Constituent candidate) {

		if (currentConstituent.get() == candidate)
			return currentFeatures.get();

		Set<String> features = new TreeSet<String>();

		Tree<Pair<String, IntPair>> argumentTree = VerbFeatureHelper
				.getArgumentTree(candidate);

		features.add("p#" + VerbFeatureHelper.getPredicateLemma(candidate));

		features.add("pp#" + VerbFeatureHelper.getPredicatePOS(candidate));

		features.add("v#" + VerbFeatureHelper.getPredicateVoice(candidate));

		features.add("pt#"
				+ VerbFeatureHelper.getPhraseType(candidate, argumentTree));

		Pair<String, String> headWordPOS = VerbFeatureHelper
				.getHeadWordPOS(argumentTree);
		features.add("h#" + headWordPOS.getFirst());

		features.add("hp#" + headWordPOS.getSecond());

		features.add("po#" + VerbFeatureHelper.getLinearPosition(candidate));

		features.add("pa#" + VerbFeatureHelper.getPathToPredicate(candidate));

		features.add("s#" + VerbFeatureHelper.getSubcatFrame(candidate));

		String[] wordsTags = VerbFeatureHelper
				.getArgumentWordsAndTags(candidate);

		for (String wordTag : wordsTags)
			features.add(wordTag);

		wordsTags = VerbFeatureHelper.getContextTags(candidate);

		for (String wordTag : wordsTags)
			features.add(wordTag);

		String[] allVerbClasses = VerbFeatureHelper.getVerbClass(candidate);

		if (allVerbClasses != null) {
			for (String s : allVerbClasses) {
				features.add("vc#" + s.trim());
			}
		} else {
			features.add("vc#unknown");
		}

		// not used in fex file of vasin??
		features.add("l#" + VerbFeatureHelper.getNumberOfWords(candidate));

		features.add("c#" + VerbFeatureHelper.getNumberOfChunks(candidate));

		String[] chunkEmbedding = VerbFeatureHelper
				.getChunkEmbedding(candidate);

		for (int i = 0; i < chunkEmbedding.length; i++)
			features.add("cu#" + chunkEmbedding[i]);

		// features.add("cp#" + VerbFeatureHelper.getChunkPattern(candidate));

		features.add("cl#" + VerbFeatureHelper.getChunkPatternLength(candidate));

		try {
			features.add("clp#"
					+ VerbFeatureHelper.getClauseRelativePosition(candidate));
		} catch (Exception e) {
			// System.out.println(candidate.getTextAnnotation().getTokenizedText());
			features.add("clp#" + "unknown");
		}

		features.add("cc#" + VerbFeatureHelper.getClauseCoverage(candidate));

		features.add("n#" + VerbFeatureHelper.getNegatedVerb(candidate));

		features.add("m#" + VerbFeatureHelper.getModalVerb(candidate));

		// this line has to be removed since syn feature is not a part of
		// argument identifier
		// features.add("syn#" +
		// VerbFeatureHelper.getSyntacticFrame(candidate));

		// remove the following feature for getting back to
		// argumentclassifier040f or whatever
		features.add("prt#"
				+ VerbFeatureHelper.getPredicateContextWord(candidate));

		// String[] ff = (String [])features.toArray(new String[0]);

		// for(int i = 0;i<features.size();i++)
		// System.out.println(ff[i]);

		/*
		 * features.add("lsis.ptype:" +
		 * VerbFeatureHelper.getLeftSisterPhraseType(candidate));
		 * features.add("lsis.hw:" +
		 * VerbFeatureHelper.getLeftSisterHeadWord(candidate));
		 * features.add("lsis.pos:" +
		 * VerbFeatureHelper.getLeftSisterHeadPOS(candidate));
		 * 
		 * features.add("rsis.ptype:" +
		 * VerbFeatureHelper.getRightSisterPhraseType(candidate));
		 * 
		 * features.add("rsis.hw:" +
		 * VerbFeatureHelper.getRightSisterHeadWord(candidate));
		 * features.add("rsis.hw.pos:" +
		 * VerbFeatureHelper.getRightSisterHeadPOS(candidate));
		 * 
		 * features.add("parent.ptype:" +
		 * VerbFeatureHelper.getParentPhraseType(candidate));
		 */
		// features.addAll(getConjunctions(features));

		features.add("b#?");

		currentConstituent.set(candidate);
		currentFeatures.set(features);

		return features;
	}

	@Override
	public String getName() {
		return "srl";
	}

}
