package edu.illinois.cs.cogcomp.srl.learners;

import java.util.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.edison.data.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.learners.*;
import edu.illinois.cs.cogcomp.srl.utilities.*;
import edu.illinois.cs.cogcomp.srl.main.SRLConfig;


constraint NoOverlaps(TextAnnotation sentence)
{
    List predicates = SRLUtils.getVerbPredicates(sentence);

    int currentPredicateId = 0;							

    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();
						
    while(currentPredicateId < predicates.size()) {
	Constituent verb = (Constituent)predicates.get(currentPredicateId);


	List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);

	for (int j = 0; j < sentence.getTokens().length; ++j) {
	    if (verb.getStartSpan() == j) continue;

	    LinkedList containsWord = new LinkedList();
	    for (Iterator I = argumentCandidates.iterator(); I.hasNext(); )
		{
		    Constituent candidate = (Constituent) I.next();
    		 
		    if (candidate.getStartSpan()<=j && candidate.getEndSpan() > j)
			containsWord.add(candidate);
		}
	    if(containsWord.size()>1)
		{
		    atmost 1 of (Constituent a in containsWord)  VerbArgumentClassifier(a) !: "null";
		}
	}
	currentPredicateId++;	     
    }
}

constraint NoDuplicates(TextAnnotation sentence)
{

    List predicates = SRLUtils.getVerbPredicates(sentence);

    int currentPredicateId = 0;		
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();					
						
    while(currentPredicateId < predicates.size())
	{
	    Constituent verb = (Constituent)predicates.get(currentPredicateId);

	    List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);

	    currentPredicateId ++;
	    atmost 1 of (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A0";
	    atmost 1 of (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A1";
	    atmost 1 of (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A2";
	    atmost 1 of (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A3";
	    atmost 1 of (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A4";
	    atmost 1 of (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A5";
	    atmost 1 of (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AA";
	}

}

constraint References(TextAnnotation sentence)
{
    List predicates = SRLUtils.getVerbPredicates(sentence);

    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();					
    int currentPredicateId = 0;							
							
    while(currentPredicateId < predicates.size())

	{
	    Constituent verb = (Constituent)predicates.get(currentPredicateId);

	    List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);


	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-A0")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A0");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-A1")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A1");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-A2")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A2");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-A3")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A3");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-A4")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A4");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-A5")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "A5");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AA")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AA");
	
	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-ADV")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-ADV");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-CAU")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-CAU");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-DIR")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-DIR");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-DIS")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-DIS");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-EXT")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-EXT");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-LOC")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-LOC");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-MNR")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-MNR");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-MOD")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-MOD");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-NEG")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-NEG");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-PNC")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-PNC");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-PRD")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-PRD");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-REC")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-REC");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-TM")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-TM");

	    (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "R-AM-TMP")
		=> (exists (Constituent a in argumentCandidates) VerbArgumentClassifier(a) :: "AM-TMP");

	    currentPredicateId++;
	}

}

constraint Continuences(TextAnnotation sentence)
{

    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();					
    List predicates = SRLUtils.getVerbPredicates(sentence);

    int currentPredicateId = 0;							
							
    while(currentPredicateId < predicates.size()) {
	Constituent verb = (Constituent)predicates.get(currentPredicateId);

	List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);

	for (int j = 0; j < argumentCandidates.size(); ++j) {
	    Constituent cb = (Constituent)argumentCandidates.get(j);
	    LinkedList before = new LinkedList();

	    for (int k = 0; k < argumentCandidates.size(); ++k) {
		Constituent c = (Constituent)argumentCandidates.get(k);
		if (c.getEndSpan() <= cb.getStartSpan())
		    before.add(c);
	    }

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-A0"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "A0");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-A1"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "A1");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-A2"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "A2");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-A3"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "A3");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-A4"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "A4");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-A5"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "A5");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-ADV"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-ADV");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-CAU"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-CAU");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-DIR"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-DIR");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-DIS"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-DIS");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-EXT"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-EXT");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-LOC"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-LOC");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-MNR"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-MNR");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-MOD"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-MOD");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-NEG"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-NEG");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-PNC"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-PNC");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-PRD"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-PRD");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-REC"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-REC");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-TM"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-TM");

	    VerbArgumentClassifier(argumentCandidates.get(j)) :: "C-AM-TMP"
		=> (exists (Constituent a in before) VerbArgumentClassifier(a) :: "AM-TMP");
	}
	currentPredicateId++;
    }

}

constraint LegalArguments(TextAnnotation sentence)
{
    VerbArgumentIdentifier identifier = new VerbArgumentIdentifier();					
    List predicates = SRLUtils.getVerbPredicates(sentence);

    int currentPredicateId = 0;							
						
    while(currentPredicateId < predicates.size()) {
	Constituent verb = (Constituent)predicates.get(currentPredicateId);

	List argumentCandidates = XuePalmerHeuristic.generateFilteredCandidatesForPredicate(verb, identifier);
	
	LinkedList legal = PropBankUtilities.getLegalArguments(verb.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier));

	forall (Constituent a in argumentCandidates)
	    exists (String type in legal)
	    VerbArgumentClassifier(a) :: type;

	currentPredicateId++;
    }
}

inference SRLInferenceXpressMP head  TextAnnotation sentence
{
    Constituent a      { return a.getTextAnnotation(); }
    normalizedby new Softmax()

    subjectto  {
	@NoOverlaps(sentence) /\ @LegalArguments(sentence) /\ @NoDuplicates(sentence) /\@Continuences(sentence) /\ @References(sentence);
    }

    with new ILPInference(new XpressMPHook())
}


inference SRLInferenceBeamSearch head  TextAnnotation sentence
{
    Constituent a      { return a.getTextAnnotation(); }
  
    normalizedby new Softmax()

    subjectto {
	@NoOverlaps(sentence) /\ @LegalArguments(sentence) /\ @NoDuplicates(sentence) /\@Continuences(sentence) /\ @References(sentence);
    }

    with new ILPInference(new BeamSearch(SRLConfig.getInstance().getBeamSize()))
}


discrete ArgumentTypeXpressMP(Constituent a) <- SRLInferenceXpressMP(VerbArgumentClassifier)

discrete ArgumentTypeBeamSearch(Constituent a) <- SRLInferenceBeamSearch(VerbArgumentClassifier)
