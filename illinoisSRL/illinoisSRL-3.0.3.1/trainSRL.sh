#!/bin/bash

./compileLBJ ./lbj/VerbSRLIdentifier.lbj

./compileLBJ ./lbj/VerbSRLClassifier.lbj

./compileLBJ ./lbj/VerbSRLConstraints.lbj

./srl.sh -v --train --train-cl-num-iters 60 --train-cv-file data/verb_data_column_charniak/02.feats --train-dev-file data/verb_data_column_charniak/23.feats --train-id-num-iters 60 --train-models-dir models_new --train-training-file data/verb_data_column_charniak/train.all.feats 
