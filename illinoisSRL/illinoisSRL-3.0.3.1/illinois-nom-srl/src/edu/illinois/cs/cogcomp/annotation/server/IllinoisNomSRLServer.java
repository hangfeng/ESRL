package edu.illinois.cs.cogcomp.annotation.server;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import edu.illinois.cs.cogcomp.srl.curator.IllinoisSRLServer;
import edu.illinois.cs.cogcomp.srl.main.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IllinoisNomSRLServer {
    private static Logger logger = LoggerFactory.getLogger(IllinoisNomSRLServer.class);
    
    /*
     * most this code is boiler plate. All you need to modify is createOptions
     * and main to get a server working!
     */
    public static Options createOptions() {
	Option port = OptionBuilder.withLongOpt("port").withArgName("PORT")
	    .hasArg().withDescription("port to open server on").create("p");
	Option threads = OptionBuilder.withLongOpt("threads")
	    .withArgName("THREADS").hasArg()
	    .withDescription("number of threads to run").create("t");
	Option config = OptionBuilder.withLongOpt("config")
	    .withArgName("CONFIG").hasArg()
	    .withDescription("configuration file").create("c");
		
	Option help = new Option("h", "help", false, "print this message");
	Options options = new Options();
	options.addOption(port);
	options.addOption(threads);
	options.addOption(config);
	options.addOption(help);
	return options;
    }


    public static void main(String[] args) {
	int threads = 1;
	int port = 9390;
	String configFile = "";

	CommandLineParser parser = new GnuParser();
	Options options = createOptions();
	HelpFormatter hformat = new HelpFormatter();
	CommandLine line = null;
	try {
	    line = parser.parse(options, args);
	} catch (ParseException e) {
	    logger.error(e.getMessage());
	    hformat.printHelp("java " + IllinoisNomSRLServer.class.getName(),
			      options, true);
	    System.exit(1);
	}
	if (line.hasOption("help")) {
	    hformat.printHelp("java " + IllinoisNomSRLServer.class.getName(),
			      options, true);
	    System.exit(1);
	}

	port = Integer.parseInt(line.getOptionValue("port", "9090"));

	try {
	    threads = Integer.parseInt(line.getOptionValue("threads", "2"));
	} catch (NumberFormatException e) {
	    logger.warn("Couldn't interpret {} as a number.",
			line.getOptionValue("threads"));
	}
	if (threads < 0) {
	    threads = 1;
	} else if (threads == 0) {
	    threads = 2;
	}

	configFile = line.getOptionValue("config", "");
	
	// initialize the configuration 
	SRLConfig.getInstance(configFile);

	// create the curator server
	IllinoisSRLServer server = new IllinoisSRLServer(NomSRLSystem.getInstance(), true);

	// run the curator server
	server.runServer(port, threads);
    }
}