package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.edison.features.helpers.WordHelpers;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Queries;
import edu.illinois.cs.cogcomp.edison.sentences.SpanLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.POSUtils;

public class ClauseCandidateHeuristic {

	// These lists are from gold training data.
	private static final Set<String> invalidClauseStarts = new HashSet<String>(
			Arrays.asList("-RRB-", "RP", "."));

	private static final Set<String> invalidClauseEnds = new HashSet<String>(
			Arrays.asList("LS", "PDT", "WP$", "SYM", "#", "$", "-LRB-"));

	private static final Set<String> invalidClauseChunkSpans = new HashSet<String>(
			Arrays.asList("SBAR", "PRT", "INTJ", "LST"));

	private static final Set<String> invalidAfterPrepositions = new HashSet<String>(
			Arrays.asList("JJ", "RP", "RBR", "RBS", "JJS", "LS", "JJR", "FW",
					"NN", "NNPS", "VB", "VBN", "PDT", "``", "WP$", "SYM",
					"WDT", "WP", "#", "$", "POS", "VBG", "EX", "-LRB-", "PRP$",
					"UH", "NNP", "CD"));

	private static final Set<String> invalidAfterAdjectives = new HashSet<String>(
			Arrays.asList("RP", "RBR", "RBS", "JJS", "LS", "JJR", "FW", "NN",
					"NNPS", "VBN", "PDT", "WP$", "SYM", "WP", "#", "$", "POS",
					"EX", "PRP$", "UH", "NNS", "CD"));
	private static final Set<String> invalidAfterAdverbs = new HashSet<String>(
			Arrays.asList("RP", "RBR", "RBS", "JJS", "LS", "JJR", "FW", "NN",
					"NNPS", "VBN", "PDT", "WP$", "SYM", "WDT", "WP", "#", "$",
					"POS", "EX", "PRP$", "UH", "NNP"));
	private static final Set<String> invalidAfterNouns = new HashSet<String>(
			Arrays.asList("RP", "RBS", "JJS", "LS", "JJR", "FW", "NNPS", "PDT",
					"WP$", "SYM", "WP", "#", "$", "POS", "UH"));
	private static final Set<String> invalidAfterVerbs = new HashSet<String>(
			Arrays.asList("RP", "RBS", "JJS", "LS", "JJR", "FW", "NNPS", "PDT",
					"WP$", "SYM", "#", "POS", "PRP$", "UH"));

	private static final Set<String> invalidAfterClauses = new HashSet<String>(
			Arrays.asList("RP", "RBS", "JJS", "LS", "JJR", "FW", "NNPS", "PDT",
					"WP$", "SYM", "#", "POS", "UH"));

	private final static Set<String> allowedPunctionStarts = new HashSet<String>(
			Arrays.asList("``", "(", "{", "--", "`", ",", "''", "...", "'"));

	public static Set<IntPair> getClauseCandidates(TextAnnotation ta) {

		SpanLabelView chunks = (SpanLabelView) ta
				.getView(ViewNames.SHALLOW_PARSE);

		SpanLabelView ne = (SpanLabelView) ta.getView(ViewNames.NER);

		Set<IntPair> set = new HashSet<IntPair>();
		for (int start = 0; start < ta.size(); start++) {

			// clauses can never start with certain POS tags
			String startPOS = WordHelpers.getPOS(ta, start);
			if (invalidClauseStarts.contains(startPOS))
				continue;

			String startToken = ta.getToken(start).toLowerCase();

			if (POSUtils.isPOSPunctuation(startPOS)
					&& !allowedPunctionStarts.contains(startToken))
				continue;

			for (int end = start + 1; end < ta.size() + 1; end++) {

				// clauses can never end with certain POS tags
				String endPOS = WordHelpers.getPOS(ta, end - 1);
				if (invalidClauseEnds.contains(endPOS))
					continue;

				String nextPOS = null;
				if (end != ta.size())
					nextPOS = WordHelpers.getPOS(ta, end);

				if (nextPOS != null) {

					// Clauses cannot be followed by certain POS tags
					if (invalidAfterClauses.contains(nextPOS))
						continue;

					// if the end POS is a preposition and the next POS is one
					// of a certain set of POSes, then this is not a clause.
					if (POSUtils.isPOSPreposition(endPOS))
						if (invalidAfterPrepositions.contains(nextPOS))
							continue;

					// ditto for verbs
					if (POSUtils.isPOSVerb(endPOS))
						if (invalidAfterVerbs.contains(nextPOS))
							continue;

					// ditto for nouns
					if (POSUtils.isPOSNoun(endPOS)
							|| POSUtils.isPOSPronoun(endPOS))
						if (invalidAfterNouns.contains(nextPOS))
							continue;

					// ditto for adjectives
					if (POSUtils.isPOSAdjective(endPOS))
						if (invalidAfterAdjectives.contains(nextPOS))
							continue;

					// ditto for adverbs
					if (POSUtils.isPOSAdverb(endPOS))
						if (invalidAfterAdverbs.contains(nextPOS))
							continue;

				}

				// clauses can never be the same span as certain chunks
				List<Constituent> cs = chunks.getConstituentsCoveringSpan(
						start, end);
				if (cs.size() == 1)
					if (invalidClauseChunkSpans.contains(cs.get(0).getLabel()))
						continue;

				// if the span is enclosed by punctuations on both ends, then
				// this might be a clause.
				if (start > 0 && end != ta.size()) {
					String beforePOS = WordHelpers.getPOS(ta, start - 1);
					String afterPOS = WordHelpers.getPOS(ta, end);

					if (POSUtils.isPOSPunctuation(beforePOS)
							&& POSUtils.isPOSPunctuation(afterPOS)) {
						set.add(new IntPair(start, end));
						continue;
					}

				}

				if (!isFinalThatWhich(ta, start, end))
					continue;

				if (!hasVerb(ta, start, end, chunks))
					continue;

				if (endsInMiddleOfChunk(ta, start, end, chunks))
					continue;

				if (startsInMiddleOfChunk(ta, start, end, chunks, ne))
					continue;

				set.add(new IntPair(start, end));
			}
		}

		set.add(new IntPair(0, ta.size()));

		return set;
	}

	private static boolean startsInMiddleOfChunk(TextAnnotation ta, int start,
			int end, SpanLabelView chunks, SpanLabelView ne) {

		Constituent c = new Constituent("", "", ta, start, end);

		List<Constituent> chunkCoveringFirstToken = chunks
				.getConstituentsCoveringToken(c.getStartSpan());

		// there has to be a chunk somewhere here
		if (chunkCoveringFirstToken.size() == 0)
			return false;

		Constituent chunk = chunkCoveringFirstToken.get(0);

		// the first token has to lie in the middle of it
		if (!Queries.exclusivelyOverlaps(c).transform(chunk))
			return false;

		String label = chunk.getLabel();

		// ignore all VP and CONJP. They can do whatever they want
		if (label.equals("VP") || label.equals("CONJP"))
			return false;

		String prev = null;
		if (c.getStartSpan() != 0)
			prev = WordHelpers.getPOS(ta, c.getStartSpan() - 1);

		// if the previous token is a CC, then we will entertain this
		if ("CC".equals(prev))
			return false;

		boolean lastTokenInChunk = c.getStartSpan() == chunk.getEndSpan() - 1;

		boolean prevRB = prev != null && prev.contains("RB");

		// these POS tag details by looking at the training set
		if (label.equals("ADVP"))
			if (prevRB || "IN".equals(prev) || lastTokenInChunk)
				return false;

		if (label.equals("ADJP"))
			if (prevRB || "IN".equals(prev) || "WDT".equals(prev))
				return false;

		if (label.equals("SBAR"))
			if (prevRB || "IN".equals(prev))
				return false;

		if (label.equals("NP")) {
			String firstToken = ta.getToken(c.getStartSpan()).toLowerCase();

			String firstChunkToken = ta.getToken(chunk.getStartSpan())
					.toLowerCase();

			Set<String> validFirstTokens = new HashSet<String>(Arrays.asList(
					"that", "which", "those", "who"));

			if (validFirstTokens.contains(firstChunkToken)
					|| validFirstTokens.contains(firstToken))
				return false;

			if ("WP$".equals(prev) || "WP".equals(prev) || prevRB
					|| lastTokenInChunk)
				return false;

			Constituent overlap = new Constituent("", "", ta, c.getStartSpan(),
					chunk.getEndSpan());

			StringBuilder posSeq = new StringBuilder();
			for (int i = overlap.getStartSpan(); i < overlap.getEndSpan(); i++) {
				char pos = WordHelpers.getPOS(ta, i).toLowerCase().charAt(0);
				posSeq.append(pos);
			}

			boolean nounPhrase = posSeq.toString().matches("d*j*n*");

			if (nounPhrase)
				return false;

			boolean chunkEndsInNE = ne.where(
					Queries.sameSpanAsConstituent(overlap)).count() > 0;

			if (chunkEndsInNE)
				return false;
		}

		return true;

	}

	private static boolean endsInMiddleOfChunk(TextAnnotation ta, int start,
			int end, SpanLabelView chunks) {
		// do not end in the middle of a chunk if the chunk is not
		// followed by a conjunction: Susceptible to chunk and POS
		// errors. (Violated with predicted chunk and POS tags in
		// training set ~0.1% of the time).

		boolean endsInMiddleOfChunk = false;
		Constituent c = new Constituent("", "", ta, start, end);

		List<Constituent> chunkCoveringLastToken = chunks
				.getConstituentsCoveringToken(end - 1);
		if (chunkCoveringLastToken.size() > 0) {
			Constituent chunk = chunkCoveringLastToken.get(0);

			boolean nextWordCC = false;
			if (end != ta.size()) {
				String next = WordHelpers.getPOS(ta, c.getEndSpan());
				nextWordCC = next.equals("CC");
			}

			if (Queries.exclusivelyOverlaps(c).transform(chunk) && !nextWordCC) {
				endsInMiddleOfChunk = true;
			}
		}

		return endsInMiddleOfChunk;
	}

	private static boolean hasVerb(TextAnnotation ta, int start, int end,
			SpanLabelView chunks) {
		// Ignore candidates that don't have a verb or overlaps with a
		// verb phrase: Susceptible to POS tag errors! (violated with
		// predicted POS tags in training set ~1% of the times)

		Constituent c = new Constituent("", "", ta, start, end);

		int numVerbPhrases = chunks.where(Queries.hasOverlap(c))
				.where(Queries.hasLabel("VP")).count();
		boolean hasVerb = numVerbPhrases > 0;

		if (!hasVerb) {
			for (int i = start; i < end; i++) {
				String pos = WordHelpers.getPOS(ta, i);
				if (POSUtils.isPOSVerb(pos)) {
					hasVerb = true;
					break;
				}
			}

		}
		return hasVerb;
	}

	private static boolean isFinalThatWhich(TextAnnotation ta, int start,
			int end) {
		boolean isCandidate = true;
		// if the last token is 'that' or 'which', then consider only
		// candidates that are followed by a verb/punctuation or CC.
		// Special cases, when the word before that is some form of 'do'
		// or the word through.
		String lastToken = ta.getToken(end - 1).toLowerCase();

		boolean lastTokenThat = lastToken.equals("that")
				|| lastToken.equals("which");

		boolean nextTokenValid = false;
		if (end != ta.size()) {
			String next = WordHelpers.getPOS(ta, end);
			nextTokenValid = POSUtils.isPOSPunctuation(next)
					|| POSUtils.isPOSVerb(next) || next.equals("CC");
		}

		// this special case is from the trainign set
		boolean previousTokenValid = false;
		if (start != 0) {
			String prevToken = ta.getToken(start - 1).toLowerCase();
			previousTokenValid = prevToken.startsWith("do")
					|| prevToken.equals("through");
		}

		if (lastTokenThat && !previousTokenValid && !nextTokenValid)
			isCandidate = false;

		return isCandidate;
	}

}
