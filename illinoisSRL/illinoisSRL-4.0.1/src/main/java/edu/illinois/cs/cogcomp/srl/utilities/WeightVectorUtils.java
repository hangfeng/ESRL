package edu.illinois.cs.cogcomp.srl.utilities;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.sl.util.WeightVector;

public class WeightVectorUtils {
	
	private final static Logger log = LoggerFactory
			.getLogger(WeightVectorUtils.class);

	public static void save(String fileName, WeightVector wv) throws IOException {
		BufferedOutputStream stream = new BufferedOutputStream(
				new GZIPOutputStream(new FileOutputStream(fileName)));

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(stream));
		
		float[] w = wv.getWeightArray();

		writer.write("WeightVector");
		writer.newLine();

		writer.write(w.length + "");
		writer.newLine();

		int numNonZero = 0;
		for (int index = 0; index < w.length; index++) {
			if (w[index] != 0) {
				writer.write(index + ":" + w[index]);
				writer.newLine();
				numNonZero++;
			}
		}

		writer.close();

		log.info("Number of non zero weights: " + numNonZero);
	}

	public static WeightVector load(String fileName) throws IOException, URISyntaxException {
		GZIPInputStream zipin = new GZIPInputStream(new FileInputStream(fileName));

		BufferedReader reader = new BufferedReader(new InputStreamReader(zipin));

		String line;

		line = reader.readLine().trim();
		if (!line.equals("WeightVector")) {
			reader.close();
			throw new IOException("Invalid model file.");
		}

		line = reader.readLine().trim();
		int size = Integer.parseInt(line);

		float[] vector = new float[size];

		while ((line = reader.readLine()) != null) {
			line = line.trim();
			String[] parts = line.split(":");
			vector[Integer.parseInt(parts[0])] = (float) Double.parseDouble(parts[1]);
		}

		zipin.close();

		return new WeightVector(vector, 1);
	}

	public static float[] readFromClassPath(String fileName) throws Exception {
		Class<WeightVectorUtils> clazz = WeightVectorUtils.class;
		List<URL> list = IOUtils.lsResources(clazz, fileName);

		if (list.size() == 0) {
			log.error("File {} not found on the classpath", fileName);
			throw new Exception("File not found on classpath");
		}
		InputStream stream = list.get(0).openStream();

		GZIPInputStream zipin = new GZIPInputStream(stream);

		BufferedReader reader = new BufferedReader(new InputStreamReader(zipin));

		String line;

		line = reader.readLine().trim();
		if (!line.equals("WeightVector")) {
			reader.close();
			throw new IOException("Invalid model file.");
		}

		line = reader.readLine().trim();
		int size = Integer.parseInt(line);

		float[] vector = new float[size];

		while ((line = reader.readLine()) != null) {
			line = line.trim();
			String[] parts = line.split(":");
			vector[Integer.parseInt(parts[0])] = Float.parseFloat(parts[1]);
		}

		zipin.close();

		return vector;
	}

	public static WeightVector loadWeightVectorFromClassPath(String modelName)
			throws Exception {
		float[] ds = readFromClassPath(modelName);

		return new WeightVector(ds, 1);
	}
}
