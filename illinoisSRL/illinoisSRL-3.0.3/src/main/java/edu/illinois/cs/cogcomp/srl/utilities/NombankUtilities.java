package edu.illinois.cs.cogcomp.srl.utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.srl.features.NomFeatureHelper;

/**
 * @author vsrikum2
 * 
 *         Jan 13, 2010
 */
public class NombankUtilities {
	private static Log log = LogFactory.getLog(NombankUtilities.class);

	public final static String[] coreArguments = { "A0", "A1", "A2", "A3",
			"A4", "A5", "A8", "A9" };

	public final static String[] modifierArguments = { "AM-ADV", "AM-CAU",
			"AM-DIR", "AM-DIS", "AM-EXT", "AM-LOC", "AM-MNR", "AM-NEG",
			"AM-PNC", "AM-PRD", "AM-TMP" };

	public final static String[] baseArguments = { "A0", "A1", "A2", "A3",
			"A4", "A5", "A8", "A9", "AM-ADV", "AM-CAU", "AM-DIR", "AM-DIS",
			"AM-EXT", "AM-LOC", "AM-MNR", "AM-NEG", "AM-PNC", "AM-PRD",
			"AM-TMP" };

	public static final String[] allArguments = { "null", "A0", "A1", "A2",
			"A3", "A4", "A5", "A8", "A9", "AM-ADV", "AM-CAU", "AM-DIR",
			"AM-DIS", "AM-EXT", "AM-LOC", "AM-MNR", "AM-NEG", "AM-PNC",
			"AM-PRD", "AM-TMP", "C-A0", "C-A1", "C-A2", "C-A3", "C-SUP",
			"R-A0", "R-A1", "R-A2", "R-A3", "R-A4", "R-A8", "R-AM-CAU",
			"R-AM-LOC", "SUP" };

	public static Map<String, LinkedList<String>> legalArgs;

	public static Set<String> allArgumentsSet;

	static {
		synchronized (NombankUtilities.class) {
			allArgumentsSet = new HashSet<String>();
			allArgumentsSet.addAll(Arrays.asList(allArguments));

			readLegalArgs();
		}
	}

	public static Map<String, LinkedList<String>> getLegalArguments() {
		return legalArgs;
	}

	public static LinkedList<String> getLegalArguments(Constituent predicate) {

		String lemma = predicate
				.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);

		if (legalArgs.containsKey(lemma))
			return legalArgs.get(lemma);
		else {
			LinkedList<String> list = new LinkedList<String>(
					Arrays.asList(allArguments));

			legalArgs.put(lemma, list);

			return list;
		}
	}

	/**
	 * @throws FileNotFoundException
	 * 
	 */
	private static void readLegalArgs() {
		try {
			legalArgs = new HashMap<String, LinkedList<String>>();

			InputStream legalArgsFile = NomFeatureHelper.class
					.getResourceAsStream("nombank.labels.allowed");
			BufferedReader in = new BufferedReader(new InputStreamReader(
					legalArgsFile));

			int lineNumber = 0;

			String line;
			// for (String line : LineIO.read(legalArgsFile)) {

			while ((line = in.readLine()) != null) {
				String[] parts = line.split("\\s+");
				String lemma = parts[0];

				assert parts.length == 2 : "Incorrect number of fields on line "
						+ lineNumber + "!";

				LinkedList<String> legalArgList = new LinkedList<String>();
				Set<String> args = new HashSet<String>(Arrays.asList(parts[1]
						.split(",")));

				for (String arg : args) {
					legalArgList.add(arg);

					if (!arg.startsWith("R-") && !args.contains("R-" + arg))
						legalArgList.add("R-" + arg);

					if (!arg.startsWith("C-") && !args.contains("C-" + arg))
						legalArgList.add("C-" + arg);
				}
				legalArgs.put(lemma, legalArgList);
				lineNumber++;
			}

			in.close();

		} catch (Exception ex) {
			log.fatal("Unable to read legal arguments for nom", ex);
			System.exit(-1);
		}
	}
}