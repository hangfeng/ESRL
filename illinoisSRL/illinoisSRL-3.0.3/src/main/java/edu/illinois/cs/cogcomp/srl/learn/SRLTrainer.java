package edu.illinois.cs.cogcomp.srl.learn;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import LBJ2.learn.BatchTrainer;
import LBJ2.learn.Learner;
import LBJ2.learn.Learner.Parameters;
import LBJ2.learn.Lexicon;
import LBJ2.learn.SparseAveragedPerceptron;
import LBJ2.learn.SparseNetworkLearner;
import LBJ2.parse.Parser;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.srl.main.SRLSystem;

public abstract class SRLTrainer {

	protected final Logger log = LoggerFactory.getLogger(SRLTrainer.class);

	protected static final String packageName = "edu/illinois/cs/cogcomp/srl/learners";

	protected final String conllColumnFormatFile;
	protected final String devColumnFile;

	protected final String modelsDirectory;
	protected final int progressOutput;

	public SRLTrainer(String conllColumnFormatFile, String devColumnFile,
			String modelsDirectory) {
		this.conllColumnFormatFile = conllColumnFormatFile;
		this.devColumnFile = devColumnFile;
		this.modelsDirectory = modelsDirectory;

		log.info("Setting up model directories");
		if (!IOUtils.exists(modelsDirectory)) {
			IOUtils.mkdir(modelsDirectory);
			IOUtils.mkdir(modelsDirectory + File.separator + packageName);
		}

		this.progressOutput = 10000;
	}

	protected Parameters cvParams(Learner learner, Parser parser,
			String preExtractFile, BatchTrainer lbj) throws Exception {
		// preextract a smaller version of the dataset

		Learner preExtract = lbj.preExtract(preExtractFile,
				Lexicon.CountPolicy.global);
		preExtract.saveLexicon();

		Parameters params = LBJTrainerUtility.cv(10, preExtractFile, learner);

		// clean up. Remove all traces of the lexicon and pre-extract file
		IOUtils.rm(preExtractFile);

		return params;
	}

	public void cv(int idNumTrainingRounds, int clNumTrainingRounds,
			String cvFile) throws Exception {
		if (IOUtils.exists(getIdLexiconFile())
				&& IOUtils.exists(getIdModelFile())) {
			log.info("Found identifier. Not training");
			log.info("To force retrain, delete {}", getIdLexiconFile());
		} else {
			log.info("Training identifier");
			cvIdentifier(idNumTrainingRounds, cvFile);
		}

		Pair<Double, Double> idThresholdBeta = getIdThreshold();

		log.info("Training classifier");
		cvClassifier(idThresholdBeta, clNumTrainingRounds, cvFile);

		log.info("Finished training");

		log.info("Identifier tuning parameters:  (threshold, beta)= {}",
				idThresholdBeta);
	}

	public void train(int idNumTrainingRounds, double idLearningRate,
			double idThickness, int clNumTrainingRounds, double clLearningRate,
			double clThickness) throws Exception {

		if (IOUtils.exists(getIdLexiconFile())
				&& IOUtils.exists(getIdModelFile())) {
			log.info("Found identifier. Not training");
			log.info("To force retrain, delete {}", getIdLexiconFile());
		} else {
			log.info("Training identifier");
			trainIdentifier(idNumTrainingRounds, idLearningRate, idThickness);
		}

		Pair<Double, Double> idThresholdBeta = getIdThreshold();

		log.info("Training classifier");
		trainClassifier(idThresholdBeta, clNumTrainingRounds, clLearningRate,
				clThickness);

		log.info("Finished training");

		log.info("Identifier tuning parameters:  (threshold, beta)= {}",
				idThresholdBeta);

	}

	private Pair<Double, Double> getIdThreshold() throws FileNotFoundException,
			IOException {
		log.info("Tuning identifier threshold");
		Pair<Double, Double> idThresholdBeta = tuneIdentifier();

		String thresholdsFile = this.modelsDirectory + File.separator
				+ packageName + File.separator + getIdentifierName()
				+ ".thresholds";

		log.info("Writing thresholds to {}", thresholdsFile);

		SRLSystem.writeThresholds(thresholdsFile, idThresholdBeta.getFirst(),
				idThresholdBeta.getSecond());
		return idThresholdBeta;
	}

	protected void setLearnerParameters(Learner learner, double learningRate,
			double thickness) {
		if (learner instanceof SparseNetworkLearner) {

			SparseNetworkLearner.Parameters p = new SparseNetworkLearner.Parameters();

			SparseAveragedPerceptron.Parameters p1 = new SparseAveragedPerceptron.Parameters();
			p1.learningRate = learningRate;
			p1.thickness = thickness;

			p.baseLTU = new SparseAveragedPerceptron(p1);

			learner.setParameters(p);
		} else if (learner instanceof SparseAveragedPerceptron) {
			SparseAveragedPerceptron.Parameters p1 = new SparseAveragedPerceptron.Parameters();
			p1.learningRate = learningRate;
			p1.thickness = thickness;
			learner.setParameters(p1);
		} else {
			log.error("Invalid learner type. "
					+ "Neither SparseAveragedPerceptron "
					+ "nor SparseNetworkLearner");
			throw new RuntimeException();
		}
	}

	protected Learner preExtract(BatchTrainer lbj, String preExtractFile,
			int pruneCount) {
		Learner preExtract = lbj.preExtract(preExtractFile,
				Lexicon.CountPolicy.global);

		log.info("Pruning features that occur fewer than {} times", pruneCount);
		lbj.pruneDataset(preExtractFile, new Lexicon.PruningPolicy(pruneCount),
				preExtract);

		log.info("Saving lexicon");
		preExtract.getLexicon().discardPrunedFeatures();

		preExtract.saveLexicon();
		return preExtract;
	}

	protected String getClModelFile() {
		return this.modelsDirectory + File.separator + packageName
				+ File.separator + getClassifierName() + ".lc";
	}

	protected String getClLexiconFile() {
		return this.modelsDirectory + File.separator + packageName
				+ File.separator + getClassifierName() + ".lex";
	}

	protected String getIdModelFile() {
		return this.modelsDirectory + File.separator + packageName
				+ File.separator + getIdentifierName() + ".lc";
	}

	protected String getIdLexiconFile() {
		return this.modelsDirectory + File.separator + packageName
				+ File.separator + getIdentifierName() + ".lex";
	}

	protected abstract void trainIdentifier(int idNumTrainingRounds,
			double idLearningRate, double idThickness) throws Exception;

	protected abstract Pair<Double, Double> tuneIdentifier();

	protected abstract void trainClassifier(Pair<Double, Double> idParams,
			int numClTrainingRounds, double clLearningRate, double clThickness)
			throws IOException;

	protected abstract String getClassifierName();

	protected abstract String getIdentifierName();

	protected abstract void cvIdentifier(int idNumTrainingRounds, String cvFile)
			throws Exception;

	protected abstract void cvClassifier(Pair<Double, Double> idParams,
			int numClTrainingRounds, String cvFile) throws Exception;

}