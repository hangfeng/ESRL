// Modifying this comment will cause the next execution of LBJ2 to overwrite this file.
// F1B88000000000000000B49CC2E4E2A4D294555580B4D2A4A0E02F17B4D4C292D2A4D2634D07ECFCB2E29CC292D4DCB215846D450B1D558A6582E4DCB2E4D4CC9C150D8CB4D270B618AA77F4DCB4D2A4C29CF22D0D4D4DB4F4D218913A104D965A052545A9A6DA05B000C2881966D6000000

package edu.illinois.cs.cogcomp.srl.learners;

import LBJ2.classify.*;
import LBJ2.infer.*;
import LBJ2.learn.*;
import LBJ2.parse.*;
import edu.illinois.cs.cogcomp.edison.features.*;
import edu.illinois.cs.cogcomp.edison.sentences.*;
import edu.illinois.cs.cogcomp.srl.data.*;
import edu.illinois.cs.cogcomp.srl.features.*;
import java.util.*;


public class VerbSRLFeatures1 extends Classifier
{
  public VerbSRLFeatures1()
  {
    containingPackage = "edu.illinois.cs.cogcomp.srl.learners";
    name = "VerbSRLFeatures1";
  }

  public String getInputType() { return "edu.illinois.cs.cogcomp.edison.sentences.Constituent"; }
  public String getOutputType() { return "discrete%"; }

  public FeatureVector classify(Object __example)
  {
    if (!(__example instanceof Constituent))
    {
      String type = __example == null ? "null" : __example.getClass().getName();
      System.err.println("Classifier 'VerbSRLFeatures1(Constituent)' defined on line 14 of VerbSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    Constituent c = (Constituent) __example;

    FeatureVector __result;
    __result = new FeatureVector();
    String __id;
    String __value;

    Object __values = (new VerbFeatureGenerator()).getFeatures(c);

    if (__values instanceof java.util.Collection)
    {
      for (java.util.Iterator __I = ((java.util.Collection) __values).iterator(); __I.hasNext(); )
      {
        __id = __I.next().toString();
        __result.addFeature(new DiscretePrimitiveStringFeature(this.containingPackage, this.name, __id, "true", valueIndexOf("true"), (short) 0));
      }
    }
    else
    {
      for (java.util.Iterator __I = ((java.util.Map) __values).entrySet().iterator(); __I.hasNext(); )
      {
        java.util.Map.Entry __e = (java.util.Map.Entry) __I.next();
        __id = __e.getKey().toString();
        __value = __e.getValue().toString();
        __result.addFeature(new DiscretePrimitiveStringFeature(this.containingPackage, this.name, __id, __value, valueIndexOf(__value), (short) 0));
      }
    }

    return __result;
  }

  public FeatureVector[] classify(Object[] examples)
  {
    if (!(examples instanceof Constituent[]))
    {
      String type = examples == null ? "null" : examples.getClass().getName();
      System.err.println("Classifier 'VerbSRLFeatures1(Constituent)' defined on line 14 of VerbSRLIdentifier.lbj received '" + type + "' as input.");
      new Exception().printStackTrace();
      System.exit(1);
    }

    return super.classify(examples);
  }

  public int hashCode() { return "VerbSRLFeatures1".hashCode(); }
  public boolean equals(Object o) { return o instanceof VerbSRLFeatures1; }
}

