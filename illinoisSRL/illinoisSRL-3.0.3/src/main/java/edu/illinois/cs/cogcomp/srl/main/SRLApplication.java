/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.main;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.srl.learn.NomSRLTrainer;
import edu.illinois.cs.cogcomp.srl.learn.SRLTrainer;
import edu.illinois.cs.cogcomp.srl.learn.VerbSRLTrainer;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;

/**
 * This is the main SRL application, which can be used for command line
 * processing of text.
 * 
 * @author Vivek Srikumar
 */
public class SRLApplication {

	private static final Logger log = LoggerFactory
			.getLogger(SRLApplication.class);

	private static String configFile;
	private static boolean interactiveMode;

	private static boolean serverMode;
	private static int serverPort;
	private static int serverNumThreads;

	private static boolean batchMode;
	private static String inputFile;
	private static String outputFile;

	private static boolean whitespace;

	private static boolean nom, verb;

	private static String devFile;
	private static String trainFile;
	private static boolean trainMode;
	private static String modelsDir;
	private static int idNumTrainingRounds;
	private static int clNumTrainingRounds;
	private static String cvFile;

	public static void main(String[] args) {
		readOptions(args);

		SRLConfig config = SRLConfig.getInstance(configFile);
		log.info("Finished reading configuration.");

		Runnable runnable = null;

		if (trainMode) {

			// double idLearningRate = -1, idThickness = -1, clLearningRate =
			// -1, clThickness = -1;
			SRLTrainer trainer = null;
			if (verb) {
				log.info("Training Verb SRL.");
				trainer = new VerbSRLTrainer(trainFile, devFile, modelsDir);

				// idLearningRate = 0.1;
				// idThickness = 3.5;
				// clLearningRate = 0.05;
				// clThickness = 5.0;

			} else if (nom) {
				log.info("Training NOM SRL.");
				trainer = new NomSRLTrainer(trainFile, devFile, modelsDir,
						NomLexEntry.VERBAL);

				// idLearningRate = 0.1;
				// idThickness = 4.0;
				// clLearningRate = 0.05;
				// clThickness = 5.0;

			}

			try {

				// log.info("Learning parameters, fixed by CV. ");
				// log.info("Identifier learning rate = {}, thickness = {}",
				// idLearningRate, idThickness);
				// log.info("Classifier learning rate = {}, thickness = {}",
				// clLearningRate, clThickness);

				log.info("Writing models and lexicon to {}", modelsDir);
				// trainer.train(idNumTrainingRounds, idLearningRate,
				// idThickness,
				// clNumTrainingRounds, clLearningRate, clThickness);

				trainer.cv(idNumTrainingRounds, clNumTrainingRounds, cvFile);

			} catch (Exception e) {
				log.error("Error while training", e);
				System.exit(-1);
			}

		} else {

			SRLSystem srlSystem = null;

			if (verb)
				srlSystem = VerbSRLSystem.getInstance();
			else if (nom)
				srlSystem = NomSRLSystem.getInstance();

			if (serverMode) {
				runnable = new SRLApplicationServer(srlSystem, config,
						serverPort, serverNumThreads);
			} else if (batchMode) {
				runnable = new BatchSRLApplication(srlSystem, config,
						inputFile, outputFile, whitespace);
			} else if (interactiveMode) {
				runnable = new InteractiveSRLApplication(srlSystem, config);
			} else {
				assert false : "The application can be in one of server, "
						+ "batch, train or interactive modes";
			}
			runnable.run();
		}

	}

	@SuppressWarnings("static-access")
	private static void readOptions(String[] args) {

		Options options = new Options();

		options.addOption(OptionBuilder
				.withLongOpt("config")
				.withArgName("config file")
				.hasArg()
				.withDescription(
						"The configuration file. Default = '"
								+ SRLConfig.SRL_CONFIG_PROPERTIES_FILE + "'.")
				.create('c'));

		options.addOption(OptionBuilder
				.withLongOpt("server")
				.withArgName("port")
				.hasArg()
				.withDescription(
						"Start as a curator server at the specified port. See also -t")
				.create('s'));

		options.addOption(OptionBuilder
				.withLongOpt("batch")
				.withArgName("input file")
				.hasArg()
				.withDescription(
						"Run in batch mode using the input file. "
								+ "The file should have one sentence per line.  Requires --batch-output-file.")
				.create('b'));

		options.addOption(OptionBuilder
				.withLongOpt("batch-output-file")
				.withArgName("output file")
				.hasArg()
				.withDescription(
						"The SRL output is written in the CoNLL column format to this file. "
								+ "Works only with batch mode.").create());

		options.addOption(OptionBuilder
				.withLongOpt("batch-whitespace")
				.withDescription(
						"Indicates that the sentences in the file "
								+ "are whitespace tokenized. If this is not "
								+ "present, the sentences are tokenized using the"
								+ " curator's tokenizer. "
								+ "Works only with batch mode").create());

		options.addOption("i", "interactive", false,
				"Start in interactive mode.");

		options.addOption("t", "train", false, "Train the SRL System.");

		options.addOption(OptionBuilder
				.withLongOpt("train-training-file")
				.withArgName("file")
				.hasArg()
				.withDescription(
						"CoNLL column format file for training the SRL system. "
								+ "Required in training mode").create());

		options.addOption(OptionBuilder
				.withLongOpt("train-dev-file")
				.withArgName("file")
				.hasArg()
				.withDescription(
						"CoNLL column format file to be used as development set. "
								+ "Required in training mode.").create());

		options.addOption(OptionBuilder
				.withLongOpt("train-models-dir")
				.withArgName("directory")
				.hasArg()
				.withDescription(
						"Directory to store models and lexicon after training")
				.create());

		options.addOption(OptionBuilder
				.withLongOpt("train-id-num-iters")
				.withArgName("num-iters")
				.hasArg()
				.withDescription(
						"Number of rounds of training for identifier. Required in training mode.")
				.create());

		options.addOption(OptionBuilder
				.withLongOpt("train-cl-num-iters")
				.withArgName("num-iters")
				.hasArg()
				.withDescription(
						"Number of rounds of training for classifier. Required in training mode.")
				.create());

		options.addOption(OptionBuilder
				.withLongOpt("train-cv-file")
				.withArgName("num-iters")
				.hasArg()
				.withDescription(
						"CoNLL column format file for running cross-validation to pick training "
								+ "parameters. This is typically a smaller file than the entire "
								+ "training set. Required in training mode.")
				.create());

		options.addOption(OptionBuilder
				.withLongOpt("server-threads")
				.withDescription(
						"Number of threads for the SRL curator server (Default = 1). "
								+ "This option is ignored if --server is not specified.. ")
				.create());

		options.addOption("h", "help", false, "Prints this message.");

		options.addOption("n", "nom", false, "Perform nominal SRL");
		options.addOption("v", "verb", false, "Perform verb SRL");

		CommandLineParser parser = new GnuParser();
		CommandLine cli = null;
		try {
			cli = parser.parse(options, args);
		} catch (ParseException ex) {
			System.err.println("Error: " + ex.getMessage());
			printHelp(options);
			System.exit(-1);
		}

		if (cli.hasOption('h') || cli.hasOption("help")) {
			printHelp(options);
			System.exit(0);
		}

		if (cli.hasOption('n') || cli.hasOption("nom"))
			nom = true;
		else if (cli.hasOption('v') || cli.hasOption("verb"))
			verb = true;
		else {
			System.err
					.println("One of verb or nom SRL should be chosen (-v or -n).");
			printHelp(options);
			System.exit(-1);
		}

		configFile = cli.getOptionValue("config",
				SRLConfig.SRL_CONFIG_PROPERTIES_FILE);

		serverMode = false;
		interactiveMode = false;
		batchMode = false;

		if (cli.hasOption("server")) {

			log.info("Starting SRL in server mode...");
			serverMode = true;
			serverPort = Integer.parseInt(cli.getOptionValue("server"));

			if (cli.hasOption("server-threads"))
				serverNumThreads = Integer.parseInt(cli
						.getOptionValue("server-threads"));
			else
				serverNumThreads = 1;

		} else if (cli.hasOption('i') || cli.hasOption("interactive")) {

			log.info("Starting SRL in interactive mode...");
			interactiveMode = true;
		} else if (cli.hasOption('t') || cli.hasOption("train")) {
			log.info("Training the SRL system");

			trainMode = true;
			if (!cli.hasOption("train-training-file")) {
				System.err
						.println("Error: Training mode requires an input file to train. "
								+ "train-training-file needs to be set");
				printHelp(options);
				System.exit(-1);
			}

			if (!cli.hasOption("train-dev-file")) {
				System.err
						.println("Error: Training mode requires an development file. "
								+ "train-dev-file needs to be set");
				printHelp(options);
				System.exit(-1);

			}

			if (!cli.hasOption("train-id-num-iters")) {
				System.err
						.println("Error: Training mode requires number of iterations for identifier (train-id-num-iters). ");
				printHelp(options);
				System.exit(-1);

			}

			if (!cli.hasOption("train-cl-num-iters")) {
				System.err
						.println("Error: Training mode requires number of iterations for identifier (train-cl-num-iters). ");
				printHelp(options);
				System.exit(-1);

			}

			if (!cli.hasOption("train-cv-file")) {
				System.err
						.println("Error: Training mode requires a cross-validation input file (train-cv-file). ");
				printHelp(options);
				System.exit(-1);

			}

			trainFile = cli.getOptionValue("train-training-file");
			devFile = cli.getOptionValue("train-dev-file");

			modelsDir = cli.getOptionValue("train-models-dir", "models");

			idNumTrainingRounds = Integer.parseInt(cli
					.getOptionValue("train-id-num-iters"));
			clNumTrainingRounds = Integer.parseInt(cli
					.getOptionValue("train-cl-num-iters"));
			cvFile = cli.getOptionValue("train-cv-file");

		} else {
			if (!cli.hasOption("batch")) {
				System.err
						.println("One of [interactive,batch,server,train] modes "
								+ "must be chosen");
				printHelp(options);
				System.exit(-1);
			}

			if (!cli.hasOption("batch-output-file")) {
				System.err.println("Error: Batch mode requires"
						+ "the output file to be specified");
				printHelp(options);
				System.exit(-1);
			}

			batchMode = true;
			inputFile = cli.getOptionValue("batch");
			outputFile = cli.getOptionValue("batch-output-file");
			whitespace = (cli.hasOption('w') || cli.hasOption("whitespace"));
		}

	}

	private static void printHelp(Options options) {
		HelpFormatter formatter = new HelpFormatter();

		String usage = "[-v | -n] [-i | -s <port> | -b <file>] <options>";
		String header = "Run the Illinois Semantic Role Labeler. "
				+ "Either -v or -n should be specified for nominal "
				+ "or verb SRL.";
		String footer = "For more information, goto http://cogcomp.cs.illinois.edu";
		formatter.printHelp(usage, header, options, footer);
	}
}
