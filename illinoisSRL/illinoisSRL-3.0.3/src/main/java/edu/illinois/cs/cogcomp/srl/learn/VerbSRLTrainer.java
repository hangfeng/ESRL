package edu.illinois.cs.cogcomp.srl.learn;

import java.io.File;
import java.io.IOException;

import LBJ2.learn.BatchTrainer;
import LBJ2.learn.Learner;
import LBJ2.learn.Learner.Parameters;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.srl.data.SRLClassifierParser;
import edu.illinois.cs.cogcomp.srl.data.XuePalmerIdentifierFilterParser;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentClassifier;
import edu.illinois.cs.cogcomp.srl.learners.VerbArgumentIdentifier;
import edu.illinois.cs.cogcomp.srl.testers.IdentifierThresholdTuner;

public class VerbSRLTrainer extends SRLTrainer {

	public VerbSRLTrainer(String conllColumnFormatFile, String devColumnFile,
			String modelsDirectory) {
		super(conllColumnFormatFile, devColumnFile, modelsDirectory);
	}

	@Override
	protected void trainClassifier(Pair<Double, Double> idParams,
			int numClTrainingRounds, double clLearningRate, double clThickness)
			throws IOException {

		VerbArgumentIdentifier identfier = new VerbArgumentIdentifier(
				getIdModelFile(), this.getIdLexiconFile());

		log.info("Pre-extracting classifier");

		double threshold = idParams.getFirst();
		double beta = idParams.getSecond();

		SRLClassifierParser parser = new SRLClassifierParser(
				conllColumnFormatFile, threshold, beta, identfier);
		VerbArgumentClassifier learner = new VerbArgumentClassifier(
				getClModelFile(), getClLexiconFile());

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String clTrainPreExtractFile = modelsDirectory + File.separator
				+ "verb.cl.preextract";
		Learner preExtract = preExtract(lbj, clTrainPreExtractFile, 4);

		// learn
		log.info("Starting classifier training");

		learner.setLabelLexicon(preExtract.getLabelLexicon());
		setLearnerParameters(learner, clLearningRate, clThickness);
		lbj.train(numClTrainingRounds);

		log.info("Finished training");
		log.info("Cleaning up");

		learner.save();
		IOUtils.rm(clTrainPreExtractFile);

		log.info("Finished training classifier");
	}

	@Override
	protected void cvClassifier(Pair<Double, Double> idParams,
			int numClTrainingRounds, String cvFile) throws Exception {

		double threshold = idParams.getFirst();
		double beta = idParams.getSecond();

		VerbArgumentIdentifier identfier = new VerbArgumentIdentifier(
				getIdModelFile(), this.getIdLexiconFile());

		log.info("Starting cross validation");
		SRLClassifierParser cvParser = new SRLClassifierParser(
				cvFile, threshold, beta, identfier);

		VerbArgumentClassifier cvLearner = new VerbArgumentClassifier(
				getClModelFile() + ".cv", getClLexiconFile() + ".cv");

		BatchTrainer cvLbj = new BatchTrainer(cvLearner, cvParser,
				progressOutput);

		Parameters params = cvParams(cvLearner, cvParser, modelsDirectory
				+ File.separator + "verb.cl.preextract.cv", cvLbj);

		IOUtils.rm(getClModelFile() + ".cv");
		IOUtils.rm(getClLexiconFile() + ".cv");

		log.info("Pre-extracting classifier");

		SRLClassifierParser parser = new SRLClassifierParser(
				conllColumnFormatFile, threshold, beta, identfier);
		VerbArgumentClassifier learner = new VerbArgumentClassifier(
				getClModelFile(), getClLexiconFile());

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String clTrainPreExtractFile = modelsDirectory + File.separator
				+ "verb.cl.preextract";
		Learner preExtract = preExtract(lbj, clTrainPreExtractFile, 4);

		// learn
		log.info("Starting classifier training");

		learner.setLabelLexicon(preExtract.getLabelLexicon());
		learner.setParameters(params);

		lbj.train(numClTrainingRounds);

		log.info("Finished training");
		log.info("Cleaning up");

		learner.save();
		IOUtils.rm(clTrainPreExtractFile);

		log.info("Finished training classifier");
	}

	@Override
	protected Pair<Double, Double> tuneIdentifier() {
		VerbArgumentIdentifier identfier = new VerbArgumentIdentifier(
				getIdModelFile(), this.getIdLexiconFile());

		IdentifierThresholdTuner tuner = new IdentifierThresholdTuner(identfier, 2);

		return tuner.tuneThreshold(devColumnFile, 0.01, 0.01, 0.5, 0.01, 0.01,
				0.5);
	}

	@Override
	protected void trainIdentifier(int idNumTrainingRounds,
			double idLearningRate, double idThickness) throws Exception {

		// first preExtract

		log.info("Preextracting identifier");
		Learner learner = new VerbArgumentIdentifier(getIdModelFile(),
				getIdLexiconFile());
		XuePalmerIdentifierFilterParser parser = new XuePalmerIdentifierFilterParser(
				"", conllColumnFormatFile);

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String idTrainPreExtractFile = modelsDirectory + File.separator
				+ "verb.id.preextract";
		Learner preExtract = preExtract(lbj, idTrainPreExtractFile, 3);

		// learn

		log.info("Starting identifier training");
		learner.setLabelLexicon(preExtract.getLabelLexicon());
		setLearnerParameters(learner, idLearningRate, idThickness);
		lbj.train(idNumTrainingRounds);

		log.info("Finished training");
		// cleanup

		log.info("Cleaning up.");

		learner.save();
		IOUtils.rm(idTrainPreExtractFile);

		log.info("Finished training");

	}

	@Override
	protected String getClassifierName() {
		return VerbArgumentClassifier.class.getSimpleName();
	}

	@Override
	protected String getIdentifierName() {
		return VerbArgumentIdentifier.class.getSimpleName();
	}

	@Override
	protected void cvIdentifier(int idNumTrainingRounds, String cvFile)
			throws Exception {

		log.info("Starting cross validation");
		Learner cvLearner = new VerbArgumentIdentifier(
				getIdModelFile() + ".cv", getIdLexiconFile() + ".cv");

		XuePalmerIdentifierFilterParser cvParser = new XuePalmerIdentifierFilterParser(
				"", cvFile);

		BatchTrainer cvLbj = new BatchTrainer(cvLearner, cvParser,
				progressOutput);

		Parameters params = cvParams(cvLearner, cvParser, modelsDirectory
				+ File.separator + "verb.id.preextract.cv", cvLbj);

		IOUtils.rm(getIdModelFile() + ".cv");
		IOUtils.rm(getIdLexiconFile() + ".cv");

		// first preExtract

		log.info("Preextracting identifier");
		Learner learner = new VerbArgumentIdentifier(getIdModelFile(),
				getIdLexiconFile());
		XuePalmerIdentifierFilterParser parser = new XuePalmerIdentifierFilterParser(
				"", conllColumnFormatFile);

		BatchTrainer lbj = new BatchTrainer(learner, parser, progressOutput);

		String idTrainPreExtractFile = modelsDirectory + File.separator
				+ "verb.id.preextract";
		Learner preExtract = preExtract(lbj, idTrainPreExtractFile, 3);

		// learn

		log.info("Starting identifier training");
		learner.setLabelLexicon(preExtract.getLabelLexicon());
		// setLearnerParameters(learner, idLearningRate, idThickness);
		learner.setParameters(params);

		lbj.train(idNumTrainingRounds);

		log.info("Finished training");
		// cleanup

		log.info("Cleaning up.");

		learner.save();
		IOUtils.rm(idTrainPreExtractFile);

		log.info("Finished training");

	}
}
