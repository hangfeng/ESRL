package edu.illinois.cs.cogcomp.srl.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.srl.curator.IllinoisSRLServer;

public class SRLApplicationServer implements Runnable {
	private static final Logger log = LoggerFactory
			.getLogger(SRLApplicationServer.class);
	private IllinoisSRLServer server;
	private final int serverPort;
	private final int numThreads;

	public SRLApplicationServer(SRLSystem srlSystem, SRLConfig config, int serverPort, int numThreads) {
		this.serverPort = serverPort;
		this.numThreads = numThreads;
		boolean beamSearch = config.doBeamSearch();

		if (beamSearch) {
			log.info("Running SRL with beam search inference. Beam size = {}",
					config.getBeamSize());
		} else {
			log.info("Running ILP inference with Gurobi. The environment should be setup for this to work.");
		}

		server = new IllinoisSRLServer(srlSystem, beamSearch);

	}

	@Override
	public void run() {
		server.runServer(serverPort, numThreads);
	}

}
