/**
 * 
 */
package edu.illinois.cs.cogcomp.srl.features;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexEntry.NomLexClasses;
import edu.illinois.cs.cogcomp.srl.utilities.NomLexReader;

/**
 * @author Vivek Srikumar
 * 
 *         Jan 5, 2010
 */
public class NomFeatureHelper extends FeatureHelper {

	private final static Logger log = LoggerFactory
			.getLogger(NomFeatureHelper.class);

	private static final String nomlexFileName = "NOMLEX-plus-clean.1.0";

	public static NomLexReader nomLex;

	static {

		try {
			log.info("Loading NOMLEX. Looking for file {}", nomlexFileName);
			InputStream resource = NomFeatureHelper.class
					.getResourceAsStream(nomlexFileName);

			nomLex = new NomLexReader(resource);
		} catch (Exception e) {
			log.error("Unable to read nomlex.", e);
			System.exit(-1);
		}

	}

	// public static String getNomType(Constituent argument) {
	//
	// int predicatePosition = getPredicatePosition(argument);
	// Constituent predicate = argument.getIncomingRelations().get(0)
	// .getSource();
	//
	// TextAnnotation ta = argument.getTextAnnotation();
	// String predicateWord = ta.getToken(predicatePosition).toLowerCase()
	// .trim();
	// String predicateLemma = predicate
	// .getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);
	//
	// // System.out.println(predicateLemma);
	//
	// return getNomType(predicateWord, predicateLemma);
	// }
	//
	// public static String getNomType(String predicateWord, String
	// predicateLemma) {
	// Map<NomLexClasses, NomLexEntry> nomLexEntry;
	// if (nomLex.containsEntry(predicateWord))
	// nomLexEntry = nomLex.getNomLexEntry(predicateWord);
	// else if (nomLex.containsEntry(predicateLemma))
	// nomLexEntry = nomLex.getNomLexEntry(predicateLemma);
	// else
	// return "UNKNOWN_NOMINALIZATION_CLASS";
	//
	// if (nomLexEntry.containsKey("NOM")) {
	// return nomLexEntry.get("NOM").nom_type;
	// } else {
	// final String type = nomLexEntry.keySet().iterator().next();
	// return nomLexEntry.get(type).nom_type;
	// }
	// }

	public static Set<NomLexClasses> getNomLexClass(String predicateWord,
			String predicateLemma) {

		List<NomLexEntry> nomLexEntry;

		if (nomLex.containsEntry(predicateWord))
			nomLexEntry = nomLex.getNomLexEntry(predicateWord);
		else if (nomLex.containsEntry(predicateLemma))
			nomLexEntry = nomLex.getNomLexEntry(predicateLemma);
		else
			return new HashSet<NomLexEntry.NomLexClasses>(
					Arrays.asList(NomLexClasses.UNKNOWN_CLASS));

		Set<NomLexClasses> set = new HashSet<NomLexEntry.NomLexClasses>();
		for (NomLexEntry s : nomLexEntry) {
			set.add(s.nomClass);
		}

		return set;
	}

	public static String[] getNounClassDeverbalNoun(Constituent argument) {
		int predicatePosition = getPredicatePosition(argument);
		Constituent predicate = argument.getIncomingRelations().get(0)
				.getSource();

		TextAnnotation ta = argument.getTextAnnotation();
		String predicateWord = ta.getToken(predicatePosition);
		String predicateLemma = predicate
				.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);

		return getVerbClassDeverbalNoun(predicateWord, predicateLemma);
	}

	public static String[] getVerbClassDeverbalNoun(String predicateWord,
			String predicateLemma) {

		// System.out.println(predicateLemma);

		List<NomLexEntry> nomLexEntry;
		if (nomLex.containsEntry(predicateWord))
			nomLexEntry = nomLex.getNomLexEntry(predicateWord);
		else if (nomLex.containsEntry(predicateLemma))
			nomLexEntry = nomLex.getNomLexEntry(predicateLemma);
		else
			return new String[] { "NOT_IN_NOMLEX" };

		Set<String> classes = new HashSet<String>();
		for (NomLexEntry e : nomLexEntry) {
			if (NomLexEntry.VERBAL.contains(e.nomClass)) {
				classes.add(e.verb);
			}
		}

		return classes.toArray(new String[classes.size()]);
	}
}