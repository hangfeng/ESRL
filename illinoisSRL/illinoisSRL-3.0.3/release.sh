#!/bin/bash -e

VERSION=`mvn org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | grep -v 'INFO'`

# First copy all the models
BIN=target/classes
LBJBIN=target/classes
SRC=src/main/java/
GSP=$SRC

MODELSDIR=models

# Move all .lex and .lc files to the models directory.  Change in
# v3.0.2: This is not needed because the training is performed
# manually
# for file in `find $LBJBIN -name '*.lc'`; do 
#     dir=`dirname $file | sed s#$LBJBIN## | sed 's#^/##'`;
#     echo 'Copying '$file' to '$MODELSDIR/$dir;
#     mkdir -p $MODELSDIR/$dir && cp $file $MODELSDIR/$dir;
# done

# for file in `find $LBJBIN -name '*.lex'`; do 
#     dir=`dirname $file | sed s#$LBJBIN##`;
#     echo 'Copying '$file' to '$MODELSDIR/$dir;
#     mkdir -p $MODELSDIR/$dir && cp $file $MODELSDIR/$dir;
# done

# LBJ sometimes leaves behind HUGE .ex files that are not really
# needed. These should be deleted.
for file in `find src | grep -E 'ex$'`; do 
    rm $file
done

# Now clean and package
mvn clean package

# also package the models
tmpdir=tmp-Srl-$RANDOM
mkdir -p $tmpdir/edu/illinois/cs/cogcomp/srl/{learners,features}
cp models/edu/illinois/cs/cogcomp/srl/learners/Verb* $tmpdir/edu/illinois/cs/cogcomp/srl/learners/
cp models/edu/illinois/cs/cogcomp/srl/features/verbClass.txt $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cp models/edu/illinois/cs/cogcomp/srl/features/verb.legal.arguments $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cd $tmpdir
jar cf illinoisSRL-verb-models-$VERSION.jar edu
mv illinoisSRL-verb-models-$VERSION.jar ..
cd ..
rm -rdf $tmpdir

tmpdir=tmp-Srl-$RANDOM
mkdir -p $tmpdir/edu/illinois/cs/cogcomp/srl/{learners,features}
cp models/edu/illinois/cs/cogcomp/srl/learners/Nom* $tmpdir/edu/illinois/cs/cogcomp/srl/learners/
cp models/edu/illinois/cs/cogcomp/srl/features/NOMLEX-plus-clean.1.0 $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cp models/edu/illinois/cs/cogcomp/srl/features/nombank.labels.allowed $tmpdir/edu/illinois/cs/cogcomp/srl/features/
cd $tmpdir
jar cf illinoisSRL-nom-models-$VERSION.jar edu
mv illinoisSRL-nom-models-$VERSION.jar ..
cd ..
rm -rdf $tmpdir

# get all dependencies
mvn dependency:copy-dependencies

# Prepare release directory
release=illinoisSRL-$VERSION 
rm -rdf $release

mkdir -p $release/{bin,doc,models,config}
cp target/illinoisSRL-$VERSION.jar $release/bin
cp target/dependency/*.jar $release/bin

mv illinoisSRL-verb-models-$VERSION.jar $release/models
mv illinoisSRL-nom-models-$VERSION.jar $release/models

cp ./dist/config/* $release/config

cp doc/manual.html $release/doc
cp doc/style.css $release/doc
cp doc/manual.pdf $release/doc

cp ./dist/srl.sh $release/srl.sh

cp RELEASE.txt $release

chmod +x $release/srl.sh

tar cfvz $release.tar.gz $release

mv $release.tar.gz releases
