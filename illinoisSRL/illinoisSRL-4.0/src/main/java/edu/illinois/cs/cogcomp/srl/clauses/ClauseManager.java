package edu.illinois.cs.cogcomp.srl.clauses;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.datastructures.IQueryable;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Lexicon;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.edison.annotators.ClauseViewGenerator;
import edu.illinois.cs.cogcomp.edison.data.IResetableIterator;
import edu.illinois.cs.cogcomp.edison.features.FeatureExtractor;
import edu.illinois.cs.cogcomp.edison.features.manifest.FeatureManifest;
import edu.illinois.cs.cogcomp.edison.sentences.Selectors;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.indsup.learning.StructuredProblem;
import edu.illinois.cs.cogcomp.indsup.learning.WeightVector;
import edu.illinois.cs.cogcomp.srl.caches.SentenceDBHandler;
import edu.illinois.cs.cogcomp.srl.data.Dataset;
import edu.illinois.cs.cogcomp.srl.features.FeatureGenerators;

public class ClauseManager {
	private final static Logger log = LoggerFactory
			.getLogger(ClauseManager.class);

	private final FeatureExtractor fex;
	private final ClauseFeatureExtractor structuredFex;
	private Lexicon lexicon;

	private String lexiconFile;
	private String weightsFile;

	private WeightVector weights;

	public final boolean trainingMode;

	public ClauseManager(boolean loadFromClasspath) throws Exception {

		initializeFeatureManifest();

		String features = "clauses/features.fex";

		InputStream file = (IOUtils.lsResources(ClauseManager.class, features))
				.get(0).openStream();
		FeatureManifest featureManifest = new FeatureManifest(file);

		fex = featureManifest.createFex();

		if (loadFromClasspath) {
			lexiconFile = "clauses/clauses.lex";
			weightsFile = "clauses/clauses.model.lc";
		} else {
			lexiconFile = "models/clauses.lex";
			weightsFile = "models/clauses.model.lc";
		}

		this.trainingMode = !loadFromClasspath;

		structuredFex = new ClauseFeatureExtractor(this);

	}

	private void initializeFeatureManifest() {
		FeatureManifest.setFeatureExtractor("has-verb",
				FeatureGenerators.hasVerb);

		FeatureManifest.setTransformer("candidate-boundary-transformer",
				FeatureGenerators.candidateBoundaryTransformer);
	}

	public FeatureExtractor getFeatureExtractor() {
		return fex;
	}

	public Lexicon getLexicon() {

		if (lexicon == null) {
			synchronized (this) {
				if (lexicon == null) {
					try {

						if (IOUtils.exists(lexiconFile)) {
							lexicon = new Lexicon(new FileInputStream(new File(
									lexiconFile)));
							log.info("Loaded lexicon from {} with {} entries",
									lexiconFile, lexicon.size());

						} else {

							List<URL> ls = IOUtils.lsResources(
									ClauseManager.class, lexiconFile);

							if (ls.size() == 0) {
								log.info("Lexicon not found. Creating new lexicon...");
								lexicon = new Lexicon(true, false);
							} else {
								lexicon = new Lexicon(ls.get(0).openStream());
								log.info(
										"Loaded lexicon from {} with {} entries",
										lexiconFile, lexicon.size());
							}
						}
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}

		return lexicon;
	}

	public StructuredProblem getStructuredProblem(Dataset dataset)
			throws Exception {
		return getStructuredProblem(dataset, -1);
	}

	public StructuredProblem getStructuredProblem(Dataset dataset,
			int numExamples) throws Exception {
		StructuredProblem problem = new StructuredProblem();

		log.info("Loading {}", dataset);
		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(dataset);

		int i = 0;
		while (data.hasNext()) {
			TextAnnotation ta = data.next();

			if (ta.size() == 0)
				continue;

			Pair<ClauseInstance, ClauseStructure> pair = getExample(ta);
			problem.input_list.add(pair.getFirst());
			problem.output_list.add(pair.getSecond());

			i++;
			if (i % 1000 == 0)
				log.info("{} sentences loaded, #features = {}", i, getLexicon()
						.size());

			if (numExamples > 0) {
				if (i == numExamples)
					break;
			}

		}
		log.info("{} examples loaded", problem.size());

		return problem;
	}

	public Pair<ClauseInstance, ClauseStructure> getExample(TextAnnotation ta)
			throws Exception {

		ClauseInstance x = new ClauseInstance(this, ta);
		ClauseStructure y = null;
		if (ta.hasView(ViewNames.PARSE_GOLD)) {
			View view = getClauseView(ta, ViewNames.PARSE_GOLD,
					getGoldViewName());
			ta.addView(getGoldViewName(), view);

			Set<IntPair> set = new HashSet<IntPair>();
			IQueryable<IntPair> clauseSpans = view.select(Selectors.span());
			for (IntPair span : clauseSpans) {
				if (x.isValidSpan(span.getFirst(), span.getSecond())) {
					set.add(span);
				}
			}

			y = new ClauseStructure(this, x, set, null);
		}

		return new Pair<ClauseInstance, ClauseStructure>(x, y);

	}

	public View getClauseView(TextAnnotation ta, String parse,
			String clauseViewName) {
		ClauseViewGenerator clauses = new ClauseViewGenerator(parse,
				clauseViewName);

		ta.addView(clauses);

		return ta.getView(clauses.getViewName());
	}

	public WeightVector getWeightVector() {
		if (this.weights == null) {
			synchronized (this) {
				if (this.weights == null) {
					try {

						log.info("Loading weight vector for clauses from {}",
								weightsFile);
						long start = System.currentTimeMillis();
						this.weights = new WeightVector(weightsFile);
						long end = System.currentTimeMillis();

						log.info(
								"Finished loading weights for clauses. Took {} ms",
								(end - start));
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}

		return this.weights;
	}

	public String getGoldViewName() {
		return getPredictedViewName() + "_GOLD";
	}

	@SuppressWarnings("deprecation")
	public String getPredictedViewName() {
		return ViewNames.CLAUSES;
	}

	public ClauseFeatureExtractor getStructuredFeatureExtractor() {
		return structuredFex;
	}
}
