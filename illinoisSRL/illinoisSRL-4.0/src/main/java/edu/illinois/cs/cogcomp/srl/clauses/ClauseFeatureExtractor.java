package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.algorithms.Sorters;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Lexicon;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.cogcomp.edison.features.DiscreteFeature;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.features.RealFeature;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Queries;
import edu.illinois.cs.cogcomp.edison.sentences.Sentence;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotationUtilities;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.indsup.learning.FeatureVector;

public class ClauseFeatureExtractor {

	private ClauseManager manager;

	public ClauseFeatureExtractor(ClauseManager manager) {
		this.manager = manager;
	}

	public FeatureVector getFeatures(ClauseInstance x, IntPair span,
			Set<IntPair> otherClauses) throws Exception {

		// first find all the internal clauses, ordered from left to right
		List<IntPair> internalClauses = getInternalClauses(span, otherClauses);

		// next start building a feature vector using the base features

		assert x.isValidSpan(span.getFirst(), span.getSecond());

		FeatureVector baseFeats = x.getFeatures(span.getFirst(),
				span.getSecond());

		// now add the structured features.
		Set<Feature> features = new HashSet<Feature>();

		Constituent c = new Constituent("", "", x.ta, span.getFirst(),
				span.getSecond());

		// 1. Chunk embedding, with internal clauses reduced to single tokens
		// called "S". Note, sometimes the chunk boundaries need not align with
		// the clause boundary. If a chunk straddles the boundary of an internal
		// clause, then assume that it ends at the boundary before entering the
		// internal clause.

		List<String> labels = getLabelSequenceFromView(x, internalClauses, c,
				ViewNames.SHALLOW_PARSE);

		// add the full path as a string that concatenates the labels
		features.add(DiscreteFeature.create("chunk-clause-path:"
				+ StringUtils.join("-", labels)));

		// // add bigrams of these labels
		// features.addAll(bigrams("chunk-clause", labels));

		// 2. same thing for POS
		labels = getLabelSequenceFromView(x, internalClauses, c, ViewNames.POS);

		// // add the full path as a string that concatenates the labels
		// features.add(DiscreteFeature.create("pos-path:"
		// + StringUtils.join("-", labels)));

		// add bigrams of these labels
		features.addAll(bigrams("pos-clause", labels));

		// if the span is the full sentence, add an indicator.

		Sentence sentence = x.ta.getSentenceFromToken(span.getFirst());

		if (span.getFirst() == sentence.getStartSpan()
				&& span.getSecond() == sentence.getEndSpan()) {
			features.add(DiscreteFeature.create("FULL_SENTENCE"));
		}

		Lexicon lexicon = manager.getLexicon();

		FeatureVector featureVector;
		synchronized (lexicon) {
			Map<String, Float> featureMap = new HashMap<String, Float>();
			for (Feature f : features) {
				featureMap.put(f.getName(), f.getValue());
				if (manager.trainingMode && !lexicon.contains(f.getName())) {
					lexicon.previewFeature(f.getName());
				}

			}

			Pair<int[], float[]> feats = lexicon.getFeatureVector(featureMap);

			featureVector = new FeatureVector(feats.getFirst(),
					feats.getSecond());

		}
		return FeatureVector.plus(baseFeats, featureVector);

	}

	private Set<Feature> bigrams(String name, List<String> labels) {

		Counter<String> bigrams = new Counter<String>();
		for (int i = 0; i < labels.size() - 1; i++) {
			bigrams.incrementCount(name + ":" + labels.get(i) + "&"
					+ labels.get(i + 1));
		}

		Set<Feature> features = new HashSet<Feature>();
		for (String s : bigrams.items()) {
			features.add(RealFeature.create(s, (float) bigrams.getCount(s)));
		}
		return features;
	}

	private List<String> getLabelSequenceFromView(ClauseInstance x,
			List<IntPair> internalClauses, Constituent c, String viewName) {
		List<String> labels = new ArrayList<String>();

		IntPair current = null;
		int nextId = 0;

		for (Constituent chunk : x.ta.select(viewName)
				.where(Queries.containedInConstituent(c))
				.orderBy(TextAnnotationUtilities.constituentStartComparator)) {

			// if the chunk is contained in the current internal clause, ignore
			// it.
			if (current != null) {
				if (contains(current, chunk.getSpan())) {
					continue;
				} else {
					// the current is no longer "current". We have to test the
					// next one
					current = null;
				}
			}

			if (current == null && nextId < internalClauses.size()) {
				IntPair cand = internalClauses.get(nextId);

				// if the next clause contains the current chunk, then the next
				// clause is the current clause. Record this and also a label.
				if (contains(cand, chunk.getSpan())) {
					current = cand;
					nextId++;
					labels.add("S");
					continue;
				}
			}

			// Finally, we are outside all internal clauses, so record the label
			// of this chunk
			labels.add(chunk.getLabel());
		}
		return labels;
	}

	private List<IntPair> getInternalClauses(IntPair span,
			Set<IntPair> otherClauses) {
		Set<IntPair> internalClauses = new HashSet<IntPair>(otherClauses);

		internalClauses.remove(span);

		for (IntPair p : otherClauses) {
			if (p.getFirst() < span.getFirst())
				internalClauses.remove(p);
			else if (p.getSecond() > span.getSecond())
				internalClauses.remove(p);
		}

		// remove all internal spans that are contained in other internal span.
		for (IntPair p : otherClauses) {
			if (!internalClauses.contains(p))
				continue;

			boolean found = false;
			for (IntPair p1 : internalClauses) {
				if (p1.equals(p))
					continue;

				// check if p1 contains p
				if (contains(p1, p)) {
					found = true;
					break;
				}
			}

			if (found)
				internalClauses.remove(p);
		}

		return Sorters.sortSet(internalClauses, new Comparator<IntPair>() {

			@Override
			public int compare(IntPair o1, IntPair o2) {
				if (o1.getFirst() < o2.getFirst())
					return -1;
				else
					return 1;
			}
		});

	}

	private boolean contains(IntPair outer, IntPair inner) {
		return outer.getFirst() <= inner.getFirst()
				&& outer.getSecond() >= inner.getSecond();
	}
}
