package edu.illinois.cs.cogcomp.srl.core;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.illinois.cs.cogcomp.core.algorithms.Sorters;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.math.MathUtilities;
import edu.illinois.cs.cogcomp.edison.features.Feature;
import edu.illinois.cs.cogcomp.edison.features.manifest.FeatureManifest;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.indsup.learning.FeatureVector;
import edu.illinois.cs.cogcomp.indsup.learning.WeightVector;
import edu.illinois.cs.cogcomp.infer.ilp.ILPSolverFactory;
import edu.illinois.cs.cogcomp.srl.Constants;
import edu.illinois.cs.cogcomp.srl.SRLProperties;
import edu.illinois.cs.cogcomp.srl.data.FrameData;
import edu.illinois.cs.cogcomp.srl.data.FramesManager;
import edu.illinois.cs.cogcomp.srl.data.LegalArguments;
import edu.illinois.cs.cogcomp.srl.features.FeatureGenerators;
import edu.illinois.cs.cogcomp.srl.features.ProjectedPath;
import edu.illinois.cs.cogcomp.srl.inference.SRLConstraints;
import edu.illinois.cs.cogcomp.srl.inference.SRLILPInference;
import edu.illinois.cs.cogcomp.srl.jlis.SRLMulticlassLabel;
import edu.illinois.cs.cogcomp.srl.jlis.SRLMulticlassInstance;

/**
 * Manages the lexicon, models and feature extractors for a verb or nom SRL.
 * 
 * @author Vivek Srikumar
 * 
 */
public abstract class SRLManager {

	private final static Logger log = LoggerFactory.getLogger(SRLManager.class);

	/**
	 * Indicates the 'null' label
	 */
	public final static String NULL_LABEL = "<null>";

	/**
	 * A flag that indicates that the manager is in training mode, which prompts
	 * extensive caching.
	 */
	public final boolean trainingMode;

	/**
	 * The lexicon. This is shared by all the three models.
	 */

	/**
	 * Use softmax to rescale scores for inference?
	 */
	public final boolean softmax;

	/**
	 * This class works with a parse trees from this parser, which controls both
	 * candidate extraction and feature generation.
	 */
	public final String defaultParser;

	private final HashMap<String, Integer> argToId, senseToId;

	private final Set<String> allArgumentsSet, allSensesSet;

	private final LegalArguments knownLegalArguments;
	private final Map<String, Set<String>> legalArgumentsCache;

	protected final boolean loadFromClassPath;

	private ArgumentIdentifier identifier;

	private final Set<SRLConstraints> constraints = new HashSet<SRLConstraints>();

	private final Map<Models, ModelInfo> modelInfo;

	/**
	 * Run inference without constraints? This is mostly used for debugging.
	 */
	private boolean unconstrainedInference = false;

	public final SRLExampleGenerator exampleGenerator;

	protected SRLManager(boolean trainingMode, boolean loadFromClassPath,
			boolean softmax, String defaultParser) throws Exception {
		this.trainingMode = trainingMode;
		this.loadFromClassPath = loadFromClassPath;
		this.softmax = softmax;
		this.defaultParser = defaultParser;

		initializeFeatureManifest(defaultParser);

		allArgumentsSet = Collections.unmodifiableSet(new TreeSet<String>(
				Arrays.asList(getArgumentLabels())));

		allSensesSet = Collections.unmodifiableSet(new TreeSet<String>(Arrays
				.asList(getSenseLabels())));

		senseToId = getLabelIdMap(getSenseLabels());
		argToId = getLabelIdMap(getArgumentLabels());
		this.knownLegalArguments = new LegalArguments(getVerbNom()
				+ ".legal.arguments");

		this.legalArgumentsCache = new ConcurrentHashMap<String, Set<String>>();

		log.info("{} Arguments: " + Sorters.sortSet(argToId.keySet()),
				argToId.size());
		log.info("{} senses: " + Sorters.sortSet(senseToId.keySet()),
				senseToId.size());

		modelInfo = new HashMap<Models, ModelInfo>();

		initializeModelInfo();

		initializeConstraints();

		exampleGenerator = new SRLExampleGenerator(this);
	}

	/**
	 * Load all the feature extractors
	 * 
	 * @throws Exception
	 */
	private void initializeModelInfo() throws Exception {
		for (Models m : Models.values()) {
			ModelInfo info = new ModelInfo(this, m);
			modelInfo.put(m, info);

		}
	}

	/**
	 * Load all the constraints.
	 * 
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	private void initializeConstraints() throws Exception {
		String file = "constraints/" + getVerbNom() + ".constraints";
		log.info("Adding all constraints specified in {}", file);
		InputStream in;
		if (IOUtils.exists(file)) {
			in = new FileInputStream(new File(file));
		} else {
			List<URL> ls = IOUtils.lsResources(SRLManager.class, file);
			in = ls.get(0).openStream();
		}
		Scanner scanner = new Scanner(in);
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();

			int semiColon = line.indexOf(';');
			if (semiColon >= 0)
				line = line.substring(0, semiColon);

			line = line.trim();
			if (line.length() == 0)
				continue;
			try {
				SRLConstraints constraint = SRLConstraints.valueOf(line);
				log.info("Including constraint {}", constraint);

				this.addConstraint(constraint);
			} catch (Exception e) {
				log.error("Error with constraint {}", line);
				throw e;
			}
		}

		scanner.close();
		in.close();
	}

	protected HashMap<String, Integer> getLabelIdMap(String[] strings) {

		HashMap<String, Integer> label2Id = new HashMap<String, Integer>();
		for (int i = 0; i < strings.length; i++) {
			label2Id.put(strings[i], i);
		}

		return label2Id;
	}

	private void initializeFeatureManifest(String defaultParser) {
		Feature.setUseAscii();
		Feature.setKeepString();

		FeatureManifest.setJWNLConfigFile(SRLProperties.getInstance()
				.getWordNetFile());

		FeatureManifest.setFeatureExtractor("hyphen-argument-feature",
				FeatureGenerators.hyphenTagFeature);

		// These three are from Surdeanu etal.
		FeatureManifest.setTransformer("parse-left-sibling",
				FeatureGenerators.getParseLeftSibling(defaultParser));

		FeatureManifest.setTransformer("parse-right-sibling",
				FeatureGenerators.getParseLeftSibling(defaultParser));

		FeatureManifest.setFeatureExtractor("pp-features",
				FeatureGenerators.ppFeatures(defaultParser));

		// Introduced in Toutanova etal.
		FeatureManifest.setFeatureExtractor("projected-path",
				new ProjectedPath(defaultParser));
	}

	public Set<String> getAllArguments() {
		return allArgumentsSet;
	}

	public Set<String> getAllSenses() {

		return allSensesSet;
	}

	public void removeConstraint(SRLConstraints c) {
		this.constraints.remove(c);
	}

	public void addConstraint(SRLConstraints c) {
		this.constraints.add(c);
	}

	public Set<SRLConstraints> getConstraints() {
		return Collections.unmodifiableSet(this.constraints);
	}

	public int getNumLabels(Models type) {

		if (type == Models.Identifier || type == Models.Predicate)
			return 2;
		else if (type == Models.Classifier)
			return getNumArguments();
		else
			return getNumSenses();
	}

	public int getArgumentId(String label) {

		// XXX: Not sure why I'm doing this. Some misguided sense of continuity
		// to the old data format, I think. I'm sure I will pay hell for this
		// sometime in the future with hours of debugging!
		label = label.replace("Support", "SUP");

		if (argToId.containsKey(label))
			return argToId.get(label);
		else {
			log.error(label + " is not a valid argument. Expecting one of "
					+ this.argToId.keySet() + ", replacing with " + NULL_LABEL);
			return argToId.get(NULL_LABEL);
		}
	}

	public int getSenseId(String label) {

		// XXX: special case for Propbank. Not clear what this label means, but
		// it occurs occasionally in the data and we don't really want to
		// predict this
		if (label.equals("XX"))
			return senseToId.get("01");
		else if (senseToId.containsKey(label))
			return senseToId.get(label);
		else
			throw new NullPointerException(label + " not a valid sense");
	}

	public abstract VerbNom getVerbNom();

	protected abstract String[] getArgumentLabels();

	protected abstract String[] getSenseLabels();

	public abstract Set<String> getCoreArguments();

	public abstract Set<String> getModifierArguments();

	protected abstract int getNumArguments();

	protected abstract int getNumSenses();

	public abstract String getArgument(int id);

	public abstract String getSense(int id);

	public abstract ArgumentCandidateGenerator getArgumentCandidateGenerator();

	public abstract FramesManager getFrameManager();

	public abstract AbstractPredicateDetector getHeuristicPredicateDetector();

	public abstract AbstractPredicateDetector getLearnedPredicateDetector();

	public abstract int getPruneSize(Models model);

	public void setUnconstrainedInference(boolean value) {
		log.info("Setting unconstrainedInference = {}", value);
		this.unconstrainedInference = value;
	}

	public boolean isUnconstrained() {
		return this.unconstrainedInference;
	}

	public ModelInfo getModelInfo(Models model) {
		return modelInfo.get(model);
	}

	public ArgumentIdentifier getArgumentIdentifier() {
		if (this.identifier == null) {
			synchronized (this) {
				if (this.identifier == null) {

					log.info("Loading argument identifier");
					try {
						InputStream in;
						if (this.loadFromClassPath) {
							log.debug("Looking for {} in the classpath",
									getIdentifierScaleFile());
							List<URL> urls = IOUtils.lsResources(
									SRLManager.class, getIdentifierScaleFile());
							if (urls.size() == 0)
								log.error("Argument identifier scale file not found!");
							URL url = urls.get(0);

							in = url.openStream();
						} else {

							log.debug("Looking for {} in the file system",
									getIdentifierScaleFile());
							in = new FileInputStream(new File(
									getIdentifierScaleFile()));
						}

						Pair<Double, Double> pair = readIdentifierScale(in);

						this.identifier = new ArgumentIdentifier(
								pair.getFirst(), pair.getSecond(), this);

						log.info("Finished initializing argument identifier");
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}

		return this.identifier;
	}

	public void writeIdentifierScale(double A, double B) throws IOException {

		String file = getIdentifierScaleFile();

		log.info("Writing identifier scaling info to {}", file);

		BufferedOutputStream stream = new BufferedOutputStream(
				new GZIPOutputStream(new FileOutputStream(file)));

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				stream));

		writer.write("IdentifierScale");
		writer.newLine();

		writer.write("" + A);
		writer.newLine();

		writer.write("" + B);

		writer.close();
	}

	private Pair<Double, Double> readIdentifierScale(InputStream in)
			throws IOException {
		GZIPInputStream zipin = new GZIPInputStream(in);
		BufferedReader reader = new BufferedReader(new InputStreamReader(zipin));

		String line;

		line = reader.readLine().trim();
		if (!line.equals("IdentifierScale")) {
			throw new IOException("Invalid identifier scalefile");
		}

		double A = Double.parseDouble(reader.readLine().trim());
		double B = Double.parseDouble(reader.readLine().trim());

		log.info("Argument identifier scaler (A,B) = ({},{})", A, B);

		zipin.close();

		return new Pair<Double, Double>(A, B);
	}

	private String getFeatureIdentifier(Models type) {
		return getModelInfo(type).featureManifest.getIncludedFeatures()
				.replaceAll("\\s+", "").hashCode()
				+ "";
	}

	public FeatureVector getFeatureVector(SRLMulticlassInstance x, Models m) {
		// some pre-extracting step MUST
		// have cached the feature vector.

		return x.getCachedFeatureVector(m);
	}

	public String getGoldViewName() {
		return getPredictedViewName() + "_GOLD";
	}

	public abstract String getPredictedViewName();

	public String getSRLSystemIdentifier() {
		if (this.getVerbNom() == VerbNom.Verb)
			return Constants.verbSRLSystemIdentifier;
		else
			return Constants.nomSRLSystemIdentifier;
	}

	public String getSRLSystemCuratorName() {
		if (this.getVerbNom() == VerbNom.Verb)
			return Constants.verbSRLSystemCuratorName;
		else
			return Constants.nomSRLSystemCuratorName;
	}

	public String getSRLSystemVersion() {
		return Constants.systemVersion;
	}

	/**
	 * The name of he file that contains the identifer scale information
	 * 
	 * @return
	 */
	public String getIdentifierScaleFile() {
		String identifier = getFeatureIdentifier(Models.Identifier);

		return "models/" + this.getVerbNom() + "." + Models.Identifier.name()
				+ "." + defaultParser + "." + identifier + ".scale";
	}

	/**
	 * The name of the file that contains the lexicon
	 * 
	 * @param m
	 * 
	 * @return
	 */
	public String getLexiconFileName(Models m) {
		return "models/lexicon." + getVerbNom() + "." + m + ".lex";
	}

	/**
	 * The name of the file that contains the model for the given model type
	 * 
	 * @param type
	 * @return
	 */
	public String getModelFileName(Models type) {
		String identifier = getFeatureIdentifier(type);

		return "models/" + this.getVerbNom() + "." + type.name() + "."
				+ defaultParser + "." + identifier + ".lc";
	}

	/**
	 * Checks if the given argId is a legal argument for a predicate. The argId
	 * should be correspond to a valid argument, according to the function
	 * getArgumentId.
	 * 
	 * @param predicate
	 * @param argId
	 * @return
	 */
	public boolean isValidArgument(String predicate, int argId) {
		if (0 <= argId && argId < getNumArguments())
			return this.getLegalArguments(predicate).contains(
					getArgument(argId));
		else {
			log.error("Invalid label ID {} for predicate {}", argId, predicate);
			return false;
		}
	}

	/**
	 * Checks if the given sense id is a valid predicate sense for the given
	 * predicate. The sense id should be valid according to the function
	 * getSenseId.
	 * 
	 * @param predicate
	 * @param senseId
	 * @return
	 */
	public boolean isValidSense(String predicate, int senseId) {
		assert 0 <= senseId && senseId < getNumSenses();

		return this.getLegalSenses(predicate).contains(getSense(senseId));
	}

	/**
	 * Checks if the input label is a valid label Id of the specified model type
	 * for the input x.
	 * 
	 * @param x
	 * @param type
	 * @param label
	 * @return
	 */
	public boolean isValidLabel(SRLMulticlassInstance x, Models type, int label) {
		if (type == Models.Identifier || type == Models.Predicate)
			return label == 0 || label == 1;
		else if (type == Models.Classifier)
			return this.getLegalArguments(x.getPredicateLemma()).contains(
					this.getArgument(label));
		else
			return this.getLegalSenses(x.getPredicateLemma()).contains(
					this.getSense(label));
	}

	/**
	 * Checks if the argument id specified is the special NULL_LABEL.
	 * 
	 * @param argLabelId
	 * @return
	 */
	public boolean isNullLabel(int argLabelId) {
		return NULL_LABEL.equals(this.getArgument(argLabelId));
	}

	/**
	 * Returns the set of legal arguments for the given lemma. This function
	 * uses the frame files to get the list of valid core arguments. All the
	 * modifiers are treated as legal arguments. In addition, all C-args and
	 * R-args of legal core/modifier arguments are also considered legal. For
	 * unknown predicates, all arguments are legal.
	 * 
	 * @param lemma
	 * @return
	 */
	public Set<String> getLegalArguments(String lemma) {
		FramesManager frameMan = getFrameManager();

		if (knownLegalArguments.hasLegalArguments(lemma)) {

			Set<String> set = new HashSet<String>();

			set.addAll(Arrays.asList("AM-ADV", "AM-DIS", "AM-LOC", "AM-MNR",
					"AM-MOD", "AM-NEG", "AM-TMP"));

			set.addAll(knownLegalArguments.getLegalArguments(lemma));

			if (lemma.equals("%"))
				lemma = "perc-sign";

			if (lemma.equals("namedrop"))
				lemma = "name-drop";

			if (frameMan.frameData.containsKey(lemma))
				set.addAll(frameMan.getFrame(lemma).getLegalArguments(lemma));
			else
				log.warn("Unseen lemma {}", lemma);

			return set;
		} else {

			Set<String> knownPredicates = frameMan.getPredicates();
			if (knownPredicates.contains(lemma)) {

				if (legalArgumentsCache.containsKey(lemma))
					return legalArgumentsCache.get(lemma);
				else {

					HashSet<String> set = new HashSet<String>(frameMan
							.getFrame(lemma).getLegalArguments(lemma));

					set.addAll(this.getModifierArguments());

					for (String s : new ArrayList<String>(set)) {
						set.add("C-" + s);
						set.add("R-" + s);
					}

					set.add(NULL_LABEL);

					legalArgumentsCache.put(lemma, set);

					return set;
				}
			} else
				return getAllArguments();
		}
	}

	/**
	 * Get the set of valid senses for this predicate using the frame files. For
	 * unknown predicates, only the sense 01 is allowed.
	 * 
	 * @param predicate
	 * @return
	 */
	public Set<String> getLegalSenses(String predicate) {
		FramesManager frameMan = getFrameManager();

		if (frameMan.getPredicates().contains(predicate)) {
			Set<String> senses = frameMan.getFrame(predicate).getSenses();

			if (senses.size() > 0)
				return senses;
			else {
				log.error("Unknown predicate {}. Allowing only sense 01",
						predicate);
			}
		}
		return new HashSet<String>(Arrays.asList("01"));

	}

	/**
	 * Get valid arguments for each sense of a given lemma from the frame files.
	 * For unknown predicates, only the sense 01 is allowed with all arguments
	 * 
	 * @param lemma
	 * @return
	 */
	public Map<String, Set<String>> getLegalLabelsForSense(String lemma) {
		FramesManager frameMan = getFrameManager();

		Map<String, Set<String>> map = new HashMap<String, Set<String>>();

		if (frameMan.getPredicates().contains(lemma)) {
			FrameData frame = frameMan.getFrame(lemma);
			for (String sense : frame.getSenses()) {
				Set<String> argsForSense = new HashSet<String>(
						frame.getArgsForSense(sense));
				argsForSense.add(NULL_LABEL);

				map.put(sense, argsForSense);
			}
		} else {
			map.put("01", getAllArguments());
		}

		return map;
	}

	/**
	 * Scores instance for the different labels allowed for it
	 * 
	 * @param x
	 * @param type
	 * @return
	 */
	public double[] getScores(SRLMulticlassInstance x, Models type,
			boolean rescoreInvalidLabels) {
		int numLabels = this.getNumLabels(type);
		double[] scores = new double[numLabels];

		WeightVector w;
		try {
			w = this.getModelInfo(type).getWeights();
			assert w != null;
		} catch (Exception e) {
			log.error("Unable to load weight vector for {}", type, e);
			throw new RuntimeException(e);
		}

		for (int label = 0; label < numLabels; label++) {

			if (!this.isValidLabel(x, type, label) && rescoreInvalidLabels) {
				scores[label] = -50;
			} else {

				SRLMulticlassLabel y = new SRLMulticlassLabel(x, label, type,
						this);
				scores[label] = w.dotProduct(y.getFeatureVector());
			}
		}

		if (softmax)
			scores = MathUtilities.softmax(scores);

		return scores;

	}

	public SRLILPInference getInference(ILPSolverFactory solver,
			List<Constituent> predicates) throws Exception {
		return new SRLILPInference(solver, this, predicates, true);
	}

}
