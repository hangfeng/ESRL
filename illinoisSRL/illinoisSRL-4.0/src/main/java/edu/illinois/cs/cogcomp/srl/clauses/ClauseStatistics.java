package edu.illinois.cs.cogcomp.srl.clauses;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.illinois.cs.cogcomp.core.datastructures.IQueryable;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.edison.annotators.ClauseViewGenerator;
import edu.illinois.cs.cogcomp.edison.data.IResetableIterator;
import edu.illinois.cs.cogcomp.edison.features.helpers.WordHelpers;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.Queries;
import edu.illinois.cs.cogcomp.edison.sentences.SpanLabelView;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.POSUtils;
import edu.illinois.cs.cogcomp.srl.caches.SentenceDBHandler;
import edu.illinois.cs.cogcomp.srl.data.Dataset;

public class ClauseStatistics {

	public static void run() {
		Dataset dataset = Dataset.PTBTrainDev;

		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(dataset);

		String viewName = "Clauses_Gold";
		ClauseViewGenerator clauses = new ClauseViewGenerator(
				ViewNames.PARSE_GOLD, viewName);

		Counter<String> counter = new Counter<String>();

		Set<String> startPoses = new HashSet<String>();
		Set<String> endPoses = new HashSet<String>();

		Set<String> endPlusOnePosAfterPreposition = new HashSet<String>();
		Set<String> endPlusOnePosAfterAdjective = new HashSet<String>();

		Set<String> endPlusOnePosAfterVerb = new HashSet<String>();
		Set<String> endPlusOnePosAfterAdverb = new HashSet<String>();

		Set<String> endPlusOnePosAfterNoun = new HashSet<String>();

		Set<String> endPlusOnePoses = new HashSet<String>();

		Set<String> sameSpanChunks = new HashSet<String>();

		Set<String> allPoses = new HashSet<String>();
		Set<String> allChunks = new HashSet<String>();

		while (data.hasNext()) {
			TextAnnotation ta = data.next();
			ta.addView(clauses);
			View view = ta.getView(viewName);

			for (int i = 0; i < ta.size(); i++) {
				allPoses.add(WordHelpers.getPOS(ta, i));
			}

			SpanLabelView chunks = (SpanLabelView) ta
					.getView(ViewNames.SHALLOW_PARSE);

			SpanLabelView ne = (SpanLabelView) ta.getView(ViewNames.NER);

			for (Constituent c : chunks) {
				allChunks.add(c.getLabel());
			}

			for (Constituent c : view) {
				// how often do two clauses start at the same token?
				int count = view.where(Queries.sameStartSpanAs(c)).count() - 1;
				counter.incrementCount("Same start", count);

				// how often do two clauses end at the same token?

				count = view.where(Queries.sameEndSpanAs(c)).count() - 1;
				counter.incrementCount("Same end", count);

				// how many clauses are "contained" within another clause?
				count = 0;
				for (Constituent c1 : view.where(Queries
						.containedInConstituentExclusive(c))) {

					if (view.where(Queries.containedInConstituentExclusive(c1))
							.count() == 0)
						count++;
				}

				int numVerbPhrases = chunks.where(Queries.hasOverlap(c))
						.where(Queries.hasLabel("VP")).count();
				boolean hasVerb = numVerbPhrases > 0;

				for (int i = c.getStartSpan(); i < c.getEndSpan(); i++) {
					String pos = WordHelpers.getPOS(ta, i);
					if (POSUtils.isPOSVerb(pos)) {
						hasVerb = true;
						break;
					}
				}

				if (!hasVerb) {
					counter.incrementCount("No verb");
				}

				// pos information
				String startPOS = WordHelpers.getPOS(ta, c.getStartSpan());
				startPoses.add(startPOS);

				String endPOS = WordHelpers.getPOS(ta, c.getEndSpan() - 1);
				endPoses.add(endPOS);

				// if the clause ends with an preposition, what can follow it
				if (c.getEndSpan() != ta.size()) {
					String next = WordHelpers.getPOS(ta, c.getEndSpan());

					endPlusOnePoses.add(next);

					if (POSUtils.isPOSPreposition(endPOS))
						endPlusOnePosAfterPreposition.add(next);

					// if the clause ends with an adjective, what can follow it
					if (POSUtils.isPOSAdjective(endPOS))
						endPlusOnePosAfterAdjective.add(next);

					// if the clause ends with an verb, what can follow it
					if (POSUtils.isPOSVerb(endPOS))
						endPlusOnePosAfterVerb.add(next);

					// if the clause ends with an adverb, what can follow it
					if (POSUtils.isPOSAdverb(endPOS))
						endPlusOnePosAfterAdverb.add(next);

					// if the clause ends with an noun, what can follow it
					if (POSUtils.isPOSNoun(endPOS)
							|| POSUtils.isPOSPronoun(endPOS))

						endPlusOnePosAfterNoun.add(next);
				}

				// how often does a clause end in the middle of an chunk?
				List<Constituent> chunkCoveringLastToken = chunks
						.getConstituentsCoveringToken(c.getEndSpan() - 1);

				if (chunkCoveringLastToken.size() > 0) {
					Constituent chunk = chunkCoveringLastToken.get(0);

					boolean nextWordCC = false;
					if (c.getEndSpan() != ta.size()) {
						String next = WordHelpers.getPOS(ta, c.getEndSpan());
						nextWordCC = next.equals("CC");
					}

					if (Queries.exclusivelyOverlaps(c).transform(chunk)
							&& !nextWordCC) {
						counter.incrementCount("Ends in middle of chunk");
					}
				}

				// can the starting token break a chunk? Which chunks?

				List<Constituent> chunkCoveringFirstToken = chunks
						.getConstituentsCoveringToken(c.getStartSpan());

				if (chunkCoveringFirstToken.size() > 0) {
					Constituent chunk = chunkCoveringFirstToken.get(0);

					String label = chunk.getLabel();

					String firstToken = ta.getToken(c.getStartSpan())
							.toLowerCase();

					String firstPOS = WordHelpers.getPOS(ta, c.getStartSpan());
					boolean startsWithVP = label.equals("VP");

					String firstChunkToken = ta.getToken(chunk.getStartSpan())
							.toLowerCase();

					String prev = null;
					if (c.getStartSpan() != 0)
						prev = WordHelpers.getPOS(ta, c.getStartSpan() - 1);

					boolean lastTokenInChunk = c.getStartSpan() == chunk
							.getEndSpan() - 1;

					Set<String> validFirstTokens = new HashSet<String>(
							Arrays.asList("that", "which", "those", "who"));

					Constituent overlap = new Constituent("", "", ta,
							c.getStartSpan(), chunk.getEndSpan());

					StringBuilder posSeq = new StringBuilder();
					for (int i = overlap.getStartSpan(); i < overlap
							.getEndSpan(); i++) {
						char pos = WordHelpers.getPOS(ta, i).toLowerCase()
								.charAt(0);
						posSeq.append(pos);
					}

					boolean nounPhrase = posSeq.toString().matches("d*j*n*");

					boolean chunkEndsInNE = ne.where(
							Queries.sameSpanAsConstituent(overlap)).count() > 0;

					boolean validNP = label.equals("NP")
							&& (firstChunkToken.equals("that")
									|| validFirstTokens
											.contains(firstChunkToken)
									|| validFirstTokens.contains(firstToken)
									|| "WP$".equals(prev) || "WP".equals(prev)
									|| "RB".equals(prev) || "RBR".equals(prev)
									|| "WRB".equals(prev) || lastTokenInChunk
									|| nounPhrase || chunkEndsInNE);

					boolean previousTokenCC = "CC".equals(prev);

					boolean validAdvp = label.equals("ADVP")
							&& (prev != null && (prev.contains("RB")
									|| prev.equals("IN") || lastTokenInChunk));

					boolean validAdjp = label.equals("ADJP")
							&& (prev != null && (prev.contains("RB")
									|| prev.equals("IN") || prev.equals("WDT")));

					boolean validSBAR = label.equals("SBAR")
							&& (prev != null && (prev.contains("RB") || prev
									.equals("IN")));

					boolean conjP = label.equals("CONJP");

					if (Queries.exclusivelyOverlaps(c).transform(chunk)
							&& !startsWithVP && !validNP && !previousTokenCC
							&& !validAdvp && !validSBAR && !conjP && !validAdjp) {
						counter.incrementCount("Starts in the middle of chunk");
						counter.incrementCount("Starts in the middle of "
								+ label);

					}
				}

				String lastToken = ta.getToken(c.getEndSpan() - 1)
						.toLowerCase();

				boolean lastTokenThat = lastToken.equals("that")
						|| lastToken.equals("which");

				boolean nextTokenValid = false;
				if (c.getEndSpan() != ta.size()) {
					String next = WordHelpers.getPOS(ta, c.getEndSpan());
					nextTokenValid = POSUtils.isPOSPunctuation(next)
							|| POSUtils.isPOSVerb(next) || next.equals("CC");
				}

				boolean previousTokenValid = false;
				if (c.getStartSpan() != 0) {
					String prevToken = ta.getToken(c.getStartSpan() - 1)
							.toLowerCase();
					previousTokenValid = prevToken.startsWith("do")
							|| prevToken.equals("through");
				}

				if (lastTokenThat && !nextTokenValid && !previousTokenValid) {
					counter.incrementCount("Ends with that/which");
				}

				// phrase information
				IQueryable<Constituent> queryable = chunks.where(Queries
						.sameSpanAsConstituent(c));
				if (queryable.count() > 0) {
					String label = queryable.iterator().next().getLabel();
					counter.incrementCount("Same span as chunk: " + label);
					sameSpanChunks.add(label);
				}

				String firstToken = ta.getToken(c.getStartSpan()).toLowerCase();
				String firstPOS = WordHelpers.getPOS(ta, c.getStartSpan());
				Set<String> allowedPunctionStarts = new HashSet<String>(
						Arrays.asList("``", "(", "{", "--", "`", ",", "''",
								"...", "'"));

				if (POSUtils.isPOSPunctuation(firstPOS)
						&& !allowedPunctionStarts.contains(firstToken)) {
					counter.incrementCount("Starts with punctuation");
					counter.incrementCount("Starts with punctuation "
							+ firstToken);

				}

				counter.incrementCount("total clauses");
			}

			// if there is a chunk called SBAR, then there must be a clause that
			// it starts

			for (Constituent c : chunks.where(Queries.hasLabel("SBAR"))) {
				if (view.where(Queries.sameStartSpanAs(c)).count() == 0) {
					System.out.println(chunks);
					System.out.println(view);

					counter.incrementCount("SBAR does not start a clause");
				}
			}

			int numSBARs = chunks.where(Queries.hasLabel("VP")).count();
			int numVPs = chunks.where(Queries.hasLabel("SBAR")).count();
			int numClauses = view.count();

			if (numSBARs + numVPs > numClauses) {
				counter.incrementCount("Num clauses > SBAR + VP");
			}

			if (numClauses < numVPs) {
				counter.incrementCount("Num clauses < num VPs");
			}

		}

		for (String item : counter.items()) {
			System.out.println(item + "\t" + counter.getCount(item));
		}

		System.out.println("Clauses can not start with "
				+ difference(allPoses, startPoses));
		System.out.println("Clauses can not end with "
				+ difference(allPoses, endPoses));

		System.out.println("Clauses cannot be followed by "
				+ difference(allPoses, endPlusOnePoses));

		System.out
				.println("Clauses can end with preposition only if they are not followed by "
						+ difference(allPoses, endPlusOnePosAfterPreposition));

		System.out
				.println("Clauses can end with adjective only if they are not followed by "
						+ difference(allPoses, endPlusOnePosAfterAdjective));

		System.out
				.println("Clauses can end with adverb only if they are not followed by "
						+ difference(allPoses, endPlusOnePosAfterAdverb));

		System.out
				.println("Clauses can end with noun/pronoun only if they are not followed by "
						+ difference(allPoses, endPlusOnePosAfterNoun));

		System.out
				.println("Clauses can end with verb only if they are not followed by "
						+ difference(allPoses, endPlusOnePosAfterVerb));

		System.out.println("No verbs found in " + counter.getCount("No verb")
				+ " clauses");

		System.out.println("Clauses cannot have the same span as chunks "
				+ difference(allChunks, sameSpanChunks));
	}

	private static Set<String> difference(Set<String> all, Set<String> toRemove) {
		Set<String> set = new HashSet<String>(all);
		set.removeAll(toRemove);
		return set;
	}

}
