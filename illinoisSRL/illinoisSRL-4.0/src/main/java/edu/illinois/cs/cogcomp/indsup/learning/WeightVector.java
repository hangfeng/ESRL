package edu.illinois.cs.cogcomp.indsup.learning;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.indsup.inference.AbstractStructureFinder;
import edu.illinois.cs.cogcomp.indsup.inference.IInstance;
import edu.illinois.cs.cogcomp.indsup.inference.IStructure;
import edu.illinois.cs.cogcomp.indsup.learning.L2Loss.L2LossInstanceWithAlphas;

/**
 * The weight vector.
 * 
 * <p>
 * Update (VS): Now uses the different implementation of the densevector (which
 * is actually 'semi-sparse' to save memory).
 * 
 * @author Ming-Wei Chang, Vivek Srikumar
 * 
 */
@SuppressWarnings("serial")
public class WeightVector extends DenseVector {

	/**
	 * 
	 * @param n
	 *            The size of the weightvector
	 */
	public WeightVector(int n) {
		super(n);
	}

	/**
	 * wv = in * multipler
	 * 
	 * @param in
	 * @param multiplier
	 */
	public WeightVector(double[] in, double multiplier) {
		super();
		for (int i = 0; i < in.length; i++)
			this.setElement(i, (float) (in[i] * multiplier));
	}

	public WeightVector(String fileName) throws Exception {
		super();
		load(fileName);
	}

	public int getWeightVectorLength() {
		return super.getVectorLength();
	}

	/**
	 * wv = old
	 * 
	 * @param old
	 * @param additional_space
	 */
	public WeightVector(WeightVector old, int additional_space) {
		super();

		// The additional space is ignored they are all zeros anyway. Maybe
		// something else can be done...

		for (int k : old.weights.keys()) {
			float[] oldF = old.getBlock(k);
			float[] newF = new float[oldF.length];

			System.arraycopy(oldF, 0, newF, 0, newF.length);
			this.weights.put(k, newF);
		}

		this.maxBlockStart = old.maxBlockStart;

		extendable = old.extendable;
	}

	/**
	 * Please use the addFeatureVector function instead !
	 * 
	 * @param fv
	 * @param alpha
	 */
	@Deprecated
	public synchronized void addToW(FeatureVector fv, double alpha) {
		super.addSparseFeatureVector(fv, alpha);
	}

	public double predictLCLRBinaryScore(IStructure is, IInstance ins) {
		FeatureVector fv = is.getFeatureVector();
		fv.normalize(ins.size());
		fv.slowAddFeature(L2LossInstanceWithAlphas.INDIRECT_GLOBAL_BIAS, 1.0f);

		return dotProduct(fv);
	}

	/**
	 * If you use LCLR or JLIS, ALWAYS use this function to get the prediction
	 * score for binary labeled examples
	 * 
	 * @param ins
	 *            testing example
	 * @param s_finder
	 *            inference solver
	 * @return the score of the prediction (if > 0, predict positive, OW,
	 *         predict negative)
	 * @throws Exception
	 */

	public double predictLCLRBinaryScore(IInstance ins,
			AbstractStructureFinder s_finder) throws Exception {
		IStructure is = s_finder.getBestStructure(this, ins);
		return predictLCLRBinaryScore(is, ins);
	}

	public double getGlobalBiasTerm() {
		return getElement(0);
	}

	/**
	 * should avoid using this function, currently only for liblinear
	 * 
	 * @return
	 */
	public double[] getWeightArray() {
		return super.getInternalArray();
	}

	public void save(String fileName) throws IOException {
		BufferedOutputStream stream = new BufferedOutputStream(
				new GZIPOutputStream(new FileOutputStream(fileName)));

		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
				stream));

		writer.write("WeightVector");
		writer.newLine();

		writer.write("" + this.weights.size());
		writer.newLine();

		// System.out.println("Number of blocks: " + this.weights.size() + " ("
		// + this.weights.keys().length + ")");

		int numNonZero = 0;
		int[] keys = weights.keys();

		for (int keyId = 0; keyId < keys.length; keyId++) {
			int start = keys[keyId];

			// System.out.println("Saving block starting with " + start);
			float[] data = weights.get(start);

			assert data.length == blockSize;

			for (int i = 0; i < data.length; i++) {
				int index = i + start;
				if (data[i] != 0) {
					writer.write(index + ":" + data[i]);
					writer.newLine();
					numNonZero++;
				}
			}
		}

		writer.close();

		System.out.println("Number of non-zero weights: " + numNonZero);

	}

	private void load(String fileName) throws IOException, URISyntaxException {
		InputStream inputStream;

		if (IOUtils.exists(fileName)) {
			inputStream = new FileInputStream(fileName);
		} else {

			List<URL> ls = IOUtils.lsResources(WeightVector.class, fileName);
			inputStream = ls.get(0).openStream();

		}
		GZIPInputStream zipin = new GZIPInputStream(inputStream);

		BufferedReader reader = new BufferedReader(new InputStreamReader(zipin));

		String line;

		line = reader.readLine().trim();
		if (!line.equals("WeightVector")) {
			reader.close();
			throw new IOException("Invalid model file.");
		}

		line = reader.readLine().trim();

		while ((line = reader.readLine()) != null) {

			String[] parts = line.split(":");

			int id = Integer.parseInt(parts[0]);
			float value = Float.parseFloat(parts[1]);
			this.setElement(id, value);
		}

		zipin.close();
	}
}
