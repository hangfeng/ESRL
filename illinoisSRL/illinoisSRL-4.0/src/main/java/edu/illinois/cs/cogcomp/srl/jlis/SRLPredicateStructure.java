package edu.illinois.cs.cogcomp.srl.jlis;

import edu.illinois.cs.cogcomp.indsup.inference.IStructure;
import edu.illinois.cs.cogcomp.indsup.learning.FeatureVector;
import edu.illinois.cs.cogcomp.srl.core.Models;
import edu.illinois.cs.cogcomp.srl.core.SRLManager;

public class SRLPredicateStructure implements IStructure {

	public final SRLPredicateInstance x;
	private final int[] argLabels;
	private final int sense;
	private final SRLManager manager;

	public SRLPredicateStructure(SRLPredicateInstance x, int[] argLabels,
			int sense, SRLManager manager) {
		this.x = x;
		this.argLabels = argLabels;
		this.sense = sense;
		this.manager = manager;

	}

	@Override
	public FeatureVector getFeatureVector() {

		throw new RuntimeException("Not yet implemented!");
	}

	public int getArgLabel(int candidateId) {
		return argLabels[candidateId];
	}

	public int getSense() {
		return sense;
	}

	public SRLMulticlassLabel getClassifierMulticlassLabel(int candidateId) {
		return new SRLMulticlassLabel(x.getCandidateInstances()
				.get(candidateId), argLabels[candidateId], Models.Classifier,
				manager);
	}

	public SRLMulticlassLabel getIdentifierMulticlassLabel(int candidateId) {

		boolean isNull = manager.isNullLabel(argLabels[candidateId]);

		int c = isNull ? 0 : 1;

		return new SRLMulticlassLabel(x.getCandidateInstances()
				.get(candidateId), c, Models.Identifier, manager);
	}

	public SRLMulticlassLabel getSenseMulticlassLabel() {
		return new SRLMulticlassLabel(x.getSenseInstance(), sense,
				Models.Sense, manager);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();

		for (int candidateId = 0; candidateId < this.argLabels.length; candidateId++) {
			sb.append("Candidate " + candidateId + ": "
					+ manager.getArgument(argLabels[candidateId]) + "\n");
		}

		sb.append("Sense: " + manager.getSense(sense) + "\n");

		return sb.toString();
	}

}
