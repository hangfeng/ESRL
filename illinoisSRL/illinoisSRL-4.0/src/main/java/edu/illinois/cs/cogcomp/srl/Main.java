package edu.illinois.cs.cogcomp.srl;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.ConfigurationException;

import edu.illinois.cs.cogcomp.core.algorithms.Sorters;
import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandDescription;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandIgnore;
import edu.illinois.cs.cogcomp.core.utilities.commands.InteractiveShell;
import edu.illinois.cs.cogcomp.edison.annotators.ClauseViewGenerator;
import edu.illinois.cs.cogcomp.edison.annotators.HeadFinderDependencyViewGenerator;
import edu.illinois.cs.cogcomp.edison.data.IResetableIterator;
import edu.illinois.cs.cogcomp.edison.data.corpora.PennTreebankReader;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.View;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.srl.caches.SentenceDBHandler;
import edu.illinois.cs.cogcomp.srl.clauses.ClauseCandidateHeuristic;
import edu.illinois.cs.cogcomp.srl.clauses.ClauseExperiment;
import edu.illinois.cs.cogcomp.srl.clauses.ClauseManager;
import edu.illinois.cs.cogcomp.srl.core.Models;
import edu.illinois.cs.cogcomp.srl.data.Dataset;
import edu.illinois.cs.cogcomp.srl.data.LegalArguments;
import edu.illinois.cs.cogcomp.srl.data.NombankReader;
import edu.illinois.cs.cogcomp.srl.data.PropbankReader;
import edu.illinois.cs.cogcomp.srl.experiment.TextPreProcessor;
import edu.illinois.cs.cogcomp.srl.inference.constraints.CrossArgumentRetainedModifiers;
import edu.illinois.cs.cogcomp.srl.nom.NomSRLManager;
import edu.illinois.cs.cogcomp.srl.verb.VerbSRLManager;
import edu.illinois.cs.cogcomp.srl.core.SRLManager;
import edu.illinois.cs.cogcomp.srl.core.VerbNom;

public class Main {

	private static boolean MERGE_CONTIGUOUS_C_ARGS = true;

	@CommandIgnore
	public static void main(String[] arguments) throws ConfigurationException {

		InteractiveShell<Main> shell = new InteractiveShell<Main>(Main.class);

		if (arguments.length == 0) {
			System.err.println("Usage: <config-file> command");
			System.err.println("Required parameter config-file missing.");
			shell.showDocumentation();
		} else if (arguments.length == 1) {
			System.err.println("Usage: <config-file> command");
			shell.showDocumentation();
		} else {
			long start_time = System.currentTimeMillis();
			try {
				String configFile = arguments[0];
				SRLProperties.initialize(configFile);

				String[] args = new String[arguments.length - 1];
				System.arraycopy(arguments, 1, args, 0, args.length);
				shell.runCommand(args);

				long runTime = (System.currentTimeMillis() - start_time) / 1000;
				System.out.println("This experiment took " + runTime + " secs");

			} catch (AssertionError e) {
				e.printStackTrace();
				System.exit(-1);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void cacheCurator(String sections_) throws Exception {
		SRLProperties instance = SRLProperties.getInstance();

		String treebankHome = instance.getPennTreebankHome();

		PennTreebankReader reader;
		if (sections_.equals("all")) {
			reader = new PennTreebankReader(treebankHome);
		} else {
			String[] sections = sections_.split(",");

			reader = new PennTreebankReader(treebankHome, sections);
		}

		int filterCount = 0;
		int count = 0;
		int done = 0;

		String prevSection = null;

		Counter<String> counts = new Counter<String>();

		for (TextAnnotation ta : reader) {

			String id = ta.getId();
			String section = id.split("/")[1];

			if (!section.equals(prevSection)) {
				System.out.println("Section: " + section);

				if (prevSection != null) {
					System.out.println("\t" + prevSection + ": "
							+ counts.getCount(prevSection));
				}

				prevSection = section;
			}

			counts.incrementCount(section);

			if (SentenceDBHandler.instance.contains(ta)) {
				done++;

				if (done % 500 == 0) {
					System.out.println(done
							+ " sentences had already been done");
				}

				continue;
			}

			if (!filterTextAnnotation(ta)) {
				filterCount++;
				System.out.println("Filtered: " + ta.getTokenizedText());
				System.out.println("Filter count = " + filterCount);
				continue;
			}

			try {
				TextPreProcessor.preProcessText(ta, true);
			} catch (Exception ex) {

				System.out.println(ta);
				throw ex;
			}
			count++;
			if (count % 100 == 0) {
				System.out.println(count + " sentences cached, " + done
						+ " sentences previously done, " + filterCount
						+ " sentences filtered");
			}

			SentenceDBHandler.instance.addTextAnnotation(Dataset.PTBAll, ta);
		}

		for (String section : Sorters.sortSet(counts.items())) {
			System.out.println("| " + section + " | "
					+ (int) (counts.getCount(section)));
		}
	}

	public static void printSentenceCacheSummary() throws Exception {
		SentenceDBHandler.instance.printDatabaseStatistics();
	}

	@CommandIgnore
	public static boolean filterTextAnnotation(TextAnnotation ta) {
		if (ta.getTokenizedText().trim().equals("@"))
			return false;

		// this sentence from section 0 crashes the stanford parser
		if (ta.getTokenizedText()
				.startsWith(
						"The following were barred or , where noted , suspended and consented to findings without admitting or denying wrongdoing : Edward L. Cole "))
			return false;

		// this sentence from section 24 crashes the berkeley parser
		if (ta.getTokenizedText()
				.startsWith(
						"@ Charles H. Tenney II , chairman of Unitil Corp. , purchased 34,602 shares "))
			return false;

		return true;
	}

	public static void updateViews() throws Exception {

		IResetableIterator<TextAnnotation> dataset = SentenceDBHandler.instance
				.getDataset(Dataset.PTBAll);

		Counter<String> addedViews = new Counter<String>();

		int count = 0;
		while (dataset.hasNext()) {
			TextAnnotation ta = dataset.next();

			Set<String> views = new HashSet<String>(ta.getAvailableViews());

			TextPreProcessor.preProcessText(ta, false);

			Set<String> newViews = new HashSet<String>(ta.getAvailableViews());
			newViews.removeAll(views);

			if (newViews.size() > 0) {
				SentenceDBHandler.instance.updateTextAnnotation(ta);

				for (String s : newViews)
					addedViews.incrementCount(s);
			}
			count++;
			if (count % 1000 == 0)
				System.out.println(count + " sentences done");
		}

		System.out.println("New views: ");
		for (String s : addedViews.items()) {
			System.out.println(s + "\t" + addedViews.getCount(s));
		}

	}

	@CommandDescription(description = "Prints a summary of the sentence"
			+ " cache", usage = "summarizeSentenceCache")
	public static void summarizeSentenceCache() throws Exception {
		SentenceDBHandler.instance.printDatabaseStatistics();

		for (Dataset d : Dataset.values()) {
			SentenceDBHandler.instance.printDatasetStatistics(d);
		}

	}

	public static void initializeDatasets() throws Exception {
		SRLProperties instance = SRLProperties.getInstance();

		String treebankHome = instance.getPennTreebankHome();

		PennTreebankReader reader = new PennTreebankReader(treebankHome);

		Map<Dataset, Set<String>> d2s = new HashMap<Dataset, Set<String>>();
		for (Dataset d : Dataset.values()) {
			d2s.put(d, new HashSet<String>(Arrays.asList(d.getSections())));
		}

		System.out.println("Initializing datasets");
		SentenceDBHandler.instance.initializeDatasets(instance
				.getSentenceDBFile());

		System.out.println("Created all datasets");

		int c = 0;
		Counter<Dataset> counter = new Counter<Dataset>();
		while (reader.hasNext()) {
			TextAnnotation ta = reader.next();

			String id = ta.getId();
			String section = id.split("/")[1];

			for (Dataset d : d2s.keySet()) {
				if (d2s.get(d).contains(section)) {
					SentenceDBHandler.instance.addTextAnnotation(d, ta);
					counter.incrementCount(d);
				}
			}
			c++;
			if (c % 1000 == 0)
				System.out.println(c + " sentences done");
		}

		for (Dataset d : counter.items()) {
			System.out.println(d + ": " + counter.getCount(d));
		}

	}

	public static void addGoldView(String verbNom_, String section)
			throws Exception {
		SRLProperties instance = SRLProperties.getInstance();
		String treebankHome = instance.getPennTreebankHome();

		VerbNom verbNom = VerbNom.valueOf(verbNom_);

		String[] sections;
		if (section.equals("all")) {
			sections = instance.getAllSections();
		} else
			sections = new String[] { section };
		SRLManager manager = getManager(verbNom, true, false, false,
				ViewNames.PARSE_CHARNIAK);

		Iterator<TextAnnotation> data;

		if (verbNom == VerbNom.Verb) {
			String propbankHome = instance.getPropbankHome();
			data = new PropbankReader(treebankHome, propbankHome, sections,
					manager.getGoldViewName(), MERGE_CONTIGUOUS_C_ARGS);
		} else {
			String nombankHome = instance.getNombankHome();
			data = new NombankReader(treebankHome, nombankHome, sections,
					manager.getGoldViewName(), MERGE_CONTIGUOUS_C_ARGS);
		}

		int count = 0;
		while (data.hasNext()) {
			TextAnnotation ta = data.next();
			if (ta.hasView(manager.getGoldViewName())) {
				SentenceDBHandler.instance.updateTextAnnotation(
						ta.getTokenizedText(), manager.getGoldViewName(),
						ta.getView(manager.getGoldViewName()), true);
			}

			count++;
			if (count % 1000 == 0)
				System.out.println(count + " sentences done");
		}
	}

	public static void testGoldReader(String verbNom_) throws Exception {

		SRLProperties instance = SRLProperties.getInstance();
		String treebankHome = instance.getPennTreebankHome();

		VerbNom verbNom = VerbNom.valueOf(verbNom_);

		String[] sections = instance.getAllTrainSections();
		SRLManager manager = getManager(verbNom, true, false, false,
				ViewNames.PARSE_CHARNIAK);

		Iterator<TextAnnotation> data;

		if (verbNom == VerbNom.Verb) {
			String propbankHome = instance.getPropbankHome();
			data = new PropbankReader(treebankHome, propbankHome, sections,
					manager.getGoldViewName(), MERGE_CONTIGUOUS_C_ARGS);
		} else {
			String nombankHome = instance.getNombankHome();
			data = new NombankReader(treebankHome, nombankHome, sections,
					manager.getGoldViewName(), MERGE_CONTIGUOUS_C_ARGS);
		}

		while (data.hasNext()) {
			TextAnnotation ta = data.next();

			if (!ta.getTokenizedText().startsWith("The study , by Susan"))
				continue;

			if (ta.hasView(manager.getGoldViewName())) {

				System.out.println(ta.getId() + ": " + ta.getTokenizedText());
				View view = ta.getView(manager.getGoldViewName());
				System.out.println(view);
				System.console().readLine();
			}
		}
	}

	@CommandIgnore
	public static SRLManager getManager(VerbNom verbNom, boolean trainingMode,
			boolean loadFromClassPath, boolean softmax, String defaultParser)
			throws Exception {

		assert defaultParser.equals("Charniak")
				|| defaultParser.equals("Berkeley");

		String viewName = Parser.valueOf(defaultParser).getViewName();

		if (verbNom == VerbNom.Verb)
			return new VerbSRLManager(trainingMode, loadFromClassPath, softmax,
					viewName);
		else
			return new NomSRLManager(trainingMode, loadFromClassPath, softmax,
					viewName);
	}

	public static enum Parser {
		Charniak {
			public String getViewName() {
				return ViewNames.PARSE_CHARNIAK;
			}
		},
		Berkeley {
			public String getViewName() {
				return ViewNames.PARSE_BERKELEY;
			}
		};

		public abstract String getViewName();
	}

	@CommandDescription(description = "Pre-extracts and prunes features for the specified model.",

	usage = "preExtract [Verb | Nom] [Charniak | Berkeley] [Predicate | Identifier | Classifier | Sense]")
	public static void preExtract(String verbNom_, String defaultParser,
			String model) throws Exception {
		SRLExperiments expt = new SRLExperiments(verbNom_, defaultParser);
		expt.preExtract(model);
	}

	public static void train(String verbNom, String defaultParser, String model)
			throws Exception {
		SRLExperiments expt = new SRLExperiments(verbNom, defaultParser);
		expt.train(model);
	}

	public static void tuneIdentifier(String verbNom, String defaultParser)
			throws Exception {
		SRLExperiments expt = new SRLExperiments(verbNom, defaultParser);
		expt.tuneIdentifier();
	}

	@CommandDescription(usage = "testComponent [Verb | Nom] [Charniak | Berkeley] [Predicate | Identifier | Classifier | Sense] <debug-info>")
	public static void testComponent(String verbNom, String defaultParser,
			String model, String debug) throws Exception {
		SRLExperiments expt = new SRLExperiments(verbNom, defaultParser);
		expt.testComponent(model, Dataset.PTB23, debug);
	}

	public static void evaluate(String verbNom, String defaultParser,
			String inferenceType, String softmax) throws Exception {

		assert inferenceType.equals("ilp")
				|| inferenceType.equals("no-overlap")
				|| inferenceType.equals("lag") || inferenceType.equals("beam");

		SRLExperiments expt = new SRLExperiments(verbNom, defaultParser,
				Boolean.parseBoolean(softmax));
		expt.evaluate(Dataset.PTB23, inferenceType);
	}

	public static void compareInference(String verbNom, String defaultParser)
			throws Exception {
		SRLExperiments expt = new SRLExperiments(verbNom, defaultParser);
		expt.compareInference(Dataset.PTB23);
	}

	public static void verify() throws Exception {
		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(Dataset.PTBTrainDev);

		while (data.hasNext()) {

			TextAnnotation ta = data.next();
			System.out.println(ta);

			ta.addView(new HeadFinderDependencyViewGenerator(
					ViewNames.PARSE_CHARNIAK));
		}
	}

	public static void testClauseHeuristic() throws Exception {
		ClauseManager manager = new ClauseManager(false);

		IResetableIterator<TextAnnotation> dataset = SentenceDBHandler.instance
				.getDataset(Dataset.PTB23);

		int count = 0;
		EvaluationRecord eval = new EvaluationRecord();
		while (dataset.hasNext()) {
			TextAnnotation ta = dataset.next();

			View clauseGold = manager.getClauseView(ta, ViewNames.PARSE_GOLD,
					"Clauses_Gold");
			Set<IntPair> set = new HashSet<IntPair>();

			for (Constituent c : clauseGold) {
				IntPair span = c.getSpan();
				set.add(span);

			}

			Set<IntPair> candidates = ClauseCandidateHeuristic
					.getClauseCandidates(ta);
			for (IntPair span : candidates) {

				eval.incrementPredicted();

				boolean gold = set.contains(span);
				if (gold) {
					eval.incrementGold();
					eval.incrementCorrect();
					set.remove(span);
				} else {
					if (count > 1000 && count < 1010) {
						Constituent c = new Constituent("", "", ta,
								span.getFirst(), span.getSecond());

						System.out.println("Extra: " + c);
					}

					count++;

				}
			}

			eval.incrementGold(set.size());

			for (IntPair span : set) {
				Constituent c = new Constituent("", "", ta, span.getFirst(),
						span.getSecond());

				// System.out.println(ta);
				// System.out.println(c);
			}
		}

		System.out.println(eval.toString());

	}

	public static void clauseExperiment() throws Exception {

		double c = ClauseExperiment.cv(Dataset.PTB0204);

		ClauseExperiment.train(Dataset.PTBTrainDev, c, 20);

		ClauseExperiment.test(Dataset.PTB23);
	}

	public static void clauseExperimentSmall() throws Exception {
		ClauseExperiment.train(Dataset.PTB0204, 0.1, 1);

		ClauseExperiment.test(Dataset.PTB23);
	}

	public static void testClauses() throws Exception {
		ClauseExperiment.test(Dataset.PTB23);

	}

	public static void testClausesCharniak() throws Exception {
		ClauseManager manager = new ClauseManager(false);

		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(Dataset.PTB23);

		EvaluationRecord record = new EvaluationRecord();
		while (data.hasNext()) {
			TextAnnotation ta = data.next();

			ta.addView(ClauseViewGenerator.CHARNIAK);

			View clausesCharniak = ta.getView(ViewNames.CLAUSES_CHARNIAK);

			View view = manager.getClauseView(ta, ViewNames.PARSE_GOLD,
					ViewNames.CLAUSES);

			Set<IntPair> gold = new HashSet<IntPair>();
			for (Constituent cc : view) {
				gold.add(cc.getSpan());
			}

			Set<IntPair> pred = new HashSet<IntPair>();
			for (Constituent cc : clausesCharniak) {
				pred.add(cc.getSpan());
			}

			for (IntPair span : gold) {
				record.incrementGold();
				if (pred.contains(span)) {
					record.incrementCorrect();
					record.incrementPredicted();
					pred.remove(span);
				}
			}

			record.incrementPredicted(pred.size());

		}

		System.out.println(record);
	}

	public static void testMemoryRequirement(String verbNom_,
			String defaultParser) throws Exception {
		VerbNom verbNom = VerbNom.valueOf(verbNom_);

		SRLManager manager = Main.getManager(verbNom, false, false, false,
				defaultParser);

		manager.getModelInfo(Models.Predicate).loadWeightVector();
		manager.getModelInfo(Models.Sense).loadWeightVector();
		manager.getModelInfo(Models.Identifier).loadWeightVector();
		manager.getModelInfo(Models.Classifier).loadWeightVector();

		manager.getModelInfo(Models.Identifier).getLexicon();
		manager.getModelInfo(Models.Predicate).getLexicon();
		manager.getModelInfo(Models.Sense).getLexicon();
		manager.getModelInfo(Models.Classifier).getLexicon();

		System.out.println("Loaded all models. Press enter to quit");
		System.console().readLine();

	}

	public static void testInference(String verbNom_, String defaultParser)
			throws Exception {
		SRLExperiments exp = new SRLExperiments(verbNom_, defaultParser);

		exp.testInference();
	}

	@CommandDescription(description = "Use this to generate a legal arguments file.",

	usage = "printLegalArgs [Verb | Nom] <output-file>")
	public static void printLegalArgs(String verbNom_, String outputFile)
			throws FileNotFoundException {

		VerbNom verbNom = VerbNom.valueOf(verbNom_);

		String gold = null;
		if (verbNom == VerbNom.Verb)
			gold = "SRL_GOLD";
		else
			gold = "NOM_GOLD";

		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(Dataset.PTBTrainDev);

		LegalArguments.generateLegalArgumentsFile(data, gold, verbNom);
	}

	public static void printValidCrossPredicateModifierLabels(String verbNom_,
			String outputDir) throws FileNotFoundException {

		VerbNom verbNom = VerbNom.valueOf(verbNom_);

		String gold = null;
		if (verbNom == VerbNom.Verb)
			gold = "SRL_GOLD";
		else
			gold = "NOM_GOLD";

		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(Dataset.PTBTrainDev);

		CrossArgumentRetainedModifiers.generateLegalArgumentsFile(data, gold,
				verbNom, outputDir);
	}

}
