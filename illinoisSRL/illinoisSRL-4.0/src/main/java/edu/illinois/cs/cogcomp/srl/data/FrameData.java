package edu.illinois.cs.cogcomp.srl.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author Vivek Srikumar
 * 
 */
public class FrameData {
	private String lemma;

	private String frameFile;

	private static class SenseFrameData {
		Map<String, ArgumentData> argDescription = new HashMap<String, ArgumentData>();
		String verbClass = "UNKNOWN";
		List<Example> examples = new ArrayList<Example>();;
	}

	private static class ArgumentData {
		String description;
		Set<String> vnTheta = new HashSet<String>();
	}

	private static class Example {
		String text;
		String name;
		Map<String, String> argDescriptions = new HashMap<String, String>();
		Map<String, String> argExamples = new HashMap<String, String>();
	}

	private Map<String, SenseFrameData> senseFrameData;

	public FrameData(String lemma, String file) {
		this.lemma = lemma;
		frameFile = file;
		senseFrameData = new HashMap<String, SenseFrameData>();

	}

	public int getNumExamples(String sense) {
		return this.senseFrameData.get(sense).examples.size();
	}

	public String getExampleText(String sense, int id) {
		return this.senseFrameData.get(sense).examples.get(id).text;
	}

	public String getExampleName(String sense, int id) {
		return this.senseFrameData.get(sense).examples.get(id).name;
	}

	public Set<String> getExampleArgsWithDescription(String sense, int id) {
		return this.senseFrameData.get(sense).examples.get(id).argDescriptions
				.keySet();
	}

	public String getExampleArgDescription(String sense, int id, String arg) {
		return this.senseFrameData.get(sense).examples.get(id).argDescriptions
				.get(arg);

	}

	public String getExampleArgSample(String sense, int id, String arg) {
		return this.senseFrameData.get(sense).examples.get(id).argExamples
				.get(arg);

	}

	public void addExample(String sense, String name, String text,
			Map<String, String> argDescriptions, Map<String, String> argExamples) {
		Example ex = new Example();
		ex.name = name;
		ex.text = text;
		ex.argDescriptions = argDescriptions;
		ex.argExamples = argExamples;
		this.senseFrameData.get(sense).examples.add(ex);
	}

	public String getLemma() {
		return lemma;
	}

	public String getFrameFile() {
		return frameFile;
	}

	public void addSense(String sense, String verbClass) {

		this.senseFrameData.put(sense, new SenseFrameData());
		this.senseFrameData.get(sense).verbClass = verbClass;
	}

	public Set<String> getSenses() {
		return this.senseFrameData.keySet();
	}

	public void addArgument(String sense, String arg) {
		assert this.senseFrameData.containsKey(sense);

		senseFrameData.get(sense).argDescription.put(arg, new ArgumentData());
	}

	public Set<String> getArgsForSense(String sense) {

		assert this.senseFrameData.containsKey(sense) : sense
				+ " missing for predicate lemma " + this.lemma;
		return this.senseFrameData.get(sense).argDescription.keySet();
	}

	public void addArgumentDescription(String sense, String arg,
			String description) {
		assert this.senseFrameData.containsKey(sense);
		assert this.senseFrameData.get(sense).argDescription.containsKey(arg);

		senseFrameData.get(sense).argDescription.get(arg).description = description;
	}

	public void addArgumentVNTheta(String sense, String arg, String vnTheta) {
		assert this.senseFrameData.containsKey(sense);
		assert this.senseFrameData.get(sense).argDescription.containsKey(arg);

		senseFrameData.get(sense).argDescription.get(arg).vnTheta.add(vnTheta);

	}

	public Set<String> getVnThetaSet(String sense, String arg) {

		assert senseFrameData.containsKey(sense) : sense + " missing";
		assert senseFrameData.get(sense).argDescription.containsKey(arg) : "Argument "
				+ arg + " missing for " + sense;

		return senseFrameData.get(sense).argDescription.get(arg).vnTheta;
	}

	public String getDescription(String sense, String arg) {
		assert this.senseFrameData.containsKey(sense);

		assert this.senseFrameData.get(sense).argDescription.containsKey(arg) : "Arg: "
				+ arg + " not found for " + sense + "!";

		return this.senseFrameData.get(sense).argDescription.get(arg).description;
	}

	public String getVerbClass(String sense) {

		assert this.senseFrameData.containsKey(sense) : sense
				+ " not found for lemma " + this.lemma + "! "
				+ this.senseFrameData.keySet();
		return this.senseFrameData.get(sense).verbClass;
	}

	public Set<String> getDescriptionFeatures(String sense, String arg) {
		String description = getDescription(sense, arg);

		if (description.contains("EXT"))
			description = "Extent";

		description = description.toLowerCase();

		description = description.replaceAll("\\([^\\)]+\\)", "");

		StringBuffer sb = new StringBuffer();

		String[] split = description.split(",");
		Arrays.sort(split);

		Set<String> out = new HashSet<String>();

		for (String s : split) {
			s = s.trim();
			if (s.matches("arg[0-6]"))
				continue;
			if (s.startsWith("if"))
				continue;

			if (s.contains(" of ")) {
				String start = s.substring(0, s.indexOf(" of "));
				String trail = s.substring(s.indexOf(" of ") + 4);

				s = start + "***";
			}

			out.add(s);

			sb.append(s + ", ");
		}

		if (sb.length() == 0)
			sb.append(description + ", ");

		description = sb.toString();

		out.add(description);
		return out;
	}

	public Set<String> getLegalArguments(String lemma) {
		Set<String> l = new HashSet<String>();
		for (String s : this.getSenses())
			l.addAll(this.getArgsForSense(s));
		return l;
	}
}
