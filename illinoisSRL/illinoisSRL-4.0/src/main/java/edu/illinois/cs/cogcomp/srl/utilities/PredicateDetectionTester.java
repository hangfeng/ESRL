package edu.illinois.cs.cogcomp.srl.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import edu.illinois.cs.cogcomp.core.datastructures.IntPair;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.cogcomp.core.utilities.Table;
import edu.illinois.cs.cogcomp.edison.data.CoNLLColumnFormatReader;
import edu.illinois.cs.cogcomp.edison.data.IResetableIterator;
import edu.illinois.cs.cogcomp.edison.data.srl.NomLexEntry;
import edu.illinois.cs.cogcomp.edison.data.srl.NomLexEntry.NomLexClasses;
import edu.illinois.cs.cogcomp.edison.data.srl.NomLexReader;
import edu.illinois.cs.cogcomp.edison.sentences.Constituent;
import edu.illinois.cs.cogcomp.edison.sentences.TextAnnotation;
import edu.illinois.cs.cogcomp.edison.sentences.ViewNames;
import edu.illinois.cs.cogcomp.edison.utilities.EdisonException;
import edu.illinois.cs.cogcomp.srl.caches.SentenceDBHandler;
import edu.illinois.cs.cogcomp.srl.data.Dataset;
import edu.illinois.cs.cogcomp.srl.jlis.SRLPredicateInstance;
import edu.illinois.cs.cogcomp.srl.core.AbstractPredicateDetector;
import edu.illinois.cs.cogcomp.srl.core.SRLManager;
import edu.illinois.cs.cogcomp.srl.core.VerbNom;
import edu.illinois.cs.cogcomp.srl.jlis.SRLMulticlassInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLSentenceInstance;
import edu.illinois.cs.cogcomp.srl.jlis.SRLSentenceStructure;

/**
 * Tests the predicate detector
 * 
 * @author Vivek Srikumar
 * 
 */
public class PredicateDetectionTester {

	private SRLManager manager;
	private VerbNom verbNom;
	private AbstractPredicateDetector detector;
	private boolean extra = false;
	private boolean missed = false;
	private boolean debug = false;

	private Counter<String> extraC;
	private Counter<String> missedC;
	private EvaluationRecord eval;

	private Map<NomLexClasses, EvaluationRecord> byType;
	private NomLexClasses nomLexClass;

	public PredicateDetectionTester(SRLManager manager, VerbNom verbNom,
			AbstractPredicateDetector detector) {
		this.manager = manager;
		this.verbNom = verbNom;
		this.detector = detector;

		if (verbNom == VerbNom.Nom) {
			byType = new HashMap<NomLexClasses, EvaluationRecord>();
			for (NomLexClasses c : NomLexClasses.values()) {
				byType.put(c, new EvaluationRecord());
			}
		}
	}

	public void debugExtra() {
		extra = true;
		debug = true;
	}

	public void debugMissed() {
		missed = true;
		debug = true;
	}

	public void setNomClassFiter(NomLexClasses cls) {
		this.nomLexClass = cls;
	}

	public void evaluate(Dataset dataset) throws Exception {
		IResetableIterator<TextAnnotation> data = SentenceDBHandler.instance
				.getDataset(dataset);

		extraC = new Counter<String>();
		missedC = new Counter<String>();

		eval = new EvaluationRecord();

		while (data.hasNext()) {
			TextAnnotation ta = data.next();

			Map<IntPair, String> map = getGoldMap(ta);

			eval.incrementGold(map.size());

			List<Constituent> cc = detector.getPredicates(ta);

			eval.incrementPredicted(cc.size());

			if (verbNom == VerbNom.Nom) {
				for (String lemma : map.values()) {
					List<NomLexClasses> goldNomLexClasses = getNomLexClass(lemma);

					for (NomLexClasses c : goldNomLexClasses) {
						byType.get(c).incrementGold();
					}
				}
			}

			// if (debug) {
			// List<Constituent> h = manager.getHeuristicPredicateDetector()
			// .getPredicates(ta);
			//
			// detector.debug = true;
			// for (Constituent c : h) {
			// System.out.println("Candidate: " + c);
			// System.out.println(detector.getLemma(ta, c.getStartSpan()));
			// }
			//
			// detector.debug = false;
			//
			// System.out.println("Press enter to continue");
			// System.console().readLine();
			//
			// }

			for (Constituent c : cc) {
				String lemma = c
						.getAttribute(CoNLLColumnFormatReader.LemmaIdentifier);

				// don't evaluate on the verb 'be'
				if (verbNom == VerbNom.Verb && lemma.equals("be"))
					continue;

				List<NomLexClasses> predictedNomLexClasses = null;

				if (verbNom == VerbNom.Nom) {
					predictedNomLexClasses = getNomLexClass(lemma);

					for (NomLexClasses cl : predictedNomLexClasses) {
						byType.get(cl).incrementPredicted();
					}

				}

				if (map.containsKey(c.getSpan())) {
					map.remove(c.getSpan());
					eval.incrementCorrect();

					if (verbNom == VerbNom.Nom) {
						for (NomLexClasses cl : predictedNomLexClasses) {
							byType.get(cl).incrementCorrect();
						}

					}

				} else {
					extraC.incrementCount(lemma);
					if (extra) {
						if (verbNom == VerbNom.Nom) {
							boolean filterMatch = false;
							for (NomLexClasses cl : predictedNomLexClasses) {
								if (nomLexClass != null && cl == nomLexClass)
									filterMatch = true;
							}

							if (nomLexClass == null || filterMatch) {
								System.out.println("Extra predicate: " + c);
								if (nomLexClass != null)
									System.out.println("NomLexClass = "
											+ nomLexClass);

								System.out.println("Predicted NomLexClasses: "
										+ predictedNomLexClasses);

							}
						}
						debug(ta, c.getStartSpan());
					}
				}
			}

			for (IntPair ip : map.keySet()) {

				String lemma = map.get(ip);
				missedC.incrementCount(lemma);
				if (missed) {
					if (verbNom == VerbNom.Nom) {
						boolean filterMatch = false;
						for (NomLexClasses cl : getNomLexClass(lemma)) {
							if (nomLexClass != null && cl == nomLexClass)
								filterMatch = true;
						}
						if (nomLexClass == null || filterMatch) {
							System.out.println("Missed predicate: " + lemma);
							if (nomLexClass != null)
								System.out.println("NomLexClass = "
										+ nomLexClass);

						}
					}
					debug(ta, ip.getFirst());

				}
			}
		}
	}

	private List<NomLexClasses> getNomLexClass(String lemma)
			throws EdisonException {
		List<NomLexEntry> entries = NomLexReader.getInstance().getNomLexEntry(
				lemma);
		List<NomLexClasses> list = new ArrayList<NomLexEntry.NomLexClasses>();
		for (NomLexEntry e : entries) {
			list.add(e.nomClass);
		}
		return list;
	}

	public void printPerf() {
		System.out.println(eval);
	}

	public void printMissing() {
		System.out.println("* Missing");
		printCounter(missedC);
	}

	public void printExtra() {
		System.out.println("* Extra");
		printCounter(extraC);
	}

	public void printByType() {
		Table table = new Table();

		table.addColumn("Type");
		table.addColumn("gold");
		table.addColumn("pred");
		table.addColumn("correct");

		table.addColumn("P");
		table.addColumn("R");
		table.addColumn("F");

		for (NomLexClasses cl : NomLexClasses.values()) {
			EvaluationRecord record = byType.get(cl);

			String gold = record.getGoldCount() + "";
			String pred = record.getPredictedCount() + "";
			String correct = record.getCorrectCount() + "";
			String p = StringUtils
					.getFormattedTwoDecimal(record.getPrecision() * 100);
			String r = StringUtils
					.getFormattedTwoDecimal(record.getRecall() * 100);

			String f = StringUtils.getFormattedTwoDecimal(record.getF1() * 100);

			table.addRow(new String[] { cl.name(), gold, pred, correct, p, r, f });

		}

		System.out.println(table.toOrgTable());
	}

	private void printCounter(Counter<String> counter) {
		for (String s : new TreeSet<String>(counter.items()))
			System.out.println(" | " + s + " | " + (int) (counter.getCount(s)));
	}

	private void debug(TextAnnotation ta, int position) throws Exception {
		System.out.println(ta);
		if (ta.hasView(manager.getGoldViewName()))
			System.out.println(ta.getView(manager.getGoldViewName()));
		else
			System.out.println("Gold view not found");

		System.out.println("Heuristic predicates");
		System.out.println(manager.getHeuristicPredicateDetector()
				.getPredicates(ta));

		System.out.println(ta.getView(ViewNames.POS));

		System.out.println(ta.getView(ViewNames.SHALLOW_PARSE));

		detector.debug = true;
		System.out.println(detector.getLemma(ta, position));

		System.console().readLine();
		detector.debug = false;
	}

	private Map<IntPair, String> getGoldMap(TextAnnotation ta) throws Exception {
		Pair<SRLSentenceInstance, SRLSentenceStructure> examples = manager.exampleGenerator
				.getExamples(ta);

		Map<IntPair, String> map = new HashMap<IntPair, String>();
		for (int predicateId = 0; predicateId < examples.getFirst()
				.numPredicates(); predicateId++) {

			SRLPredicateInstance srlInstance = examples.getFirst().predicates
					.get(predicateId);
			SRLMulticlassInstance senseInstance = srlInstance
					.getSenseInstance();

			map.put(senseInstance.getSpan(), senseInstance.getPredicateLemma());
		}
		return map;
	}
}
